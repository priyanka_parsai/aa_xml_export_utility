﻿namespace RSC.Database.Api.Common.Helpers
{
    public class MarkLogic
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string Url { get; set; }
        public string ID { get; set; }

        public MarkLogic(string username, string password, string domain, string url, string id)
        {
            // TODO: Complete member initialization
            UserName = username;
            Password = password;
            Domain = domain;
            Url = url;
            ID = id;
        }

        public MarkLogic()
        {
            // TODO: Complete member initialization
        }
    }
}
