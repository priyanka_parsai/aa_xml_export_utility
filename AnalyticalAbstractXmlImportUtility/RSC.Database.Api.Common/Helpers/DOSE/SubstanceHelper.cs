﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Entities = RSC.Substances.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.DOSE
{
    public class SubstanceHelper
    {
        private XElement _substanceXElement;

        #region Constants
        private const string SYSTEMID = "SystemId";
        private const string TITLE = "Title";
        private const string INCHI = "Inchi";
        private const string INCHIKEY = "InchiKey";
        private const string CASREGNO = "CasRegNo";
        private const string CASNAME = "CasName";
        private const string SYNONYMS = "Synonyms";
        private const string MANUFACTURERCODE = "ManufacturerCode";
        private const string MOLECULARFORMULA = "MolecularFormula";
        private const string MOLECULARWEIGHT = "MolecularWeight";
        private const string PUBLICID = "PublicId";
        private const string PRINTID = "PrintId";
        private const string SUBSTACNETYPE = "Type";
        private const string ACDName = "AcdName";
        private const string TRADEMARKS = "TradeMarks";
        private const string COMPOSITION = "Composition";
        private const string IMAGE = "Image";
        private const string ID = "ID";
        private const string CDX = "Cdx";
        private const string TIF = "Tif";
        private const string MOL = "Mol";
        private const string LITERATUREREFERENCES = "LiteratureReferences";
        private const string PROPERTIES = "Properties";
        private const string USE = "Use";
        private const string NOTE = "Note";
        private const string DATA = "Data";
        private const string EINECSNO = "EinecsNo";
        private const string RTECSNO = "RtecsNo";
        private const string OCCUPATIONALEXPOSURE = "OccupationalExposure";
        private const string ECOTOXICITY = "Ecotoxicity";
        private const string ENVIRONMENTALFATE = "EnvironmentalFate";
        private const string MAMMALIANAVIANTOXICITY = "MammalianAvianToxicity";
        private const string GENOTOXICITY = "Genotoxicity";
        private const string OTHEREFFECTS = "OtherEffects";
        private const string LEGISLATION = "Legislation";
        private const string OTHERCOMMENTS = "OtherComments";
        private const string VETKEYWORDS = "VetKeywords";
        private const string KEYWORDS = "Keywords";
        private const string FALSE = "false";
        private const string Status = "Status";
        private const string TEXTSTRUCTURE = "TextStructure";

        #endregion

        #region Constructor and public methods

        public SubstanceHelper()
        {
        }

        public SubstanceHelper(string searchResults)
        {
            InitializeMainDocument(searchResults);
        }

        public Entities.Substance GetSubstance()
        {
            Entities.Substance substance = null;
            try
            {
                if (_substanceXElement != null && _substanceXElement.Elements().Any())
                {
                    substance = BuildSubstance(_substanceXElement);
                }
            }
            finally
            {
                _substanceXElement = null;
            }
            return substance;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            if (!string.IsNullOrEmpty(searchResults))
            {
                using (TextReader searchResultTextReader = new StringReader(searchResults))
                {
                    var searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                    if (searchResultsXDocument.Root != null)
                    {
                        _substanceXElement = searchResultsXDocument.Root;
                    }
                }
            }
        }

        private Entities.Substance BuildSubstance(XElement xElement)
        {
            Entities.Substance substance = null;
            try
            {
                if (xElement != null)
                {
                    substance = new Entities.Substance();
                    XElement substanceTempXElement = null;

                    substanceTempXElement = xElement.Element(XName.Get(SYSTEMID));
                    substance.SystemId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(TITLE));
                    substance.Title = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(INCHI));
                    substance.Inchi = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(INCHIKEY));
                    substance.InchiKey = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(SYNONYMS));
                    substance.Synonyms = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARFORMULA));
                    substance.MolecularFormula = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARWEIGHT));
                    substance.MolecularWeight = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(PUBLICID));
                    substance.PublicId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(PRINTID));
                    substance.PrintId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(SUBSTACNETYPE));
                    substance.Type = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASREGNO));
                    substance.CasRegNo = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASNAME));
                    substance.CasName = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(ACDName));
                    substance.AcdName = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(TRADEMARKS));
                    substance.TradeMarks = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(MANUFACTURERCODE));
                    substance.ManufacturerCode = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(COMPOSITION));
                    substance.Composition = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(IMAGE));
                    substance.Image = substanceTempXElement != null ? BuildImageData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(LITERATUREREFERENCES));
                    substance.LiteratureReferences = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(PROPERTIES));
                    substance.Properties = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(USE));
                    substance.Use = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(NOTE));
                    substance.Note = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(DATA));
                    substance.Data = substanceTempXElement != null ? BuildData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(KEYWORDS));
                    substance.Keywords = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(VETKEYWORDS));
                    substance.VetKeywords = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(Status));
                    substance.Status = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(TEXTSTRUCTURE));
                    substance.TextStructure = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xElement = null;
            }
            return substance;
        }

        private Entities.Image BuildImageData(XElement imageXElement)
        {
            Entities.Image image = null;
            if (imageXElement != null)
            {
                image = new Entities.Image();
                XElement tempXElement = imageXElement.Element(ID);
                image.Id = tempXElement != null ? tempXElement.Value : string.Empty;
                tempXElement = imageXElement.Element(CDX);
                image.Cdx = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(TIF);
                image.Tif = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(MOL);
                image.Mol = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);

                tempXElement = null;
                imageXElement = null;
            }
            return image;
        }

        private Entities.Data BuildData(XElement dataXElement)
        {
            Entities.Data data = null;
            XElement dataTempXElement;
            try
            {
                data = new Entities.Data();
                dataTempXElement = dataXElement.Element(EINECSNO);
                data.EinecsNo = dataTempXElement != null ? dataTempXElement.Value : string.Empty;

                dataTempXElement = dataXElement.Element(RTECSNO);
                data.RtecsNo = dataTempXElement != null ? dataTempXElement.Value : string.Empty;

                dataTempXElement = dataXElement.Element(OCCUPATIONALEXPOSURE);
                data.OccupationalExposure = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(ECOTOXICITY);
                data.Ecotoxicity = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(ENVIRONMENTALFATE);
                data.EnvironmentalFate = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(MAMMALIANAVIANTOXICITY);
                data.MammalianAvianToxicity = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(GENOTOXICITY);
                data.Genotoxicity = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(OTHEREFFECTS);
                data.OtherEffects = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(LEGISLATION);
                data.Legislation = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;

                dataTempXElement = dataXElement.Element(OTHERCOMMENTS);
                data.OtherComments = (dataTempXElement != null && dataTempXElement.Nodes() != null) ? string.Concat(dataTempXElement.Nodes()) : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dataTempXElement = null;

            }
            return data;
        }

        #endregion
    }
}
