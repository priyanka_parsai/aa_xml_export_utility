﻿namespace RSC.Database.Api.Common.Helpers
{
    public class Constants
    {
        public const string GetContentFileBasePath = "GetContentFileBasePath";
        public const string ResourceIDFolderStructure = "ResourceIDFolderStructure";
        public const string NoImageAvailablePath = "NoImageAvailablePath";
        public const string ImageFilesBasePath = "ImageFilesBasePath";

        public const string MarinLit = "marinlit";
        public const string Bibtex = "bibtex";
        public const string Ris = "ris";
        public const string Content = "content";
        
        public const string GET = "GET";
        public const string POST = "POST";
        public const string PUT = "PUT";
        public const string DELETE = "DELETE";

        public const string DataResourceURI = "/tangier/data-resources/{0}.xml";
        public const string ClassURI = "/tangier/content-classes/data.xml";
        public const string ProductTitleURI = "/tangier/product-titles/algiers.xml";
        public const string DataComponentURI = "/tangier/data-components/{0}.xml";
        public const string TypeURI = "/tangier/content-types/data-resource.xml";
        public const string PublisherURI = "/tangier/profile-organizations/{0}.xml";
        public const string LicenseURI = "/tangier/content-flags/license-types/{0}.xml";
        public const string MediaURI = "/tangier/content-flags/media-types/med000000000010.xml";
        public const string SubstanceURI = "/tangier/chemical-substances/{0}.xml";
        public const string MetaType = "text/xml";
        public const string DRSource = "algiers";
        public const string DROwner = "admin";
        public const string DCSource = "xlink";
        public const string DCExtentFormat = "pages";
        public const string ProfileTitleUri = "/tangier/product-titles/public-profiles.xml";
        public const string ProfileUri = "/tangier/public-profiles/{0}.xml";
        public const string SavedProfileUri = "/tangier/saved-profiles/{0}.xml";
        public const string DataResourceUri = "/tangier/data-resources/{0}.xml";
        public const string ToolUri = "/tangier/tool-services/{0}.xml";
        public const string ToolTitleUri = "/tangier/product-titles/tools-services.xml";
        public const string ChemicalDataUriFormat = "/tangier/chemical-data/{0}.xml";
        public const string ReactionUriFormat = "/tangier/chemical-reactions/{0}.xml";
        public const string SpectrumUriFormat = "/tangier/chemical-spectra/{0}.xml";

        public const string AcceptHeadersConfigurationSection = "AcceptHeadersConfiguration";
        public const string MarkLogicConfigurationSection = "MarkLogicConfigurationV3";
        public const string CacheName = "CacheName";
        public const string XMLAcceptHeader = "application/xml";
        public const string SearchNamespace = "searchNamespace";
        public const string ArticlesOctetStream = "application/octet-stream";

        public const string NotProcessed = "Not Processed";
        public const string Pending = "In Review";
        public const string Approved = "Approved";
        public const string Rejected = "Rejected";
        public const string Saved = "Saved";
        public const string Published = "Published";
        public const string Public = "Public";
        public const string Total  = "total";
        public const string EncodedLessThan = "&lt;";
        public const string LessThanPlaceholder = "#lessthan#";
        public const string LessThanEntity = "<";
        public const string MaxDegreeOfParallelism = "MaxDegreeOfParallelism";

        #region Messages
        public const string ResourceNotFound = "Resource not found.";
        public const string BadRequest = "Bad Request";
        public const string Unauthorized = "Unauthorized";
        public const string Forbidden = "Forbidden";
        public const string NotFound = "Not Found";
        public const string InvalidRequest = "Invalid request format";
        public const string InternalServerError = "500 Internal Exception";
        public const string BadGateway = "Bad Gateway";
        public const string ServiceUnavailable = "Service Unavailable";
        public const string PublisherNotFound = "No matching publisher found.";
        public const string InstituteNotFound = "No matching institute found.";
        public const string LicenseNotFound = "No matching license found.";
        public const string JournalNotFound = "No matching journal found.";
        public static string RemoveChars = "RemoveChars";
        public static string ArticleAPIURL = "ArticleAPIURL";

        public static string ActivityTrackerQueue = "ActivityTrackerQueue";
        #endregion

        public static string ActivityTrackerAPIUrl = "ActivityTrackerAPIUrl";

        public static string ActivityTrackerAcceptHeader = "ActivityTrackerAcceptHeader";

        public static string Accept = "Accept";

        public static string ActivityTrackerQueueLabel = "Activity Tracker";

        public static string GetActivityTrackerAPIUrl = "GetActivityTrackerAPIUrl";

        public static string RSCActivityTrackerMSMQ = "RSCActivityTrackerMSMQ";

        public static string GCN = "GCN";
        public static string Tangier = "Tangier";
        public static string Tunis = "Tunis";
        public static string Taza = "Taza";
        public static string Kenitra = "Kenitra";
        public static string Alerts = "Alerts";
        public const string Fes = "Fes";
        public const string Meknes = "Meknes";
        public const string Larache = "Larache";
        public const string Oran = "Oran";
        public const string Repository = "Repository";
        public const string SubstanceImagesBasePath = "SubstanceImagesBasePath";
        public const string SubstanceDownloadImageExtension = "SubstanceDownloadImageExtension";
        public const string Articles = "articles";
        public const string Substances = "substances";
        public const string Journals = "journals";
        public const string Profiles = "profiles";
        public const string Tools = "tools";
        public const string Thesis = "thesis";
        public const string Theses = "theses";
        public const string All = "all";
        public const string ChemicalData = "chemicaldata";
        public const string Reactions = "reactions";
        public const string Spectra = "spectra";
        public const string Author = "author";
        public const string Number = "number";
        public const string DateRange = "daterange";
        public const string ReactionFacet = "reaction";
        public const string unknown = "unknown";
        public const string REMOTE_ADDR = "REMOTE_ADDR";
        public const string HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";
        public const string ProductCatalogueQueryParameterName = "productCatalogue";
        public const string False="False";
        public const string True = "True";
        public const string HistoricalCollection = "Historical Collection";
        public const string ProductCatalogueAuthEntity="Product_Catalogue";
        public const string GroupNameAuthEntity = "Group_Name";
        public const string Reaction = "reaction";

        #region Citations Constants
        public const string RisCitationFile = "RSC.Database.Api.Common.XSL.CitationDownload.Ris.xslt";
        public const string RistexMediaType = "ris";
        public const string BibtexCitationFile = "RSC.Database.Api.Common.XSL.CitationDownload.BibTex.xslt";
        public const string BibtexMediaType = "bibtex";

        #endregion Citations Constants
        #region Config entry Name Constants

        public const string ClientMachineIPAddress = "ClientMachineIPAddress";
        public const string AuthSystemSessionId = "AuthSessionCookieName";
        public const string GroupName="GroupName";
        public const string AuthCallEnabled="AuthCallEnabled";
        public const string HCPlatformId = "HCPlatformIdForAuthentication";
        public const string CreditToBeDeducted = "CreditToBeDeducted";
        public const string RSCDomain = "RSCDomain";
        public const string CheckCustomHeader="CheckCustomHeader";
        public const string CustomHeaderName="CustomHeaderName";
        public const string CustomHeaderValue = "CustomHeaderValue";
        public const string WildcardFolders="WildcardFolders";
        public const string WildcardFoldersCaseSensitive="WildcardFoldersCaseSensitive";
        public const string CreateSession = "CreateSession";

        #endregion Config entry Name Constants

    }
}
