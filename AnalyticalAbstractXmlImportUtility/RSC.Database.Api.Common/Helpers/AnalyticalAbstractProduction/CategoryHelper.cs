﻿using RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using System.Linq;

namespace RSC.Database.Api.Common.Helpers.AnalyticalAbstractProduction
{
    public static class CategoryHelper
    {
        //TODO: Move Marklogic related query parameters to MLConstants
        public static string GetFormattedCategories(AAFacetTree categories)
        {
            string formattedCategories = string.Empty;
            if (categories != null)
            {
                AppendCategories(categories, ref formattedCategories);
                return formattedCategories;
            }
            return string.Empty;
        }

        private static void AppendCategories(AAFacetTree categories, ref string formattedCategories)
        {
            if (categories.ChildrenNode != null && categories.ChildrenNode.Any())
            {
                foreach (var node in categories.ChildrenNode)
                {
                    AppendCategories(node, ref formattedCategories);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(formattedCategories))
                    formattedCategories = string.Format("{0}&rs:ancat={1}", formattedCategories, categories.Id);
                else
                    formattedCategories = string.Format("{0}", categories.Id);
            }
        }


        public static string GetFormattedArticleType(Article article)
        {
            if (article != null && article.ArticleType != null)
            {
                var formattedCategories = string.Empty;
                if (article.ArticleType != null && article.ArticleType.ChildrenNode.Any())
                {
                    formattedCategories = string.Format("&rs:jnlartcat={0}",
                             string.Join("&rs:jnlartcat=", article.ArticleType.ChildrenNode.Select(x => x.Id)));

                }

                return formattedCategories;
            }
            return string.Empty;
        }
    }
}
