﻿using RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using RSCpubs.ePlatform.Service.Common.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace RSC.Database.Api.Common.Helpers.AnalyticalAbstractProduction
{
    public class ArticleHelper
    {
        #region Private Variables


        public XElement articleXElement = null;
        private const string title = "title";
        private const string description = "description";
        private const string createdDate = "created-date";
        private const string updatedDate = "updated-date";
        private const string authors = "authors";
        private const string firstName = "firstname";
        private const string lastName = "lastname";
        private const string author = "author";
        private const string journal = "journal";
        private const string journalVolume = "journal-volume";
        private const string journalIssueNo = "journal-issueno";
        private const string subsyear = "subsyear";
        private const string systemId = "systemid";
        private const string doi = "doi";
        private const string info = "info";
        private const string language = "language";
        private const string pubmedId = "PubmedId";
        private const string firstPage = "firstpage";
        private const string lastPage = "lastpage";
        private const string publicId = "publicid";
        private const string printId = "printid";
        private const string keywords = "keywords";
        private const string keyword = "keyword";
        private const string analytes = "analytes";
        private const string analyte = "analyte";
        private const string casno = "CASNo";
        private const string value = "value";
        private const string techniques = "techniques";
        private const string technique = "technique";
        private const string matrices = "matrices";
        private const string matrix = "matrix";
        private const string FacetTree = "FacetTree";
        private const string JournalCategoryTree = "JournalCategoryTree";
        private const string Status = "Status";
        private const string CreatedBy = "CreatedBy";
        private const string FirstPublishedBy = "FirstPublishedBy";
        private const string FirstPublishedDate = "FirstPublishedDate";
        private const string RawMonographXml = "RawMonographXml";

        #endregion Private Static Variables

        #region Constructors

        public ArticleHelper()
        {

        }

        public ArticleHelper(string searchResults)
        {

            InitializeMainDocument(searchResults);
        }

        #endregion

        #region Public Methods

        public Article GetArticle()
        {
            Article article = new Article();

            try
            {
                if (articleXElement != null)
                {

                    article = this.BuildArticle(articleXElement);

                }
            }
            finally
            {
                articleXElement = null;
            }

            return article;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null)
                {
                    articleXElement = searchResultsXDocument.Root;
                }

            }
        }

        private Article BuildArticle(XElement articleXElement)
        {
            Article articleInfo = null;

            #region XnamesDeclaration
            XElement tempXElement = null;
            IEnumerable<XElement> tempXElemets = null;
            #endregion

            try
            {
                if (articleXElement != null)
                {
                    articleInfo = new Article();

                    articleInfo.SystemId = GetValueFor(articleXElement, systemId);
                    articleInfo.Doi = GetValueFor(articleXElement, doi);
                    articleInfo.PubmedId = GetValueFor(articleXElement, pubmedId);
                    articleInfo.Title = GetValueFor(articleXElement, title, true);
                    articleInfo.Description = GetValueFor(articleXElement, description, true);
                    articleInfo.Info = GetValueFor(articleXElement, info, true);
                    articleInfo.Language = GetValueFor(articleXElement, language);
                    articleInfo.JournalVolume = GetValueFor(articleXElement, journalVolume);
                    articleInfo.JournalIssueNo = GetValueFor(articleXElement, journalIssueNo);
                    articleInfo.SubsYear = GetValueFor(articleXElement, subsyear);
                    articleInfo.firstpage = GetValueFor(articleXElement, firstPage);
                    articleInfo.lastpage = GetValueFor(articleXElement, lastPage);
                    articleInfo.PublicId = GetValueFor(articleXElement, publicId);
                    articleInfo.PrintId = GetValueFor(articleXElement, printId);

                    tempXElement = articleXElement.Element(XName.Get(FacetTree));
                    if (tempXElement != null)
                    {
                        articleInfo.Categories = (AAFacetTree)SerializeDeserializeHelper.DeserializeObject(tempXElement.ToString(), typeof(AAFacetTree));

                    }
                    tempXElement = null;
                    tempXElement = articleXElement.Element(XName.Get(authors));
                    if (tempXElement != null)
                    {
                        articleInfo.Authors = new List<RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction.articleAuthor>();
                        tempXElemets = tempXElement.Elements(author);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement authorXElement in tempXElemets)
                            {
                                if (authorXElement != null)
                                {
                                    RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction.articleAuthor articleAuthor = new DataContracts.AnalyticalAbstractProduction.articleAuthor();
                                    articleAuthor.firstname = GetValueFor(authorXElement, firstName, true);
                                    articleAuthor.lastname = GetValueFor(authorXElement, lastName, true);

                                    articleInfo.Authors.Add(articleAuthor);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(analytes));
                    if (tempXElement != null)
                    {
                        articleInfo.Analytes = new List<DataContracts.AnalyticalAbstractProduction.Analyte>();
                        tempXElemets = tempXElement.Elements(analyte);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement analyteXElement in tempXElemets)
                            {
                                if (analyteXElement != null)
                                {
                                    RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction.Analyte Analyte = new DataContracts.AnalyticalAbstractProduction.Analyte();
                                    Analyte.CASNo = analyteXElement.HasAttributes ? analyteXElement.Attribute(XName.Get(casno)).Value : string.Empty;
                                    Analyte.Value = analyteXElement.Value;

                                    articleInfo.Analytes.Add(Analyte);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(techniques));
                    if (tempXElement != null)
                    {
                        articleInfo.Techniques = new List<string>();
                        tempXElemets = tempXElement.Elements(technique);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement techniqueXElement in tempXElemets)
                            {
                                if (techniqueXElement != null)
                                {
                                    articleInfo.Techniques.Add(techniqueXElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = articleXElement.Element(XName.Get(matrices));
                    if (tempXElement != null)
                    {
                        articleInfo.Matrices = new List<string>();
                        tempXElemets = tempXElement.Elements(matrix);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement matrixXElement in tempXElemets)
                            {
                                if (matrixXElement != null)
                                {
                                    articleInfo.Matrices.Add(matrixXElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(keywords));
                    if (tempXElement != null)
                    {
                        articleInfo.Keywords = new List<string>();
                        tempXElemets = tempXElement.Elements(keyword);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement keywordXElement in tempXElemets)
                            {
                                if (keywordXElement != null)
                                {
                                    articleInfo.Keywords.Add(keywordXElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(journal));
                    if (tempXElement != null)
                    {
                        articleInfo.Journal = new journal();
                        tempXElement = tempXElement.Element(XName.Get(title));
                        if (tempXElement != null)
                        {
                            articleInfo.Journal.title = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement));
                        }
                    }
                    articleInfo.Status = GetValueFor(articleXElement, Status);
                    articleInfo.CreatedBy = GetValueFor(articleXElement, CreatedBy);
                    articleInfo.FirstPublishedBy = GetValueFor(articleXElement, FirstPublishedBy);
                    articleInfo.FirstPublishedDate = GetValueFor(articleXElement, FirstPublishedDate);
                    articleInfo.RawMonographXml = GetValueFor(articleXElement, RawMonographXml);
                    //Article Type
                    var tempArticleTypeXElement = articleXElement.Element(XName.Get(JournalCategoryTree));
                    if (tempArticleTypeXElement != null)
                    {
                        articleInfo.ArticleType = (JournalCategoryTree)SerializeDeserializeHelper.DeserializeObject(tempArticleTypeXElement.ToString(), typeof(JournalCategoryTree));
                        tempArticleTypeXElement = null;
                    }
                    tempXElement = null;
                }
            }
            finally
            {

                tempXElement = null;

                tempXElemets = null;
            }

            return articleInfo;
        }

        private string GetValueFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                if (isEncoded)
                {
                    returnVal = WebUtility.HtmlDecode(CommonHelpers.GetInnerXML(tempXElement));
                }
                else
                {
                    returnVal = tempXElement.Value;
                }
            }
            tempXElement = null;
            return returnVal;
        }

        private string GetValuesFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                if (isEncoded)
                {
                    returnVal = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement));
                }
                else
                {
                    returnVal = tempXElement.Value;
                }
            }
            tempXElement = null;
            return returnVal;
        }


        private List<string> BuildTrivialNames(XElement trivialNamesXElement)
        {
            List<string> trivialNames = null;
            if (trivialNamesXElement != null)
            {
                trivialNames = new List<string>();

                foreach (XElement element in trivialNamesXElement.Elements())
                {
                    if (element != null)
                    {
                        if (trivialNames == null)
                        {
                            trivialNames = new List<string>();
                        }
                        trivialNames.Add(element.Value);
                    }
                }

            }
            return trivialNames;
        }

        private List<string> BuildKeywords(XElement keywordsXElement)
        {
            List<string> keywords = null;
            if (keywordsXElement != null)
            {
                keywords = new List<string>();

                foreach (XElement element in keywordsXElement.Elements())
                {
                    if (element != null)
                    {
                        if (keywords == null)
                        {
                            keywords = new List<string>();
                        }
                        keywords.Add(element.Value);
                    }
                }

            }
            return keywords;
        }

        #endregion
    }
}
