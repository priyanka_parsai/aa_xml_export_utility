﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml;

namespace RSC.Database.Api.Common.Helpers
{
    public static class CommonHelpers
    {
        public static string GetVersionNo(HttpHeaderValueCollection<MediaTypeWithQualityHeaderValue> acceptHeader, string regex)
        {
            string versionNo = string.Empty;
            string acceptHeaderValue = string.Empty;
            if (acceptHeader != null)
            {
                foreach (var header in acceptHeader)
                {
                    Match match = Regex.Match(header.MediaType, regex);
                    if (match.Success && match.Groups != null && match.Groups.Count >= 2)
                    {
                        versionNo = match.Groups[1].Value;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(versionNo))
                {
                    versionNo = "v1";
                }
            }
            return versionNo;
        }

        public static string GetCitationType(HttpHeaderValueCollection<MediaTypeWithQualityHeaderValue> acceptHeader, string regex)
        {
            string citationType = string.Empty;
            string acceptHeaderValue = string.Empty;
            if (acceptHeader != null)
            {
                foreach (var header in acceptHeader)
                {
                    Match match = Regex.Match(header.MediaType, regex);
                    if (match.Success && match.Groups != null && match.Groups.Count >= 3)
                    {
                        citationType = Regex.Match(header.MediaType, regex).Groups[2].Value;
                        break;
                    }
                }
            }
            return citationType;
        }

        public static string GetNonMarkup(string text)
        {
            StringBuilder nonMarkupTextBuilder = new StringBuilder();
            string noMarkUpText = string.Empty;
            XDocument xDocument = null;
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    text = "<outerNode>" + text + "</outerNode>";
                    using (TextReader searchResultTextReader = new StringReader(text))
                    {
                        xDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                        nonMarkupTextBuilder = new StringBuilder();
                        foreach (XElement xElement in xDocument.Elements())
                        {
                            nonMarkupTextBuilder.Append(xElement.Value);
                        }
                    }
                    noMarkUpText = nonMarkupTextBuilder.ToString();
                    if (!string.IsNullOrEmpty(noMarkUpText))
                    {
                        noMarkUpText = noMarkUpText.Trim();
                    }
                }
            }
            finally
            {
                xDocument = null;
                nonMarkupTextBuilder = null;
            }
            return noMarkUpText;

        }

        public static string GetAttributeValue(IEnumerable<XElement> anyXElements, string attributeValue)
        {
            string returnValue = string.Empty;
            IEnumerable<XAttribute> foundAttributes = null;
            try
            {
                if (anyXElements != null)
                {
                    foundAttributes = anyXElements.Attributes().Where(attribute => attribute.Value.ToLower() == attributeValue.ToLower());

                    if (foundAttributes != null && foundAttributes.Count() > 0)
                    {
                        returnValue = foundAttributes.ElementAt(0).Parent.Value;
                    }
                }
            }
            finally
            {
                foundAttributes = null;
            }
            return returnValue;
        }

        public static string GetAttributeValue(XElement anyXElement, string attributeName)
        {
            string returnValue = string.Empty;

            IEnumerable<XAttribute> foundAttributes = null;

            try
            {
                if (anyXElement != null)
                {
                    foundAttributes = anyXElement.Attributes().Where(attribute => attribute.Name == attributeName);

                    if (foundAttributes != null && foundAttributes.Count() > 0)
                    {
                        returnValue = foundAttributes.ElementAt(0).Value;
                    }
                }
            }
            finally
            {
                foundAttributes = null;
            }

            return returnValue;
        }

        public static XElement GetElement(XElement anyXElement, string attributeValue)
        {
            // Return XElement Object
            XElement returnXElement = null;
            IEnumerable<XAttribute> anyXAttributes = null;
            try
            {
                if (anyXElement != null)
                {
                    anyXAttributes = anyXElement.Attributes().Where(attribute => attribute.Value.ToLower() == attributeValue.ToLower());
                    if (anyXAttributes != null && anyXAttributes.Count() > 0)
                    {
                        returnXElement = anyXAttributes.ElementAt(0).Parent;
                    }
                }
            }
            finally
            {
                anyXAttributes = null;
            }
            return returnXElement;
        }

        public static string GetNodeXML(XElement anyXElement, bool removeNamespace = false)
        {
            StringBuilder nodeStringBuilder = new StringBuilder();
            if (anyXElement != null)
            {
                foreach (XNode node in anyXElement.Nodes())
                {
                    if (removeNamespace)
                    {
                        nodeStringBuilder.Append(RemoveAllXmlNamespace(node.ToString()));
                    }
                    else
                    {
                        nodeStringBuilder.Append(node.ToString());
                    }
                }
            }
            //Replace &lt; to dummy string #lessthan#, as required format in Web and Production
            if (nodeStringBuilder.ToString().IndexOf(Constants.EncodedLessThan) >= 0)
            {
                nodeStringBuilder = nodeStringBuilder.Replace(Constants.EncodedLessThan, Constants.LessThanPlaceholder);
            }
            return nodeStringBuilder.ToString();
        }

        /// <summary>
        /// Get the node data including the inner html(preserving space between inner nodes)
        /// </summary>
        /// <param name="htmlData"></param>
        /// <param name="dataInnerXml"></param>
        /// <param name="removeNamespace"></param>
        /// <returns></returns>
        public static string GetInnerXML(XElement anyXElement, bool removeNamespace = false)
        {
            StringBuilder nodeStringBuilder = new StringBuilder();
            using (XmlReader data = XmlReader.Create(new StringReader(anyXElement.ToString())))
            {
                data.MoveToContent();

                if (removeNamespace)
                {
                    nodeStringBuilder.Append(RemoveAllXmlNamespace(data.ReadInnerXml()));
                }
                else
                {
                    nodeStringBuilder.Append(data.ReadInnerXml());
                }
            }
            //Replace &lt; to dummy string #lessthan#, as required format in Web and Production
            if (nodeStringBuilder.ToString().IndexOf(Constants.EncodedLessThan) >= 0)
            {
                nodeStringBuilder = nodeStringBuilder.Replace(Constants.EncodedLessThan, Constants.LessThanPlaceholder);
            }
            return nodeStringBuilder.ToString();
        }

        public static string GetFolderPathForFiles(string systemId, string basePath)
        {
            string root = string.Empty;
            string systemIDFolderStructure = ConfigurationManager.AppSettings.Get(Constants.ResourceIDFolderStructure);

            if (!string.IsNullOrEmpty(systemIDFolderStructure))
            {
                int[] systemIDParts = Array.ConvertAll(systemIDFolderStructure.Split(','), int.Parse);
                if (!systemIDParts.Any(x => x <= 0))
                {
                    root = Path.Combine(basePath, systemId.Substring(0, (systemId.Length - 12)));
                    systemId = systemId.Remove(0, systemId.Length - 12);
                    if (!Directory.Exists(root))
                    {
                        Directory.CreateDirectory(root);
                    }

                    for (int i = 0; i < systemIDParts.Length; i++)
                    {
                        string folderName = string.Empty;
                        folderName = systemId.Substring(0, systemIDParts[i]);
                        systemId = systemId.Remove(0, systemIDParts[i]);
                        root = Path.Combine(root, folderName);
                        if (!Directory.Exists(root))
                        {
                            Directory.CreateDirectory(root);
                        }
                    }
                }
            }
            else
            {
                root = Path.Combine(basePath, systemId);
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
            }
            return root;
        }

        public static string RemoveSpecialCharacters(string searchTerm)
        {
            string resultString = string.Empty;
            string charsToBeRemoved = string.Empty;
            string[] charArrayToBeRemoved = null;
            if (!string.IsNullOrEmpty(searchTerm))
            {
                resultString = searchTerm;
                charsToBeRemoved = ConfigurationManager.AppSettings.Get(Constants.RemoveChars);
                if (!string.IsNullOrEmpty(charsToBeRemoved))
                {
                    charArrayToBeRemoved = charsToBeRemoved.Split(',');
                    foreach (string charToBeRemoved in charArrayToBeRemoved)
                    {
                        resultString = resultString.Replace(charToBeRemoved, "");
                    }
                }
            }
            return resultString;
        }

        public static string GetImageUrl(string imageBaseUrl, string systemID)
        {
            string imageUrl = string.Empty;
            string folderStructure = string.Empty;
            string fileName = string.Empty;
            if (!string.IsNullOrEmpty(imageBaseUrl) && !string.IsNullOrEmpty(systemID))
            {
                folderStructure = GetImagesFolderStructure(systemID);
                fileName = systemID + ".jpg";
                imageUrl = string.Format(imageBaseUrl, folderStructure + "/" + fileName);
            }
            return imageUrl;
        }
        public static string GetImagesFolderStructure(string systemID, string ResourceIDFolderStructure = null)
        {
            string systemIDFolderStructure = ResourceIDFolderStructure ?? ConfigurationManager.AppSettings.Get(Constants.ResourceIDFolderStructure);
            string folderStructure = string.Empty;
            if (!string.IsNullOrEmpty(systemIDFolderStructure))
            {
                int[] systemIDParts = Array.ConvertAll(systemIDFolderStructure.Split(','), int.Parse);
                if (!systemIDParts.Any(x => x <= 0))
                {
                    folderStructure = systemID.Substring(0, (systemID.Length - 12));
                    systemID = systemID.Remove(0, systemID.Length - 12);
                    for (int i = 0; i < systemIDParts.Length; i++)
                    {
                        string folderName = string.Empty;
                        folderName = systemID.Substring(0, systemIDParts[i]);
                        systemID = systemID.Remove(0, systemIDParts[i]);
                        folderStructure = folderStructure + "/" + folderName;
                    }
                }
            }
            return folderStructure;
        }
        /// <summary>
        /// Revised version of above method. Updated backward slash to forward slash
        /// </summary>
        /// <param name="systemID"></param>
        /// <param name="ResourceIDFolderStructure"></param>
        /// <returns></returns>
        public static string GetImageFolderStructure(string systemID, string ResourceIDFolderStructure = null)
        {
            string systemIDFolderStructure = ResourceIDFolderStructure ?? ConfigurationManager.AppSettings.Get(Constants.ResourceIDFolderStructure);
            string folderStructure = string.Empty;
            if (!string.IsNullOrEmpty(systemIDFolderStructure))
            {
                int[] systemIDParts = Array.ConvertAll(systemIDFolderStructure.Split(','), int.Parse);
                if (!systemIDParts.Any(x => x <= 0))
                {
                    folderStructure = systemID.Substring(0, (systemID.Length - 12));
                    systemID = systemID.Remove(0, systemID.Length - 12);
                    for (int i = 0; i < systemIDParts.Length; i++)
                    {
                        string folderName = string.Empty;
                        folderName = systemID.Substring(0, systemIDParts[i]);
                        systemID = systemID.Remove(0, systemIDParts[i]);
                        folderStructure = folderStructure + "\\" + folderName;
                    }
                }
            }
            return folderStructure;
        }
        /// <summary>
        /// Remove all the xml namespaces (xmlns) attributes in the xml string
        /// </summary>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        public static string RemoveAllXmlNamespace(string xmlData)
        {
            string xmlnsPattern = "\\s+xmlns\\s*(:\\w)?\\s*=\\s*\\\"(?<url>[^\\\"]*)\\\"";
            MatchCollection matchCol = Regex.Matches(xmlData, xmlnsPattern);

            foreach (Match m in matchCol)
            {
                xmlData = xmlData.Replace(m.ToString(), "");
            }
            return xmlData;
        }

        public static string TransformCitationData(string citationData, string citationtype)
        {
            string transformedXml = string.Empty;
            string xslName = string.Empty;

            if (citationtype.Equals(Constants.BibtexMediaType, StringComparison.InvariantCultureIgnoreCase))
            {
                xslName = Constants.BibtexCitationFile;
            }
            else if (citationtype.Equals(Constants.RistexMediaType, StringComparison.InvariantCultureIgnoreCase))
            {
                xslName = Constants.RisCitationFile;
            }
            var transformationHelper = new TransformationHelper();
            transformedXml = transformationHelper.TransformWithXMLResolver(citationData, xslName);
            return transformedXml;
        }
    }
}
