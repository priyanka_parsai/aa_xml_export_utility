﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public static class AcceptHeaderHelper
    {
        public static List<AcceptHeader> GetAcceptHeaderList()
        {
            List<AcceptHeader> acceptHeaderOptions = new List<AcceptHeader>();
            ArrayList arrayList = (ArrayList)ConfigurationManager.GetSection(Constants.AcceptHeadersConfigurationSection);
            if (arrayList != null)
            {
                acceptHeaderOptions = new List<AcceptHeader>();
                for (int i = 0; i < arrayList.Count; i++)
                {
                    AcceptHeader acceptHeader = new AcceptHeader();
                    acceptHeader = (AcceptHeader)arrayList[i];
                    acceptHeaderOptions.Add(acceptHeader);
                }
            }
            arrayList = null;
            return acceptHeaderOptions;
        }
    }
}
