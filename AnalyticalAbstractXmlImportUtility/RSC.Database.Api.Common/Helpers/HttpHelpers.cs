﻿using RSC.Database.Api.Common.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public static class HttpHelpers
    {
        public static ApiAcceptHeader ParseAcceptHeader(string acceptHeader)
        {
            return new ApiAcceptHeader(acceptHeader);
        }
    }
}
