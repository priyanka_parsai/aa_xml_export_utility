﻿using System;
using System.Reflection;
using System.Xml;

namespace RSC.Database.Api.Common.Helpers
{
    /// <summary>
    /// Embedded Resource Resolver
    /// </summary>
    public class EmbeddedResourceResolver : XmlUrlResolver
    {
        private Assembly _assembly;

        public EmbeddedResourceResolver()
        {
            _assembly = Assembly.GetExecutingAssembly();
        }

        public EmbeddedResourceResolver(Assembly assembly)
        { _assembly = assembly; }

        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {
            return _assembly.GetManifestResourceStream(absoluteUri.Segments[absoluteUri.Segments.Length - 1]);
        }
    }
}
