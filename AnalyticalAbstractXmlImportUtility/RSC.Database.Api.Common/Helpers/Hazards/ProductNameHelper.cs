﻿using System.Text;
using Entities = RSC.Articles.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.Hazards
{
    public class ProductNameHelper
    {
        #region Constants

        private const string ProductNameQueryName = "&rs:hazprod={0}";
        private const string CHIMLValue = "chi";
        private const string LHBMLValue = "lhb";

        #endregion Constants

        public static string GetProductNamesQueryString(Entities.Enums.ProductName productName)
        {
            if (!string.IsNullOrEmpty(productName.ToString()))
            {
                return AppendProductNames(productName);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetProductNameValue(Entities.Enums.ProductName productName)
        {
            if (!string.IsNullOrEmpty(productName.ToString()))
            {
                return FormProductMLValueBasedonProductName(productName);
            }
            else
            {
                return string.Empty; ;
            }
        }

        /// <summary>
        /// Return Product name
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        private static string FormProductMLValueBasedonProductName(Entities.Enums.ProductName productName)
        {
            string productval = string.Empty;

            switch (productName)
            {
                case Entities.Enums.ProductName.All:
                case Entities.Enums.ProductName.CHI:
                    {
                        productval = CHIMLValue;
                        break;
                    }
                case Entities.Enums.ProductName.LHB:
                    {
                        productval = LHBMLValue;
                        break;
                    }
            }
            return productval.ToString();
        }

        /// <summary>
        /// Append Product names
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        private static string AppendProductNames(Entities.Enums.ProductName productName)
        {
            StringBuilder productNamesQuerystring = new StringBuilder();
            if (!string.IsNullOrEmpty(productName.ToString()))
            {
                if (productName.ToString() == Entities.Enums.ProductName.All.ToString())
                {
                    productNamesQuerystring.AppendFormat(ProductNameQueryName, CHIMLValue).AppendFormat(ProductNameQueryName, LHBMLValue);
                }
                else if (productName.ToString() == Entities.Enums.ProductName.CHI.ToString())
                {
                    productNamesQuerystring.AppendFormat(ProductNameQueryName, CHIMLValue);
                }
                else if (productName.ToString() == Entities.Enums.ProductName.LHB.ToString())
                {
                    productNamesQuerystring.AppendFormat(ProductNameQueryName, LHBMLValue);
                }
            }
            return productNamesQuerystring.ToString();
        }
    }
}