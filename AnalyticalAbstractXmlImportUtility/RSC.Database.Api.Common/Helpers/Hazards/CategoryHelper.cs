﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities = RSC.Articles.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.Hazards
{
    public static class CategoryHelper
    {
        #region Constants
        private const string HazardCategory = "&rs:hazcat={0}";
        #endregion
        public static string GetFormattedCategories(Entities.Category categories)
        {
            string formattedCategories = string.Empty;
            if (categories != null)
            {
               return AppendCategories(categories);
            }
            else
            {
                return formattedCategories;
            }
        }
        /// <summary>
        /// Append Category 
        /// In Case of Mutiple categoryin future Append in this method
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        private static string AppendCategories(Entities.Category categories)
        {
            string category = string.Empty;
            if (categories != null)
            {
                return string.Format(HazardCategory, categories.Id);
            }
            else
            {
                return category;
            }
        }
    }
}
