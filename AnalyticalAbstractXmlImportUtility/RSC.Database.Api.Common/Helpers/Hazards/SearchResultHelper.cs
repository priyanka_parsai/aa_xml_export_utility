﻿using RSC.Database.Api.Common.DataContracts.Search;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Entities = RSC.Articles.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.Hazards
{
    public class SearchResultHelper
    {
        #region Private variable & constants

        private string searchNamespace = string.Empty;
        private XElement searchResultsXElement = null;
        private IEnumerable<XElement> resultXElements = null;

        private const string total = "total";
        private const string start = "start";
        private const string pageLength = "page-length";
        private const string uri = "uri";
        private const string publicid = "publicid";
        private const string indexAttribute = "index";
        private const string highlight = "highlight";
        private const string type = "type";
        private const string name = "name";
        private const string count = "count";
        private const string result = "result";
        private const string highlightBeginTag = "<search:highlight xmlns:search=\"";
        private const string highlightEndTag = "</search:highlight>";
        private const string boldBeginTag = "<b>";
        private const string boldEndTag = "</b>";
        private const string italicBeginTag = "<i>";
        private const string italicEndTag = "</i>";
        private const string totalresults = "totalresults";
        private const string pagesize = "pagesize";
        private const string pagestart = "pagestart";
        private const string SearchResult = "SearchResult";
        private const string score = "score";
        private const string Lastupdate = "lastupdate";

        #endregion

        public SearchResultHelper(string searchResults)
        {
            searchNamespace = ConfigurationManager.AppSettings.Get(Constants.SearchNamespace);
            InitializeDocument(searchResults);
        }

        private void InitializeDocument(string searchResults)
        {

            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null && searchResultsXDocument.Root.Elements().Count() > 0)
                {
                    searchResultsXElement = searchResultsXDocument.Root;
                    resultXElements = searchResultsXElement.Elements(XName.Get(result, searchNamespace));
                }
            }
        }

        private int GetCount()
        {
            int count = 0;
            if (resultXElements != null)
            {
                count = resultXElements.Count();
            }
            return count;
        }

        /// <summary>
        /// Get Search result method
        /// </summary>
        /// <returns></returns>
        public Entities.SearchResults GetSearchResults()
        {
            Entities.SearchResults searchResults = null;
            Entities.Results results = null;
            if (searchResultsXElement != null)
            {
                searchResults = new Entities.SearchResults();
                results = new Entities.Results();
                long startValue = 0;
                long pageLengthValue = 0;
                long totalCount = 0;

                long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, totalresults), out totalCount);
           
                if (totalCount > 0)
                {
                    results = new Entities.Results();
                    results.Result = new List<Entities.Result>();

                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagesize), out pageLengthValue);
                    if (pageLengthValue > totalCount)
                    {
                        pageLengthValue = totalCount;
                    }
                    results.PageSize = pageLengthValue;
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagestart), out startValue);
                    results.PageNumber = (startValue / pageLengthValue) + ((startValue % pageLengthValue) > 0 ? 1 : 0);

                     resultXElements = searchResultsXElement.Elements(XName.Get(SearchResult, string.Empty));
                    for (int i = 0; i < resultXElements.Count(); i++)
                    {
                        results.Result.Add(GetResults(i, score));
                    }
                }
                results.TotalCount = totalCount;
                searchResults.Results = results;
            }
            return searchResults;
        }

        /// <summary>
        /// Get Results
        /// </summary>
        /// <param name="index"></param>
        /// <param name="indexAttributeName"></param>
        /// <returns></returns>
        private Entities.Result GetResults(int index, string indexAttributeName = "index")
        {
            Entities.Result result = null;
            XElement resultXElement = null;
            try
            {
                if (resultXElements != null && resultXElements.Count() >= index)
                {
                    result = new Entities.Result();

                    resultXElement = resultXElements.ElementAt(index);
                    if (resultXElement != null)
                    {
                        if (!string.IsNullOrEmpty(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName)))
                        {
                            result.Index = Convert.ToInt32(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName));
                        }
                        result.Uri = CommonHelpers.GetAttributeValue(resultXElement, uri);
                        result.PublicId = CommonHelpers.GetAttributeValue(resultXElement, publicid);
                    }
                }
            }
            finally
            {
                resultXElement = null;
            }
            return result;
        }
    }
}
