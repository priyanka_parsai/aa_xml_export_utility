﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Entities = RSC.Articles.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.Hazards
{
    public class FacetHelper
    {
        private const string NAME = "Name";
        private const string Count = "count";
        private const string Id = "id";
        private const string ReactionCategory = "reaction-category";
        private XElement facetResultsXElement = null;

        public FacetHelper()
        {

        }

        public FacetHelper(string facetResults)
        {
            InitializeMainDocument(facetResults);
        }

        private void InitializeMainDocument(string searchResults)
        {
            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument facetResultsXDocument = null;
                facetResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (facetResultsXDocument != null && facetResultsXDocument.Root != null && facetResultsXDocument.Root.Elements().Any())
                {
                    facetResultsXElement = facetResultsXDocument.Root;
                }
                facetResultsXDocument = null;
                searchResults = null;
            }


        }
        public List<Entities.FacetValue> GetFacets()
        {
            List<Entities.FacetValue> facetList = null;
            try
            {
                if (facetResultsXElement != null)
                {
                    facetList = this.BuildFacet(facetResultsXElement);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                facetResultsXElement = null;
            }

            return facetList;
        }
        private List<Entities.FacetValue> BuildFacet(XElement facetsXElement)
        {
            List<Entities.FacetValue> facets = null;
            IEnumerable<XElement> facetXElements = null;
            XElement facetXElement = null;
            try
            {
                if (facetsXElement != null)
                {
                    facets = new List<Entities.FacetValue>();
                    facetXElement = facetsXElement.Element(XName.Get(NAME));
                    facetXElements = facetsXElement.Elements(XName.Get(NAME));
                    foreach (var element in facetXElements)
                    {
                        if (element != null && !string.IsNullOrEmpty(element.Value))
                        {
                            var facetValue = new Entities.FacetValue();
                            facetValue.Id = CommonHelpers.GetAttributeValue(element, Id);
                            facetValue.Name = element.Value;
                            facetValue.Value = element.Value;
                            facetValue.Count = CommonHelpers.GetAttributeValue(element, Count);
                            facets.Add(facetValue);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                facetXElement = null;
                facetsXElement = null;
                facetXElements = null;
            }
            return facets;
        }
    }
}
