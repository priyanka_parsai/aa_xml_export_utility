﻿using RSC.Articles.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Entities = RSC.Articles.Domain.Entities;

namespace RSC.Database.Api.Common.Helpers.Hazards
{
    public class ArticleHelper
    {
        private XElement _articleXElement;

        #region Constants
        private const string SYSTEMID = "systemid";
        private const string PUBLICID = "publicId";
        private const string PRINTID = "printId";
        private const string TITLE = "title";
        private const string SHORTTITLE = "short-title";
        private const string DESCRIPTION = "description";
        private const string INFORMATION = "information";
        private const string LITERATUREREFERENCES = "literaturereferences";
        private const string AUTHORS = "authors";
        private const string AUTHOR = "author";
        private const string FIRSTNAME = "firstname";
        private const string LASTNAME = "lastname";
        private const string JOURNAL = "journal";
        private const string JOURNALTITLE = "journal-title";
        private const string JOURNALYEAR = "subsyear";
        private const string JOURNALVOLUME = "journal-volume";
        private const string JOURNALISSUENO = "journal-issueno";
        private const string FIRSTPAGE = "firstpage";
        private const string LASTPAGE = "lastpage";
        private const string DOI = "doi";
        private const string DATA = "data";
        private const string CHEMICALS = "chemicals";
        private const string CHEMICAL = "chemical";
        private const string GENERALS = "generals";
        private const string GENERAL = "general";
        private const string KEYWORDS = "keywords";
        private const string KEYWORD = "keyword";
        private const string PROPERTIES = "properties";
        private const string USE = "use";
        private const string NOTE = "note";
        private const string CASNO = "CASNo";
        private const string CONTENTFLAGS = "content-flags";
        private const string HAZARDCATEGORY = "HazardCategory";
        private const string HZCSYSID = "Id";
        private const string HZCNAME = "Name";
        private const string STATUS = "Status";
        private const string CREATEDBY = "CreatedBy";
        private const string LASTMODIFIEDBY = "LastModifiedBy";
        private const string FIRSTPUBLISHEDBY = "FirstPublishedBy";
        private const string FIRSTPUBLISHEDDATE = "FirstPublishedDate";
        private const string PUBLICATIONINFORMATION = "publication-information";

        private const string Status = "status";

        #endregion

        #region Constructor

        public ArticleHelper()
        {
        }

        public ArticleHelper(string data)
        {
            InitializeMainDocument(data);
        }
        #endregion Constructor

        #region Public methods
        public Entities.Article GetArticle()
        {
            Entities.Article article = null;
            try
            {
                if (_articleXElement != null && _articleXElement.Elements().Any())
                {
                    article = BuildArticle(_articleXElement);
                }
            }
            finally
            {
                _articleXElement = null;
            }
            return article;
        }
        #endregion Public methods

        #region Private Methods

        private void InitializeMainDocument(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                using (TextReader searchResultTextReader = new StringReader(data))
                {
                    var searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                    if (searchResultsXDocument.Root != null)
                    {
                        _articleXElement = searchResultsXDocument.Root;
                    }
                }
            }
        }

        private Entities.Article BuildArticle(XElement xElement)
        {
            Entities.Article article = null;
            XElement articleTempXElement = null;
            try
            {
                if (xElement != null)
                {
                    article = new Entities.Article();
                    //System Id
                    articleTempXElement = xElement.Element(XName.Get(SYSTEMID));
                    article.SystemId = articleTempXElement != null ? articleTempXElement.Value : string.Empty;
                    //Public Id
                    articleTempXElement = xElement.Element(XName.Get(PUBLICID));
                    article.PublicId = articleTempXElement != null ? articleTempXElement.Value : string.Empty;
                    //Title
                    article.Title = GetValueFor(xElement, TITLE, true);
                    //Short Title
                    article.ShortTitle = GetValueFor(xElement, SHORTTITLE, true);
                    //Description
                    article.Description = GetValueFor(xElement, DESCRIPTION, true);
                    //Information
                    articleTempXElement = xElement.Element(XName.Get(INFORMATION));
                    article.Information = null;

                    //Meta Information
                    articleTempXElement = xElement.Element(XName.Get(STATUS));
                    article.Status = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    articleTempXElement = xElement.Element(XName.Get(CREATEDBY));
                    article.CreatedBy = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    articleTempXElement = xElement.Element(XName.Get(LASTMODIFIEDBY));
                    article.LastModifiedBy = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    articleTempXElement = xElement.Element(XName.Get(FIRSTPUBLISHEDBY));
                    article.FirstPublishedBy = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    articleTempXElement = xElement.Element(XName.Get(FIRSTPUBLISHEDDATE));
                    article.FirstPublishedDate = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    //Publishing information
                    articleTempXElement = xElement.Element(XName.Get(PUBLICATIONINFORMATION));
                    article.PublicationInformation = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    //LiteratureReferences //Authors
                    articleTempXElement = xElement.Element(XName.Get(AUTHORS));
                    article.Authors = BindAuthorsData(articleTempXElement);

                    //LiteratureReferences //JournalTitle
                    article.JournalTitle = GetValueFor(xElement, JOURNALTITLE, true);

                    //LiteratureReferences //Journal Year
                    articleTempXElement = xElement.Element(XName.Get(JOURNALYEAR));
                    article.SubsYear = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    //LiteratureReferences //Journal Volume
                    articleTempXElement = xElement.Element(XName.Get(JOURNALVOLUME));
                    article.JournalVolume = articleTempXElement != null ? articleTempXElement.Value : string.Empty;


                    //LiteratureReferences //Journal First Page
                    articleTempXElement = xElement.Element(XName.Get(FIRSTPAGE));
                    article.firstpage = articleTempXElement != null ? articleTempXElement.Value : string.Empty;


                    //LiteratureReferences //Journal Last Page
                    articleTempXElement = xElement.Element(XName.Get(LASTPAGE));
                    article.lastpage = articleTempXElement != null ? articleTempXElement.Value : string.Empty;


                    //LiteratureReferences //DOI
                    articleTempXElement = xElement.Element(XName.Get(DOI));
                    article.Doi = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    //LiteratureReferences //IssueNumber
                    articleTempXElement = xElement.Element(XName.Get(JOURNALISSUENO));
                    article.JournalIssueNo = articleTempXElement != null ? articleTempXElement.Value : string.Empty;

                    //Data //Chemicals
                    articleTempXElement = xElement.Element(XName.Get(CHEMICALS));
                    article.Chemicals = articleTempXElement != null ? BuildChemicaListValues(articleTempXElement) : null;

                    //Data //GENERALS
                    articleTempXElement = xElement.Element(XName.Get(GENERALS));
                    article.Generals = articleTempXElement != null ? BuildListValues(articleTempXElement) : null;


                    //Keywords
                    articleTempXElement = xElement.Element(XName.Get(KEYWORDS));
                    article.Keywords = articleTempXElement != null ? BuildListValues(articleTempXElement) : null;

                    //content-flags //hazard-category 
                    articleTempXElement = xElement.Element(XName.Get(HAZARDCATEGORY));
                    article.Category = articleTempXElement != null ? BuildCategoryValue(articleTempXElement) : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                articleTempXElement = null;

                xElement = null;
            }
            return article;
        }

        private string GetValueFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                if (isEncoded)
                {
                    returnVal = WebUtility.HtmlDecode(CommonHelpers.GetInnerXML(tempXElement));
                }
                else
                {
                    returnVal = tempXElement.Value;
                }
            }
            tempXElement = null;
            return returnVal;
        }


        /// <summary>
        /// Build Authors
        /// </summary>
        /// <param name="listItemXElement"></param>
        /// <returns></returns>
        private List<Entities.Author> BindAuthorsData(XElement articleXElement)
        {
            List<Entities.Author> authorsList = null;
            if (articleXElement != null)
            {
                authorsList = new List<Entities.Author>();
                var tempXElementAuthors = articleXElement.Elements(AUTHOR);
                if (tempXElementAuthors != null && tempXElementAuthors.Any())
                {
                    foreach (XElement authorXElement in tempXElementAuthors)
                    {
                        if (authorXElement != null)
                        {
                            XElement _tempXElement = null;
                            Entities.Author author = new Entities.Author();
                            _tempXElement = authorXElement.Element(FIRSTNAME);
                            if (_tempXElement != null)
                            {
                                author.firstname = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(_tempXElement));
                            }
                            _tempXElement = authorXElement.Element(LASTNAME);
                            if (_tempXElement != null)
                            {
                                author.lastname = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(_tempXElement));
                            }
                            _tempXElement = null;
                            authorsList.Add(author);
                        }
                    }
                }
                tempXElementAuthors = null;
            }
            articleXElement = null;
            return authorsList;
        }

        /// <summary>
        /// Build List of Values from XElement
        /// </summary>
        /// <param name="listItemXElement"></param>
        /// <returns></returns>
        private List<string> BuildListValues(XElement listItemXElement)
        {
            List<string> items = null;
            if (listItemXElement != null)
            {
                items = new List<string>();

                foreach (XElement element in listItemXElement.Elements())
                {
                    if (element != null)
                    {
                        if (items == null)
                        {
                            items = new List<string>();
                        }
                        items.Add(element.Value);
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Build List of Chemical Values from XElement
        /// </summary>
        /// <param name="listItemXElement"></param>
        /// <returns></returns>
        private List<Chemical> BuildChemicaListValues(XElement listItemXElement)
        {
            List<Chemical> items = null;
            if (listItemXElement != null)
            {
                items = new List<Chemical>();

                foreach (XElement element in listItemXElement.Elements())
                {
                    if (element != null)
                    {
                        if (items == null)
                        {
                            items = new List<Chemical>();
                        }
                        Chemical chemical = new Chemical();
                        chemical.CASNo = element.HasAttributes ? element.Attribute(XName.Get(CASNO)).Value : string.Empty;
                        chemical.Value = element.Value.ToString();
                        items.Add(chemical);
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Build Category XElement
        /// </summary>
        /// <param name="listItemXElement"></param>
        /// <returns></returns>
        private Category BuildCategoryValue(XElement listItemXElement)
        {
            Category category = null;
            if (listItemXElement != null)
            {
                category = new Category();
                if (listItemXElement.Element(XName.Get(HZCNAME)) != null)
                {
                    category.Name = listItemXElement.Element(XName.Get(HZCNAME)).Value;
                }
                if (listItemXElement.Element(XName.Get(HZCSYSID)) != null)
                {
                    category.Id = listItemXElement.Element(XName.Get(HZCSYSID)).Value;
                }
            }
            listItemXElement = null;
            return category;
        }
        #endregion Private methods

    }
}
