﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSC.Database.Api.Common.DataContracts;
using System.Net;

namespace RSC.Database.Api.Common.Helpers
{
    public class SubstanceHelper
    {
        private XElement _substanceXElement;

        #region Constants
        private const string SYSTEMID = "SystemId";
        private const string TITLE = "Title";
        private const string DESCRIPTION = "Description";
        private const string CSID = "CSId";
        private const string INCHI = "Inchi";
        private const string INCHIKEY = "InchiKey";
        private const string CASREGNO = "CASRegNo";
        private const string CASNAME = "CASName";
        private const string SYNONYMS = "Synonyms";
        private const string SYNONYM = "Synonym";
        private const string MANUFACTURERCODE = "ManufacturerCode";
        private const string MOLECULARFORMULA = "MolecularFormula";
        private const string MOLECULAR = "Molecular";
        private const string MOLECULARWEIGHT = "MolecularWeight";
        private const string AVERAGEMASS = "AverageMass";
        private const string MONOISOTOPICMASS = "MonoisotopicMass";
        private const string NOMINALMASS = "NominalMass";
        private const string SOURCE = "Source";
        private const string THESISID = "ThesisID";
        private const string THESISCOMPOUNDNO = "ThesisCompoundNo";
        private const string REALSAMPLEAVAILABLE = "RealSampleAvailable";
        private const string CHIRAL = "Chiral";
        private const string DIASTEREOSELECTIVE = "Diastereoselective";
        private const string RACEMIC = "Racemic";
        private const string IR = "IR";
        private const string HNMR = "HNMR";
        private const string CNMR = "CNMR";
        private const string FNMR = "FNMR";
        private const string PNMR = "PNMR";
        private const string BNMR = "BNMR";
        private const string FULLNMRASSIGNMENT = "FullNMRAssignment";
        private const string MZ = "MZ";
        private const string HRMS = "HRMS";
        private const string ELEMENTALANALYSIS = "ElementalAnalysis";
        private const string MELTINGPOINT = "MeltingPoint";
        private const string BOILINGPOINT = "BoilingPoint";
        private const string OPTICALROTATION = "OpticalRotation";
        private const string CRYSTALSTRUCTUREDATA = "CrystalStructureData";
        private const string BIOLOGICALTESTINGDATA = "BiologicalTestingData";
        private const string COMMENTS = "Comments";
        //Marinlit specific started from here.
        private const string ARTICLEID = "ArticleId";
        private const string PUBLICID = "PublicId";
        private const string PRINTID = "PrintId";
        private const string ExternalId = "ExternalId";
        private const string SUBSTACNETYPE = "Type";
        private const string ACDName = "ACDName";
        private const string TRADEMARKS = "TradeMarks";
        private const string NAME = "Name";
        private const string COMPOSITION = "Composition";
        private const string IMAGE = "Image";
        private const string ID = "ID";
        private const string CDX = "Cdx";
        private const string TIF = "Tif";
        private const string MOL = "Mol";
        private const string LITERATUREREFERENCES = "LiteratureReferences";
        private const string DOI = "DOI";
        private const string LASTPAGE = "LastPage";
        private const string FIRSTPAGE = "FirstPage";
        private const string VOLUME = "Volume";
        private const string YEAR = "Year";
        private const string AUTHOR = "Author";
        private const string FIRSTNAME = "FirstName";
        private const string SURNAME = "Surname";
        private const string CSPROPERTIES = "CsProperties";
        private const string USE = "Use";
        private const string NOTE = "Note";
        private const string CSDATA = "CSData";
        private const string GROUP = "Group";
        private const string GROUPS = "Groups";
        private const string VALUE = "Value";
        private const string CNMRDATA = "CNMRData";
        private const string NUCLEI = "Nuclei";
        private const string NMRPEAK = "NMRPeak";
        private const string NMRPEAKS = "NMRPeaks";
        private const string LOCANT = "Locant";
        private const string VAR = "Var";
        private const string PROTONS = "Protons";
        private const string HNMRDATA = "HNMRData";
        private const string VETKEYWORDS = "VetKeywords";
        private const string KEYWORDS = "Keywords";
        private const string LOCATION = "Location";
        private const string LOCATIONS = "Locations";
        private const string LONGITUDE = "Longitude";
        private const string LATITUDE = "Latitude";
        private const string DEPTH = "Depth";
        private const string QF = "QF";
        private const string UNIT = "Unit";
        private const string LOGEVALUE = "LogEValue";
        private const string DETAILS = "Details";
        private const string PROPERTYDETAIL = "PropertyDetail";
        private const string MLSTATUS = "MLStatus";
        private const string FALSE = "false";
        private const string SECURITY = "Security";
        private const string CreatedDate = "CreatedDate";
        private const string UpdatedDate = "UpdatedDate";
        private const string StatusCode = "StatusCode";
        private const string Status = "Status";
        public const string ArticleCompoundNo = "ArticleCompoundNo";

        #endregion

        #region Constructor and public methods

        public SubstanceHelper()
        {
        }

        public SubstanceHelper(string searchResults)
        {
            InitializeMainDocument(searchResults);
        }

        public Substance GetSubstance()
        {
            Substance substance = null;
            try
            {
                if (_substanceXElement != null && _substanceXElement.Elements().Any())
                {
                    substance = BuildSubstance(_substanceXElement);
                }
            }
            finally
            {
                _substanceXElement = null;
            }
            return substance;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            if (!string.IsNullOrEmpty(searchResults))
            {
                using (TextReader searchResultTextReader = new StringReader(searchResults))
                {
                    var searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                    if (searchResultsXDocument.Root != null)
                    {
                        _substanceXElement = searchResultsXDocument.Root;
                    }
                }
            }
        }

        private Substance BuildSubstance(XElement xElement)
        {
            Substance substance = null;
            try
            {
                if (xElement != null)
                {
                    substance = new Substance();
                    XElement substanceTempXElement = null;

                    substanceTempXElement = xElement.Element(XName.Get(SYSTEMID));
                    substance.SystemId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(ARTICLEID));
                    substance.ArticleId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get("URI"));
                    substance.URI = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get("RscId"));
                    substance.RscId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get("DepositionBatchId"));
                    substance.DepositionBatchId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(TITLE));
                    substance.Title = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(DESCRIPTION));
                    substance.Description = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(CSID));
                    substance.CSId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(INCHI));
                    substance.Inchi = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(INCHIKEY));
                    substance.InchiKey = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(SYNONYMS));
                    substance.Synonyms = substanceTempXElement != null ? BuildSynonyms(substanceTempXElement) : null;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARFORMULA));
                    substance.MolecularFormula = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARWEIGHT));
                    substance.MolecularWeight = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(AVERAGEMASS));
                    substance.AverageMass = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(MONOISOTOPICMASS));
                    substance.MonoisotopicMass = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(NOMINALMASS));
                    substance.NominalMass = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(SOURCE));
                    substance.Source = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(THESISID));
                    substance.ThesisID = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(THESISCOMPOUNDNO));
                    substance.ThesisCompoundNo = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;


                    substanceTempXElement = xElement.Element(XName.Get(ARTICLEID));
                    substance.ArticleId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(ArticleCompoundNo));
                    substance.ArticleCompoundNo = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;


                    substanceTempXElement = xElement.Element(XName.Get(REALSAMPLEAVAILABLE));
                    substance.RealSampleAvailable = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(CHIRAL));
                    substance.Chiral = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(DIASTEREOSELECTIVE));
                    substance.Diastereoselective = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(RACEMIC));
                    substance.Racemic = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(IR));
                    substance.IR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(HNMR));
                    substance.HNMR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(CNMR));
                    substance.CNMR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(FNMR));
                    substance.FNMR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(PNMR));
                    substance.PNMR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(BNMR));
                    substance.BNMR = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(FULLNMRASSIGNMENT));
                    substance.FullNMRAssignment = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(MZ));
                    substance.MZ = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(HRMS));
                    substance.HRMS = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(ELEMENTALANALYSIS));
                    substance.ElementalAnalysis = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(MELTINGPOINT));
                    substance.MeltingPoint = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(BOILINGPOINT));
                    substance.BoilingPoint = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(OPTICALROTATION));
                    substance.OpticalRotation = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(CRYSTALSTRUCTUREDATA));
                    substance.CrystalStructureData = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(BIOLOGICALTESTINGDATA));
                    substance.BiologicalTestingData = substanceTempXElement != null && Convert.ToBoolean(substanceTempXElement.Value);

                    substanceTempXElement = xElement.Element(XName.Get(COMMENTS));
                    substance.Comments = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    //Marinlit substances
                    substanceTempXElement = _substanceXElement.Element(XName.Get(PUBLICID));
                    substance.PublicId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(PRINTID));
                    substance.PrintId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(ExternalId));
                    substance.ExternalId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(SUBSTACNETYPE));
                    substance.Type = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASREGNO));
                    substance.CASRegNo = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASNAME));
                    substance.CASName = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(ACDName));
                    substance.ACDName = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(TRADEMARKS));
                    substance.TradeMarks = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(MANUFACTURERCODE));
                    substance.ManufacturerCode = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(COMPOSITION));
                    substance.Composition = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(LOCATIONS));
                    substance.Locations = substanceTempXElement != null ? BuildLocationData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(IMAGE));
                    substance.Image = substanceTempXElement != null ? BuildImageData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(LITERATUREREFERENCES));
                    substance.LiteratureReferences = substanceTempXElement != null ? BuildLiteratureReferences(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CSPROPERTIES));
                    substance.CsProperties = substanceTempXElement != null ? BuildCsProperties(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(USE));
                    substance.Use = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(NOTE));
                    substance.Note = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CSDATA));
                    substance.CSData = substanceTempXElement != null ? BuildCsData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(KEYWORDS));
                    substance.Keywords = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(VETKEYWORDS));
                    substance.VetKeywords = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(SECURITY));
                    substance.Security = substanceTempXElement != null ? SecurityHelper.GetSecurity(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(Status));
                    substance.Status = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(StatusCode));
                    if (substanceTempXElement != null)
                    {
                        int statusCode = 0;
                        int.TryParse(substanceTempXElement.Value, out statusCode);
                        substance.StatusCode = statusCode;
                    }

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CreatedDate));
                    substance.CreatedDate = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(UpdatedDate));
                    substance.UpdatedDate = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get("published-date"));
                    substance.PublishedDate = substanceTempXElement != null ? substanceTempXElement.Value : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xElement = null;
            }
            return substance;
        }

        private List<string> BuildSynonyms(XElement synonymsXElement)
        {
            List<string> synonyms = null;
            if (synonymsXElement != null)
            {
                IEnumerable<XElement> synonymsElements = synonymsXElement.Elements(XName.Get(SYNONYM));

                foreach (XElement element in synonymsElements)
                {
                    if (element != null)
                    {
                        if (synonyms == null)
                        {
                            synonyms = new List<string>();
                        }
                        synonyms.Add(element.Value);
                    }
                }

            }
            return synonyms;
        }

        private List<Location> BuildLocationData(XElement locationsXElement)
        {
            List<Location> locations = null;
            XElement tempXElement;
            try
            {
                if (locationsXElement != null)
                {
                    locations = new List<Location>();
                    IEnumerable<XElement> locationXElementList = locationsXElement.Elements(XName.Get(LOCATION));
                    foreach (XElement locationXElement in locationXElementList)
                    {
                        if (locationXElement != null)
                        {
                            var location = new Location();

                            tempXElement = locationXElement.Element(LONGITUDE);
                            location.Longitude = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(LATITUDE);
                            location.Latitude = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(DEPTH);
                            location.Depth = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(QF);
                            location.QF = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(NAME);
                            location.Name = tempXElement != null ? tempXElement.Value : string.Empty;
                            locations.Add(location);
                        }
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                locationsXElement = null;
                tempXElement = null;
            }
            return locations;
        }

        private Image BuildImageData(XElement imageXElement)
        {
            Image image = null;
            if (imageXElement != null)
            {
                image = new Image();
                XElement tempXElement = imageXElement.Element(ID);
                image.ID = tempXElement != null ? tempXElement.Value : string.Empty;
                tempXElement = imageXElement.Element(CDX);
                image.Cdx = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(TIF);
                image.Tif = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(MOL);
                image.Mol = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);

                tempXElement = null;
                imageXElement = null;
            }
            return image;
        }

        private LiteratureRereference BuildLiteratureReferences(XElement literatureReferencesXElement)
        {
            LiteratureRereference references = null;
            XElement citationTempXElement;
            XElement ciatationSubElementXElement;
            try
            {
                if (literatureReferencesXElement != null)
                {
                    List<JournalCitation> journalCitations = null;

                    foreach (var element in literatureReferencesXElement.Elements().Where(element => element != null))
                    {
                        if (journalCitations == null)
                            journalCitations = new List<JournalCitation>();

                        citationTempXElement = element;
                        if (citationTempXElement != null)
                        {
                            var citation = new JournalCitation();

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(TITLE));
                            citation.Title = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(YEAR));
                            citation.Year = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(VOLUME));
                            citation.Volume = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(FIRSTPAGE));
                            citation.FirstPage = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(LASTPAGE));
                            citation.LastPage = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(DOI));
                            citation.DOI = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            var authorList = citationTempXElement.Elements(XName.Get(AUTHOR));
                            if (ciatationSubElementXElement != null)
                            {
                                XElement authorXElement = null;
                                foreach (var author in authorList)
                                {
                                    var citationAuthor = new CitationAuthor();
                                    if (citation.Authors == null)
                                    {
                                        citation.Authors = new List<CitationAuthor>();
                                    }
                                    if (author != null)
                                    {
                                        authorXElement = author.Element(XName.Get(FIRSTNAME));
                                        citationAuthor.FirstName = authorXElement != null ? authorXElement.Value : string.Empty;

                                        authorXElement = author.Element(XName.Get(SURNAME));
                                        citationAuthor.Surname = authorXElement != null ? authorXElement.Value : string.Empty;

                                        citation.Authors.Add(citationAuthor);
                                    }
                                }
                                authorXElement = null;
                            }
                            journalCitations.Add(citation);
                            authorList = null;
                        }
                    }

                    if (journalCitations != null)
                    {
                        if (references == null)
                            references = new LiteratureRereference();
                        references.JournalCitations = journalCitations;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                citationTempXElement = null;
                ciatationSubElementXElement = null;
            }
            return references;
        }

        private List<Property> BuildCsProperties(XElement csPropertiesXElement)
        {
            List<Property> csProperties = null;
            XElement tempXElement;

            if (csPropertiesXElement != null)
            {
                csProperties = new List<Property>();
                var properties = csPropertiesXElement.Elements(XName.Get("Property"));

                foreach (var element in properties)
                {
                    if (element != null)
                    {
                        var property = new Property();

                        tempXElement = element.Element(NAME);
                        property.Name = tempXElement != null ? WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement)) : string.Empty;
                        tempXElement = element.Element(DETAILS);

                        if (tempXElement != null)
                        {
                            var detailList = new List<PropertyDetail>();
                            var detailElements = tempXElement.Elements(XName.Get(PROPERTYDETAIL));

                            foreach (var detailElement in detailElements)
                            {
                                var detail = new PropertyDetail();
                                XElement subtempXElement = null;

                                subtempXElement = detailElement.Element(VALUE);
                                detail.Value = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                subtempXElement = detailElement.Element(LOGEVALUE);
                                detail.LogEValue = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                subtempXElement = detailElement.Element(UNIT);
                                detail.Unit = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                detailList.Add(detail);
                                subtempXElement = null;
                            }
                            detailElements = null;
                            property.Details = detailList;
                        }
                        csProperties.Add(property);
                    }
                }
                properties = null;
            }
            tempXElement = null;
            return csProperties;
        }

        private CSData BuildCsData(XElement csDataXElement)
        {
            CSData csData = null;
            XElement csDataTempXElement;
            try
            {
                csData = new CSData();
                csDataTempXElement = csDataXElement.Element(MLSTATUS);
                csData.MLStatus = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;
                csDataTempXElement = csDataXElement.Element(XName.Get(GROUPS));

                if (csDataTempXElement != null)
                {
                    var groupXElements = csDataTempXElement.Elements(XName.Get(GROUP));
                    if (groupXElements != null && groupXElements.Count() > 0)
                    {
                        csData.Groups = new List<Group>();
                        foreach (var groupElement in groupXElements)
                        {
                            if (groupElement != null)
                            {
                                var group = new Group();

                                csDataTempXElement = groupElement.Element(XName.Get(NAME));
                                group.Name = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;

                                csDataTempXElement = groupElement.Element(XName.Get(VALUE));
                                group.Value = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;

                                csData.Groups.Add(group);
                            }
                        }
                        groupXElements = null;
                    }
                }
                csDataTempXElement = csDataXElement.Element(XName.Get(CNMRDATA));
                csData.CNMRData = csDataTempXElement != null ? BuildNMRData(csDataTempXElement) : null;

                csDataTempXElement = csDataXElement.Element(XName.Get(HNMRDATA));
                csData.HNMRData = csDataTempXElement != null ? BuildNMRData(csDataTempXElement) : null;

                csDataTempXElement = csDataXElement.Element(XName.Get("MonomerIds"));
                csData.MonomerIds = csDataTempXElement != null ? BuildMonomerIDs(csDataTempXElement) : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                csDataTempXElement = null;

            }
            return csData;
        }

        private NMRData BuildNMRData(XElement nmrDataXElement)
        {
            NMRData nmrData = null;
            XElement nmrDataTempXElement;
            IEnumerable<XElement> nmrXElements;
            try
            {
                if (nmrDataXElement != null)
                {
                    nmrData = new NMRData();
                    nmrDataTempXElement = nmrDataXElement.Element(XName.Get(NMRPEAKS));
                    nmrXElements = nmrDataTempXElement != null ? nmrDataTempXElement.Elements(XName.Get(NMRPEAK)) : null;

                    if (nmrXElements != null && nmrXElements.Any())
                    {
                        nmrData.NMRPeaks = new List<NMRPeak>();
                        foreach (var nmrXElement in nmrXElements)
                        {
                            if (nmrXElement != null)
                            {
                                var peak = new NMRPeak();
                                peak.Locant = CommonHelpers.GetAttributeValue(nmrXElement, LOCANT);
                                peak.Var = CommonHelpers.GetAttributeValue(nmrXElement, VAR);
                                peak.Protons = CommonHelpers.GetAttributeValue(nmrXElement, PROTONS);
                                peak.Value = nmrXElement.Value;

                                nmrData.NMRPeaks.Add(peak);
                            }
                        }
                    }
                    nmrDataTempXElement = nmrDataXElement.Element(XName.Get(NUCLEI));
                    nmrData.Nuclei = nmrDataTempXElement != null ? nmrDataTempXElement.Value : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                nmrDataTempXElement = null;
                nmrXElements = null;
            }
            return nmrData;
        }

        private List<string> BuildMonomerIDs(XElement monomerIdsXElement)
        {
            List<string> monomerIds = null;
            if (monomerIdsXElement != null)
            {
                var monomerIdsElements = monomerIdsXElement.Elements(XName.Get("MonomerId"));

                foreach (XElement element in monomerIdsElements.Where(element => element != null))
                {
                    if (monomerIds == null)
                    {
                        monomerIds = new List<string>();
                    }
                    monomerIds.Add(element.Value);
                }
                monomerIdsElements = null;
            }
            return monomerIds;
        }

        #endregion
    }
}
