﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace RSC.Database.Api.Common.Helpers
{
    public class MarkLogicHelper
    {
        public static string MarkLogicRequestHelper(string url, string database, string method, string data = "")
        {
            Stream newStream = null;
            Stream dataStream = null;
            StreamReader reader = null;
            HttpWebResponse response = null;
            byte[] byteArray = null;
            UTF8Encoding encoding = null;
            string responseString = string.Empty;
            MarkLogic markLogicElement = null;
            HttpWebRequest request = null;

            try
            {
                markLogicElement = GetMarkLogicSettings(database);
                url = markLogicElement.Url + url;
                request = (HttpWebRequest)WebRequest.Create(url);

                request.Timeout = 600000;
                request.ReadWriteTimeout = 600000;
                request.KeepAlive = false;


                NetworkCredential credential = new NetworkCredential();
                credential.UserName = markLogicElement.UserName;
                credential.Password = markLogicElement.Password;

                request.Credentials = CredentialCache.DefaultCredentials;
                request.Credentials = credential;

                request.Method = !string.IsNullOrEmpty(method) ? method : Constants.GET; ;
                request.ContentType = Constants.XMLAcceptHeader;
                switch (request.Method.ToUpper())
                {
                    case Constants.GET:
                        break;
                    case Constants.POST:
                    case Constants.PUT:
                        if (!string.IsNullOrEmpty(data))
                        {
                            encoding = new UTF8Encoding();
                            byteArray = encoding.GetBytes(data);
                            // Set the content length of the string being posted.
                            request.ContentLength = byteArray.Length;

                            //write the content to the stream
                            newStream = request.GetRequestStream();
                            newStream.Write(byteArray, 0, byteArray.Length);
                        }
                        break;
                    case Constants.DELETE:
                        break;
                }

                response = (HttpWebResponse)request.GetResponse();
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();

                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);

                // Read the content.
                responseString = reader.ReadToEnd();

                if (request != null)
                {
                    request.Abort();
                }
            }
            catch (WebException ex)
            {
                throw new WebException(message: $"url: {request.RequestUri}; headers: {request.Headers}; body: {data}", innerException: ex);
            }
            finally
            {
                // Cleanup the streams and the response.
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                    dataStream = null;
                }
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
                if (newStream != null)
                {
                    newStream.Close();
                    newStream.Dispose();
                    newStream = null;
                }

                byteArray = null;
                data = null;

                encoding = null;
            }
            return responseString;
        }

        public static string MarkLogicRequestHelper(string url, string database, string method, out string status, string data = "")
        {
            Stream newStream = null;
            Stream dataStream = null;
            StreamReader reader = null;
            HttpWebResponse response = null;
            byte[] byteArray = null;
            ASCIIEncoding encoding = null;
            string responseString = string.Empty;
            MarkLogic markLogicElement = null;
            HttpWebRequest request = null;
            status = string.Empty;
            try
            {
                markLogicElement = GetMarkLogicSettings(database);
                url = markLogicElement.Url + url;
                request = (HttpWebRequest)WebRequest.Create(url);

                request.Timeout = 600000;
                request.ReadWriteTimeout = 600000;
                request.KeepAlive = false;


                NetworkCredential credential = new NetworkCredential();
                credential.UserName = markLogicElement.UserName;
                credential.Password = markLogicElement.Password;

                request.Credentials = CredentialCache.DefaultCredentials;
                request.Credentials = credential;

                request.Method = !string.IsNullOrEmpty(method) ? method : Constants.GET; ;
                request.ContentType = Constants.XMLAcceptHeader;
                switch (request.Method.ToUpper())
                {
                    case Constants.GET:
                        break;
                    case Constants.POST:
                    case Constants.PUT:
                        if (!string.IsNullOrEmpty(data))
                        {
                            encoding = new ASCIIEncoding();
                            byteArray = encoding.GetBytes(data);
                            // Set the content length of the string being posted.
                            request.ContentLength = byteArray.Length;

                            //write the content to the stream
                            newStream = request.GetRequestStream();
                            newStream.Write(byteArray, 0, byteArray.Length);
                        }
                        break;
                    case Constants.DELETE:
                        break;
                }

                response = (HttpWebResponse)request.GetResponse();
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();
                status = response.StatusCode.ToString();

                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);

                // Read the content.
                responseString = reader.ReadToEnd();

                if (request != null)
                {
                    request.Abort();
                }
            }
            finally
            {
                // Cleanup the streams and the response.
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                    dataStream = null;
                }
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
                if (newStream != null)
                {
                    newStream.Close();
                    newStream.Dispose();
                    newStream = null;
                }

                byteArray = null;
                data = null;

                encoding = null;
            }
            return responseString;
        }

        public static MarkLogic GetMarkLogicSettings(string database)
        {
            List<MarkLogic> markLogicElements = null;
            MarkLogic markLogicElement = null;
            // Read value from the configuration file.
            try
            {
                markLogicElements = MarkLogicConfigurationHelper.GetMarkLogicList();
                if (markLogicElements != null)
                {
                    markLogicElement = (from MarkLogic markLogic in markLogicElements where markLogic.ID.Equals(database, StringComparison.InvariantCultureIgnoreCase) select markLogic).SingleOrDefault();
                }

                bool markLogicValid = markLogicElement != null && (markLogicElement.UserName != string.Empty &&
                                       markLogicElement.Password != string.Empty &&
                                       markLogicElement.Url != string.Empty &&
                                       markLogicElement.Domain != string.Empty && markLogicElement.ID != string.Empty);

                if (markLogicValid == false)
                {
                    // throw new RSCException("Error in MarkLogic Configuration.", EventIDConstants.MarkLogicConfigurationError, nameSpace, TraceEventType.Critical);
                }
            }
            finally
            {
                markLogicElements = null;
            }

            return markLogicElement;
        }
    }
}
