﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public abstract class BaseConfigurationHelper
    {
        protected static string Get(string key, bool required = true)
        {
            string value = ConfigurationManager.AppSettings[key];
            if (required && string.IsNullOrEmpty(value))
                throw new InvalidOperationException(
                    string.Format("{0} is missing but is required to be defined in appSettings.", key));
            return value;
        }

        protected static bool GetBool(string key)
        {
            string s = Get(key);
            bool result = false;
            if (!bool.TryParse(s, out result))
                throw new InvalidOperationException(
                    string.Format("{0} is missing or has an invalid setting. " +
                                                 "'true' or 'false' are acceptable values for this setting.", key));
            return result;
        }

        protected static int GetInt(string key)
        {
            string s = Get(key);
            int result = 0;
            if (!int.TryParse(s, out result))
                throw new InvalidOperationException(
                    string.Format("{0} is missing or has an invalid setting. " +
                                    "An integer value was expected.", key));

            return result;
        }

        protected static DateTime GetDateTime(string key)
        {
            string s = Get(key);
            DateTime result;
            if (!DateTime.TryParse(s, out result))
            {
                throw new InvalidOperationException(
                    string.Format("{0} is missing or has an invalid setting.  A valid date/time format was expected.", key));
            }

            return result;
        }

        protected static T GetEnum<T>(string key) where T : struct
        {
            T result = default(T);
            if (!Enum.TryParse<T>(Get(key), out result))
                throw new InvalidOperationException(string.Format("{0} is not a valid enumeration value.", key));
            return result;
        }
    }
}
