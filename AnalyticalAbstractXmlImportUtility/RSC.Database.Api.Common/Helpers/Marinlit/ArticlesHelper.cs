﻿using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using RSC.Database.Api.Common.DataContracts;
using RSCpubs.ePlatform.Service.Common.Helpers;
using ML = RSC.Database.Api.Common.DataContracts.Marinlit;
using System;

namespace RSC.Database.Api.Common.Helpers.Marinlit
{
    public class ArticlesHelper
    {
        #region Private Variables
        public XElement articleXElement = null;
        private const string title = "title";
        private const string titleNameSpace = "http://www.rsc.org/schema/chemical/monograph";
        private const string titleNonMarkup = "titlenomarkup";
        private const string shorttitle = "short-title";
        private const string description = "description";
        private const string descriptionNonMarkup = "descriptionnomarkup";
        private const string subjects = "subjects";
        private const string subject = "subject";
        private const string articletype = "articletype";
        private const string startdate = "start-date";
        private const string enddate = "end-date";
        private const string createdDate = "created-date";
        private const string updatedDate = "updated-date";
        private const string authors = "authors";
        private const string firstname = "firstname";
        private const string lastname = "lastname";
        private const string institution = "institution";
        private const string author = "author";
        private const string journal = "journal";
        private const string abbrtitle = "abbr-title";
        private const string journalid = "journal-id";
        private const string sercode = "sercode";
        private const string issnprint = "issn-print";
        private const string issnonline = "issn-online";
        private const string publisher = "publisher";
        private const string institute = "institute";
        private const string publisheruri = "publisher-uri";
        private const string publisherurl = "publisher-url";

        private const string publisherid = "publisher-id";
        private const string publishername = "publisher-name";
        private const string publishercode = "publisher-doi-prefix";
        private const string subsyear = "subsyear";
        private const string journalvolume = "journal-volume";
        private const string journalissueid = "journal-issueid";
        private const string journaldescription = "description";

        private const string journalissueno = "journal-issueno";
        private const string systemid = "systemid";
        private const string doi = "doi";
        private const string csarid = "csarid";
        private const string pricecode = "price-code";
        private const string license = "license";
        private const string licenseid = "licenseid";
        private const string licensename = "licensename";
        private const string licenseabbriviatedname = "licenseabbriviatedname";
        private const string licenselink = "licenselink";
        private const string firstpage = "firstpage";
        private const string lastpage = "lastpage";
        private const string links = "links";
        private const string link = "link";
        private const string type = "type";
        private const string accepteddate = "accepted-date";
        private const string recieveddate = "recieved-date";
        private const string publisheddate = "published-date";
        private const string submissiondate = "submission-date";
        private const string noofcitations = "no-of-citations";
        private const string searchtext = "search-text";
        private const string searchtitle = "search-title";
        private const string firstnamenomarkup = "firstnamenomarkup";
        private const string lastnamenomarkup = "lastnamenomarkup";
        private const string rscid = "rscid";
        private const string IsTermsNConditionsAccepted = "terms-conditions-accepted";
        private const string embargoenddate = "embargo-enddate";
        private const string articlecoverimage = "article-cover-image";
        private const string journalimageurl = "image";
        private const string publisherimageurl = "publisher-image";
        private const string licenseimageurl = "licenseimage";
        private const string source = "source";
        private const string accesslevel = "accesslevel";
        private const string statuscode = "statuscode";
        private const string status = "status";
        private const string content = "content";
        private const string tags = "tags";
        private const string tag = "tag";
        private const string PublicId = "PublicId";
        private const string PrintId = "PrintId";
        private const string TrivialNames = "TrivialNames";
        private const string TrivialName = "TrivialName";
        private const string Keywords = "Keywords";
        private const string Keyword = "Keyword";
        private const string Substance = "Substance";
        private const string Substances = "Substances";
        private const string Note = "note";
        private const string TaxonomyList = "TaxonomyList";
        private const string Taxonomy = "Taxonomy";
        private const string Phylum = "Phylum";
        private const string Class = "Class";
        private const string Order = "Order";
        private const string Family = "Family";
        private const string Genus = "Genus";
        private const string Species = "Species";
        private const string Name = "Name";
        private const string Value = "Value";
        private const string IsSourceOfCompound = "IsSourceOfCompound";
        private const string Id = "Id";
        private const string LinkArticles = "LinkArticles";
        private const string Info = "information";
        private const string Location = "location";
        private const string OrganizationName = "orgname";
        private const string Address = "address";
        private const string Email = "email";
        private const string Concepts = "concepts";
        private const string Concept = "concept";
        private const string Matrices = "matrices";
        private const string Matrix = "matrix";
        private const string Analytes = "analytes";
        private const string Analyte = "analyte";
        private const string CasNo = "cas-no";
        private const string Components = "components";
        private const string Component = "component";
        private const string Language = "language";
        private const string FacetTree = "FacetTree";
        private const string NonPlantSources = "NonPlantSources";
        private const string NonPlantSource = "NonPlantSource";
        private const string BiologicalActivities = "BiologicalActivities";
        private const string BiologicalActivity = "BiologicalActivity";
        private const string CompoundClasses = "CompoundClasses";
        private const string CompoundClass = "CompoundClass";
        private const string NaturalProducts = "NaturalProducts";
        private const string NaturalProduct = "NaturalProduct";
        private const string IMAGE = "Image";
        private const string ID = "ID";
        private const string CDX = "Cdx";
        private const string TIF = "Tif";
        private const string MOL = "Mol";
        private const string FALSE = "false";
        #endregion Private Static Variables

        #region Constructors

        public ArticlesHelper()
        {

        }

        public ArticlesHelper(string searchResults)
        {

            InitializeMainDocument(searchResults);
        }

        #endregion

        #region Public Methods

        public ML.article GetArticle()
        {
            ML.article article = new ML.article();

            try
            {
                if (articleXElement != null)
                {

                    article = this.BuildArticle(articleXElement);

                }
            }
            finally
            {
                articleXElement = null;
            }

            return article;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null)
                {
                    articleXElement = searchResultsXDocument.Root;
                }

            }
        }

        private ML.article BuildArticle(XElement articleXElement)
        {
            ML.article articleInfo = null;

            #region XnamesDeclaration


            XElement systemidXElement = null;
            XElement rscidXElement = null;
            XElement isAcceptedXElement = null;
            XElement doiXElement = null;
            XElement csaridXElement = null;
            XElement pricecodeXElement = null;
            XElement titleXElement = null;
            XElement shorttitleXElement = null;
            XElement descriptionXElement = null;
            XElement subjectsXElement = null;
            IEnumerable<XElement> subjectXElements = null;
            XElement articletypeXElement = null;
            XElement startdateXElement = null;
            XElement enddateXElement = null;
            XElement authorsXElement = null;
            IEnumerable<XElement> authorXElements = null;
            XElement firstnameXElement = null;
            XElement lastnameXElement = null;
            XElement journalXElement = null;
            XElement abbrtitleXElement = null;
            XElement journalidXElement = null;
            XElement sercodeXElement = null;
            XElement issnprintXElement = null;
            XElement issnonlineXElement = null;
            XElement publisherXElement = null;
            XElement publisheridXElement = null;
            XElement publishernameXElement = null;
            XElement publishercodeXElement = null;
            XElement publisheruriXElement = null;
            XElement subsyearXElement = null;
            XElement journalvolumeXElement = null;
            XElement journalissueidXElement = null;
            XElement journalissuenoXElement = null;
            XElement journaldescriptionXElement = null;
            XElement licenseXElement = null;
            XElement licenseidXElement = null;
            XElement licensenameXElement = null;
            XElement licenseabbriviatednameXElement = null;
            XElement licenselinkXElement = null;
            XElement firstpageXElement = null;
            XElement lastpageXElement = null;
            XElement linksXElement = null;
            IEnumerable<XElement> linkXElements = null;
            XElement accepteddateXElement = null;
            XElement publisheddateXElement = null;
            XElement recieveddateXElement = null;
            XElement submissiondateXElement = null;
            XElement noofcitationsXElement = null;
            IEnumerable<XElement> searchTextXElements = null;
            XElement searchTitleXElement = null;
            XElement titleNonMarkupXElement = null;
            XElement descriptionNonMarkupXElement = null;

            XElement statusCodeXElement = null;
            XElement firstnameNoMarkupXElement = null;
            XElement lastnameNoMarkupXElement = null;
            XElement institutionXElement = null;
            XElement embargoenddateXElement = null;
            XElement articlecoverimageXElement = null;
            XElement jounalimageXElement = null;
            XElement publisherimageXElement = null;
            XElement licenseimageXElement = null;

            XElement publisherUrlXElement = null;
            XElement sourceXElement = null;
            XElement accesslevelXElement = null;
            XElement contentXElement = null;
            XElement tagsXElement = null;
            IEnumerable<XElement> tagXElements = null;
            XElement tempXElement = null;
            XElement infoXElement = null;
            IEnumerable<XElement> tempXElemets = null;
            #endregion

            try
            {
                if (articleXElement != null)
                {
                    articleInfo = new ML.article();

                    systemidXElement = articleXElement.Element(XName.Get(systemid));
                    if (systemidXElement != null)
                    {
                        articleInfo.systemid = systemidXElement.Value;
                    }

                    rscidXElement = articleXElement.Element(XName.Get(rscid));
                    if (rscidXElement != null)
                    {
                        articleInfo.rscid = rscidXElement.Value;
                    }

                    accesslevelXElement = articleXElement.Element(XName.Get(accesslevel));
                    if (accesslevelXElement != null)
                    {
                        articleInfo.accesslevel = accesslevelXElement.Value;
                    }

                    isAcceptedXElement = articleXElement.Element(XName.Get(IsTermsNConditionsAccepted));
                    if (isAcceptedXElement != null)
                    {
                        bool isTNCAccepted = false;
                        bool.TryParse(isAcceptedXElement.Value, out isTNCAccepted);
                        articleInfo.IsTermsNConditionsAccepted = isTNCAccepted;
                    }

                    doiXElement = articleXElement.Element(XName.Get(doi));
                    if (doiXElement != null)
                    {
                        articleInfo.doi = doiXElement.Value;
                    }
                    csaridXElement = articleXElement.Element(XName.Get(csarid));
                    if (csaridXElement != null)
                    {
                        articleInfo.csarid = csaridXElement.Value;
                    }

                    pricecodeXElement = articleXElement.Element(XName.Get(pricecode));
                    if (pricecodeXElement != null)
                    {
                        articleInfo.pricecode = pricecodeXElement.Value;
                    }

                    titleXElement = articleXElement.Element(XName.Get(title, titleNameSpace));
                    if (titleXElement != null)
                    {
                        articleInfo.title = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(titleXElement, true));
                    }
                    shorttitleXElement = articleXElement.Element(XName.Get(shorttitle));
                    if (shorttitleXElement != null)
                    {
                        articleInfo.shorttitle = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(shorttitleXElement));
                    }
                    titleNonMarkupXElement = articleXElement.Element(XName.Get(titleNonMarkup));
                    if (titleNonMarkupXElement != null)
                    {
                        articleInfo.titlenomarkup = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(titleNonMarkupXElement));
                    }
                    searchTitleXElement = articleXElement.Element(XName.Get(searchtitle));
                    if (searchTitleXElement != null)
                    {
                        articleInfo.searchtitle = WebUtility.HtmlDecode(searchTitleXElement.Value);
                    }
                    searchTextXElements = articleXElement.Elements(XName.Get(searchtext));
                    if (searchTextXElements != null && searchTextXElements.Count() > 0)
                    {
                        articleInfo.searchText = new List<string>();
                        foreach (XElement searchTextXElement in searchTextXElements)
                        {
                            articleInfo.searchText.Add(searchTextXElement.Value);
                        }
                    }

                    descriptionXElement = articleXElement.Element(XName.Get(description));
                    if (descriptionXElement != null)
                    {
                        articleInfo.description = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(descriptionXElement));
                    }

                    descriptionNonMarkupXElement = articleXElement.Element(XName.Get(descriptionNonMarkup));
                    if (descriptionNonMarkupXElement != null)
                    {
                        articleInfo.descriptionnomarkup = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(descriptionNonMarkupXElement));
                    }

                    journalvolumeXElement = articleXElement.Element(XName.Get(journalvolume));
                    if (journalvolumeXElement != null)
                    {
                        articleInfo.journalvolume = journalvolumeXElement.Value;
                    }

                    journalissueidXElement = articleXElement.Element(XName.Get(journalissueid));
                    if (journalissueidXElement != null)
                    {
                        articleInfo.journalissueid = journalissueidXElement.Value;
                    }

                    articletypeXElement = articleXElement.Element(XName.Get(articletype));
                    if (articletypeXElement != null)
                    {
                        articleInfo.articletype = articletypeXElement.Value;
                    }

                    startdateXElement = articleXElement.Element(XName.Get(startdate));
                    if (startdateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(startdateXElement.Value))
                        {
                            articleInfo.startdate = startdateXElement.Value;
                        }
                    }

                    enddateXElement = articleXElement.Element(XName.Get(enddate));
                    if (enddateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(enddateXElement.Value))
                        {
                            articleInfo.enddate = enddateXElement.Value;
                        }
                    }

                    accepteddateXElement = articleXElement.Element(XName.Get(accepteddate));
                    if (accepteddateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(accepteddateXElement.Value))
                        {
                            articleInfo.accepteddate = accepteddateXElement.Value;
                        }
                    }
                    publisheddateXElement = articleXElement.Element(XName.Get(publisheddate));
                    if (publisheddateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(publisheddateXElement.Value))
                        {
                            articleInfo.publisheddate = publisheddateXElement.Value;
                        }
                    }
                    embargoenddateXElement = articleXElement.Element(XName.Get(embargoenddate));
                    if (embargoenddateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(embargoenddateXElement.Value))
                        {
                            articleInfo.embargoenddate = embargoenddateXElement.Value;
                        }
                    }
                    articlecoverimageXElement = articleXElement.Element(XName.Get(articlecoverimage));
                    if (articlecoverimageXElement != null)
                    {
                        if (!string.IsNullOrEmpty(articlecoverimageXElement.Value))
                        {
                            articleInfo.articlecoverimage = articlecoverimageXElement.Value;
                        }
                    }
                    recieveddateXElement = articleXElement.Element(XName.Get(recieveddate));
                    if (recieveddateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(recieveddateXElement.Value))
                        {
                            articleInfo.recieveddate = recieveddateXElement.Value;
                        }
                    }
                    submissiondateXElement = articleXElement.Element(XName.Get(submissiondate));
                    if (submissiondateXElement != null)
                    {
                        if (!string.IsNullOrEmpty(submissiondateXElement.Value))
                        {
                            articleInfo.submissiondate = submissiondateXElement.Value;
                        }
                    }
                    noofcitationsXElement = articleXElement.Element(XName.Get(noofcitations));
                    if (noofcitationsXElement != null)
                    {
                        long noofCit = 0;
                        long.TryParse(noofcitationsXElement.Value, out noofCit);
                        articleInfo.noofcitations = noofCit;
                    }

                    journalissueidXElement = articleXElement.Element(XName.Get(journalissueid));
                    if (journalissueidXElement != null)
                    {
                        articleInfo.journalissueid = journalissueidXElement.Value;
                    }

                    journalissuenoXElement = articleXElement.Element(XName.Get(journalissueno));
                    if (journalissuenoXElement != null)
                    {
                        articleInfo.journalissueno = journalissuenoXElement.Value;
                    }

                    subsyearXElement = articleXElement.Element(XName.Get(subsyear));
                    if (subsyearXElement != null)
                    {
                        articleInfo.subsyear = subsyearXElement.Value;
                    }

                    firstpageXElement = articleXElement.Element(XName.Get(firstpage));
                    if (firstpageXElement != null)
                    {
                        articleInfo.firstpage = firstpageXElement.Value;
                    }

                    lastpageXElement = articleXElement.Element(XName.Get(lastpage));
                    if (lastpageXElement != null)
                    {
                        articleInfo.lastpage = lastpageXElement.Value;
                    }

                    sourceXElement = articleXElement.Element(XName.Get(source));
                    if (sourceXElement != null)
                    {
                        articleInfo.source = sourceXElement.Value;
                    }

                    contentXElement = articleXElement.Element(XName.Get(content));
                    if (contentXElement != null)
                    {
                        articleInfo.content = contentXElement.Value;
                    }

                    tagsXElement = articleXElement.Element(XName.Get(tags));
                    if (tagsXElement != null)
                    {
                        tagXElements = tagsXElement.Elements(XName.Get(tag));
                        articleInfo.tags = new List<string>();
                        foreach (XElement tagXElement in tagXElements)
                        {
                            articleInfo.tags.Add(tagXElement.Value);
                        }

                    }

                    infoXElement = articleXElement.Element(XName.Get(Info));
                    if (infoXElement != null)
                    {
                        articleInfo.Information = new Information();
                        articleInfo.Information.Location = new ArticleLocation();
                        tempXElemets = infoXElement.Elements(XName.Get(Location));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                tempXElement = xElement.Element(XName.Get(OrganizationName));
                                if (tempXElement != null)
                                {
                                    articleInfo.Information.Location.OrganizationName = tempXElement.Value;
                                }
                                tempXElement = xElement.Element(XName.Get(Address));
                                if (tempXElement != null)
                                {
                                    articleInfo.Information.Location.Address = tempXElement.Value;
                                }
                            }
                        }
                        tempXElement = infoXElement.Element(XName.Get(Email));
                        if (tempXElement != null)
                        {
                            articleInfo.Information.Email = tempXElement.Value;
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(Concepts));
                    if (tempXElement != null)
                    {
                        articleInfo.Concepts = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(Concept));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.Concepts.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(Analytes));
                    if (tempXElement != null)
                    {
                        articleInfo.Analytes = new List<Analyte>();
                        tempXElemets = tempXElement.Elements(XName.Get(Analyte));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                Analyte analyte = new Analyte();
                                XAttribute attrib = xElement.Attribute(XName.Get(CasNo));
                                if (attrib != null && !string.IsNullOrWhiteSpace(attrib.Value))
                                {
                                    analyte.CASNo = attrib.Value;
                                }
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    analyte.Value = xElement.Value;
                                }
                                articleInfo.Analytes.Add(analyte);
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(Matrices));
                    if (tempXElement != null)
                    {
                        articleInfo.Matrices = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(Matrix));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.Matrices.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(Language));
                    if (tempXElement != null)
                    {
                        articleInfo.Language = tempXElement.Value;
                        tempXElement = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(FacetTree));
                    if (tempXElement != null)
                    {
                        articleInfo.Categories = (FacetTree)SerializeDeserializeHelper.DeserializeObject(tempXElement.ToString(), typeof(FacetTree));
                        tempXElement = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(IMAGE));
                    articleInfo.Image = tempXElement != null ? BuildImageData(tempXElement) : null;
                    tempXElement = null;
                    journalXElement = articleXElement.Element(XName.Get(journal));
                    if (journalXElement != null)
                    {
                        articleInfo.journal = new journal();
                        titleXElement = journalXElement.Element(XName.Get(title));
                        if (titleXElement != null)
                        {
                            articleInfo.journal.title = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(titleXElement));
                        }
                        abbrtitleXElement = journalXElement.Element(XName.Get(abbrtitle));
                        if (abbrtitleXElement != null)
                        {
                            articleInfo.journal.abbrtitle = WebUtility.HtmlDecode(abbrtitleXElement.Value);
                        }
                        journalidXElement = journalXElement.Element(XName.Get(journalid));
                        if (journalidXElement != null)
                        {
                            articleInfo.journal.journalid = journalidXElement.Value;
                        }
                        sercodeXElement = journalXElement.Element(XName.Get(sercode));
                        if (sercodeXElement != null)
                        {
                            articleInfo.journal.sercode = sercodeXElement.Value;
                        }
                        issnonlineXElement = journalXElement.Element(XName.Get(issnonline));
                        if (issnonlineXElement != null)
                        {
                            articleInfo.journal.issnonline = issnonlineXElement.Value;
                        }
                        issnprintXElement = journalXElement.Element(XName.Get(issnprint));
                        if (issnprintXElement != null)
                        {
                            articleInfo.journal.issnprint = issnprintXElement.Value;
                        }

                        journaldescriptionXElement = journalXElement.Element(XName.Get(journaldescription));
                        if (journaldescriptionXElement != null)
                        {
                            articleInfo.journal.description = WebUtility.HtmlDecode(journaldescriptionXElement.Value);
                        }
                        jounalimageXElement = journalXElement.Element(XName.Get(journalimageurl));
                        if (jounalimageXElement != null)
                        {
                            articleInfo.journal.image = jounalimageXElement.Value;
                        }

                        if (string.IsNullOrEmpty(articleInfo.journal.journalid) && !string.IsNullOrEmpty(articleInfo.journal.uri))
                        {
                            string[] journaluriparts = articleInfo.journal.uri.Split('/');
                            articleInfo.journal.journalid = journaluriparts.Last();
                            articleInfo.journal.journalid = articleInfo.journal.journalid.Replace(".xml", "");

                        }
                    }
                    publisherXElement = articleXElement.Element(XName.Get(publisher));
                    if (publisherXElement != null)
                    {
                        articleInfo.publisher = new publisher();
                        publisheridXElement = publisherXElement.Element(publisherid);
                        if (publisheridXElement != null)
                        {
                            articleInfo.publisher.publishersysid = publisheridXElement.Value;
                        }
                        publishernameXElement = publisherXElement.Element(publishername);
                        if (publishernameXElement != null)
                        {
                            articleInfo.publisher.publishername = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(publishernameXElement));
                        }
                        publishercodeXElement = publisherXElement.Element(publishercode);
                        if (publishercodeXElement != null)
                        {
                            articleInfo.publisher.publisherdoiprefix = publishercodeXElement.Value;
                        }
                        publisheruriXElement = publisherXElement.Element(XName.Get(publisheruri));
                        if (publisheruriXElement != null)
                        {
                            articleInfo.publisher.uri = publisheruriXElement.Value;
                        }
                        publisherUrlXElement = publisherXElement.Element(XName.Get(publisherurl));
                        if (publisherUrlXElement != null)
                        {
                            articleInfo.publisher.url = publisherUrlXElement.Value;
                        }
                        publisherimageXElement = publisherXElement.Element(XName.Get(publisherimageurl));
                        if (publisherimageXElement != null)
                        {
                            articleInfo.publisher.publisherimage = publisherimageXElement.Value;
                        }
                    }

                    publisherXElement = articleXElement.Element(XName.Get(institute));
                    if (publisherXElement != null)
                    {
                        articleInfo.institute = new publisher();
                        publisheridXElement = publisherXElement.Element(publisherid);
                        if (publisheridXElement != null)
                        {
                            articleInfo.institute.publishersysid = publisheridXElement.Value;
                        }
                        publishernameXElement = publisherXElement.Element(publishername);
                        if (publishernameXElement != null)
                        {
                            articleInfo.institute.publishername = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(publishernameXElement));
                        }
                        publishercodeXElement = publisherXElement.Element(publishercode);
                        if (publishercodeXElement != null)
                        {
                            articleInfo.institute.publisherdoiprefix = publishercodeXElement.Value;
                        }
                        publisheruriXElement = publisherXElement.Element(XName.Get(publisheruri));
                        if (publisheruriXElement != null)
                        {
                            articleInfo.institute.uri = publisheruriXElement.Value;
                        }
                        publisherUrlXElement = publisherXElement.Element(XName.Get(publisherurl));
                        if (publisherUrlXElement != null)
                        {
                            articleInfo.institute.url = publisherUrlXElement.Value;
                        }
                        publisherimageXElement = publisherXElement.Element(XName.Get(publisherimageurl));
                        if (publisherimageXElement != null)
                        {
                            articleInfo.institute.publisherimage = publisherimageXElement.Value;
                        }
                    }
                    subjectsXElement = articleXElement.Element(XName.Get(subjects));
                    if (subjectsXElement != null)
                    {
                        articleInfo.subjects = new List<ML.subject>();
                        subjectXElements = subjectsXElement.Elements(subject);
                        if (subjectXElements != null && subjectXElements.Count() > 0)
                        {
                            foreach (XElement subjectXElement in subjectXElements)
                            {
                                if (subjectXElement != null)
                                {
                                    ML.subject subjectInfo = new ML.subject();
                                    subjectInfo.id = CommonHelpers.GetAttributeValue(subjectXElement, "id");
                                    subjectInfo.Value = WebUtility.HtmlDecode(subjectXElement.Value);
                                    articleInfo.subjects.Add(subjectInfo);
                                }
                            }
                        }
                    }

                    licenseXElement = articleXElement.Element(XName.Get(license));
                    if (licenseXElement != null)
                    {
                        articleInfo.license = new license();
                        licenseidXElement = licenseXElement.Element(XName.Get(licenseid));
                        if (licenseidXElement != null)
                        {
                            articleInfo.license.licenseid = licenseidXElement.Value;
                        }
                        licenselinkXElement = licenseXElement.Element(XName.Get(licenselink));
                        if (licenselinkXElement != null)
                        {
                            articleInfo.license.licenselink = licenselinkXElement.Value;
                        }
                        licenseabbriviatednameXElement = licenseXElement.Element(XName.Get(licenseabbriviatedname));
                        if (licenseabbriviatednameXElement != null)
                        {
                            articleInfo.license.licenseabbriviatedname = WebUtility.HtmlDecode(licenseabbriviatednameXElement.Value);
                        }
                        licensenameXElement = licenseXElement.Element(XName.Get(licensename));
                        if (licensenameXElement != null)
                        {
                            articleInfo.license.licensename = WebUtility.HtmlDecode(licensenameXElement.Value);
                        }
                        licenseimageXElement = licenseXElement.Element(XName.Get(licenseimageurl));
                        if (licenseimageXElement != null)
                        {
                            articleInfo.license.licenseimage = licenseimageXElement.Value;
                        }
                    }

                    linksXElement = articleXElement.Element(XName.Get(links));
                    if (linksXElement != null)
                    {
                        articleInfo.links = new List<ML.articleLink>();
                        linkXElements = linksXElement.Elements(XName.Get(link));
                        if (linkXElements != null && linkXElements.Count() > 0)
                        {
                            foreach (XElement linkXElement in linkXElements)
                            {
                                ML.articleLink articleLink = new ML.articleLink();
                                articleLink.type = CommonHelpers.GetAttributeValue(linkXElement, type);
                                articleLink.Value = linkXElement.Value;
                                articleInfo.links.Add(articleLink);
                            }
                        }
                    }
                    authorsXElement = articleXElement.Element(XName.Get(authors));
                    if (authorsXElement != null)
                    {
                        articleInfo.authors = new List<ML.articleAuthor>();
                        authorXElements = authorsXElement.Elements(author);
                        if (authorXElements != null && authorXElements.Count() > 0)
                        {
                            foreach (XElement authorXElement in authorXElements)
                            {
                                if (authorXElement != null)
                                {
                                    ML.articleAuthor articleAuthor = new ML.articleAuthor();
                                    firstnameXElement = authorXElement.Element(firstname);
                                    if (firstnameXElement != null)
                                    {
                                        articleAuthor.firstname = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(firstnameXElement));
                                    }
                                    lastnameXElement = authorXElement.Element(lastname);
                                    if (lastnameXElement != null)
                                    {
                                        articleAuthor.lastname = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(lastnameXElement));
                                    }

                                    firstnameNoMarkupXElement = authorXElement.Element(firstnamenomarkup);
                                    if (firstnameNoMarkupXElement != null)
                                    {
                                        articleAuthor.firstnamenomarkup = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(firstnameNoMarkupXElement));
                                    }
                                    lastnameNoMarkupXElement = authorXElement.Element(lastnamenomarkup);
                                    if (lastnameNoMarkupXElement != null)
                                    {
                                        articleAuthor.lastnamenomarkup = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(lastnameNoMarkupXElement));
                                    }
                                    institutionXElement = authorXElement.Element(institution);
                                    if (institutionXElement != null)
                                    {
                                        articleAuthor.institution = institutionXElement.Value;
                                    }
                                    articleAuthor.Correspondance = CommonHelpers.GetAttributeValue(authorXElement, "correspondance");

                                    tempXElement = authorXElement.Element(XName.Get(Email));
                                    articleAuthor.Email = tempXElement != null ? tempXElement.Value : string.Empty;

                                    tempXElement = authorXElement.Element(XName.Get("affiliations"));
                                    if (tempXElement != null)
                                    {
                                        articleAuthor.Affiliations = new List<string>();
                                        tempXElemets = tempXElement.Elements("affiliation");
                                        if (tempXElemets != null && tempXElemets.Any())
                                        {
                                            foreach (var affiliationXelement in tempXElemets)
                                            {
                                                articleAuthor.Affiliations.Add(affiliationXelement.Value);
                                            }
                                        }
                                    }

                                    articleInfo.authors.Add(articleAuthor);
                                }
                            }
                        }
                    }
                    tempXElement = articleXElement.Element(XName.Get(PublicId));
                    articleInfo.PublicId = tempXElement != null ? tempXElement.Value : string.Empty;

                    tempXElement = articleXElement.Element(XName.Get(PrintId));
                    articleInfo.PrintId = tempXElement != null ? tempXElement.Value : string.Empty;

                    tempXElement = articleXElement.Element(XName.Get(Note, titleNameSpace));
                    articleInfo.Note = tempXElement != null ? WebUtility.HtmlDecode(CommonHelpers.GetInnerXML(tempXElement)) : string.Empty;

                    tempXElement = articleXElement.Element(XName.Get(TrivialNames));
                    articleInfo.TrivialNames = tempXElement != null ? BuildTrivialNames(tempXElement) : null;

                    tempXElement = articleXElement.Element(XName.Get(LinkArticles));
                    articleInfo.LinkArticles = tempXElement != null ? BuildLinkArticles(tempXElement) : null;

                    tempXElement = articleXElement.Element(XName.Get(Keywords));
                    articleInfo.Keywords = tempXElement != null ? BuildKeywords(tempXElement) : null;

                    tempXElement = articleXElement.Element(XName.Get(Substances));
                    articleInfo.Substances = tempXElement != null ? BuildSubstances(tempXElement) : null;

                    tempXElement = articleXElement.Element(XName.Get(TaxonomyList));
                    articleInfo.TaxonomyList = tempXElement != null ? BuildTaxonomyList(tempXElement) : null;

                    tempXElement = articleXElement.Element(XName.Get(createdDate));
                    articleInfo.CreatedDate = tempXElement != null ? tempXElement.Value : null;

                    tempXElement = articleXElement.Element(XName.Get(updatedDate));
                    articleInfo.UpdatedDate = tempXElement != null ? tempXElement.Value : null;

                    tempXElement = articleXElement.Element(XName.Get(createdDate));
                    articleInfo.CreatedDate = tempXElement != null ? tempXElement.Value : null;

                    statusCodeXElement = articleXElement.Element(XName.Get(statuscode));
                    if (statusCodeXElement != null)
                    {
                        int statusCode = 0;
                        int.TryParse(statusCodeXElement.Value, out statusCode);
                        articleInfo.statuscode = statusCode;
                    }
                    else
                    {
                        articleInfo.statuscode = 0;
                    }

                    tempXElement = articleXElement.Element(XName.Get(status));
                    articleInfo.status = tempXElement != null ? tempXElement.Value : null;

                    tempXElement = articleXElement.Element(XName.Get(Components));
                    articleInfo.Components = tempXElement != null ? BuildComponents(tempXElement) : null;
                    // NPU specific elements
                    tempXElement = articleXElement.Element(XName.Get(NaturalProducts));
                    if (tempXElement != null)
                    {
                        articleInfo.NaturalProducts = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(NaturalProduct));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.NaturalProducts.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(BiologicalActivities));
                    if (tempXElement != null)
                    {
                        articleInfo.BiologicalActivities = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(BiologicalActivity));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.BiologicalActivities.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(NonPlantSources));
                    if (tempXElement != null)
                    {
                        articleInfo.NonPlantSources = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(NonPlantSource));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.NonPlantSources.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                    tempXElement = articleXElement.Element(XName.Get(CompoundClasses));
                    if (tempXElement != null)
                    {
                        articleInfo.CompoundClasses = new List<string>();
                        tempXElemets = tempXElement.Elements(XName.Get(CompoundClass));
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (var xElement in tempXElemets)
                            {
                                if (!string.IsNullOrWhiteSpace(xElement.Value))
                                {
                                    articleInfo.CompoundClasses.Add(xElement.Value);
                                }
                            }
                        }
                        tempXElement = null; tempXElemets = null;
                    }
                }
            }
            finally
            {
                systemidXElement = null;
                doiXElement = null;
                pricecodeXElement = null;
                titleXElement = null;
                shorttitleXElement = null;
                descriptionXElement = null;
                subjectsXElement = null;
                subjectXElements = null;
                articletypeXElement = null;
                startdateXElement = null;
                enddateXElement = null;
                authorsXElement = null;
                authorXElements = null;
                firstnameXElement = null;
                lastnameXElement = null;
                journalXElement = null;
                abbrtitleXElement = null;
                journalidXElement = null;
                sercodeXElement = null;
                issnprintXElement = null;
                issnonlineXElement = null;
                publisherXElement = null;
                publisheridXElement = null;
                publishernameXElement = null;
                publishercodeXElement = null;
                publisheruriXElement = null;
                subsyearXElement = null;
                journalvolumeXElement = null;
                journalissueidXElement = null;
                journalissuenoXElement = null;
                licenseXElement = null;
                licenseidXElement = null;
                licensenameXElement = null;
                licenseabbriviatednameXElement = null;
                licenselinkXElement = null;
                firstpageXElement = null;
                lastpageXElement = null;
                linksXElement = null;
                linkXElements = null;
                accepteddateXElement = null;
                publisheddateXElement = null;
                recieveddateXElement = null;
                submissiondateXElement = null;
                noofcitationsXElement = null;
                searchTextXElements = null;
                searchTitleXElement = null;
                sourceXElement = null;
                tempXElement = null;
                institutionXElement = null;
                infoXElement = null;
                tempXElemets = null;
            }

            return articleInfo;
        }
        private Image BuildImageData(XElement imageXElement)
        {
            Image image = null;
            if (imageXElement != null)
            {
                image = new Image();
                XElement tempXElement = imageXElement.Element(ID);
                image.ID = tempXElement != null ? tempXElement.Value : string.Empty;
                tempXElement = imageXElement.Element(CDX);
                image.Cdx = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(TIF);
                image.Tif = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(MOL);
                image.Mol = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);

                tempXElement = null;
                imageXElement = null;
            }
            return image;
        }
        private List<ML.Component> BuildComponents(XElement componentsXElement)
        {
            List<ML.Component> components = null;
            IEnumerable<XElement> componentXElements = componentsXElement.Elements(XName.Get(Component));
            XElement tempXElement = null;
            if (componentXElements != null && componentXElements.Any())
            {
                foreach (XElement componentXElement in componentXElements)
                {
                    ML.Component component = new ML.Component();
                    tempXElement = componentXElement.Element(XName.Get(title));
                    component.Title = tempXElement != null ? tempXElement.Value : string.Empty;
                    tempXElement = componentXElement.Element(XName.Get(description));
                    component.Description = tempXElement != null ? tempXElement.Value : string.Empty;
                    components.Add(component);
                }
            }
            return components;

        }
        private List<string> BuildTrivialNames(XElement trivialNamesXElement)
        {
            List<string> trivialNames = null;
            if (trivialNamesXElement != null)
            {
                trivialNames = new List<string>();

                foreach (XElement element in trivialNamesXElement.Elements())
                {
                    if (element != null)
                    {
                        if (trivialNames == null)
                        {
                            trivialNames = new List<string>();
                        }
                        trivialNames.Add(element.Value);
                    }
                }

            }
            return trivialNames;
        }
        private List<string> BuildLinkArticles(XElement linkArticlesXElement)
        {
            List<string> linkArticles = null;
            if (linkArticlesXElement != null)
            {
                linkArticles = new List<string>();

                IEnumerable<XElement> linkElements = linkArticlesXElement.Elements(XName.Get(Id));
                foreach (XElement element in linkElements)
                {
                    if (element != null)
                    {
                        if (linkArticles == null)
                        {
                            linkArticles = new List<string>();
                        }
                        linkArticles.Add(element.Value);
                    }
                }
                linkElements = null;
            }
            return linkArticles;
        }
        private List<string> BuildKeywords(XElement keywordsXElement)
        {
            List<string> keywords = null;
            if (keywordsXElement != null)
            {
                keywords = new List<string>();

                foreach (XElement element in keywordsXElement.Elements())
                {
                    if (element != null)
                    {
                        if (keywords == null)
                        {
                            keywords = new List<string>();
                        }
                        keywords.Add(element.Value);
                    }
                }

            }
            return keywords;
        }
        private List<Substance> BuildSubstances(XElement substancesXElement)
        {
            List<Substance> substances = null;
            IEnumerable<XElement> substanceXElements = null;
            try
            {
                if (substancesXElement != null)
                {
                    substanceXElements = substancesXElement.Elements(XName.Get("Substance"));
                    if (substanceXElements != null && substanceXElements.Count() > 0)
                    {
                        substances = new List<Substance>();
                        int index = 1;
                        Substance substance = null;
                        SubstanceHelper helper = null;
                        foreach (XElement element in substanceXElements)
                        {
                            if (element != null)
                            {
                                int currentIndex = index;
                                substance = new Substance();
                                helper = new SubstanceHelper(element.ToString());
                                substance = helper.GetSubstance();
                                helper = null;
                                if (substance != null)
                                {
                                    substance.Index = currentIndex;
                                    substances.Add(substance);
                                }
                            }
                            index++;
                        }
                    }
                }
            }
            finally
            {
                substanceXElements = null;
            }
            return substances;
        }
        private List<Taxonomy> BuildTaxonomyList(XElement taxonomyXElement)
        {
            List<Taxonomy> taxonomies = null;
            if (taxonomyXElement != null)
            {
                IEnumerable<XElement> taxonomyXElements = taxonomyXElement.Elements(XName.Get(Taxonomy));

                if (taxonomyXElements != null && taxonomyXElements.Count() > 0)
                {
                    taxonomies = new List<Taxonomy>();
                    foreach (XElement element in taxonomyXElements)
                    {
                        Taxonomy taxonomy = new Taxonomy();
                        XElement texonomyTempXElement = null;

                        texonomyTempXElement = element.Element(XName.Get(IsSourceOfCompound));
                        taxonomy.IsSourceOfCompound = texonomyTempXElement != null ? Convert.ToBoolean(texonomyTempXElement.Value) : false;
                        texonomyTempXElement = element.Element(XName.Get(Phylum));
                        taxonomy.Phylum = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Class));
                        taxonomy.Class = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Order));
                        taxonomy.Order = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Family));
                        taxonomy.Family = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Genus));
                        taxonomy.Genus = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Species));
                        taxonomy.Species = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;
                        taxonomies.Add(taxonomy);
                    }
                }
                taxonomyXElements = null;
            }

            return taxonomies;
        }
        private NameValue BuildTaxonomyType(XElement taxonomyXElement)
        {
            NameValue nameValue = null;
            XElement taxonomyTempSubXElement = null;
            try
            {
                if (taxonomyXElement != null)
                {
                    nameValue = new NameValue();


                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Name));
                    nameValue.Name = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;

                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Value));
                    nameValue.Value = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;
                }
            }
            finally
            {
                taxonomyXElement = null;
                taxonomyTempSubXElement = null;
            }
            return nameValue;
        }
        #endregion
    }
}
