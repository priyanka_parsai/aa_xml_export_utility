﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ML = RSC.Database.Api.Common.DataContracts.Marinlit;

namespace RSC.Database.Api.Common.Helpers.Marinlit
{
    public class MarkLogicSearchHelper
    {
        #region Private variable & constants
        private string searchNamespace = string.Empty;
        private XElement searchResultsXElement = null;
        private IEnumerable<XElement> resultXElements = null;
        private IEnumerable<XElement> facetXElements = null;

        private const string total = "total";
        private const string start = "start";
        private const string pageLength = "page-length";
        private const string uri = "uri";
        private const string publicid = "publicid";
        private const string indexAttribute = "index";
        private const string snippet = "snippet";
        private const string match = "match";
        private const string highlight = "highlight";
        private const string facetElementName = "facet";
        private const string facetvalueElementName = "facet-value";
        private const string name = "name";
        private const string count = "count";
        private const string type = "type";
        private const string result = "result";
        private const string highlightBeginTag = "<search:highlight xmlns:search=\"";
        private const string highlightEndTag = "</search:highlight>";
        private const string boldBeginTag = "<b>";
        private const string boldEndTag = "</b>";
        private const string italicBeginTag = "<i>";
        private const string italicEndTag = "</i>";
        private const string totalresults = "totalresults";
        private const string pagesize = "pagesize";
        private const string pagestart = "pagestart";
        private const string SearchResult = "SearchResult";
        private const string score = "score";
        private const string Lastupdate = "lastupdate";


        #endregion

        public MarkLogicSearchHelper(string searchResults)
        {
            searchNamespace = ConfigurationManager.AppSettings.Get(Constants.SearchNamespace);
            InitializeDocument(searchResults);
        }

        private void InitializeDocument(string searchResults)
        {

            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null && searchResultsXDocument.Root.Elements().Count() > 0)
                {
                    searchResultsXElement = searchResultsXDocument.Root;
                    resultXElements = searchResultsXElement.Elements(XName.Get(result, searchNamespace));
                    facetXElements = searchResultsXElement.Elements(XName.Get(facetElementName, searchNamespace));

                }
            }


        }

        private int GetCount()
        {
            int count = 0;
            if (resultXElements != null)
            {
                count = resultXElements.Count();
            }
            return count;
        }

        public ML.SearchResults GetResults()
        {
            ML.SearchResults searchResults = null;
            ML.Results results = null;
            List<ML.Facet> facets = null;
            if (searchResultsXElement != null)
            {
                searchResults = new ML.SearchResults();
                results = new ML.Results();
                long startValue = 0;
                long pageLengthValue = 0;
                long totalCount = 0;

                long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, total), out totalCount);
                results.totalCount = totalCount;
                if (totalCount > 0)
                {
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pageLength), out pageLengthValue);
                    if (pageLengthValue > totalCount)
                    {
                        pageLengthValue = totalCount;
                    }
                    results.pageSize = pageLengthValue;
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, start), out startValue);
                    results.pageNo = (startValue / pageLengthValue) + ((startValue % pageLengthValue) > 0 ? 1 : 0);

                    results.result = new List<ML.Result>();
                    for (int i = 0; i < resultXElements.Count(); i++)
                    {
                        results.result.Add(GetResults(i));
                    }
                }
                facets = GetFactes();
                searchResults.results = results;
                searchResults.facets = facets;
            }
            return searchResults;
        }
        /// <summary>
        /// Get Search result method
        /// </summary>
        /// <returns></returns>
        public ML.SearchResults GetSearchResults()
        {
            ML.SearchResults searchResults = null;
            ML.Results results = null;
            List<ML.Facet> facets = null;
            if (searchResultsXElement != null)
            {
                searchResults = new ML.SearchResults();
                results = new ML.Results();
                long startValue = 0;
                long pageLengthValue = 0;
                long totalCount = 0;

                long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, totalresults), out totalCount);
                results.totalCount = totalCount;
                if (totalCount > 0)
                {
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagesize), out pageLengthValue);
                    if (pageLengthValue > totalCount)
                    {
                        pageLengthValue = totalCount;
                    }
                    results.pageSize = pageLengthValue;
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagestart), out startValue);
                    results.pageNo = (startValue / pageLengthValue) + ((startValue % pageLengthValue) > 0 ? 1 : 0);

                    results.result = new List<ML.Result>();

                    resultXElements = searchResultsXElement.Elements(XName.Get(SearchResult, string.Empty));
                    for (int i = 0; i < resultXElements.Count(); i++)
                    {
                        results.result.Add(GetResults(i, score));
                    }
                }
                results.lastUpdated = CommonHelpers.GetAttributeValue(searchResultsXElement, Lastupdate);
                facets = GetFactes();
                searchResults.results = results;
                searchResults.facets = facets;
            }
            return searchResults;
        }
        private List<ML.Facet> GetFactes()
        {
            List<ML.Facet> facets = null;
            IEnumerable<XElement> facetvalueXElements = null;
            try
            {
                if (facetXElements != null && facetXElements.Count() > 0)
                {
                    facets = new List<ML.Facet>();
                    foreach (XElement facetXElement in facetXElements)
                    {
                        ML.Facet facet = new ML.Facet();
                        facet.name = CommonHelpers.GetAttributeValue(facetXElement, name);
                        facet.type = CommonHelpers.GetAttributeValue(facetXElement, type);
                        facetvalueXElements = facetXElement.Elements(XName.Get(facetvalueElementName, searchNamespace));
                        if (facetvalueXElements != null && facetvalueXElements.Count() > 0)
                        {
                            facet.facetvalue = new List<ML.FacetValue>();
                            foreach (XElement facetvalueXElement in facetvalueXElements)
                            {
                                ML.FacetValue facetValue = new ML.FacetValue();
                                facetValue.name = CommonHelpers.GetAttributeValue(facetvalueXElement, name);
                                facetValue.count = CommonHelpers.GetAttributeValue(facetvalueXElement, count);
                                facetValue.Value = facetvalueXElement.Value;
                                facet.facetvalue.Add(facetValue);
                            }
                        }
                        facets.Add(facet);
                    }
                }
            }
            finally
            {
                facetvalueXElements = null;
            }
            return facets;
        }

        private ML.Result GetResults(int index, string indexAttributeName = "index")
        {
            ML.Result result = null;

            XElement resultXElement = null;
            XElement snippetXElement = null;
            XElement matchXElement = null;
            XName snippetXName = null;
            XName matchXName = null;
            try
            {
                if (resultXElements != null && resultXElements.Count() >= index)
                {
                    result = new ML.Result();

                    resultXElement = resultXElements.ElementAt(index);
                    if (resultXElement != null)
                    {
                        if (!string.IsNullOrEmpty(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName)))
                        {
                            result.index = Convert.ToInt32(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName));
                        }
                        result.uri = CommonHelpers.GetAttributeValue(resultXElement, uri);
                        result.publicid = CommonHelpers.GetAttributeValue(resultXElement, publicid);
                        if (result.uri.Contains("data-resources"))
                        {
                            result.type = Constants.Articles;
                        }
                        else if (result.uri.Contains("chemical-substances"))
                        {
                            result.type = Constants.Substances;
                        }
                        else if (result.uri.Contains("journal-titles"))
                        {
                            result.type = Constants.Journals;
                        }
                        else if (result.uri.Contains("public-profiles"))
                        {
                            result.type = Constants.Profiles;
                        }
                        else if (result.uri.Contains("tool-services"))
                        {
                            result.type = Constants.Tools;
                        }
                        else if (result.uri.Contains("thesis-titles"))
                        {
                            result.type = Constants.Thesis;
                        }
                        else if (result.uri.Contains("chemical-data"))
                        {
                            result.type = Constants.ChemicalData;
                        }
                        else if (result.uri.Contains("chemical-reactions"))
                        {
                            result.type = Constants.Reactions;
                        }
                        else if (result.uri.Contains("chemical-spectra"))
                        {
                            result.type = Constants.Spectra;
                        }
                        snippetXName = XName.Get(snippet, searchNamespace);
                        snippetXElement = resultXElement.Element(snippetXName);
                        if (snippetXElement != null)
                        {
                            matchXName = XName.Get(match, searchNamespace);
                            matchXElement = snippetXElement.Element(matchXName);
                            result.match = CommonHelpers.GetNodeXML(matchXElement);
                            result.match.Trim();
                            result.match.Replace("\n", "");
                            result.match.Replace("\r", "");
                            result.match.Replace("\t", "");

                            result.match.Replace(highlightBeginTag + searchNamespace + "\">", boldBeginTag + italicBeginTag);
                            result.match.Replace(highlightEndTag, italicEndTag + boldEndTag);
                        }
                    }
                }
            }
            finally
            {
                resultXElement = null;
                snippetXElement = null;
                matchXElement = null;
                snippetXName = null;
                matchXName = null;
            }
            return result;
        }
    }
}
