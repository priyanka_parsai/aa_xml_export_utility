﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSC.Database.Api.Common.DataContracts;
using RSCpubs.ePlatform.Service.Common.Helpers;

namespace RSC.Database.Api.Common.Helpers
{
    public class ReactionHelper
    {
        #region Private Variables


        public XElement reactionXElement = null;
        private const string title = "title";
        private const string shortTitle = "short-title";
        private const string description = "description";
        private const string createdDate = "created-date";
        private const string updatedDate = "updated-date";
        private const string authors = "authors";
        private const string firstName = "firstname";
        private const string lastName = "lastname";
        private const string institution = "institution";
        private const string author = "author";
        private const string journal = "journal";
        private const string abbrTitle = "abbr-title";
        private const string journalVolume = "journal-volume";
        private const string journalIssueNo = "journal-issueno";
        private const string subsyear = "subsyear";
        private const string systemId = "systemid";
        private const string doi = "doi";
        private const string pubmedId = "PubmedId";
        private const string firstPage = "firstpage";
        private const string lastPage = "lastpage";
        private const string type = "type";
        private const string publicId = "publicid";
        private const string printId = "printid";
        private const string keywords = "keywords";
        private const string keyword = "keyword";
        private const string substance = "Substance";
        private const string substances = "Substances";
        private const string note = "note";
        private const string name = "Name";
        private const string value = "Value";
        private const string info = "information";
        private const string location = "location";
        private const string organizationName = "orgname";
        private const string language = "language";
        private const string FacetTree = "FacetTree";
        private const string reactions = "reactions";
        private const string reaction = "reaction";
        private const string IMAGE = "Image";
        private const string ID = "ID";
        private const string CDX = "Cdx";
        private const string TIF = "Tif";
        private const string MOL = "Mol";
        private const string FALSE = "false";
        private const string Status = "Status";
        private const string CreatedBy = "CreatedBy";
        private const string CreatedDate = "CreatedDate";
        private const string FirstPublishedBy = "FirstPublishedBy";
        private const string FirstPublishedDate = "FirstPublishedDate";
        private const string Data= "data";

        #endregion Private Static Variables

        #region Constructors

        public ReactionHelper()
        {

        }

        public ReactionHelper(string searchResults)
        {

            InitializeMainDocument(searchResults);
        }

        #endregion

        #region Public Methods

        public Reaction GetReaction()
        {
            Reaction reaction = new Reaction();

            try
            {
                if (reactionXElement != null)
                {

                    reaction = this.BuildReaction(reactionXElement);

                }
            }
            finally
            {
                reactionXElement = null;
            }

            return reaction;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null)
                {
                    reactionXElement = searchResultsXDocument.Root;
                }

            }
        }

        private Reaction BuildReaction(XElement reactionXElement)
        {
            Reaction reactionInfo = null;

            #region XnamesDeclaration
            XElement tempXElement = null;
            IEnumerable<XElement> tempXElemets = null;
            #endregion

            try
            {
                if (reactionXElement != null)
                {
                    reactionInfo = new Reaction();

                    reactionInfo.systemid = GetValueFor(reactionXElement, systemId);
                    reactionInfo.doi = GetValueFor(reactionXElement, doi);
                    reactionInfo.PubmedId = GetValueFor(reactionXElement, pubmedId);
                    reactionInfo.title = GetValueFor(reactionXElement, title, true);
                    reactionInfo.shorttitle = GetValueFor(reactionXElement, shortTitle, true);
                    reactionInfo.description = GetValueFor(reactionXElement, description, true);
                    reactionInfo.journalvolume = GetValueFor(reactionXElement, journalVolume);
                    reactionInfo.journalissueno = GetValueFor(reactionXElement, journalIssueNo);
                    reactionInfo.subsyear = GetValueFor(reactionXElement, subsyear);
                    reactionInfo.firstpage = GetValueFor(reactionXElement, firstPage);
                    reactionInfo.lastpage = GetValueFor(reactionXElement, lastPage);
                    reactionInfo.information = GetValueFor(reactionXElement, info, true);
                    reactionInfo.language = GetValueFor(reactionXElement, language);
                    reactionInfo.publicid = GetValueFor(reactionXElement, publicId);
                    reactionInfo.printid = GetValueFor(reactionXElement, printId);
                    reactionInfo.note = GetValueFor(reactionXElement, note, true);
                    reactionInfo.data = GetValueFor(reactionXElement, Data, true);

                    tempXElement = reactionXElement.Element(XName.Get(FacetTree));
                    if (tempXElement != null)
                    {
                        reactionInfo.Categories = (FacetTree)SerializeDeserializeHelper.DeserializeObject(tempXElement.ToString(), typeof(FacetTree));

                    }
                    tempXElement = null;
                    tempXElement = reactionXElement.Element(XName.Get(authors));
                    if (tempXElement != null)
                    {
                        reactionInfo.authors = new List<reactionAuthor>();
                        tempXElemets = tempXElement.Elements(author);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement authorXElement in tempXElemets)
                            {
                                if (authorXElement != null)
                                {
                                    reactionAuthor reactionAuthor = new reactionAuthor();
                                    reactionAuthor.firstname = GetValueFor(authorXElement, firstName, true);
                                    reactionAuthor.lastname = GetValueFor(authorXElement, lastName, true);

                                    reactionInfo.authors.Add(reactionAuthor);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = reactionXElement.Element(XName.Get(keywords));
                    if (tempXElement != null)
                    {
                        reactionInfo.keywords = new List<string>();
                        tempXElemets = tempXElement.Elements(keyword);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement keywordXElement in tempXElemets)
                            {
                                if (keywordXElement != null)
                                {
                                    reactionInfo.keywords.Add(keywordXElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = reactionXElement.Element(XName.Get(reactions));
                    if (tempXElement != null)
                    {
                        reactionInfo.reactions = new List<string>();
                        tempXElemets = tempXElement.Elements(reaction);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement xElement in tempXElemets)
                            {
                                if (xElement != null)
                                {
                                    reactionInfo.reactions.Add(xElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = reactionXElement.Element(XName.Get(journal));
                    if (tempXElement != null)
                    {
                        reactionInfo.journal = new journal();
                        tempXElement = tempXElement.Element(XName.Get(title));
                        if (tempXElement != null)
                        {
                            reactionInfo.journal.title = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement));
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = reactionXElement.Element(XName.Get(IMAGE));
                    reactionInfo.Image = tempXElement != null ? BuildImageData(tempXElement) : null;
                    reactionInfo.Status = GetValueFor(reactionXElement, Status);
                    reactionInfo.CreatedBy = GetValueFor(reactionXElement, CreatedBy);
                    reactionInfo.CreatedDate = GetValueFor(reactionXElement, CreatedDate);
                    reactionInfo.FirstPublishedBy = GetValueFor(reactionXElement, FirstPublishedBy);
                    reactionInfo.FirstPublishedDate = GetValueFor(reactionXElement, FirstPublishedDate);

                    tempXElement = null;
                }
            }
            finally
            {

                tempXElement = null;

                tempXElemets = null;
            }

            return reactionInfo;
        }

        private string GetValueFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                if (isEncoded)
                {
                    returnVal = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement));
                }
                else
                {
                    returnVal = tempXElement.Value;
                }
            }
            tempXElement = null;
            return returnVal;
        }

        private string GetValuesFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                if (isEncoded)
                {
                    returnVal = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement));
                }
                else
                {
                    returnVal = tempXElement.Value;
                }
            }
            tempXElement = null;
            return returnVal;
        }


        private List<string> BuildTrivialNames(XElement trivialNamesXElement)
        {
            List<string> trivialNames = null;
            if (trivialNamesXElement != null)
            {
                trivialNames = new List<string>();

                foreach (XElement element in trivialNamesXElement.Elements())
                {
                    if (element != null)
                    {
                        if (trivialNames == null)
                        {
                            trivialNames = new List<string>();
                        }
                        trivialNames.Add(element.Value);
                    }
                }

            }
            return trivialNames;
        }

        private List<string> BuildKeywords(XElement keywordsXElement)
        {
            List<string> keywords = null;
            if (keywordsXElement != null)
            {
                keywords = new List<string>();

                foreach (XElement element in keywordsXElement.Elements())
                {
                    if (element != null)
                    {
                        if (keywords == null)
                        {
                            keywords = new List<string>();
                        }
                        keywords.Add(element.Value);
                    }
                }

            }
            return keywords;
        }
        private List<Substance> BuildSubstances(XElement substancesXElement)
        {
            List<Substance> substances = null;
            IEnumerable<XElement> substanceXElements = null;
            try
            {
                if (substancesXElement != null)
                {
                    substanceXElements = substancesXElement.Elements(XName.Get("Substance"));
                    if (substanceXElements != null && substanceXElements.Count() > 0)
                    {
                        substances = new List<Substance>();
                        foreach (XElement element in substanceXElements)
                        {
                            if (element != null)
                            {
                                Substance substance = new Substance();
                                SubstanceHelper helper = new SubstanceHelper(element.ToString());
                                substance = helper.GetSubstance();
                                helper = null;
                                if (substance != null)
                                {
                                    substances.Add(substance);
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                substanceXElements = null;
            }
            return substances;
        }
        private Image BuildImageData(XElement imageXElement)
        {
            Image image = null;
            if (imageXElement != null)
            {
                image = new Image();
                XElement tempXElement = imageXElement.Element(ID);
                image.ID = tempXElement != null ? tempXElement.Value : string.Empty;
                tempXElement = imageXElement.Element(CDX);
                image.Cdx = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(TIF);
                image.Tif = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(MOL);
                image.Mol = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);

                tempXElement = null;
                imageXElement = null;
            }
            return image;
        }

        #endregion
    }
}
