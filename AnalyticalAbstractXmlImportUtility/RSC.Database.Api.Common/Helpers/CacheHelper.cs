﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace RSC.Database.Api.Common.Helpers
{
    public class CacheHelper
    {
        private static ICacheManager cacheManagerInstance;
        static CacheHelper()
        {
            string chacheName = ConfigurationManager.AppSettings.Get(Constants.CacheName);
            cacheManagerInstance = CacheFactory.GetCacheManager(chacheName);
        }

        public static object GetDataFromCache(string keyName)
        {
            Object cacheData = null;
            if (!string.IsNullOrEmpty(keyName))
            {
                if (cacheManagerInstance.Contains(keyName) == true)
                {
                    cacheData = cacheManagerInstance.GetData(keyName);
                }
            }

            return cacheData;
        }

        public static void AddDataToCache(string keyName, object data)
        {
            lock (cacheManagerInstance)
            {

                object previousCacheData = cacheManagerInstance.GetData(keyName);
                if (previousCacheData != null)
                {
                    previousCacheData = data;
                }
                else
                {
                    cacheManagerInstance.Add(keyName, data, CacheItemPriority.Normal, null, new AbsoluteTime(TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationManager.AppSettings.Get("CacheExpirationTime")))));
                }
            }
        }
    }
}
