﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSC.Database.Api.Common.Helpers
{
    public class FacetHelper
    {
        private const string NAME = "Name";
        private const string Count = "count";
        private const string Id = "id";
        private const string ReactionCategory = "reaction-category";
        private XElement facetResultsXElement = null;

        public FacetHelper()
        {

        }

        public FacetHelper(string facetResults)
        {
            InitializeMainDocument(facetResults);
        }

        private void InitializeMainDocument(string searchResults)
        {
            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument facetResultsXDocument = null;
                facetResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (facetResultsXDocument != null && facetResultsXDocument.Root != null && facetResultsXDocument.Root.Elements().Any())
                {
                    facetResultsXElement = facetResultsXDocument.Root;
                }
                facetResultsXDocument = null;
                searchResults = null;
            }


        }
        public List<FacetValue> GetFacets()
        {
            List<FacetValue> facetList = null;
            try
            {
                if (facetResultsXElement != null)
                {
                    facetList = this.BuildFacet(facetResultsXElement);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                facetResultsXElement = null;
            }

            return facetList;
        }
        private List<FacetValue> BuildFacet(XElement facetsXElement)
        {
            List<FacetValue> facets = null;
            IEnumerable<XElement> facetXElements = null;
            XElement facetXElement = null;
            try
            {
                if (facetsXElement != null)
                {
                    facets = new List<FacetValue>();
                    facetXElement = facetsXElement.Element(XName.Get(NAME));
                    facetXElements = facetsXElement.Elements(XName.Get(NAME));
                    foreach (var element in facetXElements)
                    {
                        if (element != null && !string.IsNullOrEmpty(element.Value))
                        {
                            var facetValue = new FacetValue();
                            facetValue.id = CommonHelpers.GetAttributeValue(element, Id);
                            facetValue.name = element.Value;
                            facetValue.Value = element.Value;
                            facetValue.count = CommonHelpers.GetAttributeValue(element, Count);
                            facets.Add(facetValue);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                facetXElement = null;
                facetsXElement = null;
                facetXElements = null;
            }
            return facets;
        }
    }
}
