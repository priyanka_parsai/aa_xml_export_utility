﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public class Pager
    {
        public string PageNumber { get; private set; }
        public string PageSize { get; private set; }
        public int StartIndex { get; private set; }

        public Pager(string pageNumber, string pageSize)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(pageNumber))
            {
                if (int.TryParse(pageNumber, out result))
                {
                    PageNumber = GetValidPageNumber(Convert.ToInt32(pageNumber));
                }
            }
            if (!string.IsNullOrEmpty(pageSize))
            {
                if (int.TryParse(pageSize, out result))
                {
                    PageSize = GetValidPageNumber(Convert.ToInt32(pageSize));
                }
            }
            CalculateStartIndex();
        }

        public Pager(int pageNumber, int pageSize)
        {
            PageNumber = GetValidPageNumber(pageNumber);
            PageSize = GetValidPageSize(pageSize);
            CalculateStartIndex();
        }

        private string GetValidPageNumber(int pageNumber)
        {
            string retVal = string.Empty;

            if (pageNumber <= 0)
            {
                retVal = "1";
            }
            else
            {
                retVal = Convert.ToString(pageNumber);
            }

            return retVal;
        }

        private string GetValidPageSize(int pageSize)
        {
            string retVal = string.Empty;

            if (pageSize <= 0)
            {
                retVal = string.Empty;
            }
            else
            {
                retVal = Convert.ToString(pageSize);
            }

            return retVal;
        }

        private void CalculateStartIndex()
        {
            if (!string.IsNullOrEmpty(PageNumber) && !string.IsNullOrEmpty(PageSize))
            {
                StartIndex = (Convert.ToInt32(PageNumber) - 1) * Convert.ToInt32(PageSize) + 1;
            }
        }
    }
}
