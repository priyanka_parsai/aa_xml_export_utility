﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.Helpers
{
    public class ApiAcceptHeaderValidator
    {
        private string _validHeaderRegex;
        public ApiAcceptHeaderValidator(string validHeaderRegex)
        {
            _validHeaderRegex = validHeaderRegex;
        }

        /// <summary>
        /// Checks if the header collection contains and valid accept header and returns it else returns null
        /// </summary>
        /// <param name="acceptHeaders"></param>
        /// <returns></returns>
        public ApiAcceptHeader VerifyAcceptHeaders(HttpHeaderValueCollection<MediaTypeWithQualityHeaderValue> acceptHeaders)
        {
            ApiAcceptHeader header = null;
            var match = acceptHeaders.FirstOrDefault(x => ((Regex.Match(x.MediaType, _validHeaderRegex).Success)));

            if (match != null)
            {
                header = HttpHelpers.ParseAcceptHeader(match.MediaType);
            }
            return header;
        }
    }
}
