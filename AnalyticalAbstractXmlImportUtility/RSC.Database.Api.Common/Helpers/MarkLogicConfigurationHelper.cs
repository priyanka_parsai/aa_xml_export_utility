﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace RSC.Database.Api.Common.Helpers
{
    public static class MarkLogicConfigurationHelper
    {

        public static List<MarkLogic> GetMarkLogicList()
        {
            List<MarkLogic> markLogicOptions = new List<MarkLogic>();
            ArrayList arrayList = (ArrayList)ConfigurationManager.GetSection(Constants.MarkLogicConfigurationSection);
            if (arrayList != null)
            {
                markLogicOptions = new List<MarkLogic>();
                for (int i = 0; i < arrayList.Count; i++)
                {
                    MarkLogic markLogic = new MarkLogic();
                    markLogic = (MarkLogic)arrayList[i];
                    markLogicOptions.Add(markLogic);
                }
            }
            return markLogicOptions;
        }

    }
}
