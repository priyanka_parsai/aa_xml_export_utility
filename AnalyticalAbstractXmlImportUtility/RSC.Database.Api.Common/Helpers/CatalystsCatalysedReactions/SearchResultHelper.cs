﻿using RSC.Reactions.Domain.Entities;
using CCREntities = RSC.Reactions.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSC.Database.Api.Common.Helpers.CatalystsCatalysedReactions
{
    public class SearchResultHelper<T>
    {
        #region Private variable & constants

        private string searchNamespace = string.Empty;
        private XElement searchResultsXElement = null;
        private IEnumerable<XElement> resultXElements = null;
        private IEnumerable<XElement> facetXElements = null;

        private const string total = "total";
        private const string start = "start";
        private const string pageLength = "page-length";
        private const string uri = "uri";
        private const string publicid = "publicid";
        private const string indexAttribute = "index";
        private const string highlight = "highlight";
        private const string facetElementName = "facet";
        private const string facetvalueElementName = "facet-value";
        private const string type = "type";
        private const string name = "name";
        private const string count = "count";
        private const string result = "result";
        private const string highlightBeginTag = "<search:highlight xmlns:search=\"";
        private const string highlightEndTag = "</search:highlight>";
        private const string boldBeginTag = "<b>";
        private const string boldEndTag = "</b>";
        private const string italicBeginTag = "<i>";
        private const string italicEndTag = "</i>";
        private const string totalresults = "totalresults";
        private const string pagesize = "pagesize";
        private const string pagestart = "pagestart";
        private const string SearchResult = "SearchResult";
        private const string score = "score";
        private const string Lastupdate = "lastupdate";

        #endregion

        public SearchResultHelper(string searchResults)
        {
            searchNamespace = ConfigurationManager.AppSettings.Get(Constants.SearchNamespace);
            InitializeDocument(searchResults);
        }

        private void InitializeDocument(string searchResults)
        {

            using (TextReader searchResultTextReader = new StringReader(searchResults))
            {
                XDocument searchResultsXDocument = null;
                searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                if (searchResultsXDocument.Root != null && searchResultsXDocument.Root.Elements().Count() > 0)
                {
                    searchResultsXElement = searchResultsXDocument.Root;
                    resultXElements = searchResultsXElement.Elements(XName.Get(result, searchNamespace));
                    facetXElements = searchResultsXElement.Elements(XName.Get(facetElementName, searchNamespace));

                }
            }


        }

        private int GetCount()
        {
            int count = 0;
            if (resultXElements != null)
            {
                count = resultXElements.Count();
            }
            return count;
        }

        public SearchResults<T> GetResults()
        {
            SearchResults<T> searchResults = null;
            Results<T> results = null;
            List<CCREntities.Facet> facets = null;
            if (searchResultsXElement != null)
            {
                searchResults = new SearchResults<T>();
                results = new Results<T>();
                long startValue = 0;
                long pageLengthValue = 0;
                long totalCount = 0;

                long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, total), out totalCount);
                results.TotalCount = totalCount;
                if (totalCount > 0)
                {
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pageLength), out pageLengthValue);
                    if (pageLengthValue > totalCount)
                    {
                        pageLengthValue = totalCount;
                    }
                    results.PageSize = pageLengthValue;
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, start), out startValue);
                    results.PageNo = (startValue / pageLengthValue) + ((startValue % pageLengthValue) > 0 ? 1 : 0);

                    results.Result = new List<Result<T>>();
                    for (int i = 0; i < resultXElements.Count(); i++)
                    {
                        results.Result.Add(GetResults(i));
                    }
                }
                facets = GetFactes();
                searchResults.Results = results;
                searchResults.Facets = facets;
            }
            return searchResults;
        }
        /// <summary>
        /// Get Search result method
        /// </summary>
        /// <returns></returns>
        public SearchResults<T> GetSearchResults()
        {
            SearchResults<T> searchResults = null;
            Results<T> results = null;
            List<CCREntities.Facet> facets = null;
            if (searchResultsXElement != null)
            {
                searchResults = new SearchResults<T>();
                results = new Results<T>();
                long startValue = 0;
                long pageLengthValue = 0;
                long totalCount = 0;

                long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, totalresults), out totalCount);
                results.TotalCount = totalCount;
                if (totalCount > 0)
                {
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagesize), out pageLengthValue);
                    if (pageLengthValue > totalCount)
                    {
                        pageLengthValue = totalCount;
                    }
                    results.PageSize = pageLengthValue;
                    long.TryParse(CommonHelpers.GetAttributeValue(searchResultsXElement, pagestart), out startValue);
                    results.PageNo = (startValue / pageLengthValue) + ((startValue % pageLengthValue) > 0 ? 1 : 0);

                    results.Result = new List<Result<T>>();

                    resultXElements = searchResultsXElement.Elements(XName.Get(SearchResult, string.Empty));
                    for (int i = 0; i < resultXElements.Count(); i++)
                    {
                        results.Result.Add(GetResults(i, score));
                    }
                }
                results.LastUpdated = CommonHelpers.GetAttributeValue(searchResultsXElement, Lastupdate);
                facets = GetFactes();
                searchResults.Results = results;
                searchResults.Facets = facets;
            }
            return searchResults;
        }
        private List<CCREntities.Facet> GetFactes()
        {
            List<CCREntities.Facet> facets = null;
            IEnumerable<XElement> facetvalueXElements = null;
            try
            {
                if (facetXElements != null && facetXElements.Count() > 0)
                {
                    facets = new List<CCREntities.Facet>();
                    foreach (XElement facetXElement in facetXElements)
                    {
                        CCREntities.Facet facet = new CCREntities.Facet();
                        facet.Name = CommonHelpers.GetAttributeValue(facetXElement, name);
                        facet.Type = CommonHelpers.GetAttributeValue(facetXElement, type);
                        facetvalueXElements = facetXElement.Elements(XName.Get(facetvalueElementName, searchNamespace));
                        if (facetvalueXElements != null && facetvalueXElements.Count() > 0)
                        {
                            facet.FacetValues = new List<CCREntities.FacetValue>();
                            foreach (XElement facetvalueXElement in facetvalueXElements)
                            {
                                CCREntities.FacetValue facetValue = new CCREntities.FacetValue();
                                facetValue.name = CommonHelpers.GetAttributeValue(facetvalueXElement, name);
                                facetValue.count = CommonHelpers.GetAttributeValue(facetvalueXElement, count);
                                facetValue.Value = facetvalueXElement.Value;
                                facet.FacetValues.Add(facetValue);
                            }
                        }
                        facets.Add(facet);
                    }
                }
            }
            finally
            {
                facetvalueXElements = null;
            }
            return facets;
        }

        private Result<T> GetResults(int index, string indexAttributeName = "index")
        {
            Result<T> result = null;
            XElement resultXElement = null;
            try
            {
                if (resultXElements != null && resultXElements.Count() >= index)
                {
                    result = new Result<T>();

                    resultXElement = resultXElements.ElementAt(index);
                    if (resultXElement != null)
                    {
                        if (!string.IsNullOrEmpty(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName)))
                        {
                            result.Index = Convert.ToInt32(CommonHelpers.GetAttributeValue(resultXElement, indexAttributeName));
                        }
                        result.Uri = CommonHelpers.GetAttributeValue(resultXElement, uri);
                        result.PublicId = CommonHelpers.GetAttributeValue(resultXElement, publicid);
                    }
                }
            }
            finally
            {
                resultXElement = null;
            }
            return result;
        }
    }

}
