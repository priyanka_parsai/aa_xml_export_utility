﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Net.Http;
using RSC.Database.Api.Common.DataContracts;
using Common.Logging;
using RSC.Database.Api.Common.Exceptions;

namespace RSC.Database.Api.Common.Helpers
{
    public class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        private static ILog logger = null;

        static ExceptionHandlingAttribute()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            logger.Error(actionExecutedContext.Exception.Message, actionExecutedContext.Exception);

            RscApiException rscApiException = actionExecutedContext.Exception as RscApiException;
            ErrorCodes statusCode = ErrorCodes.Internal_Server_Error;

            if (actionExecutedContext.Exception != null)
            {
                Error error = null;

                if (rscApiException != null)
                {
                    statusCode = rscApiException.ErrorCode;
                    error = new Error { Code = (int)statusCode, Name = rscApiException.Message };
                }
                else
                {
                    if (actionExecutedContext.Exception.Message.ToLower().Contains("resource not found"))
                    {
                        statusCode = ErrorCodes.Not_Found;
                        error = new Error { Code = (int)statusCode, Name = Constants.ResourceNotFound };
                    }
                    else
                    {
                        error = new Error { Code = (int)statusCode, Name = Constants.InternalServerError };
                    }
                }

                Errors response = new Errors();
                if (response.ErrorList == null)
                {
                    response.ErrorList = new List<Error>();
                }
                response.ErrorList.Add(error);
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse((HttpStatusCode)statusCode, response);
            }

            base.OnException(actionExecutedContext);
        }
    }
}
