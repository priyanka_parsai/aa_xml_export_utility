﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public class HttpRequestHelper
    {
        public static string HttpRequest(string url, string method, NetworkCredential networkCredential=null,string data = "")
        {
            Stream newStream = null;
            Stream dataStream = null;
            StreamReader reader = null;
            HttpWebResponse response = null;
            byte[] byteArray = null;
            ASCIIEncoding encoding = null;
            string responseString = string.Empty;
            HttpWebRequest request = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);

                request.Timeout = 600000;
                request.ReadWriteTimeout = 600000;
                request.KeepAlive = false;

                if (networkCredential != null)
                {
                    request.Credentials = CredentialCache.DefaultCredentials;
                    request.Credentials = networkCredential;
                }
                request.Method = !string.IsNullOrEmpty(method) ? method : Constants.GET; 
                request.ContentType = Constants.XMLAcceptHeader;
                switch (request.Method.ToUpper())
                {
                    case Constants.GET:
                        break;
                    case Constants.POST:
                    case Constants.PUT:
                        if (!string.IsNullOrEmpty(data))
                        {
                            encoding = new ASCIIEncoding();
                            byteArray = encoding.GetBytes(data);
                            // Set the content length of the string being posted.
                            request.ContentLength = byteArray.Length;

                            //write the content to the stream
                            newStream = request.GetRequestStream();
                            newStream.Write(byteArray, 0, byteArray.Length);
                        }
                        break;
                    case Constants.DELETE:
                        break;
                }

                response = (HttpWebResponse)request.GetResponse();
                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();

                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);

                // Read the content.
                responseString = reader.ReadToEnd();

                if (request != null)
                {
                    request.Abort();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Cleanup the streams and the response.
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                    dataStream = null;
                }
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
                if (newStream != null)
                {
                    newStream.Close();
                    newStream.Dispose();
                    newStream = null;
                }

                byteArray = null;
                data = null;

                encoding = null;
            }
            return responseString;
        }
    }
}
