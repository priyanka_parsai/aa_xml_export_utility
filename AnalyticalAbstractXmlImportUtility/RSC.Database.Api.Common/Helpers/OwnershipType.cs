﻿namespace RSC.Database.Api.Common.Helpers
{
    public enum OwnershipType
    {
       
        all = 1,
        individual,
        organisation,
        group,
        community,
        none
    }
}
