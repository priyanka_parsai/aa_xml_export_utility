﻿using System.Net;
using RSC.Database.Api.Common.DataContracts.NPU;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSC.Database.Api.Common.DataContracts;
using System.Collections.Generic;
using System;
using RSCpubs.ePlatform.Service.Common.Helpers;

namespace RSC.Database.Api.Common.Helpers.NPU
{
    public class ArticleHelper
    {
        private XElement _articleXElement;

        private const string Authors = "Authors";
        private const string Author = "Author";
        private const string CreatedDate = "CreatedDate";
        private const string Description = "Description";
        private const string Note = "Note";
        private const string Doi = "Doi";
        private const string FirstPage = "FirstPage";
        private const string Issue = "Issue";
        private const string Journal = "Journal";
        private const string LastPage = "LastPage";
        private const string ModifiedBy = "ModifiedBy";
        private const string ModifiedDate = "ModifiedDate";
        private const string PrintId = "PrintId";
        private const string PublishedBy = "PublishedBy";
        private const string PublicId = "PublicId";
        private const string PubmedId = "PubmedId";
        private const string Language = "Language";
        private const string PublishedDate = "PublishedDate";
        private const string ShortTitle = "ShortTitle";
        private const string Status = "Status";
        private const string SystemId = "SystemId";
        private const string Image = "Image";
        private const string ID = "ID";
        private const string Cdx = "Cdx";
        private const string Tif = "Tif";
        private const string Mol = "Mol";
        private const string FALSE = "false";
        private const string Title = "Title";
        private const string Volume = "Volume";
        private const string Year = "Year";
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string BiologicalActivities = "BiologicalActivities";
        private const string BiologicalActivity = "BiologicalActivity";
        private const string NonPlantSources = "NonPlantSources";
        private const string NonPlantSource = "NonPlantSource";
        private const string CompoundClasses = "CompoundClasses";
        private const string CompoundClass = "CompoundClass";
        private const string NaturalProducts = "NaturalProducts";
        private const string NaturalProduct = "NaturalProduct";
        private const string CreatedBy = "CreatedBy";
        private const string FirstPublishedBy = "FirstPublishedBy";
        private const string FirstPublishedDate = "FirstPublishedDate";
        private const string TaxonomyList = "TaxonomyList";
        private const string Taxonomy = "Taxonomy";
        private const string Phylum = "Phylum";
        private const string Class = "Class";
        private const string Order = "Order";
        private const string Family = "Family";
        private const string Genus = "Genus";
        private const string Species = "Species";
        private const string Name = "Name";
        private const string Value = "Value";
        private const string IsSourceOfCompound = "IsSourceOfCompound";
        private const string FacetTree = "FacetTree";
        private const string Info = "Info";
        private const string Keywords = "Keywords";
        private const string Keyword = "Keyword";

        public ArticleHelper(string articleResults)
        {
            InitializeMainDocument(articleResults);
        }

        private void InitializeMainDocument(string articleResults)
        {
            using (TextReader articleResultTextReader = new StringReader(articleResults))
            {
                var articleResultsXDocument = XDocument.Load(articleResultTextReader, LoadOptions.PreserveWhitespace);
                if (articleResultsXDocument.Root != null)
                {
                    _articleXElement = articleResultsXDocument.Root;
                }
            }
        }

        public Article GetArticle()
        {
            var article = new Article();
            try
            {
                if (_articleXElement != null)
                {
                    article = this.BuildArticle(_articleXElement);
                }
            }
            finally
            {
                _articleXElement = null;
            }

            return article;
        }

        private Article BuildArticle(XElement articleXElement)
        {
            Article articleInfo = null;
            try
            {
                if (articleXElement != null)
                {
                    articleInfo = new Article();
                    articleInfo.SystemId = GetValueFor(articleXElement, SystemId);
                    articleInfo.PublicId = GetValueFor(articleXElement, PublicId);
                    articleInfo.PrintId = GetValueFor(articleXElement, PrintId);
                    articleInfo.PubmedId = GetValueFor(articleXElement, PubmedId);
                    articleInfo.Title = GetValueFor(articleXElement, Title, true);
                    articleInfo.ShortTitle = GetValueFor(articleXElement, ShortTitle, true);
                    articleInfo.Description = GetValueFor(articleXElement, Description, true);
                    articleInfo.Note = GetValueFor(articleXElement, Note, true);
                    articleInfo.Journal = GetValueFor(articleXElement, Journal);
                    articleInfo.Year = GetValueFor(articleXElement, Year);
                    articleInfo.Volume = GetValueFor(articleXElement, Volume);
                    articleInfo.Issue = GetValueFor(articleXElement, Issue);
                    articleInfo.FirstPage = GetValueFor(articleXElement, FirstPage);
                    articleInfo.LastPage = GetValueFor(articleXElement, LastPage);
                    articleInfo.Doi = GetValueFor(articleXElement, Doi);
                    articleInfo.Language = GetValueFor(articleXElement, Language);
                    articleInfo.Info = GetValueFor(articleXElement, Info, true);
                    // Bind Authors
                    articleInfo.Authors = BindAuthorsData(articleXElement);
                    articleInfo.BiologicalActivities = GetMultipleValueFor(articleXElement, BiologicalActivities, BiologicalActivity, true);
                    articleInfo.NaturalProducts = GetMultipleValueFor(articleXElement, NaturalProducts, NaturalProduct, true);
                    articleInfo.CompoundClasses = GetMultipleValueFor(articleXElement, CompoundClasses, CompoundClass, true);
                    articleInfo.NonPlantSources = GetMultipleValueFor(articleXElement, NonPlantSources, NonPlantSource, true);
                    articleInfo.Keywords = GetMultipleValueFor(articleXElement, Keywords, Keyword, true);
                    // Bind Image
                    articleInfo.Image = BindImageData(articleXElement);
                    articleInfo.Status = GetValueFor(articleXElement, Status);
                    articleInfo.CreatedBy = GetValueFor(articleXElement, CreatedBy);
                    articleInfo.CreatedDate= GetValueFor(articleXElement, CreatedDate);
                    articleInfo.ModifiedBy = GetValueFor(articleXElement, ModifiedBy);
                    articleInfo.PublishedBy = GetValueFor(articleXElement, FirstPublishedBy);
                    articleInfo.PublishedDate = GetValueFor(articleXElement, FirstPublishedDate);
                    // Bind Taxonomy
                    articleInfo.TaxonomyList = BindTaxonomyData(articleXElement);
                    var tempXElement = articleXElement.Element(XName.Get(FacetTree));
                    if (tempXElement != null)
                    {
                        articleInfo.ArticleType = (FacetTree)SerializeDeserializeHelper.DeserializeObject(tempXElement.ToString(), typeof(FacetTree));
                        tempXElement = null;
                    }

                }
            }
            catch
            {
                throw;
            }
            return articleInfo;
        }

        private List<Author> BindAuthorsData(XElement articleXElement)
        {
            var tempXElement = articleXElement.Element(XName.Get(Authors));
            List<Author> authorsList = null;
            if (tempXElement != null)
            {
                authorsList = new List<Author>();
                var tempXElementAuthors = tempXElement.Elements(Author);
                if (tempXElementAuthors != null && tempXElementAuthors.Any())
                {
                    foreach (XElement authorXElement in tempXElementAuthors)
                    {
                        if (authorXElement != null)
                        {
                            XElement _tempXElement = null;
                            Author author = new Author();
                            _tempXElement = authorXElement.Element(FirstName);
                            if (_tempXElement != null)
                            {
                                author.FirstName = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(_tempXElement));
                            }
                            _tempXElement = authorXElement.Element(LastName);
                            if (_tempXElement != null)
                            {
                                author.LastName = WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(_tempXElement));
                            }
                            _tempXElement = null;
                            authorsList.Add(author);
                        }
                    }
                }
                tempXElementAuthors = null;
            }
            tempXElement = null;
            return authorsList;
        }

        private Image BindImageData(XElement articleXElement)
        {
            Image image = null;
            var tempXElement = articleXElement.Element(XName.Get(Image));
            if (tempXElement != null)
            {
                image = new Image();
                XElement tempXImageElement = tempXElement.Element(ID);
                image.ID = tempXImageElement != null ? tempXImageElement.Value : string.Empty;
                tempXImageElement = tempXElement.Element(Cdx);
                image.Cdx = Convert.ToBoolean(tempXImageElement != null && !string.IsNullOrEmpty(tempXImageElement.Value) ? tempXImageElement.Value : FALSE);
                tempXImageElement = tempXElement.Element(Tif);
                image.Tif = Convert.ToBoolean(tempXImageElement != null && !string.IsNullOrEmpty(tempXImageElement.Value) ? tempXImageElement.Value : FALSE);
                tempXImageElement = tempXElement.Element(Mol);
                image.Mol = Convert.ToBoolean(tempXImageElement != null && !string.IsNullOrEmpty(tempXImageElement.Value) ? tempXImageElement.Value : FALSE);

                tempXElement = null;
                tempXImageElement = null;
            }
            return image;
        }

        private List<Taxonomy> BindTaxonomyData(XElement articleXElement)
        {
            List<Taxonomy> taxonomies = null;
            var taxonomyXElement = articleXElement.Element(XName.Get(TaxonomyList));
            if (taxonomyXElement != null)
            {
                IEnumerable<XElement> taxonomyXElements = taxonomyXElement.Elements(XName.Get(Taxonomy));
                if (taxonomyXElements != null && taxonomyXElements.Count() > 0)
                {
                    taxonomies = new List<Taxonomy>();
                    foreach (XElement element in taxonomyXElements)
                    {
                        Taxonomy taxonomy = new Taxonomy();
                        XElement texonomyTempXElement = null;

                        texonomyTempXElement = element.Element(XName.Get(IsSourceOfCompound));
                        taxonomy.IsSourceOfCompound = texonomyTempXElement != null ? Convert.ToBoolean(texonomyTempXElement.Value) : false;
                        texonomyTempXElement = element.Element(XName.Get(Phylum));
                        taxonomy.Phylum = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Class));
                        taxonomy.Class = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Order));
                        taxonomy.Order = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Family));
                        taxonomy.Family = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Genus));
                        taxonomy.Genus = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Species));
                        taxonomy.Species = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;
                        taxonomies.Add(taxonomy);
                        texonomyTempXElement = null;
                    }
                }
                taxonomyXElements = null;
            }
            taxonomyXElement = null;
            return taxonomies;
        }
        private NameValue BuildTaxonomyType(XElement taxonomyXElement)
        {
            NameValue nameValue = null;
            XElement taxonomyTempSubXElement = null;
            try
            {
                if (taxonomyXElement != null)
                {
                    nameValue = new NameValue();


                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Name));
                    nameValue.Name = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;

                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Value));
                    nameValue.Value = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;
                }
            }
            finally
            {
                taxonomyXElement = null;
                taxonomyTempSubXElement = null;
            }
            return nameValue;
        }

        private string GetValueFor(XElement xelement, string name, bool isEncoded = false)
        {
            var returnVal = string.Empty;
            var tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {

                returnVal = isEncoded ? WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement)) : tempXElement.Value;
            }
            tempXElement = null;
            return returnVal;
        }

        private List<string> GetMultipleValueFor(XElement xelement, string name, string subName, bool isEncoded = false)
        {
            List<string> returnVal = null;
            var tempXElement = xelement.Element(XName.Get(name));
            if (tempXElement != null)
            {
                var tempXElements = tempXElement.Elements(subName);
                if (tempXElements != null && tempXElements.Count() > 0)
                {
                    returnVal = new List<string>();
                    foreach (XElement element in tempXElements)
                    {
                        returnVal.Add(isEncoded ? WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(element)) : element.Value);
                    }
                }
                tempXElements = null;
            }
            tempXElement = null;
            return returnVal;
        }
    }
}