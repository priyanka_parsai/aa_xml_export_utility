﻿using System;
using System.Configuration;
using System.IO;
using System.Security;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using System.Text.RegularExpressions;

namespace RSC.Database.Api.Common.Helpers
{
    public class TransformationHelper
    {
        private const string Ampersand = "&amp;";

        private static ICacheManager cacheManagerInstance;

        public TransformationHelper()
        {
            string cacheName = ConfigurationManager.AppSettings.Get(Constants.CacheName);
            cacheManagerInstance = CacheFactory.GetCacheManager(cacheName);
        }

        #region PublicMethods
        public string Transform(string xmlValue, string key, Stream xslStream, bool isEncodingReuqired = true, bool isDTDSettingsRequired = false)
        {
            string transformmedXmlValue = string.Empty;

            try
            {
                if (xslStream != null)
                {
                    using (XmlReader xslReader = XmlReader.Create(xslStream))
                    {
                        if (isEncodingReuqired)
                        {
                            xmlValue = EncodeXML(xmlValue);
                        }
                        using (StringReader stringReader = new StringReader(xmlValue))
                        {
                            XmlReader xmlReader = null;
                            xmlReader = CreateXmlReader(isDTDSettingsRequired, stringReader, xmlReader);
                            using (xmlReader)
                            {
                                XslCompiledTransform transform = null;
                                lock (cacheManagerInstance)
                                {

                                    transform = cacheManagerInstance.GetData(key) as XslCompiledTransform;
                                    if (transform == null)
                                    {
                                        transform = new XslCompiledTransform();

                                        PermissionSet myPermissions = new PermissionSet(System.Security.Permissions.PermissionState.Unrestricted);
                                        XmlResolver resolver = new XmlSecureResolver(new XmlUrlResolver(), myPermissions);

                                        transform.Load(xslReader, XsltSettings.TrustedXslt, resolver);

                                        cacheManagerInstance.Add(key, transform, CacheItemPriority.Normal, null, new AbsoluteTime(TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationManager.AppSettings.Get("CacheExpirationTime")))));
                                    }
                                }
                                transformmedXmlValue = TransformMemoryStream(transformmedXmlValue, xmlReader, transform);
                            }
                        }

                    }
                }
            }
            finally
            {
            }
            return transformmedXmlValue;
        }

        /// <summary>
        /// To be used for the Mutiple XSLT Files are embedded in XSLT
        /// </summary>
        /// <param name="xmlValue"></param>
        /// <param name="xslName"></param>
        /// <returns></returns>
        public string TransformWithXMLResolver(string xmlValue, string xslName)
        {
            string transformmedXmlValue = string.Empty;

            try
            {
                using (StringReader stringReader = new StringReader(xmlValue))
                {
                    XmlReader xmlReader = null;
                    xmlReader = XmlReader.Create(stringReader);
                    XslCompiledTransform transform = null;

                    lock (cacheManagerInstance)
                    {
                        transform = cacheManagerInstance.GetData(xslName) as XslCompiledTransform;
                        if (transform == null)
                        {
                            transform = new XslCompiledTransform();
                            EmbeddedResourceResolver resolver = new EmbeddedResourceResolver();
                            transform.Load(xslName, XsltSettings.TrustedXslt, resolver);
                            cacheManagerInstance.Add(xslName, transform, CacheItemPriority.Normal, null, new AbsoluteTime(TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationManager.AppSettings.Get("CacheExpirationTime")))));
                        }
                    }
                    transformmedXmlValue = TransformMemoryStream(transformmedXmlValue, xmlReader, transform);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
            return transformmedXmlValue;
        }
        #endregion PublicMethods

        #region PrivateMethods
        /// <summary>
        /// Create XmlReader
        /// </summary>
        /// <param name="isDTDSettingsRequired"></param>
        /// <param name="stringReader"></param>
        /// <param name="xmlReader"></param>
        /// <returns></returns>
        private static XmlReader CreateXmlReader(bool isDTDSettingsRequired, StringReader stringReader, XmlReader xmlReader)
        {
            if (isDTDSettingsRequired)
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                xmlReader = XmlReader.Create(stringReader, settings);
            }
            else
            {
                xmlReader = XmlReader.Create(stringReader);
            }
            return xmlReader;
        }

        /// <summary>
        /// Encode XML
        /// </summary>
        /// <param name="xmlValue"></param>
        /// <returns></returns>
        private static string EncodeXML(string xmlValue)
        {
            //Encode & to &amp; 
            Regex ampersandRegex = new Regex("&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)");
            // requestData = requestData.Replace("&", "&amp;");
            xmlValue = ampersandRegex.Replace(xmlValue, Ampersand);
            //xmlValue = xmlValue.Replace("&", "&amp;");

            //Replace dummy string #lessthan# to &lt; , required to save in marklogic
            if (xmlValue.IndexOf(Constants.LessThanPlaceholder) >= 0)
            {
                xmlValue = xmlValue.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan);
            }
            return xmlValue;
        }

        /// <summary>
        /// Transform Memory Stream
        /// </summary>
        /// <param name="transformmedXmlValue"></param>
        /// <param name="xmlReader"></param>
        /// <param name="transform"></param>
        /// <returns></returns>
        private static string TransformMemoryStream(string transformmedXmlValue, XmlReader xmlReader, XslCompiledTransform transform)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (StreamWriter memoryWriter = new StreamWriter(memoryStream, new UTF8Encoding(false)))
                {
                    transform.Transform(xmlReader, null, memoryWriter);

                    using (StreamReader memoryReader = new StreamReader(memoryStream))
                    {
                        memoryStream.Seek(0, SeekOrigin.Begin);   // reset file pointer
                        transformmedXmlValue = memoryReader.ReadToEnd();
                    }
                }
            }
            return transformmedXmlValue;
        }

        #endregion PrivateMethods
    }
}