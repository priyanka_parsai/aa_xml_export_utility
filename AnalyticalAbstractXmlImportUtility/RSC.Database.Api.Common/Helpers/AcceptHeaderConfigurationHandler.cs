﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RSC.Database.Api.Common.Helpers
{
    public class AcceptHeaderConfigurationHandler : IConfigurationSectionHandler
    {
        #region IConfigurationSectionHandler Members
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            ArrayList AcceptHeaderList = new ArrayList();

            foreach (XmlNode siteRef in section.ChildNodes)
            {
                AcceptHeaderList.Add(new AcceptHeader(
                    siteRef.Attributes["value"] == null ? string.Empty : siteRef.Attributes["value"].Value,
                    siteRef.Attributes["type"] == null ? string.Empty : siteRef.Attributes["type"].Value
                       ));
            }
            return AcceptHeaderList;
        }

        #endregion
    }
}
