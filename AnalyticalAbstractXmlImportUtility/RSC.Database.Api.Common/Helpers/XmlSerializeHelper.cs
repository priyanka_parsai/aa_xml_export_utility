﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.Helpers
{
    public static class XMLSerializeHelper
    {
        public static string SerializeObject(object obj, Type type)
        {
            string XmlizedString = null;
            MemoryStream memoryStream = new MemoryStream();

            XmlSerializer xs = new XmlSerializer(type);
            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8))
            {
                xs.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
            }

            return XmlizedString;
        }

        private static string UTF8ByteArrayToString(byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        public static string ConvertToXmlCharacterReference(string xml)
        {
            StringBuilder stringBuilder = new StringBuilder(xml.Length);
            string encodedXML = string.Empty;
            const char SP = '\u0020'; // anything lower than SP is a control character
            const char DEL = '\u007F'; // anything above DEL isn't ASCII, per se.

            foreach (char ch in xml)
            {
                bool isPrintableAscii = ch >= SP && ch <= DEL;

                if (isPrintableAscii) { stringBuilder.Append(ch); }
                else { stringBuilder.AppendFormat("&#x{0:X4};", (int)ch); }

            }

            encodedXML = stringBuilder.ToString();
            return encodedXML;
        }

        /// <summary>
        /// Replace Hexadecimal values with constant
        /// to preserve them during XML transformation otherise they will transform to unicode 
        /// which not acceptable by Vendor XML processing Tool
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string ReplaceHexadecimalUnicodeValues(string xml)
        { 
            StringBuilder stringBuilder = new StringBuilder(xml);
            for (int i = 0; i < stringBuilder.Length; i++)
            {
                if ((int)Convert.ToChar(stringBuilder[i]) >= 160)
                {

                    stringBuilder.Replace(stringBuilder[i].ToString(), String.Format("#HEXCONSTANT#x0{0:x2};", (int)stringBuilder[i]));
                }
            }
           return stringBuilder.ToString(); ;
        }

    }
}
