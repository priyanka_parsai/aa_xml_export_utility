﻿using RSC.Database.Api.Common.DataContracts.MarinlitProduction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace RSC.Database.Api.Common.Helpers.MarinelitProduction
{
    public class ArticleHelper
    {
        public XElement ArticleXElement = null;

        //XML node values
        #region Private Variables      

        private const string Status = "Status";
        private const string CreatedBy = "CreatedBy";
        private const string CreatedDate = "CreatedDate";
        private const string FirstPublishedBy = "FirstPublishedBy";
        private const string FirstPublishedDate = "FirstPublishedDate";
        private const string SystemId = "SystemId";
        private const string PublicId = "PublicId";
        private const string PrintId = "PrintId";
        private const string Title = "Title";
        private const string ShortTitle = "ShortTitle";
        private const string Description = "Description";
        private const string LinkArticlesXml = "LinkArticlesXml";
        private const string CitationXml = "CitationXml";
        private const string JournalTitle = "JournalTitle";
        private const string SubsYear = "SubsYear";
        private const string JournalVolume = "JournalVolume";
        private const string FirstPage = "FirstPage";
        private const string LastPage = "LastPage";
        private const string Doi = "Doi";
        private const string IssueNumber = "IssueNumber";
        private const string PubmedId = "PubmedId";
        private const string Keywords = "Keywords";
        private const string Keyword = "Keyword";
        private const string Note = "Note";
        private const string Substance = "Substance";
        private const string Substances = "Substances";
        private const string Category = "Category";
        private const string Categories = "Categories";
        private const string Author = "Author";
        private const string Authors = "Authors";
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string Taxonomies = "Taxonomies";
        private const string Taxonomy = "Taxonomy";
        private const string ArticleBaseUrl = "ArticleBaseUrl";
        private const string TrivialNames = "TrivialNames";
        private const string TrivialName = "TrivialName";
        private const string IsSourceOfCompound = "IsSourceOfCompound";
        private const string Uri = "Uri";
        private const string Id = "Id";
        private const string Name = "Name";
        private const string Value = "Value";
        private const string Phylum = "Phylum";
        private const string Class = "Class";
        private const string Order = "Order";
        private const string Family = "Family";
        private const string Genus = "Genus";
        private const string Species = "Species";
        private const string SubCategories = "SubCategories";
        private const string Ampersand = "&amp;";
        private const string LinkArticles = "LinkArticles";
        private const string AmpersandRegex = "&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)";
        #endregion Private Static Variables

        #region Constructors

        public ArticleHelper()
        {

        }

        public ArticleHelper(string searchResults)
        {
            InitializeMainDocument(searchResults);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get article data from xml
        /// </summary>
        /// <returns></returns>
        public Article GetArticle()
        {
            Article article = new Article();
            try
            {
                if (ArticleXElement != null)
                {
                    article = BuildArticle(ArticleXElement);
                }
            }
            finally
            {
                ArticleXElement = null;
            }
            return article;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Remove Ampersand and load data under XElement
        /// </summary>
        /// <param name="searchResults"></param>
        private void InitializeMainDocument(string searchResults)
        {
            Regex ampersandRegex = null;
            XDocument searchResultsXDocument = null;
            try
            {
                //Encode & to &amp; 
                ampersandRegex = new Regex(AmpersandRegex);
                searchResults = ampersandRegex.Replace(searchResults, Ampersand);

                using (TextReader searchResultTextReader = new StringReader(searchResults))
                {
                    searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                    if (searchResultsXDocument.Root != null)
                    {
                        ArticleXElement = searchResultsXDocument.Root;
                    }
                    searchResultsXDocument = null;
                }
            }
            finally
            {
                searchResultsXDocument = null;
                ampersandRegex = null;
            }
           
        }

        /// <summary>
        /// Build article info object from xml
        /// </summary>
        /// <param name="articleXElement"></param>
        /// <returns></returns>
        private Article BuildArticle(XElement articleXElement)
        {
            Article articleInfo = null;

            #region XnamesDeclaration
            XElement tempXElement = null;
            IEnumerable<XElement> tempXElemets = null;
            #endregion

            try
            {
                if (articleXElement != null)
                {
                    articleInfo = new Article();

                    articleInfo.SystemId = GetValueFor(articleXElement, SystemId);
                    articleInfo.PublicId = GetValueFor(articleXElement, PublicId);
                    articleInfo.PrintId = GetValueFor(articleXElement, PrintId);
                    articleInfo.Title = GetValueFor(articleXElement, Title, true);
                    articleInfo.ShortTitle = GetValueFor(articleXElement, ShortTitle);
                    articleInfo.Description = GetValueFor(articleXElement, Description, true);
                    articleInfo.LinkArticlesXml = GetValueFor(articleXElement, LinkArticlesXml, true);

                    articleInfo.CitationXml = GetValueFor(articleXElement, CitationXml,true);
                    articleInfo.JournalTitle = GetValueFor(articleXElement, JournalTitle, true);
                    articleInfo.SubsYear = GetValueFor(articleXElement, SubsYear);
                    articleInfo.JournalVolume = GetValueFor(articleXElement, JournalVolume);
                    articleInfo.FirstPage = GetValueFor(articleXElement, FirstPage);
                    articleInfo.LastPage = GetValueFor(articleXElement, LastPage);
                    articleInfo.Doi = GetValueFor(articleXElement, Doi);
                    articleInfo.PubmedId = GetValueFor(articleXElement, PubmedId);
                    articleInfo.Note = GetValueFor(articleXElement, Note, true);


                    articleInfo.Categories = BindCategoryData(articleXElement.Element(XName.Get(Categories)));
                  
                    tempXElement = null;
                    tempXElement = articleXElement.Element(XName.Get(Authors));
                    if (tempXElement != null)
                    {
                        articleInfo.Authors = new List<Author>();
                        tempXElemets = tempXElement.Elements(Author);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement authorXElement in tempXElemets)
                            {
                                if (authorXElement != null)
                                {
                                    Author articleAuthor = new Author();
                                    articleAuthor.FirstName = GetValueFor(authorXElement, FirstName, true);
                                    articleAuthor.LastName = GetValueFor(authorXElement, LastName, true);

                                    articleInfo.Authors.Add(articleAuthor);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(TrivialNames));
                    if (tempXElement != null)
                    {
                        articleInfo.TrivialNames = new List<string>();
                        tempXElemets = tempXElement.Elements(TrivialName);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement trivialXElement in tempXElemets)
                            {
                                if (trivialXElement != null)
                                {
                                    articleInfo.TrivialNames.Add(trivialXElement.Value);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(Keywords));
                    if (tempXElement != null)
                    {
                        articleInfo.Keywords = new List<string>();
                        tempXElemets = tempXElement.Elements(Keyword);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement keywordXElement in tempXElemets)
                            {
                                if (keywordXElement != null)
                                {
                                    articleInfo.Keywords.Add(keywordXElement.Value);
                                }
                            }
                        }
                    }
                
                    tempXElement = null;
                    tempXElement = articleXElement.Element(XName.Get(Substances));
                    if (tempXElement != null)
                    {
                        articleInfo.Substances=new List<DataContracts.MarinlitProduction.Substance>();
                        tempXElemets = tempXElement.Elements(Substance);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement substanceXElement in tempXElemets)
                            {
                                if (substanceXElement != null)
                                {
                                   SubstanceHelper helper;
                                   helper = new SubstanceHelper(substanceXElement.ToString());
                                   var substance= helper.GetSubstance();
                                   articleInfo.Substances.Add(substance);
                                }
                            }
                        }
                    }
                    tempXElement = null; tempXElemets = null;
                    tempXElement = articleXElement.Element(XName.Get(LinkArticles));
                    if (tempXElement != null)
                    {
                        articleInfo.LinkArticles = new List<string>();
                        tempXElemets = tempXElement.Elements(Id);
                        if (tempXElemets != null && tempXElemets.Count() > 0)
                        {
                            foreach (XElement linkArticleXElement in tempXElemets)
                            {
                                if (linkArticleXElement != null)
                                {
                                    articleInfo.LinkArticles.Add(linkArticleXElement.Value);
                                }
                            }
                        }
                    }
                    articleInfo.Taxonomies = BindTaxonomyData(articleXElement);
                    articleInfo.ArticleBaseUrl = GetValueFor(articleXElement, ArticleBaseUrl);
                    articleInfo.Status = GetValueFor(articleXElement, Status);
                    articleInfo.CreatedBy = GetValueFor(articleXElement, CreatedBy);
                    articleInfo.CreatedDate = GetValueFor(articleXElement, CreatedDate);
                    articleInfo.FirstPublishedBy = GetValueFor(articleXElement, FirstPublishedBy);
                    articleInfo.FirstPublishedDate = GetValueFor(articleXElement, FirstPublishedDate);
                    articleInfo.IssueNumber = GetValueFor(articleXElement, IssueNumber);
                    articleInfo.Uri = GetValueFor(articleXElement, Uri);
                    tempXElement = null;
                }
            }
            finally
            {
                tempXElement = null;
                tempXElemets = null;
            }

            return articleInfo;
        }

        /// <summary>
        /// Get node value from the xml
        /// </summary>
        /// <param name="xelement"></param>
        /// <param name="name"></param>
        /// <param name="isEncoded"></param>
        /// <returns></returns>
        private string GetValueFor(XElement xelement, string name, bool isEncoded = false)
        {
            string returnVal = string.Empty;
            XElement tempXElement = null;
            try
            {
                tempXElement = xelement.Element(XName.Get(name));
                if (tempXElement != null)
                {
                    if (isEncoded)
                    {
                        returnVal = WebUtility.HtmlDecode(CommonHelpers.GetInnerXML(tempXElement));
                    }
                    else
                    {
                        returnVal = tempXElement.Value;
                    }
                }
                return returnVal;
            }
            finally
            {
                returnVal = null;
                tempXElement = null;
            }
        }
        
        /// <summary>
        /// Bind taxonomy data from xml to taxonomies object
        /// </summary>
        /// <param name="articleXElement"></param>
        /// <returns></returns>
        private List<Taxonomy> BindTaxonomyData(XElement articleXElement)
        {
            List<Taxonomy> taxonomies = null;
            var taxonomyXElement = articleXElement.Element(XName.Get(Taxonomies));
            if (taxonomyXElement != null)
            {
                IEnumerable<XElement> taxonomyXElements = taxonomyXElement.Elements(XName.Get(Taxonomy));
                if (taxonomyXElements != null && taxonomyXElements.Count() > 0)
                {
                    taxonomies = new List<Taxonomy>();
                    foreach (XElement element in taxonomyXElements)
                    {
                        Taxonomy taxonomy = new Taxonomy();
                        XElement texonomyTempXElement = null;

                        texonomyTempXElement = element.Element(XName.Get(IsSourceOfCompound));
                        taxonomy.IsSourceOfCompound = texonomyTempXElement != null ? Convert.ToBoolean(texonomyTempXElement.Value) : false;
                        texonomyTempXElement = element.Element(XName.Get(Phylum));
                        taxonomy.Phylum = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Class));
                        taxonomy.Class = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Order));
                        taxonomy.Order = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Family));
                        taxonomy.Family = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Genus));
                        taxonomy.Genus = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;

                        texonomyTempXElement = element.Element(XName.Get(Species));
                        taxonomy.Species = texonomyTempXElement != null ? BuildTaxonomyType(texonomyTempXElement) : null;
                        taxonomies.Add(taxonomy);
                        texonomyTempXElement = null;
                    }
                }
                taxonomyXElements = null;
            }
            taxonomyXElement = null;
            return taxonomies;
        }

        /// <summary>
        /// Bind taxonomy type to the the taxonomy data
        /// </summary>
        /// <param name="taxonomyXElement"></param>
        /// <returns></returns>
        private NameValue BuildTaxonomyType(XElement taxonomyXElement)
        {
            NameValue nameValue = null;
            XElement taxonomyTempSubXElement = null;
            try
            {
                if (taxonomyXElement != null)
                {
                    nameValue = new NameValue();
                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Name));
                    nameValue.Name = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;

                    taxonomyTempSubXElement = taxonomyXElement.Element(XName.Get(Value));
                    nameValue.Value = taxonomyTempSubXElement != null ? taxonomyTempSubXElement.Value : string.Empty;
                }
            }
            finally
            {
                taxonomyXElement = null;
                taxonomyTempSubXElement = null;
            }
            return nameValue;
        }

        /// <summary>
        /// Bind category data from xml to the category object
        /// </summary>
        /// <param name="categoryXElement"></param>
        /// <returns></returns>
        private List<Category> BindCategoryData(XElement categoryXElement)
        {
            List<Category> categories = null;
            if (categoryXElement != null)
            {
                IEnumerable<XElement> categoryXElements = categoryXElement.Elements(XName.Get(Category));
                if (categoryXElements != null && categoryXElements.Count() > 0)
                {
                    categories = new List<Category>();
                    foreach (XElement element in categoryXElements)
                    {
                        Category category = new Category();
                        XElement categoryTempXElement = null;

                        categoryTempXElement = element.Element(XName.Get(Id));
                        category.Id = categoryTempXElement.Value;
                        categoryTempXElement = element.Element(XName.Get(Name));
                        category.Name = categoryTempXElement.Value;

                        categoryTempXElement = element.Element(XName.Get(SubCategories));
                        category.SubCategories = categoryTempXElement != null ? BindCategoryData(categoryTempXElement) : null;

                        categories.Add(category);
                        categoryTempXElement = null;
                    }
                }
                categoryXElements = null;
            }
            categoryXElement = null;
            return categories;
        }

        #endregion
    }
}
