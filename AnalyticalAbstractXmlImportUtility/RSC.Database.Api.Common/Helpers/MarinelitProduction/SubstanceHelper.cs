﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using DC = RSC.Database.Api.Common.DataContracts.MarinlitProduction;

namespace RSC.Database.Api.Common.Helpers.MarinelitProduction
{
    public class SubstanceHelper
    {
        private XElement _substanceXElement;

        #region Constants
        private const string SYSTEMID = "SystemId";
        private const string TITLE = "Title";
        private const string INCHI = "Inchi";
        private const string INCHIKEY = "InchiKey";
        private const string MOLECULARFORMULA = "MolecularFormula";
        private const string MOLECULARWEIGHT = "MolecularWeight";
        private const string ARTICLEID = "ArticleId";
        private const string PUBLICID = "PublicId";
        private const string ExternalId = "ExternalId";
        private const string NAME = "Name";
        private const string IMAGE = "Image";
        private const string ID = "ID";
        private const string CDX = "Cdx";
        private const string TIF = "Tif";
        private const string MOL = "Mol";
        private const string LITERATUREREFERENCES = "LiteratureReferences";
        private const string DOI = "DOI";
        private const string LASTPAGE = "LastPage";
        private const string FIRSTPAGE = "FirstPage";
        private const string VOLUME = "Volume";
        private const string YEAR = "Year";
        private const string AUTHOR = "Author";
        private const string FIRSTNAME = "FirstName";
        private const string SURNAME = "Surname";
        private const string CSPROPERTIES = "CsProperties";
        private const string CSPROPERTIESXML = "PropertiesXml";
        private const string NOTE = "Note";
        private const string NOTEXML = "NoteXml";
        private const string LOCANT = "Locant";
        private const string LOCATION = "Location";
        private const string LOCATIONS = "Locations";
        private const string LONGITUDE = "Longitude";
        private const string LATITUDE = "Latitude";
        private const string DEPTH = "Depth";
        private const string QF = "QF";
        private const string UNIT = "Unit";
        private const string LOGEVALUE = "LogEValue";
        private const string DETAILS = "Details";
        private const string PROPERTYDETAIL = "PropertyDetail";
        private const string MLSTATUS = "MlStatus";
        private const string FALSE = "false";
        public const string ArticleCompoundNo = "ArticleCompoundNo";
        private const string VALUE = "Value";
        private const string CSID = "CSId";
        private const string SYNONYMS = "Synonyms";
        private const string PRINTID = "PrintId";
        private const string SUBSTACNETYPE = "Type";
        private const string CASREGNO = "CASRegNo";
        private const string CASNAME = "CASName";
        private const string ACDName = "ACDName";
        private const string TRADEMARKS = "TradeMarks";
        private const string MANUFACTURERCODE = "ManufacturerCode";
        private const string COMPOSITION = "Composition";
        private const string USE = "Use";
        private const string CSDATA = "CsData";
        private const string GROUP = "Group";
        private const string GROUPS = "Groups";
        private const string CNMRDATA = "CnmrData";
        private const string NUCLEI = "Nuclei";
        private const string NMRPEAK = "NMRPeak";
        private const string NMRPEAKS = "NmrPeaks";
        private const string VETKEYWORDS = "VetKeywords";
        private const string KEYWORDS = "Keywords";
        private const string VAR = "Var";
        private const string PROTONS = "Protons";
        private const string HNMRDATA = "HnmrData";
        private const string SYNONYM = "Synonym";
        private const string Ver = "ver";
        private const string ISSUENUMBER = "IssueNumber";
        #endregion

        #region Constructor and public methods

        public SubstanceHelper()
        {
        }

        public SubstanceHelper(string searchResults)
        {
            InitializeMainDocument(searchResults);
        }

        public DC.Substance GetSubstance()
        {
            DC.Substance substance = null;
            try
            {
                if (_substanceXElement != null && _substanceXElement.Elements().Any())
                {
                    substance = BuildSubstance(_substanceXElement);
                }
            }
            finally
            {
                _substanceXElement = null;
            }
            return substance;
        }

        #endregion

        #region Private Methods

        private void InitializeMainDocument(string searchResults)
        {
            if (!string.IsNullOrEmpty(searchResults))
            {
                using (TextReader searchResultTextReader = new StringReader(searchResults))
                {
                    var searchResultsXDocument = XDocument.Load(searchResultTextReader, LoadOptions.PreserveWhitespace);
                    if (searchResultsXDocument.Root != null)
                    {
                        _substanceXElement = searchResultsXDocument.Root;
                    }
                }
            }
        }

        private DC.Substance BuildSubstance(XElement xElement)
        {
            DC.Substance substance = null;
            try
            {
                if (xElement != null)
                {
                    substance = new DC.Substance();
                    XElement substanceTempXElement = null;

                    substanceTempXElement = xElement.Element(XName.Get(SYSTEMID));
                    substance.SystemId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(DOI));
                    substance.Doi = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(ARTICLEID));
                    substance.ArticleId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get("URI"));
                    substance.Uri = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(TITLE));
                    substance.Title = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(CSID));
                    substance.CsId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(INCHI));
                    substance.Inchi = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(INCHIKEY));
                    substance.InchiKey = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(SYNONYMS));
                    substance.Synonyms = substanceTempXElement != null ? BuildSynonyms(substanceTempXElement) : null;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARFORMULA));
                    substance.MolecularFormula = (substanceTempXElement != null && substanceTempXElement.Nodes() != null) ? string.Concat(substanceTempXElement.Nodes()) : null;

                    substanceTempXElement = xElement.Element(XName.Get(MOLECULARWEIGHT));
                    substance.MolecularWeight = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = xElement.Element(XName.Get(ARTICLEID));
                    substance.ArticleId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    //Marinlit substances
                    substanceTempXElement = _substanceXElement.Element(XName.Get(PUBLICID));
                    substance.PublicId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(PRINTID));
                    substance.PrintId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(ExternalId));
                    substance.ExternalId = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(SUBSTACNETYPE));
                    substance.Type = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASREGNO));
                    substance.CasRegNo = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CASNAME));
                    substance.CasName = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(ACDName));
                    if (substanceTempXElement != null)
                    {
                        substance.AcdName = substanceTempXElement.Value;
                        substance.AcdNameVersion = CommonHelpers.GetAttributeValue(substanceTempXElement, Ver);
                    }
                   

                    substanceTempXElement = _substanceXElement.Element(XName.Get(TRADEMARKS));
                    substance.TradeMarks = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(MANUFACTURERCODE));
                    substance.ManufacturerCode = substanceTempXElement != null ? substanceTempXElement.Value : string.Empty;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(COMPOSITION));
                    substance.Composition = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(LOCATIONS));
                    substance.Locations = substanceTempXElement != null ? BuildLocationData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(IMAGE));
                    substance.Image = substanceTempXElement != null ? BuildImageData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(LITERATUREREFERENCES));
                    substance.JournalCitation = substanceTempXElement != null ? BuildJournalCitation(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CSPROPERTIES));
                    substance.CsProperties = substanceTempXElement != null ? BuildCsProperties(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(USE));
                    substance.Use = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(NOTE));
                    substance.Note = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(CSDATA));
                    substance.CsData = substanceTempXElement != null ? BuildCsData(substanceTempXElement) : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(KEYWORDS));
                    substance.Keywords = substanceTempXElement != null ? substanceTempXElement.Value : null;

                    substanceTempXElement = _substanceXElement.Element(XName.Get(VETKEYWORDS));
                    substance.VetKeywords = substanceTempXElement != null ? substanceTempXElement.Value : null;
                  
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xElement = null;
            }
            return substance;
        }


        private List<DC.Location> BuildLocationData(XElement locationsXElement)
        {
            List<DC.Location> locations = null;
            XElement tempXElement;
            try
            {
                if (locationsXElement != null)
                {
                    locations = new List<DC.Location>();
                    IEnumerable<XElement> locationXElementList = locationsXElement.Elements(XName.Get(LOCATION));
                    foreach (XElement locationXElement in locationXElementList)
                    {
                        if (locationXElement != null)
                        {
                            var location = new DC.Location();

                            tempXElement = locationXElement.Element(LONGITUDE);
                            location.Longitude = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(LATITUDE);
                            location.Latitude = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(DEPTH);
                            location.Depth = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(QF);
                            location.QF = tempXElement != null ? tempXElement.Value : string.Empty;

                            tempXElement = locationXElement.Element(NAME);
                            location.Name = tempXElement != null ? tempXElement.Value : string.Empty;
                            locations.Add(location);
                        }
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                locationsXElement = null;
                tempXElement = null;
            }
            return locations;
        }

        private DC.Image BuildImageData(XElement imageXElement)
        {
            DC.Image image = null;
            if (imageXElement != null)
            {
                image = new DC.Image();
                XElement tempXElement = imageXElement.Element(ID);
                image.Id = tempXElement != null ? tempXElement.Value : string.Empty;
                tempXElement = imageXElement.Element(CDX);
                image.Cdx = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(TIF);
                image.Tif = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);
                tempXElement = imageXElement.Element(MOL);
                image.Mol = Convert.ToBoolean(tempXElement != null && !string.IsNullOrEmpty(tempXElement.Value) ? tempXElement.Value : FALSE);

                tempXElement = null;
                imageXElement = null;
            }
            return image;
        }

        private DC.JournalCitation BuildJournalCitation(XElement literatureReferencesXElement)
        {
            DC.JournalCitation journalCitation = null;
            XElement citationTempXElement;
            XElement ciatationSubElementXElement;
            try
            {
                if (literatureReferencesXElement != null)
                {
                    List<DC.JournalCitation> journalCitations = null;

                    foreach (var element in literatureReferencesXElement.Elements().Where(element => element != null))
                    {
                        if (journalCitations == null)
                            journalCitations = new List<DC.JournalCitation>();

                        citationTempXElement = element;
                        if (citationTempXElement != null)
                        {
                            var citation = new DC.JournalCitation();

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(TITLE));
                            citation.Title = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(YEAR));
                            citation.Year = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(VOLUME));
                            citation.Volume = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(ISSUENUMBER));
                            citation.IssueNumber = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(FIRSTPAGE));
                            citation.FirstPage = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(LASTPAGE));
                            citation.LastPage = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            ciatationSubElementXElement = citationTempXElement.Element(XName.Get(DOI));
                            citation.Doi = ciatationSubElementXElement != null ? ciatationSubElementXElement.Value : string.Empty;

                            var authorList = citationTempXElement.Elements(XName.Get(AUTHOR));
                            if (ciatationSubElementXElement != null)
                            {
                                XElement authorXElement = null;
                                foreach (var author in authorList)
                                {
                                    var citationAuthor = new DC.Author();
                                    if (citation.Authors == null)
                                    {
                                        citation.Authors = new List<DC.Author>();
                                    }
                                    if (author != null)
                                    {
                                        authorXElement = author.Element(XName.Get(FIRSTNAME));
                                        citationAuthor.FirstName = authorXElement != null ? authorXElement.Value : string.Empty;

                                        authorXElement = author.Element(XName.Get(SURNAME));
                                        citationAuthor.LastName = authorXElement != null ? authorXElement.Value : string.Empty;

                                        citation.Authors.Add(citationAuthor);
                                    }
                                }
                                authorXElement = null;
                            }
                            journalCitations.Add(citation);
                            authorList = null;
                        }
                    }
                    if (journalCitations != null && journalCitations.Any())
                    {
                        journalCitation = journalCitations[0];
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                citationTempXElement = null;
                ciatationSubElementXElement = null;
            }
            return journalCitation;
        }

        private List<DC.Property> BuildCsProperties(XElement csPropertiesXElement)
        {
            List<DC.Property> csProperties = null;
            XElement tempXElement;

            if (csPropertiesXElement != null)
            {
                csProperties = new List<DC.Property>();
                var properties = csPropertiesXElement.Elements(XName.Get("Property"));

                foreach (var element in properties)
                {
                    if (element != null)
                    {
                        var property = new DC.Property();

                        tempXElement = element.Element(NAME);
                        property.Name = tempXElement != null ? WebUtility.HtmlDecode(CommonHelpers.GetNodeXML(tempXElement)) : string.Empty;
                        tempXElement = element.Element(DETAILS);

                        if (tempXElement != null)
                        {
                            var detailList = new List<DC.PropertyDetail>();
                            var detailElements = tempXElement.Elements(XName.Get(PROPERTYDETAIL));

                            foreach (var detailElement in detailElements)
                            {
                                var detail = new DC.PropertyDetail();
                                XElement subtempXElement = null;

                                subtempXElement = detailElement.Element(VALUE);
                                detail.Value = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                subtempXElement = detailElement.Element(LOGEVALUE);
                                detail.LogEValue = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                subtempXElement = detailElement.Element(UNIT);
                                detail.Unit = subtempXElement != null ? subtempXElement.Value : string.Empty;

                                detailList.Add(detail);
                                subtempXElement = null;
                            }
                            detailElements = null;
                            property.Details = detailList;
                        }
                        csProperties.Add(property);
                    }
                }
                properties = null;
            }
            tempXElement = null;
            return csProperties;
        }

        private DC.CsData BuildCsData(XElement csDataXElement)
        {
            DC.CsData csData = null;
            XElement csDataTempXElement;
            try
            {
                csData = new DC.CsData();
                csDataTempXElement = csDataXElement.Element(MLSTATUS);
                csData.MlStatus = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;
                csDataTempXElement = csDataXElement.Element(XName.Get(GROUPS));

                if (csDataTempXElement != null)
                {
                    var groupXElements = csDataTempXElement.Elements(XName.Get(GROUP));
                    if (groupXElements.Any())
                    {
                        csData.Groups = new List<DC.Group>();
                        foreach (var groupElement in groupXElements)
                        {
                            if (groupElement != null)
                            {
                                var group = new DC.Group();

                                csDataTempXElement = groupElement.Element(XName.Get(NAME));
                                group.Name = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;

                                csDataTempXElement = groupElement.Element(XName.Get(VALUE));
                                group.Value = csDataTempXElement != null ? csDataTempXElement.Value : string.Empty;

                                csData.Groups.Add(group);
                            }
                        }
                        groupXElements = null;
                    }
                }
                csDataTempXElement = csDataXElement.Element(XName.Get(CNMRDATA));
                csData.CnmrData = csDataTempXElement != null ? BuildNMRData(csDataTempXElement) : null;

                csDataTempXElement = csDataXElement.Element(XName.Get(HNMRDATA));
                csData.HnmrData = csDataTempXElement != null ? BuildNMRData(csDataTempXElement) : null;

                csDataTempXElement = csDataXElement.Element(XName.Get("MonomerIds"));
                csData.MonomerIds = csDataTempXElement != null ? BuildMonomerIDs(csDataTempXElement) : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                csDataTempXElement = null;

            }
            return csData;
        }

        private DC.NmrData BuildNMRData(XElement nmrDataXElement)
        {
            DC.NmrData nmrData = null;
            XElement nmrDataTempXElement;
            IEnumerable<XElement> nmrXElements;
            try
            {
                if (nmrDataXElement != null)
                {
                    nmrData = new DC.NmrData();
                    nmrDataTempXElement = nmrDataXElement.Element(XName.Get(NMRPEAKS));
                    nmrXElements = nmrDataTempXElement != null ? nmrDataTempXElement.Elements(XName.Get(NMRPEAK)) : null;

                    if (nmrXElements != null && nmrXElements.Any())
                    {
                        nmrData.NmrPeaks = new List<DC.NmrPeak>();
                        foreach (var nmrXElement in nmrXElements)
                        {
                            if (nmrXElement != null)
                            {
                                var peak = new DC.NmrPeak();
                                peak.Locant = CommonHelpers.GetAttributeValue(nmrXElement, LOCANT);
                                peak.Var = CommonHelpers.GetAttributeValue(nmrXElement, VAR);
                                peak.Protons = CommonHelpers.GetAttributeValue(nmrXElement, PROTONS);
                                peak.Value = nmrXElement.Value;

                                nmrData.NmrPeaks.Add(peak);
                            }
                        }
                    }
                    nmrDataTempXElement = nmrDataXElement.Element(XName.Get(NUCLEI));
                    nmrData.Nuclei = nmrDataTempXElement != null ? nmrDataTempXElement.Value : null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                nmrDataTempXElement = null;
                nmrXElements = null;
            }
            return nmrData;
        }

        private List<string> BuildMonomerIDs(XElement monomerIdsXElement)
        {
            List<string> monomerIds = null;
            if (monomerIdsXElement != null)
            {
                var monomerIdsElements = monomerIdsXElement.Elements(XName.Get("MonomerId"));

                foreach (XElement element in monomerIdsElements.Where(element => element != null))
                {
                    if (monomerIds == null)
                    {
                        monomerIds = new List<string>();
                    }
                    monomerIds.Add(element.Value);
                }
                monomerIdsElements = null;
            }
            return monomerIds;
        }

        private List<string> BuildSynonyms(XElement synonymsXElement)
        {
            List<string> synonyms = null;
            if (synonymsXElement != null)
            {
                IEnumerable<XElement> synonymsElements = synonymsXElement.Elements(XName.Get(SYNONYM));

                foreach (XElement element in synonymsElements)
                {
                    if (element != null)
                    {
                        if (synonyms == null)
                        {
                            synonyms = new List<string>();
                        }
                        synonyms.Add(element.Value);
                    }
                }

            }
            return synonyms;
        }
        #endregion
    }
}
