﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Web;
using RSC.Database.Api.Common.DataContracts;
using RSCpubs.ePlatform.Service.Common.Helpers;

namespace RSC.Database.Api.Common.Helpers
{
    public static class APIRequestHelper
    {
        public static string GetData(string url, NameValueCollection headers)
        {
            string resultString = string.Empty;
            using (var client = new HttpClient())
            {

                if (headers != null && headers.Count > 0)
                {
                    foreach (string name in headers)
                    {
                        client.DefaultRequestHeaders.Add(name, headers[name]);
                    }
                }
                client.DefaultRequestHeaders.Add("Keep-Alive", "true");
                var result = client.GetAsync(url).Result;
                if (result.IsSuccessStatusCode)
                {
                    resultString = result.Content.ReadAsStringAsync().Result;
                }
            }
            return resultString;
        }

        public static string PostData(string url, string postContent, NameValueCollection headers = null)
        {
            string resultContent = string.Empty;
            using (var client = new HttpClient())
            {

                var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("", postContent)
            });

                if (headers != null && headers.Count > 0)
                {
                    foreach (string name in headers)
                    {
                        client.DefaultRequestHeaders.Add(name, headers[name]);
                    }
                }

                var result = client.PostAsync(url, content).Result;
                if (result.IsSuccessStatusCode)
                {
                    resultContent = result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    resultContent = result.Content.ReadAsStringAsync().Result;
                    Errors errors = (Errors)SerializeDeserializeHelper.DeserializeObject(resultContent, typeof(Errors));
                    if (errors != null && errors.ErrorList != null && errors.ErrorList.Count > 0)
                    {

                        if (errors.ErrorList.FirstOrDefault().Code != 400)
                        {
                            HttpException httpException = new HttpException(errors.ErrorList.FirstOrDefault().Code, errors.ErrorList.FirstOrDefault().Name);
                            throw httpException;
                        }
                    }
                }
            }
            return resultContent;


        }
    }
}
