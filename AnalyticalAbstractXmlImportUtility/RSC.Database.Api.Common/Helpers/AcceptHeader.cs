﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.Helpers
{
    public class AcceptHeader
    {
        private string value;
        private string type;
        public string Value
        {
            get { return value; }
        }
        public string Type
        {
            get { return type; }
        }
        public AcceptHeader()
        {
        }
        public AcceptHeader(string accptHeader, string accptHeaderType)
        {
            value = accptHeader;
            type = accptHeaderType;
        }
    }
}
