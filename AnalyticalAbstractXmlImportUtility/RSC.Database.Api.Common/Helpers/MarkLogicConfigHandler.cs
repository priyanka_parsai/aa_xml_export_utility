﻿using System.Collections;
using System.Configuration;
using System.Xml;

namespace RSC.Database.Api.Common.Helpers
{
    public class MarkLogicConfigHandler : IConfigurationSectionHandler
    {
        #region IConfigurationSectionHandler Members

        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            ArrayList MarkLogicList = new ArrayList();

            foreach (XmlNode markLogic in section.ChildNodes)
            {
                MarkLogicList.Add(new MarkLogic(
                    markLogic.Attributes["username"] == null ? string.Empty : markLogic.Attributes["username"].Value,
                    markLogic.Attributes["password"] == null ? string.Empty : markLogic.Attributes["password"].Value,
                    markLogic.Attributes["domain"] == null ? string.Empty : markLogic.Attributes["domain"].Value,
                    markLogic.Attributes["url"] == null ? string.Empty : markLogic.Attributes["url"].Value,
                    markLogic.Attributes["id"] == null ? string.Empty : markLogic.Attributes["id"].Value
                       ));
            }
            return MarkLogicList;
        }

        #endregion
    }
}
