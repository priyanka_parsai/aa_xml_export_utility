﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.Helpers
{
    public static class SecurityHelper
    {
        private const string EMBARGO = "Embargo";
        private const string STARTDATE = "StartDate";
        private const string ENDDATE = "EndDate";
        private const string SUBMITTER = "Submitter";
        private const string RSCID = "rscid";
        private const string Email = "email";
        private const string OWNERSHIP = "Ownership";
        private const string OWNERSHIPTYPE = "OwnershipType";
        private const string ID = "Id";
        private const string SHARING = "Sharing";
        private const string SHARE = "Share";
        private const string WITH = "With";
        private const string ACCESS = "Access";
        
        public static Security GetSecurity(XElement securityXElement)
        {
            Security security = null;
            if (securityXElement != null)
            {
                security = new Security();
                var securityTempXElement = securityXElement.Element(XName.Get(EMBARGO));
                XElement securitySubTempXElement;

                if (securityTempXElement != null)
                {
                    security.Embargo = new Embargo();

                    securitySubTempXElement = securityTempXElement.Element(XName.Get(STARTDATE));
                    security.Embargo.StartDate = Convert.ToDateTime(securitySubTempXElement != null ? securitySubTempXElement.Value : string.Empty);

                    securitySubTempXElement = securityTempXElement.Element(XName.Get(ENDDATE));
                    security.Embargo.EndDate = Convert.ToDateTime(securitySubTempXElement != null ? securitySubTempXElement.Value : string.Empty);
                }
                securityTempXElement = securityXElement.Element(XName.Get(SUBMITTER));
                if (securityTempXElement != null)
                {
                    security.Submitter = new Submitter();
                    securitySubTempXElement = securityTempXElement.Element(XName.Get(RSCID));
                    security.Submitter.RSCId = securitySubTempXElement != null
                        ? new Guid(securitySubTempXElement.Value)
                        : Guid.Empty;
                    securitySubTempXElement = securityTempXElement.Element(XName.Get(Email));
                    security.Submitter.EmailId = securitySubTempXElement != null
                        ? (securitySubTempXElement.Value)
                        : string.Empty;
                }
                securityTempXElement = securityXElement.Element(XName.Get(OWNERSHIP));
                if (securityTempXElement != null)
                {
                    security.Ownership = new Ownership();

                    securitySubTempXElement = securityTempXElement.Element(XName.Get(OWNERSHIPTYPE));
                    security.Ownership.OwnershipType = securitySubTempXElement != null
                        ? (OwnershipType)Enum.Parse(typeof(OwnershipType), securitySubTempXElement.Value, true) : OwnershipType.none;

                    securitySubTempXElement = securityTempXElement.Element(XName.Get(ID));
                    security.Ownership.Id = securitySubTempXElement != null
                        ? securitySubTempXElement.Value
                        : string.Empty;

                }
                securityTempXElement = securityXElement.Element(XName.Get(SHARING));
                if (securityTempXElement != null)
                {
                    security.Sharing = new List<SecurityShare>();
                    var securitySharElements = securityTempXElement.Elements(XName.Get(SHARE));
                    foreach (var element in securitySharElements)
                    {
                        if (element != null)
                        {
                            var securityShare = new SecurityShare();

                            securitySubTempXElement = element.Element(XName.Get(WITH));
                            securityShare.With = securitySubTempXElement != null
                                ? securitySubTempXElement.Value
                                : string.Empty;


                            securitySubTempXElement = element.Element(XName.Get(ACCESS));
                            securityShare.Access = securitySubTempXElement != null
                                ? securitySubTempXElement.Value
                                : string.Empty;

                            securitySubTempXElement = element.Element(XName.Get(ID));
                            securityShare.Id = securitySubTempXElement != null
                                ? securitySubTempXElement.Value
                                : string.Empty;

                            security.Sharing.Add(securityShare);
                        }
                    }
                }
                securityTempXElement = null;
                securitySubTempXElement = null;
            }
            return security;
        }

    }
}
