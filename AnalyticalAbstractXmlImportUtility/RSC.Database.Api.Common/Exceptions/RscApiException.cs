﻿using System;

namespace RSC.Database.Api.Common.Exceptions
{
    public class RscApiException : Exception
    {
        #region Field members

        public string Message { get; set; }
        public ErrorCodes ErrorCode { get; set; }



        #endregion Field members

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RscApiException()
        {

        }

        /// <summary>
        /// Initializes with a specified error message.
        /// </summary>
        /// <param name="errorMessage">A message that describes the error.</param>
        public RscApiException(string errorMessage)
            : base(errorMessage)
        {
            Message = errorMessage;
        }

        /// <summary>
        /// Initializes with a specified error message and source.
        /// </summary>
        /// <param name="errorMessage">A message that describes the error.</param>
        /// <param name="errorCode">ErrorCode of the error</param>
        public RscApiException(string errorMessage, ErrorCodes errorCode)
            : this(errorMessage)
        {
            ErrorCode = errorCode;
        }



        #endregion Constructors
    }
}
