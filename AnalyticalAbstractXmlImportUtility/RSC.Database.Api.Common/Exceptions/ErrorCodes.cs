﻿namespace RSC.Database.Api.Common.Exceptions
{
    public enum ErrorCodes
    {
        Bad_Request = 400,
        Unauthorized = 401,
        Forbidden = 403,
        Not_Found = 404,
        Not_Acceptable = 406,
        Internal_Server_Error = 500,
        Bad_Gateway = 502,
        Service_Unavailable = 503
    }
}
