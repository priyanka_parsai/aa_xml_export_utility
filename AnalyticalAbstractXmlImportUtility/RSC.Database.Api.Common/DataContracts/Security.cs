﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    [DataContract(Name = "security",Namespace = "")]
    public class Security
    {
        [System.Xml.Serialization.XmlElementAttribute("embargo",IsNullable=true)]
        [DataMember(Name = "embargo")]
        public Embargo Embargo { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("submitter")]
        [DataMember(Name = "submitter")]
        public Submitter Submitter { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ownership")]
        [DataMember(Name = "ownership")]
        public Ownership Ownership { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("sharing")]
        [System.Xml.Serialization.XmlArrayItemAttribute("share", IsNullable = true)]
        [DataMember(Name = "sharing")]
        public List<SecurityShare> Sharing { get; set; }

    }
}
