﻿using System;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    public interface IMarinLitSearchCriteria
    {
        [DataMember]
        string AFreeText
        {
            get;
            set;
        }

        [DataMember]
        string SFreeText
        {
            get;
            set;
        }

        [DataMember]
        string Title
        {
            get;
            set;
        }

        [DataMember]
        string Id
        {
            get;
            set;
        }

        [DataMember]
        string CSID
        {
            get;
            set;
        }

        [DataMember]
        string Doi
        {
            get;
            set;
        }

        [DataMember]
        string JournalTitle
        {
            get;
            set;
        }

        [DataMember]
        string Year
        {
            get;
            set;
        }

        [DataMember]
        string Volume
        {
            get;
            set;
        }

        [DataMember]
        string FPage
        {
            get;
            set;
        }

        [DataMember]
        string Authors
        {
            get;
            set;
        }

        [DataMember]
        string Phylum
        {
            get;
            set;
        }

        [DataMember]
        string Class
        {
            get;
            set;
        }

        [DataMember]
        string Order
        {
            get;
            set;
        }

        [DataMember]
        string Family
        {
            get;
            set;
        }

        [DataMember]
        string Genus
        {
            get;
            set;
        }

        [DataMember]
        string Species
        {
            get;
            set;
        }

        [DataMember]
        string IsTaxCompound
        {
            get;
            set;
        }

        [DataMember]
        string MarineCat
        {
            get;
            set;
        }

        [DataMember]
        string TrivialName
        {
            get;
            set;
        }

        //[DataMember]
        //public string PageNo
        //{
        //    get;
        //    set;
        //}
        //[DataMember]
        //public string PageSize
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        string SortBy
        {
            get;
            set;
        }

        [DataMember]
        string VersionNumber
        {
            get;
            set;
        }

        [DataMember]
        string GeoLoc
        {
            get;
            set;
        }

        string PublicId
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        string Status
        {
            get;
            set;
        }

        [DataMember]
        string Formula
        {
            get;
            set;
        }

        [DataMember]
        string Weight
        {
            get;
            set;
        }

        [DataMember]
        string UVMax
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string met
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string mets
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string metd
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string mett
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string arome
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string vnme
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string meo
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string men
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string mes
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string mene
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string mine
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string sp2h
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cc
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string vn
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cch
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string dic
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cc3
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string tlk
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cao
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string c2ho
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string c1ho
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string ch2o2
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cho2
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string co2
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string co
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string cho
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string acy
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string aco
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string am
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string chn
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string nit
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string initr
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string ben
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b1
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b12
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b13
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b14
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b123
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b124
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b135
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b1234
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b1235
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b1245
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b12345
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string b1_6
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p2
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p3
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p4
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p23
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p24
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p25
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p26
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p34
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p35
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p234
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p235
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p236
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p246
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p345
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p2345
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p2346
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p2356
        {
            get;
            set;
        }

        [DataMember]
        [FunctionalGroup]
        string p2_6
        {
            get;
            set;
        }

        [DataMember]
        string CNMR
        {
            get;
            set;
        }

        [DataMember]
        string HNMR
        {
            get;
            set;
        }

        [DataMember]
        string AtoZ
        {
            get;
            set;
        }

        [DataMember]
        string Type
        {
            get;
            set;
        }

        [DataMember]
        string Taxonomy
        {
            get;
            set;
        }

        string DocStatus
        {
            get;
            set;
        }

        string PubmedId
        {
            get;
            set;
        }

        string User
        {
            get;
            set;
        }

        string CreatedBy { get; set; }

        string ModifiedBy { get; set; }

        string CreatedDate1 { get; set; }

        string CreatedDate2 { get; set; }

        string ModifiedDate1 { get; set; }

        string ModifiedDate2 { get; set; }

        string DateRangeMode { get; set; }

        string DateRangeBands { get; set; }

        string DateRangeType { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FunctionalGroup : Attribute
    {
    }
}