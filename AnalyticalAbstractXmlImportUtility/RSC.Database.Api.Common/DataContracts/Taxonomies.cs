﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    [DataContract(Namespace = "")]
    public class Taxonomies
    {
        [DataMember]
        public List<FacetValue> Phylums
        {
            get;
            set;
        }
        [DataMember]
        public List<FacetValue> Classes
        {
            get;
            set;
        }

        [DataMember]
        public List<FacetValue> Orders
        {
            get;
            set;
        }
        [DataMember]
        public List<FacetValue> Families
        {
            get;
            set;
        }
        [DataMember]
        public List<FacetValue> Genuses
        {
            get;
            set;
        }
        [DataMember]
        public List<FacetValue> Species
        {
            get;
            set;
        }

        [DataMember]
        public bool IsShowProducingCompounds
        {
            get;
            set;
        }
    }
}
