﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("FacetTree")]
    [DataContract(Name = "FacetTree")]
    public class AnCatFacetTree
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute("FacetTree")]
        [DataMember(Name = "FacetTree")]
        public List<AnCatFacetTree> ChildrenNode { get; set; }
    }
}
