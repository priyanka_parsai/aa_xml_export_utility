﻿namespace RSC.Database.Api.Common.DataContracts
{
    public class ReactionSearchCriteria : SearchCriteria
    {
        #region Properties


        public string AFreeText
        {
            get;
            set;
        }

        public string Authors
        {
            get;
            set;
        }

        public string CreatedBy
        {
            get;
            set;
        }

        public string CreatedDate1
        {
            get;
            set;
        }

        public string CreatedDate2
        {
            get;
            set;
        }

        public string Doi
        {
            get;
            set;
        }

        public string FPage
        {
            get;
            set;
        }

        public string FromDate
        {
            get;
            set;
        }

        public bool FullText
        {
            get;
            set;
        }

        public string Id
        {
            get;
            set;
        }

        public bool IsCatalyticReactions
        {
            get;
            set;
        }

        public bool IsStereoselectiveReactions
        {
            get;
            set;
        }

        public string JournalTitle
        {
            get;
            set;
        }

        public string ModifiedBy
        {
            get;
            set;
        }

        public string ModifiedDate1
        {
            get;
            set;
        }

        public string ModifiedDate2
        {
            get;
            set;
        }

        public string PublicId
        {
            get;
            set;
        }

        public string Reaction
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string ToDate
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Uri
        {
            get;
            set;
        }

        public string VersionNumber
        {
            get;
            set;
        }

        public string Volume
        {
            get;
            set;
        }

        public string Year
        {
            get;
            set;
        }
        public string Status
        {
            get;
            set;
        }

        public string PubmedId
        {
            get;
            set;
        }

        public  string User
        {
            get;
            set;
        }
        #endregion Properties
    }
}
