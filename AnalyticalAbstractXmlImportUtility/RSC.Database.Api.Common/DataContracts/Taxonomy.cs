﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class Taxonomy
    {
        [DataMember()]
        public NameValue Phylum
        {
            get;
            set;
        }
        [DataMember()]
        [XmlElement]
        public NameValue Class
        {
            get;
            set;
        }

        [DataMember]
        [XmlElement]
        public NameValue Order
        {
            get;
            set;
        }
        [DataMember]
        [XmlElement]
        public NameValue Family
        {
            get;
            set;
        }
        [DataMember]
        public NameValue Genus
        {
            get;
            set;
        }
        [DataMember]
        [XmlElement]
        public NameValue Species
        {
            get;
            set;
        }
        [DataMember]
        [XmlElement]
        public bool IsSourceOfCompound
        {
            get;
            set;
        }
    }
}
