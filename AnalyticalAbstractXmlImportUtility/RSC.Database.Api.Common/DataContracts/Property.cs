﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    public class Property
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("PropertyDetail", IsNullable = true)]
        [DataMember]
        public List<PropertyDetail> Details{get;set;}

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Name{get;set;}

    }
    
    [DataContract]
    public class PropertyDetail
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Value{get;set;}

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string LogEValue{get;set;}

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Unit{get;set;}

    }
}
