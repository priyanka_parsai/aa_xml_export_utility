﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    [System.Xml.Serialization.XmlRootAttribute()]
    public class Spectrum
    {
        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string RscId { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SystemId { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Title { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TitleNoMarkup { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string IsTermsNConditionsAccepted { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Description { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DescriptionNoMarkup { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Source { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public int StatusCode { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Status { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CreatedDate { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UpdatedDate { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public Security Security { get; set; }
    }
}
