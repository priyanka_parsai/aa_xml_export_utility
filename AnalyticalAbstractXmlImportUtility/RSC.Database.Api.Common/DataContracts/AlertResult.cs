﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    [DataContract]
    public class AlertResult
    {
        [System.Xml.Serialization.XmlArrayItemAttribute()]
        [DataMember]
        public List<Alert> Alerts { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string TotalResults { get; set; }

        public AlertResult()
        {

        }

        public AlertResult(List<Alert> _alerts, string _totalResults)
        {
            this.Alerts = _alerts;
            this.TotalResults = _totalResults;
        }
    }
}
