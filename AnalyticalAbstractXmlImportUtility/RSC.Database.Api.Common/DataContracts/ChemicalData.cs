﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    [System.Xml.Serialization.XmlRootAttribute()]
    public class ChemicalData
    {
        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string RscId { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SystemId { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Title { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string TitleNoMarkup { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Description { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string DescriptionNoMarkup { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public int StatusCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Status { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Source { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string IsTermsNConditionsAccepted{get;set;}

        [System.Xml.Serialization.XmlArrayAttribute("Authors")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Author")]
        [DataMember(Name="Author")]
        public List<Author> Authors { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("Tags")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Tag")]
        [DataMember(Name = "Tag")]
        public List<string> Tags { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string CreatedDate { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Licence { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string UpdatedDate { get;set;}

        [System.Xml.Serialization.XmlArrayAttribute("Reactions")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Reaction")]
        [DataMember(Name = "Reaction")]
        public List<string> Reactions { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("Substances")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Substance")]
        [DataMember(Name = "Substance")]
        public List<string> Substances { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("Spectra")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Spectrum")]
        [DataMember(Name = "Spectrum")]
        public List<string> Spectra { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("security")]
        public Security Security { get; set; }
    }
}
