﻿namespace RSC.Database.Api.Common.DataContracts
{
    public class SearchCriteria
    {
        public SearchCriteria()
        {
            //TODO: Consider loading defaults from a config file or if we need to leave it up to the client
            Keywords = string.Empty;
            PageNo = 1;
            //PageSize = 10;
            StatusCode = null;
        }

        public string Keywords { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string SortBy { get; set; }
        public string OrderDir { get; set; }
        public int? StatusCode { get; set; }
        public string RscId { get; set; }
    }
}
