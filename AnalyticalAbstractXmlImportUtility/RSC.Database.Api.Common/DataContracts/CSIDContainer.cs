﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;


namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable]
    [DataContract]
    public class CSIDContainer
    {
        [DataMember]
        [System.Xml.Serialization.XmlArrayItemAttribute()]
        [JsonProperty("json")]
        public List<string> CSID { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [JsonProperty("total")]
        public string TotalCount { get; set; }
    }
}
