﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using RSC.Database.Api.Common.DataContracts;

// 
// This source code was auto-generated by xsd, Version=4.0.30319.17929.

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
[DataContract(Namespace = "")]
public partial class Substances
{
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public List<Substance> SubstanceList
    {
        get;
        set;
    }
}
/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
[DataContract(Namespace = "")]
public partial class Substance
{
    [XmlIgnore]
    [IgnoreDataMember]
    public int Index { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string SystemId
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string DepositionBatchId
    {
        get;
        set;
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ArticleId
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ArticleCompoundNo { get; set; }


    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Source { get; set; }

    [System.Xml.Serialization.XmlArrayItemAttribute("Synonym", IsNullable = true)]
    [DataMember]
    public List<string> Synonyms
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string URI
    {
        get;
        set;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string MolecularFormula
    {
        get;
        set;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string MolecularWeight
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string AverageMass
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string MonoisotopicMass
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string NominalMass
    {
        get;
        set;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Title
    {
        get;
        set;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string CSId
    {

        get;
        set;
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Inchi
    {

        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string InchiKey
    {

        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Smiles
    {

        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ALogP
    {
        get;
        set;
    }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ThesisID { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ThesisCompoundNo { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string RealSampleAvailable { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Chiral { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Diastereoselective { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool Racemic { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool IR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool HNMR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool CNMR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool FNMR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool PNMR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool BNMR { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool FullNMRAssignment { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool MZ { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool HRMS { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool ElementalAnalysis { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool MeltingPoint { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool BoilingPoint { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool OpticalRotation { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool CrystalStructureData { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public bool BiologicalTestingData { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Comments { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string RscId { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string PublicId { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string PrintId { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ExternalId { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Type { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string CASRegNo { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ACDName { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string CASName { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string TradeMarks { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Composition { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string ManufacturerCode { get; set; }

    [System.Xml.Serialization.XmlArrayItemAttribute("Location", IsNullable = true)]
    [DataMember]
    public List<Location> Locations { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public Image Image { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public LiteratureRereference LiteratureReferences { get; set; }

    [System.Xml.Serialization.XmlArrayItemAttribute("Property", IsNullable = true)]
    [DataMember]
    public List<Property> CsProperties { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Use { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Note { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Keywords { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string VetKeywords { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public CSData CSData{get;set;}

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public Security Security{get;set;}

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string CreatedDate { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string UpdatedDate { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public int StatusCode{get;set;}

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Status { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string TitleNoMarkup { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string Description { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string DescriptionNoMarkup { get; set; }

    [System.Xml.Serialization.XmlElementAttribute()]
    [DataMember]
    public string PublishedDate { get; set; }
}
