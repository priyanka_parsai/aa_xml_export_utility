﻿using System.Runtime.Serialization;
using RSC.Database.Api.Common.Helpers;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract(Name = "ownership", Namespace = "")]
    public class Ownership
    {
        [System.Xml.Serialization.XmlAttributeAttribute("type")]
        [DataMember(Name = "type")]
        public OwnershipType OwnershipType { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        [DataMember(Name = "id")]
        public string Id { get; set; }
    }
}
