﻿namespace RSC.Database.Api.Common.DataContracts
{
    public class ValidationError
    {
        public string Field { get; set; }
        public string ValidationMessage { get; set; }
    }
}
