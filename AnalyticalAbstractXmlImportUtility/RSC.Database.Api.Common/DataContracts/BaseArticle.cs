﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable]
    [DataContract]
    public class BaseArticle
    {
        #region Public Properties

        [XmlArrayAttribute("Authors")]
        [XmlArrayItemAttribute("Author", IsNullable = true)]
        [DataMember(Name = "Author")]
        public List<Author> Authors { get; set; }

        [XmlElement]
        [DataMember]
        public string CreatedBy { get; set; }

        [XmlElement]
        [DataMember]
        public string CreatedDate { get; set; }

        [XmlIgnore]
        [DataMember]
        public string Description { get; set; }

        [XmlElement("Description")]
        public XmlCDataSection DescriptionWithXml
        {
            get
            {
                return new XmlDocument().CreateCDataSection(HttpUtility.HtmlDecode(Description));
            }
            set
            {
                Description = value.Value;
            }
        }

        [XmlElement]
        [DataMember]
        public string Doi { get; set; }

        [XmlElement]
        [DataMember]
        public string FirstPage { get; set; }

        [XmlElement]
        [DataMember]
        public string Issue { get; set; }

        [XmlElement]
        [DataMember]
        public string Journal { get; set; }

        [XmlElement]
        [DataMember]
        public string Language { get; set; }

        [XmlElement]
        [DataMember]
        public string LastPage { get; set; }

        [XmlElement]
        [DataMember]
        public string ModifiedBy { get; set; }

        [XmlElement]
        [DataMember]
        public string ModifiedDate { get; set; }

        [XmlElement]
        [DataMember]
        public string PrintId { get; set; }

        [XmlElement]
        [DataMember]
        public string PublicId { get; set; }

        [XmlElement]
        [DataMember]
        public string PublishedBy { get; set; }

        [XmlElement]
        [DataMember]
        public string PublishedDate { get; set; }

        [XmlElement]
        [DataMember]
        public string ShortTitle { get; set; }

        [XmlElement]
        [DataMember]
        public string Status { get; set; }

        [XmlElement]
        [DataMember]
        public string SystemId { get; set; }

        [XmlIgnore]
        [DataMember]
        public string Title { get; set; }

        [XmlElement("Title")]
        public XmlCDataSection TitleWithXml
        {
            get
            {
                return new XmlDocument().CreateCDataSection(HttpUtility.HtmlDecode(Title));
            }
            set
            {
                Title = value.Value;
            }
        }

        [XmlElement]
        [DataMember]
        public string Volume { get; set; }

        [XmlElement]
        [DataMember]
        public string Year { get; set; }

        [XmlElement]
        [DataMember]
        public string PubmedId { get; set; }

        #endregion Public Properties
    }
}