﻿using System;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract(Name = "embargo")]
    public class Embargo
    {
        [System.Xml.Serialization.XmlElementAttribute("startdate")]
        [DataMember(Name = "startdate")]
        public DateTime StartDate { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("enddate")]
        [DataMember(Name = "enddate")]
        public DateTime EndDate { get; set; }

    }
}
