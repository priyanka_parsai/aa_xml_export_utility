﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("JournalCategoryTree")]
    [DataContract(Namespace = "", Name = "JournalCategoryTree")]
    public class JournalCategoryTreeAA
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [System.Xml.Serialization.XmlElementAttribute("JournalCategoryTree")]
        [DataMember(Name = "JournalCategoryTree")]
        public List<JournalCategoryTreeAA> ChildrenNode { get; set; }
    }
}
