﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{

    [Serializable, XmlRoot("errors")]
        [DataContract(Name = "errors")]
        public class Errors
        {
            [XmlElement("error")]
            [DataMember(Name = "error")]
            public List<Error> ErrorList
            {
                get;
                set;
            }

            public List<ValidationError> ValidationErrors
            {
                get;
                set;
            }
        }
    
}
