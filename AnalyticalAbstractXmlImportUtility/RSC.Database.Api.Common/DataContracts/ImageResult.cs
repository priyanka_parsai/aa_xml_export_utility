﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    public class ImageResult
    {
        [System.Xml.Serialization.XmlAttributeAttribute("type")]
        [DataMember(Name="type")]
        public string ImageType { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute("src")]
        [DataMember(Name= "src")]
        public string ImageSrc { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute("alt")]
        [DataMember(Name="alt")]
        public string ImageAlt { get; set; }
    }
}
