﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    public class LiteratureRereference
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public List<JournalCitation> JournalCitations { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string DOI { get; set; }

    }

    [DataContract]
    public class JournalCitation
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public List<CitationAuthor> Authors { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Title { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Year { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Volume { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string FirstPage { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string LastPage { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string DOI { get; set; }

    }

    [DataContract]
    public class CitationAuthor
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string FirstName { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Surname { get; set; }

    }
}
