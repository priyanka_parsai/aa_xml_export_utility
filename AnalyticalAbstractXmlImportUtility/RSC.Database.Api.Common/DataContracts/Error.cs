﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("error")]
    [DataContract(Name = "error")]
    public class Error
    {
        [XmlAttribute("code")]
        [DataMember(Name = "code")]
        public int Code
        {
            get;
            set;
        }
        [XmlText()]
        [DataMember]
        public string Name
        {
            get;
            set;
        }

    }
}
