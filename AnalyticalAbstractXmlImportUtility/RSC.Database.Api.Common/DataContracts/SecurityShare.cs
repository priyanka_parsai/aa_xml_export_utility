﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract(Name = "share")]
    public class SecurityShare
    {
        [System.Xml.Serialization.XmlElementAttribute("with")]
        [DataMember(Name = "with")]
        public string With { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("access")]
        [DataMember(Name = "access")]
        public string Access { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("id")]
        [DataMember(Name = "id")]
        public string Id { get; set; }
    }
}
