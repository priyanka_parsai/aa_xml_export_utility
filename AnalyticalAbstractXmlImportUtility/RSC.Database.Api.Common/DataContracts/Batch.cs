﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [System.Xml.Serialization.XmlRoot("depositionbatch")]
    [DataContract(Name="depositionbatch")]
    public class Batch
    {
        [System.Xml.Serialization.XmlElementAttribute("id")]
        [DataMember(Name="id")]
        public string Id { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("submitter")]
        [DataMember(Name="submitter")]
        public Submitter Submitter { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("collection")]
        [DataMember(Name="collection")]
        public Collection Collection { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("files")]
        [System.Xml.Serialization.XmlArrayItemAttribute("file")]
        [DataMember(Name="file")]
        public List<DepositedFile> Files { get;set;}
    }

    [System.Xml.Serialization.XmlRoot()]
    [DataContract]
    public class DepositedFile 
    {
        [System.Xml.Serialization.XmlElementAttribute("id")]
        [DataMember(Name="id")]
        public string Id { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("type")]
        [DataMember(Name="type")]
        public string Type { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("format")]
        [DataMember(Name="format")]
        public string Format { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("path")]
        [DataMember(Name="path")]
        public string Path { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("externalid")]
        [DataMember(Name="externalid")]
        public string ExternalId { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("status")]
        [DataMember(Name="status")]
        public string Status { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("message")]
        [DataMember(Name="message")]
        public string Message { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("statusupdatedon")]
        [DataMember(Name="statusupdatedon")]
        public string StatusUpdatedOn { get; set; }
    }

    [System.Xml.Serialization.XmlRoot()]
    [DataContract]
    public class Collection
    {
        [System.Xml.Serialization.XmlElementAttribute("id")]
        [DataMember(Name="id")]
        public string Id{get;set;}

        [System.Xml.Serialization.XmlElementAttribute("name")]
        [DataMember(Name="name")]
        public string Name{get;set;}
    }
}
