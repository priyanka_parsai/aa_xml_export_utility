﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    interface INPUSearchCriteria
    {
        [DataMember]
        string BiolAct { get; set; }

        [DataMember]
        string CmpdClass { get; set; }

        [DataMember]
        string NatProd { get; set; }

        [DataMember]
        string NonPlant { get; set; }

        [DataMember]
        string FromDate { get; set; }

        [DataMember]
        string ToDate { get; set; }

        [DataMember]
        string ArticleType { get; set; }
    }
}
