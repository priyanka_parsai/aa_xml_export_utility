﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [DataContract(Namespace = "")]
    public partial class Information
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public ArticleLocation Location
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Email
        {
            get;
            set;
        }
    }
}
