﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [DataContract(Namespace = "")]
    public partial class ArticleLocation
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string OrganizationName
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Address
        {
            get;
            set;
        }
    }
}
