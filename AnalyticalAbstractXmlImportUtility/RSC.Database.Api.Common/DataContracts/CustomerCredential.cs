﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("CustomerCredential")]
    [DataContract(Namespace = "", Name = "CustomerCredential")]
    public class CustomerCredential
    {
        /// <summary>
        /// Subscriber user name
        /// </summary>
        [DataMember]
        [XmlElement("UserName")]
        public string UserName
        {
            get;
            set;
        }
        /// <summary>
        /// Subscriber Password
        /// </summary>
        [DataMember]
        [XmlElement("Password")]
        public string Password
        {
            get;
            set;
        }
        /// <summary>
        /// IP address
        /// </summary>
        [DataMember]
        [XmlElement("IPAddress")]
        public string IPAddress
        {
            get;
            set;
        }
        /// <summary>
        /// Current accessing platform Id
        /// </summary>
        [DataMember]
        [XmlElement("PlatformID")]
        public string PlatformID
        {
            get;
            set;
        }
        /// <summary>
        /// urlrefernce
        /// </summary>
        [DataMember]
        [XmlElement("URLRefference")]
        public string URLRefference
        {
            get;
            set;
        }
        /// <summary>
        /// User RSC id
        /// </summary>
        [DataMember]
        [XmlElement("RSCID")]
        public string RSCID
        {
            get;
            set;
        }
        /// <summary>
        /// Current session id
        /// </summary>
        [DataMember]
        [XmlElement("SessionID")]
        public string SessionID
        {
            get;
            set;
        }
        /// <summary>
        /// Athense ID
        /// </summary>
        [DataMember]
        [XmlElement("AthensID")]
        public string AthensID
        {
            get;
            set;
        }
        /// <summary>
        /// Shibboleth id
        /// </summary>
        [DataMember]
        [XmlElement("ShibbolethID")]
        public string ShibbolethID
        {
            get;
            set;
        }


    }
}
