﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable]
    [DataContract]
    public class FacetTree
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public ushort Count
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute("FacetTree")]
        [DataMember(Name = "FacetTree")]
        public List<FacetTree> ChildrenNode { get; set; }
    }
}
