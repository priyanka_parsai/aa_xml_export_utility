﻿using System.ComponentModel;

namespace RSC.Database.Api.Common.DataContracts
{
    public class JournalSearchCriteria
    {
        public string Id { get; set; }
        public string IssnPrint { get; set; }
        public string IssnOnline { get; set; }
        public string Sercode { get; set; }
        public string Uri { get; set; }
        [DefaultValue(1)]
        public int PageNo { get; set; }
        [DefaultValue("10")]
        public string PageSize { get; set; }
    }
}
