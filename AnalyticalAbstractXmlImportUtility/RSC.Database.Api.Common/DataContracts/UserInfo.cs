﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("Users")]
    [DataContract(Name = "Users")]
    public class UserInfo
    {
        [System.Xml.Serialization.XmlElementAttribute("user")]
        [DataMember(Name = "user")]
        public List<string> user { get; set; }
    }
      
}



