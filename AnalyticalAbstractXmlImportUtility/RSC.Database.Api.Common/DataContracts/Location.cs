﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable]
    [DataContract]
    public class Location
    {
        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Longitude { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Latitude { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Depth { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public string QF { get; set; }

        [System.Xml.Serialization.XmlElementAttribute()]
        [DataMember]
        public List<Substance> SubstanceList { get; set; }
    }
}
