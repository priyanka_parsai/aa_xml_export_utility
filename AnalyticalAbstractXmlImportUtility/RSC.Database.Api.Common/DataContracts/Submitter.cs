﻿using System;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract(Name = "submitter")]
    public class Submitter
    {
        [System.Xml.Serialization.XmlElementAttribute("rscid")]
        [DataMember(Name = "rscid")]
        public Guid RSCId { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("email")]
        [DataMember(Name = "email")]
        public string EmailId{ get; set; }

    }
}
