﻿namespace RSC.Database.Api.Common.DataContracts
{
    public class ProfileSearchCriteria:SearchCriteria
    {
        public string Id { get; set; }
        public string Uri { get; set; }
        public string Name { get; set; }
        public string Organization { get; set; }
    }
}
