﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.DataContracts
{
    /// <summary>
    /// Splits up the accept header into its various components
    /// Eg. application/vnd.rsc.articles-v1+json
    /// Content = application/vnd.rsc.articles
    /// Version = v1
    /// Format = json
    /// </summary>
    public class ApiAcceptHeader
    {
        public ApiAcceptHeader()
        { }

        public ApiAcceptHeader(string acceptHeader)
        {
            ParseAcceptHeader(acceptHeader);
        }

        public string Content { get; set; }
        public string Version { get; set; }
        public string Format { get; set; }

        private void ParseAcceptHeader(string acceptHeader)
        {
            if (string.IsNullOrWhiteSpace(acceptHeader))
                return;

            var parts = acceptHeader.Split('+');
            if (parts.Length == 2)
            {
                Format = parts[1].Trim();
            }
            var subPart = parts[0].Split('-');
            if (subPart.Length == 2)
            {
                Version = subPart[1].Trim();
            }
            Content = subPart[0].Trim();
        }
        
        public override string ToString()
        {
            var acceptHeader = new StringBuilder(Content);
            
            if (!string.IsNullOrWhiteSpace(Version))
                acceptHeader.AppendFormat("-{0}", Version);

            if (!string.IsNullOrWhiteSpace(Format))
                acceptHeader.AppendFormat("+{0}", Format);

            return acceptHeader.ToString();
        }
    }
}
