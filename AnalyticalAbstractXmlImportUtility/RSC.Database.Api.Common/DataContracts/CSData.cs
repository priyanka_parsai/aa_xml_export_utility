﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
   [DataContract(Namespace = "")]
    public class CSData
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string MLStatus { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Group", IsNullable = true)]
        [DataMember]
        public List<Group> Groups { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public NMRData CNMRData { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public NMRData HNMRData { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("MonomerId", IsNullable = false)]
        [DataMember]
        public List<string> MonomerIds { get; set; }

    }

     [DataContract(Namespace = "")]
    public class Group
    {
        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Value { get; set; }

    }

   [DataContract(Namespace = "")]
    public class NMRData
    {

        [System.Xml.Serialization.XmlArrayItemAttribute("NMRPeak", IsNullable = true)]
        [DataMember]
        public List<NMRPeak> NMRPeaks { get; set; }

        [System.Xml.Serialization.XmlElementAttribute]
        [DataMember]
        public string Nuclei { get; set; }
    }

    [DataContract(Namespace = "")]
    public class NMRPeak
    {
        [System.Xml.Serialization.XmlAttributeAttribute]
        [DataMember]
        public string Locant { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute]
        [DataMember]
        public string Var { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute]
        [DataMember]
        public string Protons { get; set; }

        [System.Xml.Serialization.XmlTextAttribute]
        [DataMember]
        public string Value { get; set; }
    }
}
