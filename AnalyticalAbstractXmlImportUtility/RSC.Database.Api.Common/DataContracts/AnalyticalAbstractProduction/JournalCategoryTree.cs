﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    [Serializable]
  [DataContract]
    public class JournalCategoryTree
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public ushort Count
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute("JournalCategoryTree")]
        [DataMember(Name = "JournalCategoryTree")]
        public List<JournalCategoryTree> ChildrenNode { get; set; }
    }
}
