﻿using System.Collections.Generic;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class Facet
    {
        #region Public Constructors

        public Facet()
        {
            FacetValues = new List<FacetValue>();
        }

        #endregion Public Constructors

        #region Public Properties
       
        public List<FacetValue> FacetValues { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        #endregion Public Properties
    }
}