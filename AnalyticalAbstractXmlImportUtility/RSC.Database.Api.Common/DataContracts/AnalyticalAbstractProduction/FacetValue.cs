﻿namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class FacetValue
    {
        #region Public Properties

        public string Count { get; set; }

        public bool IsSelected { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        #endregion Public Properties
    }
}