﻿using System.Collections.Generic;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class Results
    {
        #region Public Properties
      
        public long PageNumber { get; set; }

        public long PageSize { get; set; }

        public List<Result> Result { get; set; }

        public long TotalCount { get; set; }

        #endregion Public Properties
    }
}