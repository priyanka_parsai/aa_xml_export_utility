﻿using System.ComponentModel;
namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public enum DocumentStatus
    {
        [Description("registration")]
        Registration,
        [Description("generation")]
        Generation,
        [Description("review")]
        Review,
        [Description("approved")]
        Approved,
        [Description("rejected")]
        Rejected,
        [Description("published")]
        Published,
        [Description("unpublished")]
        Unpublished,
        [Description("discard")]
        Discard
    }
}
