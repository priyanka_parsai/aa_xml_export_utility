﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [DataContract(Namespace = "")]
    public partial class Analyte
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMember]
        public string CASNo
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlText()]
        [DataMember]
        public string Value
        {
            get;
            set;
        }
    }

}
