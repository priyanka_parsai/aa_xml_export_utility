﻿namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class SearchCriteria
    {
        public SearchCriteria()
        {
            PageNo = 1;
            PageSize = 50;
        }

        public string Id { get; set; }

        public string PublicId { get; set; }

        public string Doi { get; set; }

        public string Title { get; set; }

        public string PubmedId { get; set; }

        public string User { get; set; }

        public string Uri { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public string SortBy { get; set; }

        public string OrderDirection { get; set; }

        public string JournalTitle { get; set; }

        public string Authors { get; set; }

        public string Type { get; set; }

        public string AFreeText { get; set; }

        public string Status { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string CreatedDate1 { get; set; }

        public string CreatedDate2 { get; set; }

        public string ModifiedDate1 { get; set; }

        public string ModifiedDate2 { get; set; }

        public string Analyte { get; set; }

        public string Matrix { get; set; }

        public string Concept { get; set; }

        public string Category { get; set; }

        public string PrintId { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string SortByDirection { get; set; }

        public bool IgnoreArticleDetails { get; set; }
    }
}