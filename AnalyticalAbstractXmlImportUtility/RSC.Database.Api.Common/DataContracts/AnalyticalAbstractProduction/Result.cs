﻿namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class Result
    {
        #region Public Properties

        public Article Article { get; set; }

        public Substance Substance { get; set; }
      
        #endregion Public Properties
    }
}