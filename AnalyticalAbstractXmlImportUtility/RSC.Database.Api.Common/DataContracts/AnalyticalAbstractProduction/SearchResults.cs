﻿using System.Collections.Generic;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    public class SearchResults
    {
        public SearchResults()
        {
            Facets = new List<Facet>();
            Criteria = new SearchCriteria();
            Results = new Results();
        }
        
        public List<Facet> Facets { get; set; }

        public Results Results { get; set; }

        public SearchCriteria Criteria { get; set; }
    }
}
