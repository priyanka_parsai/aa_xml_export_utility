﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    [Serializable]
    public class Category
    {
        public string Id { get; set; }

        public string Name { get; set; }

        [XmlArray("SubCategories")]
        [XmlArrayItem("Category", IsNullable = false)]
        public List<Category> SubCategories { get; set; }
    }

}
