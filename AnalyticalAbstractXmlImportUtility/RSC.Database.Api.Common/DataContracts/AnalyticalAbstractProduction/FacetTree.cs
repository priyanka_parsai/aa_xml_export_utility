﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    [Serializable]
    [DataContract]
    [XmlRoot("FacetTree")]
    public class AAFacetTree
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get;
            set;
        }
        [System.Xml.Serialization.XmlElementAttribute("FacetTree")]
        [DataMember(Name = "FacetTree")]
        public List<AAFacetTree> ChildrenNode { get; set; }
    }
}
