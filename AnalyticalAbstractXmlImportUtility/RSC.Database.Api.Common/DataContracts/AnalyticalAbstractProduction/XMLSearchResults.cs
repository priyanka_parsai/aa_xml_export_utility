﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    [Serializable]
    [XmlRoot("Monographs")]
    public class XMLSearchResults
    {
        [XmlArray]
        public List<string> Monograph { get; set; }
    }
}
