﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction
{
    [Serializable]
    [DataContract(Namespace = "")]
   public class NameValues
    {
        public List<NameValue> NameValuesList { get; set; }

        public long TotalCount { get; set; }
    }
}
