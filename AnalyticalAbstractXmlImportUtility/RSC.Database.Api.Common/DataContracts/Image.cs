﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    public class Image
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public bool Cdx { get; set; }

        [DataMember]
        public bool Tif { get; set; }

        [DataMember]
        public bool Mol { get; set; }
    }
}
