﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RSC.AuthSystem.ServiceLayer.AccessControlEngine.Proxy;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("CustomerInfo")]
    [DataContract(Namespace = "", Name = "CustomerInfo")]
    public class CustomerInfo
    {
        [DataMember]
        [XmlElement("AuthSessionID")]
        public string AuthSessionID
        {
            get;
            set;
        }
        [DataMember]
        [XmlElement("Organisation")]
        public Organisation Organisation
        {
            get;
            set;
        }
    }
}
