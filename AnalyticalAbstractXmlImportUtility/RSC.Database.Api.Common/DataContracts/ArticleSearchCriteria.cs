﻿using System.Collections.Generic;

namespace RSC.Database.Api.Common.DataContracts
{
    public class ArticleSearchCriteria : SearchCriteria, INPUSearchCriteria, IMarinLitSearchCriteria
    {
        public ArticleSearchCriteria()
        {
            Institutes = new List<string>();
            Publishers = new List<string>();
            Subjects = new List<string>();
            ArticleTypes = new List<string>();
            Journals = new List<string>();
        }

        public string Id { get; set; }

        public string Uri { get; set; }

        public string Doi { get; set; }

        public string PrintId { get; set; }

        public bool FullText { get; set; }

        public List<string> Institutes { get; set; }

        public List<string> Publishers { get; set; }

        public List<string> Subjects { get; set; }

        public List<string> ArticleTypes { get; set; }

        public List<string> Journals { get; set; }

        #region IMarinLitSearchCriteria Implementation

        public string AFreeText
        {
            get;
            set;
        }

        public string SFreeText
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string CSID
        {
            get;
            set;
        }

        public string JournalTitle
        {
            get;
            set;
        }

        public string Year
        {
            get;
            set;
        }

        public string Volume
        {
            get;
            set;
        }

        public string FPage
        {
            get;
            set;
        }

        public string Authors
        {
            get;
            set;
        }

        public string Phylum
        {
            get;
            set;
        }

        public string Class
        {
            get;
            set;
        }

        public string Order
        {
            get;
            set;
        }

        public string Family
        {
            get;
            set;
        }

        public string Genus
        {
            get;
            set;
        }

        public string Species
        {
            get;
            set;
        }

        public string IsTaxCompound
        {
            get;
            set;
        }

        public string MarineCat
        {
            get;
            set;
        }

        public string TrivialName
        {
            get;
            set;
        }

        public string SortBy
        {
            get;
            set;
        }

        public string VersionNumber
        {
            get;
            set;
        }

        public string PublicId
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string Formula
        {
            get;
            set;
        }

        public string Weight
        {
            get;
            set;
        }

        public string UVMax
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string met
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string mets
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string metd
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string mett
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string arome
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string vnme
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string meo
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string men
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string mes
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string mene
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string mine
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string sp2h
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cc
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string vn
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cch
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string dic
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cc3
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string tlk
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cao
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string c2ho
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string c1ho
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string ch2o2
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cho2
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string co2
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string co
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string cho
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string acy
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string aco
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string am
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string chn
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string nit
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string initr
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string ben
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b1
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b12
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b13
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b14
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b123
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b124
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b135
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b1234
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b1235
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b1245
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b12345
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string b1_6
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p2
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p3
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p4
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p23
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p24
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p25
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p26
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p34
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p35
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p234
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p235
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p236
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p246
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p345
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p2345
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p2346
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p2356
        {
            get;
            set;
        }

        [FunctionalGroup]
        public string p2_6
        {
            get;
            set;
        }

        public string CNMR
        {
            get;
            set;
        }

        public string HNMR
        {
            get;
            set;
        }

        public string AtoZ
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string ancat
        {
            get;
            set;
        }

        public string analyte
        {
            get;
            set;
        }

        public string concept
        {
            get;
            set;
        }

        public string matrix
        {
            get;
            set;
        }

        public string PublishedDate
        {
            get;
            set;
        }

        public string Taxonomy
        {
            get;
            set;
        }

        public string GeoLoc
        {
            get;
            set;
        }

        #endregion IMarinLitSearchCriteria Implementation

        #region INPUSearchCriteria Implementation

        public string BiolAct { get; set; }

        public string CmpdClass { get; set; }

        public string NatProd { get; set; }

        public string NonPlant { get; set; }

        public string ArticleType { get; set; }

        #endregion INPUSearchCriteria Implementation

        public string DocStatus
        {
            get;
            set;
        }

        public string PubmedId
        {
            get;
            set;
        }

        public string User
        {
            get;
            set;
        }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string CreatedDate1 { get; set; }

        public string CreatedDate2 { get; set; }

        public string ModifiedDate1 { get; set; }

        public string ModifiedDate2 { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string DateRangeMode { get; set; }

        public string DateRangeBands { get; set; }

        public string DateRangeType { get; set; }

        //whether the article is review or not
        public bool IsReview { get; set; }

    }
}