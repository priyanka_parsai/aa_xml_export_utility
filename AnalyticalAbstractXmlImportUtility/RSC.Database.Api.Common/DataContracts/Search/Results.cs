﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.Search
{
    public class Results<T>
    {
        [XmlAttribute()]
        [DataMember]
        public string Href
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public long PageNo
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public long PageSize
        {
            get;
            set;
        }

        [XmlElementAttribute("Result")]
        [DataMember(Name = "Result")]
        public List<Result<T>> Result
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public long TotalCount
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public string LastUpdated
        {
            get;
            set;
        }
    }
}
