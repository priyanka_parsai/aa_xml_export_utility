﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.Search
{
    public class FacetValue
    {
        #region Public Properties

        [XmlAttributeAttribute()]
        [DataMember]
        public string Count
        {
            get;
            set;
        }

        [XmlAttribute()]
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [XmlTextAttribute()]
        [DataMember]
        public string Value
        {
            get;
            set;
        }

        #endregion Public Properties
    }
}
