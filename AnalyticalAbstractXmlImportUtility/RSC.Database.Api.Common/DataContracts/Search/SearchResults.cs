﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.Search
{
    [Serializable]
    [DataContract(Name = "SearchResults")]
    [XmlRoot("SearchResults")]
    public class SearchResults<T>
    {
        [XmlArrayItem("Facet")]
        [DataMember(Name = "Facet")]
        public List<Facet> Facets
        {
            get;
            set;
        }

        [XmlElement]
        [DataMember]
        public Results<T> Results
        {
            get;
            set;
        }
    }
}
