﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.Search
{
    public class Result<T>
    {
        #region Public Properties

        [XmlElement()]
        [DataMember]
        public T Article { get; set; }

        [XmlElement()]
        [DataMember]
        public T Reaction { get; set; }

        [XmlElement()]
        [DataMember]
        public T Substance { get; set; }

        [XmlElement()]
        [DataMember]
        public int Index
        {
            get;
            set;
        }

        [XmlAttribute()]
        [DataMember]
        public string Uri
        {
            get;
            set;
        }

        [XmlAttribute()]
        [DataMember]
        public string PublicId
        {
            get;
            set;
        }

        #endregion Public Properties
    }
}
