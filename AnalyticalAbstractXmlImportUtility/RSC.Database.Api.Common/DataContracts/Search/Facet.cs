﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts.Search
{
    public class Facet
    {
        #region Public Properties

        [XmlElementAttribute("FacetValue")]
        [DataMember(Name = "FacetValue")]
        public List<FacetValue> FacetValues
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [XmlAttributeAttribute()]
        [DataMember]
        public string Type
        {
            get;
            set;
        }

        #endregion Public Properties
    }
}