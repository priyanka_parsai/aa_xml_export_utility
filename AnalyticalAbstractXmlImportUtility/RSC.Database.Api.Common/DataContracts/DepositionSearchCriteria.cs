﻿using System.Collections.Generic;

namespace RSC.Database.Api.Common.DataContracts
{
    public class DepositionSearchCriteria:SearchCriteria
    {
        
        public List<string> StatusList { get; set; }
        public List<string> SourceList { get; set; }

        public DepositionSearchCriteria()
        {
            StatusList = new List<string>();
            SourceList = new List<string>();
        }
    }
}
