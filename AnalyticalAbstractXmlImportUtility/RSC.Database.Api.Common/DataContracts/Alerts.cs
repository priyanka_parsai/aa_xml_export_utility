﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class Alerts
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Alert")]
        [DataMember]
        public List<Alert> AlertList { get; set; }
    }
}