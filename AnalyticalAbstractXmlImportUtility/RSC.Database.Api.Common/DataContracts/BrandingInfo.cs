﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [Serializable, XmlRoot("BrandingInfo")]
    [DataContract(Namespace = "", Name = "BrandingInfo")]
    public class BrandingInfo
    {
        [DataMember]
        [XmlElement("BrandingAvailable")]
        public bool BrandingAvailable { get; set; }

        [DataMember]
        [XmlElement("InstituteId")]
        public string InstituteID { get; set; }

        [DataMember]
        [XmlElement("LibraryName")]
        public string LibraryName { get; set; }

        [DataMember]
        [XmlElement("LibraryURL")]
        public string LibraryURL { get; set; }

        [DataMember]
        [XmlElement("LogoMimeType")]
        public string LogoMimeType { get; set; }

        [DataMember]
        [XmlElement("LibraryLogo")]
        public byte[] LibraryLogo { get; set; }

        [DataMember]
        [XmlElement("LibrarianName")]
        public string LibrarianName { get; set; }

        [DataMember]
        [XmlElement("IsLogoAvailable")]
        public bool IsLogoAvailable { get; set; }
    }
}
