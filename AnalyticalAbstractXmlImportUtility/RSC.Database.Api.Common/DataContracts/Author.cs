﻿using System.Runtime.Serialization;

namespace RSC.Database.Api.Common.DataContracts
{
    [DataContract]
    [System.Xml.Serialization.XmlRootAttribute()]
    public class Author
    {
        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FirstName{get;set;}

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FirstNameNoMarkup { get; set; }

        [DataMember(Name="LastName")]
        [System.Xml.Serialization.XmlElementAttribute("LastName")]
        public string LastName { get; set; }

        [DataMember(Name="LastNameNoMarkup")]
        [System.Xml.Serialization.XmlElementAttribute("LastNameNoMarkup")]
        public string LastNameNoMarkup { get; set; }

        [DataMember]
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Institution { get; set; }
    }
}
