﻿using System.Runtime.Serialization;

namespace RSC.Database.Common
{
    [DataContract]
    public class LookUps
    {
        [DataMember]
        public string systemid { get; set; }
        [DataMember]
        public string publicid { get; set; }
    }
}
