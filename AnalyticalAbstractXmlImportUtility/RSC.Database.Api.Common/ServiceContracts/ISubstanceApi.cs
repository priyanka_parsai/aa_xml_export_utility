﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface ISubstanceApi
    {
        Substance GetSubstanceById(string id);

        Substance GetSubstanceByUri(string uri);

        HttpResponseMessage GetSubstanceImage(string id);

        HttpResponseMessage GetSubstanceImageForDownload(string id);

        HttpResponseMessage GetMolFileForSubstance(string id);
    }
}
