﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface IBrandingApi
    {
        BrandingInfo GetBrandingByInstitute(string id);
    }
}
