﻿using RSC.Database.Api.Common.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface IReactionApi
    {
        Reaction GetReactionById(string id);

        Reaction GetReactionByUri(string uri);

        Reaction GetReactionByPublicId(string pubId);

        HttpResponseMessage GetStructureFileForReaction(string id);

        HttpResponseMessage GetReactionImageForDownload(string id);

        HttpResponseMessage GetReactionImage(string id);
    }
}
