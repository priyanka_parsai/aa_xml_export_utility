﻿using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface IAuthGatewayApi
    {
        #region Public Methods

        CustomerInfo GetCustomerDetails(string authCheckAccessString);

        AuthDetails RequestDecision(string authCheckAccessString);

        AuthDetails TestDecision(string authCheckAccessString);

        #endregion Public Methods
    }
}