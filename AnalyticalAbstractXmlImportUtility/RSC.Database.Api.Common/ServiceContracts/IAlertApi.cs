﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface IAlertApi
    {
        #region Public Methods

        bool DeleteAlert(string alertID);

        List<Alert> GetEmailAlerts(string date, string platformId);

        AlertResult GetUserAlerts(string userId, string platformId, string pageno = null, string pagesize = null);

        bool SaveAlert(string alert);

        #endregion Public Methods
    }
}
