﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface IArticleApi
    {
        article GetArticleById(string id, bool fullText);

        article GetArticleByUri(string uri, bool fullText);

        article GetArticleByPublicId(string pubId, bool fullText);

        string GetCitation(string id, string type);

        HttpResponseMessage GetArticleImage(string id);

        HttpResponseMessage GetArticleImageForDownload(string id);

        HttpResponseMessage GetStructureFileForArticle(string id);
    }
}
