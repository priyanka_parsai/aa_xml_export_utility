﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSC.Database.Api.Common.DataContracts;

namespace RSC.Database.Api.Common.ServiceContracts
{
    public interface ISearchApi
    {
        #region Public Methods

        SearchResults ArticleSearch(ArticleSearchCriteria criteria, string csIdData = null);

        string GetCitations(string systemIds, string type);

        CSIDContainer GetCsIds(SubstanceSearchCriteria criteria);

        T GetFacet<T>(ArticleSearchCriteria criteria, string name, string csIdData = null);

        T GetReactionFacet<T>(ReactionSearchCriteria criteria, string name);

        SearchResults SubstanceSearch(SubstanceSearchCriteria criteria, string csIdData = null);

        #endregion Public Methods
    }
}
