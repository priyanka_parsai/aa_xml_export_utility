﻿using System;
using System.Linq;
using System.Web;

namespace RSC.Database.Api.Common.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Removes the specified characters from the string
        /// </summary>
        /// <param name="value">String value</param>
        /// <param name="charsToRemove">The characters to be removed from the string</param>
        /// <returns>A new string which does not contain the characters specified array</returns>
        public static string RemoveCharacters(this string value, string[] charsToRemove)
        {
            if (string.IsNullOrWhiteSpace(value) || charsToRemove == null)
                return value;

            var result = value;
            foreach (string charToBeRemoved in charsToRemove)
            {
                result = result.Replace(charToBeRemoved, "");
            }

            return result;
        }

        /// <summary>
        /// Removes the specified characters from the string
        /// </summary>
        /// <param name="value">String value</param>
        /// <param name="charsToRemove">A comma seperated string containing the characters that need to be removed</param>
        /// <returns>A new string which does not contain the specified characters</returns>
        public static string RemoveCharacters(this string value, string charsToRemove)
        {
            if (string.IsNullOrWhiteSpace(charsToRemove))
                return value;

            return RemoveCharacters(value, charsToRemove.Split(','));
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static bool IsNotNullOrWhiteSpace(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        /// <summary>
        /// Returns a new string that does not contain whitespaces
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveWhitespaces(this string value)
        {
            return value.Replace(" ", string.Empty);
        }

        public static string GetQueryString<T>(this T obj)
              where T : class
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }
}
