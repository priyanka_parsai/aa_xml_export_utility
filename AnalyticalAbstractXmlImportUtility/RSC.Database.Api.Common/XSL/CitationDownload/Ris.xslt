﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"  xmlns:meta="http://www.rsc.org/schema/tangier/metadata" xmlns:con="http://www.rsc.org/schema/tangier/container" xmlns:mg="http://www.rsc.org/schema/chemical/monograph">
<xsl:output method="text" indent="no" media-type="string"/>
<xsl:include href="RSC.Database.Api.Common.XSL.CitationDownload.SpecialCharacterMapping.xslt" />
<xsl:include href="RSC.Database.Api.Common.XSL.CitationDownload.RISSpecificCharacters.xslt" />
  <!--Use only when debugging Indiviual XSLT
  <xsl:include href="SpecialCharacterMapping.xslt" />
  <xsl:include href="RISSpecificCharacters.xslt" />-->
<xsl:template match="/">
<xsl:apply-templates/>
</xsl:template>
  <xsl:variable name="AllReplacementValues">
    <xsl:call-template name="ReplacementValuesList"/>
    <xsl:call-template name="RISSpecificValuesList"/>
  </xsl:variable>
<xsl:template match="ListofArticles | ListofReactions">
<xsl:for-each select="Article | Reaction">
<xsl:for-each select="con:container">
<xsl:for-each select="con:content">
<xsl:for-each select="mg:monograph">
<xsl:variable name="TY">TY  - JOUR</xsl:variable>
<xsl:variable name="T1">T1  - <xsl:apply-templates  select="mg:title"/></xsl:variable>
<xsl:variable name="N2">N2  - <xsl:apply-templates select="mg:description"/></xsl:variable>
<xsl:variable name="A1">
<xsl:for-each select="mg:literature-references/mg:cit/mg:cit-auth">
<xsl:text>A1  - </xsl:text><xsl:value-of select="mg:surname"/><xsl:value-of select="', '"/><xsl:value-of select="mg:fname"/><xsl:text>&#10;</xsl:text>
</xsl:for-each>
</xsl:variable>
<xsl:variable name="JO">JO  - <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-title"/></xsl:variable>
<xsl:variable name="VL">VL  - <xsl:if test="translate(mg:literature-references/mg:cit/mg:cit-vol,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')!='UNASSIGNED'"><xsl:value-of select="mg:literature-references/mg:cit/mg:cit-vol"/></xsl:if></xsl:variable>
<xsl:variable name="Y1">Y1  - <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-year"/></xsl:variable>
<xsl:variable name="IS">IS  - <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-iss"/></xsl:variable>
<xsl:variable name="SP">SP  - <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-fpage"/></xsl:variable>
<xsl:variable name="EP">EP  - <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-lpage"/></xsl:variable>
<xsl:variable name="DO">DO  - <xsl:value-of select="mg:literature-references/mg:cit/mg:doi-link"/></xsl:variable>
<xsl:variable name="M3">M3  - <xsl:value-of select="mg:literature-references/mg:cit/mg:doi-link"/></xsl:variable>
<xsl:variable name="PB">PB  - </xsl:variable>
<xsl:variable name="UR">UR  - http://dx.doi.org/<xsl:value-of select="mg:literature-references/mg:cit/mg:doi-link"/></xsl:variable>
<xsl:variable name="SN">SN  - </xsl:variable>
<xsl:variable name="JF">JF  - </xsl:variable>
<xsl:variable name="ER">ER  - </xsl:variable>
<xsl:value-of select="$TY"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$T1"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$A1"/>
<xsl:value-of select="$Y1"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$SP"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$EP"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$JF"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$JO"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$VL"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$IS"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$PB"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$SN"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$DO"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$M3"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$UR"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$N2"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="$ER"/><xsl:text>&#10;</xsl:text>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>
</xsl:template>
  <xsl:template match="text()">
    <xsl:call-template name="replace-string-With-Special-Character">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  <xsl:template name="replace-string-With-Special-Character">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="msxsl:node-set($AllReplacementValues)//char[contains(normalize-space($text),normalize-space(value))] and normalize-space($text) !=''">
        <xsl:for-each select="msxsl:node-set($AllReplacementValues)//char[contains(normalize-space($text),normalize-space(value))][1]">
          <xsl:call-template name="replace-string-With-Special-Character">
            <xsl:with-param name="text" select="substring-before($text, normalize-space(value))"/>
          </xsl:call-template>
          <xsl:copy-of select="mapping/."/>
          <xsl:call-template name="replace-string-With-Special-Character">
            <xsl:with-param name="text" select="substring-after($text, normalize-space(value))"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>