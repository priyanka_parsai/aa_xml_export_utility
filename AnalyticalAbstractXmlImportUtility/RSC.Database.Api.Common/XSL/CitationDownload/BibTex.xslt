﻿<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:meta="http://www.rsc.org/schema/tangier/metadata" xmlns:con="http://www.rsc.org/schema/tangier/container" xmlns:mg="http://www.rsc.org/schema/chemical/monograph">
  <xsl:output method="text" indent="no" media-type="string"/>
  <xsl:include href="RSC.Database.Api.Common.XSL.CitationDownload.SpecialCharacterMapping.xslt" />
  <xsl:include href="RSC.Database.Api.Common.XSL.CitationDownload.BibTexSpecificCharacters.xslt" />
  <!--Use only when debugging Indiviual XSLT-->
 <!-- <xsl:include href="SpecialCharacterMapping.xslt" />
  <xsl:include href="BibTexSpecificCharacters.xslt" />-->
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
    <xsl:variable name="AllReplacementValues">
     <xsl:call-template name="BibTexSpecificValuesList"/>
     <xsl:call-template name="ReplacementValuesList"/>
    </xsl:variable>
  <xsl:template match="ListofArticles | ListofReactions">
    <xsl:for-each select="Article | Reaction">
      <xsl:for-each select="con:container">
        <xsl:for-each select="con:content">
          <xsl:for-each select="mg:monograph">
            <xsl:variable name="systemid">
              <xsl:value-of select="mg:publicid"/>
            </xsl:variable>
            <xsl:variable name="title">
              <xsl:apply-templates select="mg:title"/>
            </xsl:variable>
            <xsl:variable name="description">
              <xsl:apply-templates select="mg:description"/>
            </xsl:variable>
            <xsl:variable name="authors">
              <xsl:for-each select="mg:literature-references/mg:cit/mg:cit-auth">
                <xsl:value-of select="mg:surname"/>
                <xsl:value-of select="', '"/>
                <xsl:value-of select="mg:fname"/>
                <xsl:if test="position() != last()"> and </xsl:if>
              </xsl:for-each>
            </xsl:variable>
            <xsl:variable name="journaltitle">
              <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-title"/>
            </xsl:variable>
            <xsl:variable name="year">
              <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-year"/>
            </xsl:variable>
            <xsl:variable name="volume">
              <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-vol"/>
            </xsl:variable>
            <xsl:variable name="issue">
              <xsl:if test="mg:literature-references/mg:cit/mg:cit-iss">
                <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-iss"/>
              </xsl:if>
            </xsl:variable>
            <xsl:variable name="pages">
              <xsl:if test="mg:literature-references/mg:cit/mg:cit-fpage">
                <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-fpage"/>
              </xsl:if>-<xsl:if test="mg:literature-references/mg:cit/mg:cit-lpage">
                <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-lpage"/>
              </xsl:if>
            </xsl:variable>
            <xsl:variable name="doi">
              <xsl:value-of select="mg:literature-references/mg:cit/mg:doi-link"/>
            </xsl:variable>
            <xsl:variable name="url"><xsl:value-of select="mg:literature-references/mg:cit/mg:doi-link"/>
            </xsl:variable>
            <xsl:text>@Article{</xsl:text><xsl:value-of select="$systemid"/><xsl:text>,</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>author ={</xsl:text><xsl:value-of select="$authors"/><xsl:text>},</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>title ="</xsl:text><xsl:value-of select="$title"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>journal ="</xsl:text><xsl:value-of select="$journaltitle"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>year ="</xsl:text><xsl:value-of select="$year"/>",<xsl:text>&#10;</xsl:text>
            <xsl:if test="translate($volume,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')!='UNASSIGNED'">
            <xsl:text>volume ="</xsl:text><xsl:value-of select="$volume"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            </xsl:if>
            <xsl:text>issue ="</xsl:text><xsl:value-of select="$issue"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>pages ="</xsl:text><xsl:value-of select="$pages"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>publisher ="",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>doi ="</xsl:text><xsl:value-of select="$doi"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>url ="</xsl:text><xsl:text>http://dx.doi.org/</xsl:text><xsl:value-of select ="$url"/><xsl:text>",</xsl:text><xsl:text>&#10;</xsl:text>
            <xsl:text>abstract ="</xsl:text><xsl:value-of select="$description"/><xsl:text>"}</xsl:text><xsl:text>&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
  <xsl:template match="text()">
    <xsl:call-template name="replace-string-With-Special-Character">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  <xsl:template name="replace-string-With-Special-Character">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="msxsl:node-set($AllReplacementValues)//char[contains(normalize-space($text),normalize-space(value))] and normalize-space($text) !=''">
        <xsl:for-each select="msxsl:node-set($AllReplacementValues)//char[contains(normalize-space($text),normalize-space(value))][1]">
          <xsl:call-template name="replace-string-With-Special-Character">
            <xsl:with-param name="text" select="substring-before($text, normalize-space(value))"/>
          </xsl:call-template>
          <xsl:copy-of select="mapping/."/>
          <xsl:call-template name="replace-string-With-Special-Character">
            <xsl:with-param name="text" select="substring-after($text, normalize-space(value))"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
 </xsl:stylesheet>