﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:template name="ReplacementValuesList">
    <char>
      <name>Gamma</name>
      <value>Γ</value>
      <unicode>0393</unicode>
      <mapping>[Gamma]</mapping>
      <alt>Gamma capital Gamma, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Dagger</name>
      <value>‡</value>
      <unicode>2021</unicode>
      <mapping>[double dagger]</mapping>
      <alt>ddagger B: =double dagger</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Delta</name>
      <value>Δ</value>
      <unicode>0394</unicode>
      <mapping>[capital Delta]</mapping>
      <alt>Delta capital Delta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gg</name>
      <value>⋙</value>
      <unicode>22d9</unicode>
      <mapping>[triple gtr-than]</mapping>
      <alt>triple greaater than</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gt</name>
      <value>≫</value>
      <unicode>226b</unicode>
      <mapping>[dbl greater-than]</mapping>
      <alt>doulble greater-than sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lambda</name>
      <value>Λ</value>
      <unicode>039b</unicode>
      <mapping>[capital Lambda]</mapping>
      <alt>Lambda capital Lambda, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ll</name>
      <value>⋘</value>
      <unicode>22d8</unicode>
      <mapping>[triple less-than]</mapping>
      <alt>triple less-than</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lt</name>
      <value>≪</value>
      <unicode>226a</unicode>
      <mapping>[double less-than]</mapping>
      <alt>double less-than</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Omega</name>
      <value>Ω</value>
      <unicode>03a9</unicode>
      <mapping>[capital Omega]</mapping>
      <alt>Omega capital Omega, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Phi</name>
      <value>Φ</value>
      <unicode>03a6</unicode>
      <mapping>[capital Phi]</mapping>
      <alt>Phi capital Phi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Pi</name>
      <value>Π</value>
      <unicode>03a0</unicode>
      <mapping>[capital Pi]</mapping>
      <alt>Pi capital Pi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Prime</name>
      <value>″</value>
      <unicode>2033</unicode>
      <mapping>[double prime]</mapping>
      <alt>double prime or second</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Psi</name>
      <value>Ψ</value>
      <unicode>03a8</unicode>
      <mapping>[capital Psi]</mapping>
      <alt>Psi capital Psi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Rarr</name>
      <value>↠</value>
      <unicode>21a0</unicode>
      <mapping>[twoheadrightarrow]</mapping>
      <alt>two head right arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Sigma</name>
      <value>Σ</value>
      <unicode>03a3</unicode>
      <mapping>[capital Sigma]</mapping>
      <alt>Sigma capital Sigma, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Theta</name>
      <value>Θ</value>
      <unicode>0398</unicode>
      <mapping>[capital Theta]</mapping>
      <alt>Theta capital Theta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Upsi</name>
      <value>ϒ</value>
      <unicode>03d2</unicode>
      <mapping>[capital Upsilon]</mapping>
      <alt>Upsilon capital Upsilon, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Verbar</name>
      <value>‖</value>
      <unicode>2016</unicode>
      <mapping>[dbl vertical bar]</mapping>
      <alt>double vertical bar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Xi</name>
      <value>Ξ</value>
      <unicode>039e</unicode>
      <mapping>[capital Xi]</mapping>
      <alt>Xi capital Xi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>YAcy</name>
      <value>Я</value>
      <unicode>042f</unicode>
      <mapping>[capital YA]</mapping>
      <alt>YA capital YA, Cyrillic</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>alpha</name>
      <value>α</value>
      <unicode>03b1</unicode>
      <mapping>[small alpha]</mapping>
      <alt>small alpha, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>and</name>
      <value>∧</value>
      <unicode>2227</unicode>
      <mapping>[logical and]</mapping>
      <alt>wedge logical and</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ang</name>
      <value>∠</value>
      <unicode>2220</unicode>
      <mapping>[angle]</mapping>
      <alt>angle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>angsph</name>
      <value>∢</value>
      <unicode>2222</unicode>
      <mapping>[angle-spherical]</mapping>
      <alt>angle-spherical</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>angst</name>
      <value>Å</value>
      <unicode>212b</unicode>
      <mapping>[Angstrom]</mapping>
      <alt>Angstrom capital A, ring</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ang90</name>
      <value>∟</value>
      <unicode>221f</unicode>
      <mapping>[right (90 degree) angle]</mapping>
      <alt>right (90 degree) angle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ap</name>
      <value>≈</value>
      <unicode>2248</unicode>
      <mapping>[approximate]</mapping>
      <alt>approximate</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ape</name>
      <value>≊</value>
      <unicode>224a</unicode>
      <mapping>[approximate, equals]</mapping>
      <alt>approximate, equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>becaus</name>
      <value>∵</value>
      <unicode>2235</unicode>
      <mapping>[because]</mapping>
      <alt>because</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>bernou</name>
      <value>ℬ</value>
      <unicode>212c</unicode>
      <mapping>[Bernoulli function]</mapping>
      <alt>Bernoulli function (script capital B) </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>beta</name>
      <value>β</value>
      <unicode>03b2</unicode>
      <mapping>[small beta]</mapping>
      <alt>small beta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <!-- bottom the same as perp -->
    <char>
      <name>bottom</name>
      <value>⊥</value>
      <unicode>22a5</unicode>
      <mapping>[bottom]</mapping>
      <alt>bottom</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dashv</name>
      <value>⊣</value>
      <unicode>22a3</unicode>
      <mapping>[left tack]</mapping>
      <alt>left tack</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>boxvl</name>
      <value>┤</value>
      <unicode>2524</unicode>
      <mapping>[upper and lower left quadrants]</mapping>
      <alt>upper and lower left quadrants</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>boxvr</name>
      <value>├</value>
      <unicode>251c</unicode>
      <mapping>[upper and lower right quadrants]</mapping>
      <alt>upper and lower right quadrants</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>bsim</name>
      <value>∽</value>
      <unicode>223d</unicode>
      <mapping>[reverse similar]</mapping>
      <alt>reverse similar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>bsime</name>
      <value>⋍</value>
      <unicode>22cd</unicode>
      <mapping>[reverse similar, eq]</mapping>
      <alt>reverse similar, eq</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>bull</name>
      <value>•</value>
      <unicode>2022</unicode>
      <mapping>[round bullet, filled]</mapping>
      <alt>round bullet, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cap</name>
      <value>∩</value>
      <unicode>2229</unicode>
      <mapping>[intersection]</mapping>
      <alt>intersection</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Cap</name>
      <value>⋒</value>
      <unicode>22d2</unicode>
      <mapping>[double intersection]</mapping>
      <alt>double intersection</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>caret</name>
      <value>⁁</value>
      <unicode>2041</unicode>
      <mapping>[caret]</mapping>
      <alt>caret (insertion mark)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>check</name>
      <value>✓</value>
      <unicode>2713</unicode>
      <mapping>[check mark]</mapping>
      <alt>checkmark tick</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>chi</name>
      <value>χ</value>
      <unicode>03c7</unicode>
      <mapping>[small chi]</mapping>
      <alt>small chi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cir</name>
      <value>○</value>
      <unicode>25cb</unicode>
      <mapping>[circle, open]</mapping>
      <alt>circ B: =circle, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>compfn</name>
      <value>∘</value>
      <unicode>2218</unicode>
      <mapping>[small circle]</mapping>
      <alt>composite function (small circle)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cong</name>
      <value>≅</value>
      <unicode>2245</unicode>
      <mapping>[congruent with]</mapping>
      <alt>congruent with</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>conint</name>
      <value>∮</value>
      <unicode>222e</unicode>
      <mapping>[contour integral operator]</mapping>
      <alt>contour integral operator</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cross</name>
      <value>✗</value>
      <unicode>2717</unicode>
      <mapping>[ballot cross]</mapping>
      <alt>ballot cross</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ctdot</name>
      <value>⋯</value>
      <unicode>22ef</unicode>
      <mapping>[three dots, centered]</mapping>
      <alt>cdots, three dots, centered</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cup</name>
      <value>∪</value>
      <unicode>222a</unicode>
      <mapping>[union or logical sum]</mapping>
      <alt>union or logical sum</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dagger</name>
      <value>†</value>
      <unicode>2020</unicode>
      <mapping>[dagger]</mapping>
      <alt>dagger</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dArr</name>
      <value>⇓</value>
      <unicode>21d3</unicode>
      <mapping>[double downward arrow]</mapping>
      <alt>double downward arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>darr</name>
      <value>↓</value>
      <unicode>2193</unicode>
      <mapping>[downward arrow]</mapping>
      <alt>downward arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dash</name>
      <value>‐</value>
      <unicode>2010</unicode>
      <mapping>-</mapping>
      <alt>hyphen (true graphic)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dtri</name>
      <value>▿</value>
      <unicode>25bf</unicode>
      <mapping>[down triangle, open]</mapping>
      <alt>down triangle, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dtrif</name>
      <value>▾</value>
      <unicode>25be</unicode>
      <mapping>[black triangle down]</mapping>
      <alt>black triangle down, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ecolon</name>
      <value>≕</value>
      <unicode>2255</unicode>
      <mapping>[equals colon]</mapping>
      <alt>equals colon</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>efDot</name>
      <value>≒</value>
      <unicode>2252</unicode>
      <mapping>[eq, falling dots]</mapping>
      <alt>equals, falling dots</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>emsp</name>
      <value> </value>
      <unicode>2003</unicode>
      <mapping> </mapping>
      <alt>em space</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>emsp13</name>
      <value> </value>
      <unicode>2004</unicode>
      <mapping> </mapping>
      <alt>1/3 em space</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ensp</name>
      <value> </value>
      <unicode>2002</unicode>
      <mapping> </mapping>
      <alt>en space 1/2 em</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>epsiv</name>
      <value>ɛ</value>
      <unicode>025b</unicode>
      <mapping>[varepsilon]</mapping>
      <alt>varepsilon</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>equiv</name>
      <value>≡</value>
      <unicode>2261</unicode>
      <mapping>[identical with]</mapping>
      <alt>equiv, identical with</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>esdot</name>
      <value>≐</value>
      <unicode>2250</unicode>
      <mapping>[equals, single dot above]</mapping>
      <alt>equals, single dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>eta</name>
      <value>η</value>
      <unicode>03b7</unicode>
      <mapping>[small eta]</mapping>
      <alt>small eta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>eth</name>
      <value>ð</value>
      <unicode>00f0</unicode>
      <mapping>[small eth]</mapping>
      <alt>small eth, Icelandic</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>euro</name>
      <value>€</value>
      <unicode>20ac</unicode>
      <mapping>[euro sign]</mapping>
      <alt>euro sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>exist</name>
      <value>∃</value>
      <unicode>2203</unicode>
      <mapping>[exist]</mapping>
      <alt>exist</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>female</name>
      <value>♀</value>
      <unicode>2640</unicode>
      <mapping>[female symbol]</mapping>
      <alt>female symbol</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>fnof</name>
      <value>ƒ</value>
      <unicode>0192</unicode>
      <mapping>[function of]</mapping>
      <alt>function of (italic small f)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>forall</name>
      <value>∀</value>
      <unicode>2200</unicode>
      <mapping>[for all]</mapping>
      <alt>for all</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac13</name>
      <value>⅓</value>
      <unicode>2153</unicode>
      <mapping>1/3</mapping>
      <alt>fraction one-third</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac15</name>
      <value>⅕</value>
      <unicode>2155</unicode>
      <mapping>1/5</mapping>
      <alt>fraction one-fifth</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac16</name>
      <value>⅙</value>
      <unicode>2159</unicode>
      <mapping>1/6</mapping>
      <alt>fraction one-sixth</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac18</name>
      <value>⅛</value>
      <unicode>215b</unicode>
      <mapping>1/8</mapping>
      <alt>fraction one-eighth</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac23</name>
      <value>⅔</value>
      <unicode>2154</unicode>
      <mapping>2/3</mapping>
      <alt>fraction two-thirds</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac25</name>
      <value>⅖</value>
      <unicode>2156</unicode>
      <mapping>2/5</mapping>
      <alt>fraction two-fifths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac35</name>
      <value>⅗</value>
      <unicode>2157</unicode>
      <mapping>3/5</mapping>
      <alt>fraction three-fifths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac38</name>
      <value>⅜</value>
      <unicode>215c</unicode>
      <mapping>3/8</mapping>
      <alt>fraction three-eighths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac45</name>
      <value>⅘</value>
      <unicode>2158</unicode>
      <mapping>4/5</mapping>
      <alt>fraction four-fifths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac56</name>
      <value>⅚</value>
      <unicode>215a</unicode>
      <mapping>5/6</mapping>
      <alt>fraction five-sixths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac58</name>
      <value>⅝</value>
      <unicode>215d</unicode>
      <mapping>5/8</mapping>
      <alt>fraction five-eighths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac78</name>
      <value>⅞</value>
      <unicode>215e</unicode>
      <mapping>7/8</mapping>
      <alt>fraction seven-eighths</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frown</name>
      <value>⌢</value>
      <unicode>2322</unicode>
      <mapping>[down curve]</mapping>
      <alt>down curve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gE</name>
      <value>≧</value>
      <unicode>2267</unicode>
      <mapping>[double equals]</mapping>
      <alt>greater, double equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ge</name>
      <value>≥</value>
      <unicode>2265</unicode>
      <mapping>[greater-than-or-equal]</mapping>
      <alt>greater-than-or-equal</alt>
      <websafe>true</websafe>
    </char>


    <char>
      <name>gsim</name>
      <value>≳</value>
      <unicode>2273</unicode>
      <mapping>[greater, similar]</mapping>
      <alt>gtrsim R: greater, similar</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>hamilt</name>
      <value>ℋ</value>
      <unicode>210b</unicode>
      <mapping>[Hamiltonian]</mapping>
      <alt>Hamiltonian (script capital H) </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>harr</name>
      <value>↔</value>
      <unicode>2194</unicode>
      <mapping>[leftrightarrow]</mapping>
      <alt>leftrightarrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>hArr</name>
      <value>⇔</value>
      <unicode>21d4</unicode>
      <mapping>[Leftrightarrow]</mapping>
      <alt>Left right double arrow</alt>
      <websafe>true</websafe>
    </char>
    <!-- hellip the same as mldr -->
    <char>
      <name>hellip</name>
      <value>…</value>
      <unicode>2026</unicode>
      <mapping>...</mapping>
      <alt>ellipsis (horizontal)</alt>
      <websafe>true</websafe>
    </char>
    <!-- mldr the same as hellip -->
    <char>
      <name>mldr</name>
      <value>…</value>
      <unicode>2026</unicode>
      <mapping>[em leader]</mapping>
      <alt>em space leader</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>image</name>
      <value>ℑ</value>
      <unicode>2111</unicode>
      <mapping>[imaginary number]</mapping>
      <alt>imaginary number</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>infin</name>
      <value>∞</value>
      <unicode>221e</unicode>
      <mapping>[infinity]</mapping>
      <alt>infinity</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>inodot</name>
      <value>ı</value>
      <unicode>0131</unicode>
      <mapping>[i without dot]</mapping>
      <alt>small i without dot</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>int</name>
      <value>∫</value>
      <unicode>222b</unicode>
      <mapping>[integral operator]</mapping>
      <alt>integral operator</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iota</name>
      <value>ι</value>
      <unicode>03b9</unicode>
      <mapping>[iota]</mapping>
      <alt>small iota, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>isin</name>
      <value>∈</value>
      <unicode>2208</unicode>
      <mapping>[set membership]</mapping>
      <alt>set membership </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>kappa</name>
      <value>κ</value>
      <unicode>03ba</unicode>
      <mapping>[small kappa]</mapping>
      <alt>small kappa, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lAarr</name>
      <value>⇚</value>
      <unicode>21da</unicode>
      <mapping>[left triple arrow]</mapping>
      <alt>left triple arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lArr</name>
      <value>⇐</value>
      <unicode>21d0</unicode>
      <mapping>[is implied by]</mapping>
      <alt>left arrow, is implied by</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>larrhk</name>
      <value>↩</value>
      <unicode>21a9</unicode>
      <mapping>[hook left arrow]</mapping>
      <alt>hook left arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lE</name>
      <value>≦</value>
      <unicode>2266</unicode>
      <mapping>[less, double equals]</mapping>
      <alt>less, double equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lagran</name>
      <value>ℒ</value>
      <unicode>2112</unicode>
      <mapping>[script capital L]</mapping>
      <alt>script L or Lagrangian</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lambda</name>
      <value>λ</value>
      <unicode>03bb</unicode>
      <mapping>[small lambda]</mapping>
      <alt>small lambda, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lang</name>
      <value>〈</value>
      <unicode>2329</unicode>
      <mapping>[left angle bracket]</mapping>
      <alt>left angle bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lceil</name>
      <value>⌈</value>
      <unicode>2308</unicode>
      <mapping>[left ceiling]</mapping>
      <alt>left ceiling</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ldquor</name>
      <value>„</value>
      <unicode>201e</unicode>
      <mapping>"</mapping>
      <alt>rising double quote</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>le</name>
      <value>≤</value>
      <unicode>2264</unicode>
      <mapping>[less-than-or-equal]</mapping>
      <alt>less than or equal</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>les</name>
      <value>⩽</value>
      <unicode>2a7d</unicode>
      <mapping>[less-than-or-eq]</mapping>
      <alt>less than or equal, slant</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lfloor</name>
      <value>⌊</value>
      <unicode>230a</unicode>
      <mapping>[left floor]</mapping>
      <alt>left floor</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lmidot</name>
      <value>ŀ</value>
      <unicode>0140</unicode>
      <mapping>[small l, middle dot]</mapping>
      <alt>small l, middle dot</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>loz</name>
      <value>◊</value>
      <unicode>25ca</unicode>
      <mapping>[lozenge or total mark]</mapping>
      <alt>lozenge - lozenge or total mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lozf</name>
      <value>✦</value>
      <unicode>2726</unicode>
      <mapping>[lozenge, filled]</mapping>
      <alt>blacklozenge - lozenge, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lrarr</name>
      <value>⇆</value>
      <unicode>21c6</unicode>
      <mapping>[leftrightarrows]</mapping>
      <alt>left right arrows</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>lsim</name>
      <value>≲</value>
      <unicode>2272</unicode>
      <mapping>[less, similar]</mapping>
      <alt>less, similar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lsquor</name>
      <value>‚</value>
      <unicode>201a</unicode>
      <mapping>'</mapping>
      <alt>rising single quote, left (low)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ltri</name>
      <value>◃</value>
      <unicode>25c3</unicode>
      <mapping>[triangleleft]</mapping>
      <alt>left triangle, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ltrif</name>
      <value>◂</value>
      <unicode>25c2</unicode>
      <mapping>[blacktriangleleft]</mapping>
      <alt>black triangle left, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>male</name>
      <value>♂</value>
      <unicode>2642</unicode>
      <mapping>[male symbol]</mapping>
      <alt>male symbol</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>malt</name>
      <value>✠</value>
      <unicode>2720</unicode>
      <mapping>[maltese cross]</mapping>
      <alt>maltese cross</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>mid</name>
      <value>∣</value>
      <unicode>2223</unicode>
      <mapping>[mid]</mapping>
      <alt>mid</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>minusb</name>
      <value>⊟</value>
      <unicode>229f</unicode>
      <mapping>[minus sign in box]</mapping>
      <alt>minus sign in box</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sqcup</name>
      <value>⊔</value>
      <unicode>2294</unicode>
      <mapping>[square cup]</mapping>
      <alt>square cup</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>mnplus</name>
      <value>∓</value>
      <unicode>2213</unicode>
      <mapping>-/+</mapping>
      <alt>minus-or-plus sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nabla</name>
      <value>∇</value>
      <unicode>2207</unicode>
      <mapping>[Hamilton operator]</mapping>
      <alt>Hamilton operator</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ne</name>
      <value>≠</value>
      <unicode>2260</unicode>
      <mapping>[not equal]</mapping>
      <alt>not equal</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nearr</name>
      <value>↗</value>
      <unicode>2197</unicode>
      <mapping>[NE pointing arrow]</mapping>
      <alt>NE pointing arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nequiv</name>
      <value>≢</value>
      <unicode>2262</unicode>
      <mapping>[not equivalent]</mapping>
      <alt>not equivalent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ni</name>
      <value>∋</value>
      <unicode>220b</unicode>
      <mapping>[contains]</mapping>
      <alt>contains</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nldr</name>
      <value>‥</value>
      <unicode>2025</unicode>
      <mapping>[double baseline dot]</mapping>
      <alt>double baseline dot (en leader)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>notin</name>
      <value>∉</value>
      <unicode>2209</unicode>
      <mapping>[negated set membership]</mapping>
      <alt>negated set membership</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nrarr</name>
      <value>↛</value>
      <unicode>219b</unicode>
      <mapping>[not right arrow]</mapping>
      <alt>not right arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nu</name>
      <value>ν</value>
      <unicode>03bd</unicode>
      <mapping>[small nu]</mapping>
      <alt>small nu, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nwarr</name>
      <value>↖</value>
      <unicode>2196</unicode>
      <mapping>[NW pointing arrow]</mapping>
      <alt>NW pointing arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ohm</name>
      <value>Ω</value>
      <unicode>2126</unicode>
      <mapping>[ohm sign]</mapping>
      <alt>ohm sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ominus</name>
      <value>⊖</value>
      <unicode>2296</unicode>
      <mapping>[minus sign in circle]</mapping>
      <alt>minus sign in circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>olarr</name>
      <value>↺</value>
      <unicode>21ba</unicode>
      <mapping>[circlearrowleft A: l arr in circle]</mapping>
      <alt>circle arrow left</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>oplus</name>
      <value>⊕</value>
      <unicode>2295</unicode>
      <mapping>[plus sign in circle]</mapping>
      <alt>plus sign in circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>orarr</name>
      <value>↻</value>
      <unicode>21bb</unicode>
      <mapping>[circlearrowright A: r arr in circle]</mapping>
      <alt>circle arrow right</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>osol</name>
      <value>⊘</value>
      <unicode>2298</unicode>
      <mapping>[solidus in circle]</mapping>
      <alt>solidus in circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>otimes</name>
      <value>⊗</value>
      <unicode>2297</unicode>
      <mapping>[multiply sign in circle]</mapping>
      <alt>multiply sign in circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>par</name>
      <value>∥</value>
      <unicode>2225</unicode>
      <mapping>[parallel]</mapping>
      <alt>parallel</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>part</name>
      <value>∂</value>
      <unicode>2202</unicode>
      <mapping>[partial differential]</mapping>
      <alt>partial differential</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>permil</name>
      <value>‰</value>
      <unicode>2030</unicode>
      <mapping>[per thousand]</mapping>
      <alt>per thousand</alt>
      <websafe>true</websafe>
    </char>
    <!-- perp the same as bottom -->
    <char>
      <name>perp</name>
      <value>⊥</value>
      <unicode>22a5</unicode>
      <mapping>[perpendicular]</mapping>
      <alt>perpendicular</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>phis</name>
      <value>φ</value>
      <unicode>03c6</unicode>
      <mapping>[small phi]</mapping>
      <alt>small phi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>phiv</name>
      <value>ϕ</value>
      <unicode>03d5</unicode>
      <mapping>[curly or open phi]</mapping>
      <alt>curly or open phi</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>pi</name>
      <value>π</value>
      <unicode>03c0</unicode>
      <mapping>[small pi]</mapping>
      <alt>small pi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>piv</name>
      <value>ϖ</value>
      <unicode>03d6</unicode>
      <mapping>[varpi]</mapping>
      <alt>varpi</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>plusb</name>
      <value>⊞</value>
      <unicode>229e</unicode>
      <mapping>[plus sign in box]</mapping>
      <alt>plus sign in box</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>prime</name>
      <value>′</value>
      <unicode>2032</unicode>
      <mapping>[prime or minute]</mapping>
      <alt>prime or minute</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>prod</name>
      <value>∏</value>
      <unicode>220f</unicode>
      <mapping>[n ary product]</mapping>
      <alt>product</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>prop</name>
      <value>∝</value>
      <unicode>221d</unicode>
      <mapping>[is proportional to]</mapping>
      <alt>is proportional to</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>psi</name>
      <value>ψ</value>
      <unicode>03c8</unicode>
      <mapping>[small psi]</mapping>
      <alt>small psi, Greek</alt>
      <websafe>true</websafe>
    </char>

    <char>
      <name>rArr</name>
      <value>⇒</value>
      <unicode>21d2</unicode>
      <mapping>[implies]</mapping>
      <alt>right arrow, implies</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>radic</name>
      <value>√</value>
      <unicode>221a</unicode>
      <mapping>[surd radical]</mapping>
      <alt>surd radical</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rang</name>
      <value>〉</value>
      <unicode>232a</unicode>
      <mapping>[right angle bracket]</mapping>
      <alt>right angle bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rarr</name>
      <value>→</value>
      <unicode>2192</unicode>
      <mapping>[rightward arrow]</mapping>
      <alt>rightward arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ratio</name>
      <value>∶</value>
      <unicode>2236</unicode>
      <mapping>[ratio]</mapping>
      <alt>ratio</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rceil</name>
      <value>⌉</value>
      <unicode>2309</unicode>
      <mapping>[right ceiling]</mapping>
      <alt>right ceiling</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>real</name>
      <value>ℜ</value>
      <unicode>211c</unicode>
      <mapping>[real number]</mapping>
      <alt>real number</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rect</name>
      <value>▭</value>
      <unicode>25ad</unicode>
      <mapping>[rectangle, open]</mapping>
      <alt>rectangle, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rfloor</name>
      <value>⌋</value>
      <unicode>230b</unicode>
      <mapping>[right floor]</mapping>
      <alt>right floor</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rharu</name>
      <value>⇀</value>
      <unicode>21c0</unicode>
      <mapping>[right harpoon up]</mapping>
      <alt>right harpoon up</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rho</name>
      <value>ρ</value>
      <unicode>03c1</unicode>
      <mapping>[small rho]</mapping>
      <alt>small rho, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rhov</name>
      <value>ϱ</value>
      <unicode>03f1</unicode>
      <mapping>[var rho]</mapping>
      <alt>varrho</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rlarr</name>
      <value>⇄</value>
      <unicode>21c4</unicode>
      <mapping>[right left arrows]</mapping>
      <alt>right over left arrows</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rlhar</name>
      <value>⇌</value>
      <unicode>21cc</unicode>
      <mapping>[right left harpoons]</mapping>
      <alt>right over left harpoons</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rtri</name>
      <value>▹</value>
      <unicode>25b9</unicode>
      <mapping>[triangle right,open]</mapping>
      <alt>triangle right</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rtrif</name>
      <value>▸</value>
      <unicode>25b8</unicode>
      <mapping>[black triangle right]</mapping>
      <alt>black triangle right, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sdotb</name>
      <value>⊡</value>
      <unicode>22a1</unicode>
      <mapping>[small dot in box]</mapping>
      <alt>small dot in box</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>searr</name>
      <value>↘</value>
      <unicode>2198</unicode>
      <mapping>[SE pointing arrow]</mapping>
      <alt>SE pointing arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sigma</name>
      <value>σ</value>
      <unicode>03c3</unicode>
      <mapping>[sigma]</mapping>
      <alt>small sigma, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sigmav</name>
      <value>ς</value>
      <unicode>03c2</unicode>
      <mapping>[var sigma]</mapping>
      <alt>var sigma</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sim</name>
      <value>∼</value>
      <unicode>223c</unicode>
      <mapping>[similar]</mapping>
      <alt>similar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sime</name>
      <value>≃</value>
      <unicode>2243</unicode>
      <mapping>[similar, equals]</mapping>
      <alt>similar, equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>square</name>
      <value>□</value>
      <unicode>25a1</unicode>
      <mapping>[square]</mapping>
      <alt>square</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>squarf</name>
      <value>▪</value>
      <unicode>25aa</unicode>
      <mapping>[black square, filled]</mapping>
      <alt>black square, filled </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <!-- Same as above? -->
      <name>squarf</name>
      <value>■</value>
      <unicode>25a0</unicode>
      <mapping>[black square, filled]</mapping>
      <alt>black square, filled </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>star</name>
      <value>⋆</value>
      <unicode>22c6</unicode>
      <mapping>[star, open]</mapping>
      <alt>star, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>starf</name>
      <value>★</value>
      <unicode>2605</unicode>
      <mapping>[star, filled]</mapping>
      <alt>star, filled </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sub</name>
      <value>⊂</value>
      <unicode>2282</unicode>
      <mapping>[subset or is implied by]</mapping>
      <alt>subset or is implied by</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sube</name>
      <value>⊆</value>
      <unicode>2286</unicode>
      <mapping>[subset, equals]</mapping>
      <alt>subset, equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sum</name>
      <value>∑</value>
      <unicode>2211</unicode>
      <mapping>[summation operator]</mapping>
      <alt>summation operator</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sup</name>
      <value>⊃</value>
      <unicode>2283</unicode>
      <mapping>[superset]</mapping>
      <alt>superset or implies</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>supe</name>
      <value>⊇</value>
      <unicode>2287</unicode>
      <mapping>[superset, equals]</mapping>
      <alt>superset, equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>swarr</name>
      <value>↙</value>
      <unicode>2199</unicode>
      <mapping>[SW pointing arrow]</mapping>
      <alt>SW pointing arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>target</name>
      <value>⌖</value>
      <unicode>2316</unicode>
      <mapping>[register mark or target]</mapping>
      <alt>register mark or target</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tau</name>
      <value>τ</value>
      <unicode>03c4</unicode>
      <mapping>[small tau]</mapping>
      <alt>small tau, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name reserved="true">amp</name>
      <value>&amp;</value>
      <unicode>0026</unicode>
      <mapping>&amp;</mapping>
      <alt>ampersand</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>apos</name>
      <value>'</value>
      <unicode>0027</unicode>
      <mapping>'</mapping>
      <alt>apostrophe</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ast</name>
      <value>*</value>
      <unicode>002a</unicode>
      <mapping>*</mapping>
      <alt>asterisk</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>bsol</name>
      <value>\</value>
      <unicode>005c</unicode>
      <mapping>\</mapping>
      <alt>reverse solidus</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dollar</name>
      <value>$</value>
      <unicode>0024</unicode>
      <mapping>$</mapping>
      <alt>dollar sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>excl</name>
      <value>!</value>
      <unicode>0021</unicode>
      <mapping>!</mapping>
      <alt>exclamation mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>commat</name>
      <value>@</value>
      <unicode>0040</unicode>
      <mapping>@</mapping>
      <alt>commercial at</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>verbar</name>
      <value>|</value>
      <unicode>007c</unicode>
      <mapping>|</mapping>
      <alt>vertical bar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>equals</name>
      <value>=</value>
      <unicode>003d</unicode>
      <mapping>=</mapping>
      <alt>equals sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name reserved="true">gt</name>
      <value>&gt;</value>
      <unicode>003e</unicode>
      <mapping>&gt;</mapping>
      <alt>greater-than sign</alt>
      <websafe>true</websafe>
    </char>
    <!-- hyphen the same as shy -->
    <char>
      <name>hyphen</name>
      <value>-</value>
      <unicode>002d</unicode>
      <mapping>-</mapping>
      <alt>hyphen</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lcub</name>
      <value>{</value>
      <unicode>007b</unicode>
      <mapping>{</mapping>
      <alt>left curly bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lpar</name>
      <value>(</value>
      <unicode>0028</unicode>
      <mapping>(</mapping>
      <alt>left parenthesis</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lsqb</name>
      <value>[</value>
      <unicode>005b</unicode>
      <mapping>[</mapping>
      <alt>left square bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>num</name>
      <value>#</value>
      <unicode>0023</unicode>
      <mapping>#</mapping>
      <alt>number sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>percnt</name>
      <value>%</value>
      <unicode>0025</unicode>
      <mapping>%</mapping>
      <alt>percent sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>period</name>
      <value>.</value>
      <unicode>002e</unicode>
      <mapping>.</mapping>
      <alt>full stop, period</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>plus</name>
      <value>+</value>
      <unicode>002b</unicode>
      <mapping>+</mapping>
      <alt>plus sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>quest</name>
      <value>?</value>
      <unicode>003f</unicode>
      <mapping>?</mapping>
      <alt>question mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rcub</name>
      <value>}</value>
      <unicode>007d</unicode>
      <mapping>}</mapping>
      <alt>right curly bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rpar</name>
      <value>)</value>
      <unicode>0029</unicode>
      <mapping>)</mapping>
      <alt>right parenthesis</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rsqb</name>
      <value>]</value>
      <unicode>005d</unicode>
      <mapping>]</mapping>
      <alt>right square bracket</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sol</name>
      <value>/</value>
      <unicode>002f</unicode>
      <mapping>/</mapping>
      <alt>solidus</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name reserved="true">lt</name>
      <value>&lt;</value>
      <unicode>003c</unicode>
      <mapping>&amp;#60;</mapping>
      <alt>less-than sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ndash</name>
      <value>–</value>
      <unicode>2013</unicode>
      <mapping>-</mapping>
      <alt>en dash</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>mdash</name>
      <value>—</value>
      <unicode>2014</unicode>
      <mapping>-</mapping>
      <alt>em dash </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>minus</name>
      <value>−</value>
      <unicode>2212</unicode>
      <mapping>-</mapping>
      <alt>minus sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>copy</name>
      <value>©</value>
      <unicode>00a9</unicode>
      <mapping>[copyright sign]</mapping>
      <alt>copyright sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>brvbar</name>
      <value>¦</value>
      <unicode>00a6</unicode>
      <mapping>[broken vertical bar]</mapping>
      <alt>broken (vertical) bar</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cent</name>
      <value>¢</value>
      <unicode>00a2</unicode>
      <mapping>[cent sign]</mapping>
      <alt>cent sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>times</name>
      <value>×</value>
      <unicode>00d7</unicode>
      <mapping>[times]</mapping>
      <alt>multiply sign</alt>
      <websafe>true</websafe>
    </char>
    <!-- emsp14 the same as nbsp -->
    <char>
      <name>emsp14</name>
      <value> </value>
      <unicode>00a0</unicode>
      <mapping>[space]</mapping>
      <alt>1/4-em space</alt>
      <websafe>true</websafe>
    </char>
    <!-- frac12 the same half -->
    <char>
      <name>frac12</name>
      <value>½</value>
      <unicode>00bd</unicode>
      <mapping>1/2</mapping>
      <alt>fraction one-half</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac14</name>
      <value>¼</value>
      <unicode>00bc</unicode>
      <mapping>1/4</mapping>
      <alt>fraction one-quarter</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>frac34</name>
      <value>¾</value>
      <unicode>00be</unicode>
      <mapping>3/4</mapping>
      <alt>fraction three-quarters</alt>
      <websafe>true</websafe>
    </char>
    <!-- half the same as frac12 -->
    <char>
      <name>half</name>
      <value>½</value>
      <unicode>00bd</unicode>
      <mapping>1/2</mapping>
      <alt>fraction one-half</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iexcl</name>
      <value>¡</value>
      <unicode>00a1</unicode>
      <mapping>[inverted exclamation mark]</mapping>
      <alt>inverted exclamation mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>laquo</name>
      <value>«</value>
      <unicode>00ab</unicode>
      <mapping>'</mapping>
      <alt>angle quotation mark, left</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>mu</name>
      <value>μ</value>
      <unicode>03bc</unicode>
      <mapping>[small mu ]</mapping>
      <alt>small mu, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>micro</name>
      <value>µ</value>
      <unicode>00b5</unicode>
      <mapping>[small micro]</mapping>
      <alt>small micro</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>middot</name>
      <value>·</value>
      <unicode>00b7</unicode>
      <mapping>[middle dot]</mapping>
      <alt>middle dot</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nbsp</name>
      <value> </value>
      <unicode>00a0</unicode>
      <mapping> </mapping>
      <alt>no break (required) space</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ordf</name>
      <value>ª</value>
      <unicode>00aa</unicode>
      <mapping>[a]</mapping>
      <alt>female ordinal</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ordm</name>
      <value>º</value>
      <unicode>00ba</unicode>
      <mapping>[o]</mapping>
      <alt>masculine ordinal</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>para</name>
      <value>¶</value>
      <unicode>00b6</unicode>
      <mapping>[paragraph sign]</mapping>
      <alt>pilcrow (paragraph sign)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>plusmn</name>
      <value>±</value>
      <unicode>00b1</unicode>
      <mapping>+/-</mapping>
      <alt>plus or minus sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>pound</name>
      <value>£</value>
      <unicode>00a3</unicode>
      <mapping>[pound sign]</mapping>
      <alt>pound sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>raquo</name>
      <value>»</value>
      <unicode>00bb</unicode>
      <mapping>'</mapping>
      <alt>angle quotation mark, right</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>reg</name>
      <value>®</value>
      <unicode>00ae</unicode>
      <mapping>[registered sign]</mapping>
      <alt>circled registered sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sect</name>
      <value>§</value>
      <unicode>00a7</unicode>
      <mapping>[section sign]</mapping>
      <alt>section sign</alt>
      <websafe>true</websafe>
    </char>
    <!-- shy the same as hyphen -->
    <char>
      <name>shy</name>
      <value>-</value>
      <unicode>002d</unicode>
      <mapping>-</mapping>
      <alt>soft hyphen</alt>
      <websafe>true</websafe>
    </char>
    <!-- True diacritics -->
    <char>
      <name>Aacute</name>
      <value>Á</value>
      <unicode>00c1</unicode>
      <mapping>A</mapping>
      <alt>capital A, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Acirc</name>
      <value>Â</value>
      <unicode>00c2</unicode>
      <mapping>A</mapping>
      <alt>capital A, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Agrave</name>
      <value>À</value>
      <unicode>00c0</unicode>
      <mapping>A</mapping>
      <alt>capital A, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Aring</name>
      <value>Å</value>
      <unicode>00c5</unicode>
      <mapping>A</mapping>
      <alt>capital A, ring</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Atilde</name>
      <value>Ã</value>
      <unicode>00c3</unicode>
      <mapping>A</mapping>
      <alt>capital A, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Auml</name>
      <value>Ä</value>
      <unicode>00c4</unicode>
      <mapping>A</mapping>
      <alt>capital A, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ccedil</name>
      <value>Ç</value>
      <unicode>00c7</unicode>
      <mapping>C</mapping>
      <alt>capital C, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Eacute</name>
      <value>É</value>
      <unicode>00c9</unicode>
      <mapping>E</mapping>
      <alt>capital E, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ecirc</name>
      <value>Ê</value>
      <unicode>00ca</unicode>
      <mapping>E</mapping>
      <alt>capital E, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Egrave</name>
      <value>È</value>
      <unicode>00c8</unicode>
      <mapping>E</mapping>
      <alt>capital E, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Euml</name>
      <value>Ë</value>
      <unicode>00cb</unicode>
      <mapping>E</mapping>
      <alt> capital E, umlaut accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Iacute</name>
      <value>Í</value>
      <unicode>00cd</unicode>
      <mapping>I</mapping>
      <alt>capital I, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Icirc</name>
      <value>Î</value>
      <unicode>00ce</unicode>
      <mapping>I</mapping>
      <alt>capital I, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Igrave</name>
      <value>Ì</value>
      <unicode>00cc</unicode>
      <mapping>I</mapping>
      <alt>capital I, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Iuml</name>
      <value>Ï</value>
      <unicode>00cf</unicode>
      <mapping>I</mapping>
      <alt>capital I, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ntilde</name>
      <value>Ñ</value>
      <unicode>00d1</unicode>
      <mapping>N</mapping>
      <alt>capital N, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Oacute</name>
      <value>Ó</value>
      <unicode>00d3</unicode>
      <mapping>O</mapping>
      <alt>capital O, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ocirc</name>
      <value>Ô</value>
      <unicode>00d4</unicode>
      <mapping>O</mapping>
      <alt>capital O, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ograve</name>
      <value>Ò</value>
      <unicode>00d2</unicode>
      <mapping>O</mapping>
      <alt>capital O, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Oslash</name>
      <value>Ø</value>
      <unicode>00d8</unicode>
      <mapping>O</mapping>
      <alt>capital O, slash</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Otilde</name>
      <value>Õ</value>
      <unicode>00d5</unicode>
      <mapping>O</mapping>
      <alt>capital O, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ouml</name>
      <value>Ö</value>
      <unicode>00d6</unicode>
      <mapping>O</mapping>
      <alt>capital O, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Uacute</name>
      <value>Ú</value>
      <unicode>00da</unicode>
      <mapping>U</mapping>
      <alt>capital U, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ucirc</name>
      <value>Û</value>
      <unicode>00db</unicode>
      <mapping>U</mapping>
      <alt>capital U, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ugrave</name>
      <value>Ù</value>
      <unicode>00d9</unicode>
      <mapping>U</mapping>
      <alt>capital U, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Uuml</name>
      <value>Ü</value>
      <unicode>00dc</unicode>
      <mapping>U</mapping>
      <alt>capital U, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Yacute</name>
      <value>Ý</value>
      <unicode>00dd</unicode>
      <mapping>Y</mapping>
      <alt>capital Y, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>aacute</name>
      <value>á</value>
      <unicode>00e1</unicode>
      <mapping>a</mapping>
      <alt>small a, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>acirc</name>
      <value>â</value>
      <unicode>00e2</unicode>
      <mapping>a</mapping>
      <alt>small a, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>agrave</name>
      <value>à</value>
      <unicode>00e0</unicode>
      <mapping>a</mapping>
      <alt>small a, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>aring</name>
      <value>å</value>
      <unicode>00e5</unicode>
      <mapping>a</mapping>
      <alt>small a, ring</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>atilde</name>
      <value>ã</value>
      <unicode>00e3</unicode>
      <mapping>a</mapping>
      <alt>small a, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>auml</name>
      <value>ä</value>
      <unicode>00e4</unicode>
      <mapping>a</mapping>
      <alt>small a, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ccedil</name>
      <value>ç</value>
      <unicode>00e7</unicode>
      <mapping>c</mapping>
      <alt>small c, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>eacute</name>
      <value>é</value>
      <unicode>00e9</unicode>
      <mapping>e</mapping>
      <alt>small e, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ecirc</name>
      <value>ê</value>
      <unicode>00ea</unicode>
      <mapping>e</mapping>
      <alt>small e, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>egrave</name>
      <value>è</value>
      <unicode>00e8</unicode>
      <mapping>e</mapping>
      <alt>small e, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>euml</name>
      <value>ë</value>
      <unicode>00eb</unicode>
      <mapping>e</mapping>
      <alt>small e, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iacute</name>
      <value>í</value>
      <unicode>00ed</unicode>
      <mapping>i</mapping>
      <alt>small i, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>icirc</name>
      <value>î</value>
      <unicode>00ee</unicode>
      <mapping>i</mapping>
      <alt>small i, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>igrave</name>
      <value>ì</value>
      <unicode>00ec</unicode>
      <mapping>i</mapping>
      <alt>small i, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iuml</name>
      <value>ï</value>
      <unicode>00ef</unicode>
      <mapping>i</mapping>
      <alt>small i, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ntilde</name>
      <value>ñ</value>
      <unicode>00f1</unicode>
      <mapping>n</mapping>
      <alt>small n, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>oacute</name>
      <value>ó</value>
      <unicode>00f3</unicode>
      <mapping>o</mapping>
      <alt>small o, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ocirc</name>
      <value>ô</value>
      <unicode>00f4</unicode>
      <mapping>o</mapping>
      <alt>small o, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ograve</name>
      <value>ò</value>
      <unicode>00f2</unicode>
      <mapping>o</mapping>
      <alt>small o, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>oslash</name>
      <value>ø</value>
      <unicode>00f8</unicode>
      <mapping>o</mapping>
      <alt>latin small letter o with stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>otilde</name>
      <value>õ</value>
      <unicode>00f5</unicode>
      <mapping>o</mapping>
      <alt>small o, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ouml</name>
      <value>ö</value>
      <unicode>00f6</unicode>
      <mapping>o</mapping>
      <alt>small o, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uacute</name>
      <value>ú</value>
      <unicode>00fa</unicode>
      <mapping>u</mapping>
      <alt>small u, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ucirc</name>
      <value>û</value>
      <unicode>00fb</unicode>
      <mapping>u</mapping>
      <alt>small u, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ugrave</name>
      <value>ù</value>
      <unicode>00f9</unicode>
      <mapping>u</mapping>
      <alt>small u, grave accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uuml</name>
      <value>ü</value>
      <unicode>00fc</unicode>
      <mapping>u</mapping>
      <alt>small u, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>yacute</name>
      <value>ý</value>
      <unicode>00fd</unicode>
      <mapping>y</mapping>
      <alt>small y, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>yuml</name>
      <value>ÿ</value>
      <unicode>00ff</unicode>
      <mapping>y</mapping>
      <alt>small y, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iquest</name>
      <value>¿</value>
      <unicode>00bf</unicode>
      <mapping>[inverted question mark]</mapping>
      <alt>inverted question mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>deg</name>
      <value>°</value>
      <unicode>00b0</unicode>
      <mapping>[degree]</mapping>
      <alt>degree sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>AElig</name>
      <value>Æ</value>
      <unicode>00c6</unicode>
      <mapping>AE</mapping>
      <alt>capital AE diphthong (ligature)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>aelig</name>
      <value>æ</value>
      <unicode>00e6</unicode>
      <mapping>ae</mapping>
      <alt>small ae diphthong (ligature)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>szlig</name>
      <value>ß</value>
      <unicode>00df</unicode>
      <mapping>[German sz ligature}</mapping>
      <alt>small sharp s, German (sz ligature)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>OElig</name>
      <value>Œ</value>
      <unicode>0152</unicode>
      <mapping>OE</mapping>
      <alt>capital OE ligature</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>oelig</name>
      <value>œ</value>
      <unicode>0153</unicode>
      <mapping>oe</mapping>
      <alt>small oe ligature</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ijlig</name>
      <value>ĳ</value>
      <unicode>0133</unicode>
      <mapping>ij</mapping>
      <alt>small ij ligature</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>IJlig</name>
      <value>Ĳ</value>
      <unicode>0132</unicode>
      <mapping>IJ</mapping>
      <alt>capital IJ ligature</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Abreve</name>
      <value>Ă</value>
      <unicode>0102</unicode>
      <mapping>A</mapping>
      <alt>capital A, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Amacr</name>
      <value>Ā</value>
      <unicode>0100</unicode>
      <mapping>A</mapping>
      <alt>capital A, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Aogon</name>
      <value>Ą</value>
      <unicode>0104</unicode>
      <mapping>A</mapping>
      <alt>capital A, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Cacute</name>
      <value>Ć</value>
      <unicode>0106</unicode>
      <mapping>C</mapping>
      <alt>capital C, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ccaron</name>
      <value>Č</value>
      <unicode>010c</unicode>
      <mapping>C</mapping>
      <alt>capital C, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ccirc</name>
      <value>Ĉ</value>
      <unicode>0108</unicode>
      <mapping>C</mapping>
      <alt>capital C, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Cdot</name>
      <value>Ċ</value>
      <unicode>010a</unicode>
      <mapping>[C dot]</mapping>
      <alt>capital C, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Dcaron</name>
      <value>Ď</value>
      <unicode>010e</unicode>
      <mapping>D</mapping>
      <alt>capital D, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Dstrok</name>
      <value>Đ</value>
      <unicode>0110</unicode>
      <mapping>D</mapping>
      <alt>capital D, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ecaron</name>
      <value>Ě</value>
      <unicode>011a</unicode>
      <mapping>E</mapping>
      <alt>capital E, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Edot</name>
      <value>Ė</value>
      <unicode>0116</unicode>
      <mapping>[E dot]</mapping>
      <alt>capital E, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Emacr</name>
      <value>Ē</value>
      <unicode>0112</unicode>
      <mapping>E</mapping>
      <alt>capital E, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Eogon</name>
      <value>Ę</value>
      <unicode>0118</unicode>
      <mapping>E</mapping>
      <alt>capital E, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gbreve</name>
      <value>Ğ</value>
      <unicode>011e</unicode>
      <mapping>G</mapping>
      <alt>capital G, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gcedil</name>
      <value>Ģ</value>
      <unicode>0122</unicode>
      <mapping>G</mapping>
      <alt>capital G, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gcirc</name>
      <value>Ĝ</value>
      <unicode>011c</unicode>
      <mapping>G</mapping>
      <alt>capital G, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Gdot</name>
      <value>Ġ</value>
      <unicode>0120</unicode>
      <mapping>G</mapping>
      <alt>capital G, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Hcirc</name>
      <value>Ĥ</value>
      <unicode>0124</unicode>
      <mapping>H</mapping>
      <alt>capital H, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ibreve</name>
      <value>Ĭ</value>
      <unicode>012c</unicode>
      <mapping>I</mapping>
      <alt>Capital I, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Hstrok</name>
      <value>Ħ</value>
      <unicode>0126</unicode>
      <mapping>H</mapping>
      <alt>capital H, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Idot</name>
      <value>İ</value>
      <unicode>0130</unicode>
      <mapping>I</mapping>
      <alt>capital I, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Imacr</name>
      <value>Ī</value>
      <unicode>012a</unicode>
      <mapping>I</mapping>
      <alt>capital I, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Iogon</name>
      <value>Į</value>
      <unicode>012e</unicode>
      <mapping>I</mapping>
      <alt>capital I, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Itilde</name>
      <value>Ĩ</value>
      <unicode>0128</unicode>
      <mapping>I</mapping>
      <alt>capital I, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Jcirc</name>
      <value>Ĵ</value>
      <unicode>0134</unicode>
      <mapping>J</mapping>
      <alt>capital J, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Kcedil</name>
      <value>Ķ</value>
      <unicode>0136</unicode>
      <mapping>K</mapping>
      <alt>capital K, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lacute</name>
      <value>Ĺ</value>
      <unicode>0139</unicode>
      <mapping>L</mapping>
      <alt>capital L, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lcaron</name>
      <value>Ľ</value>
      <unicode>013d</unicode>
      <mapping>L</mapping>
      <alt>capital L, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lcedil</name>
      <value>Ļ</value>
      <unicode>013b</unicode>
      <mapping>L</mapping>
      <alt>capital L, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lmidot</name>
      <value>Ŀ</value>
      <unicode>013f</unicode>
      <mapping>[L middle dot]</mapping>
      <alt>capital L, middle dot</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Lstrok</name>
      <value>Ł</value>
      <unicode>0141</unicode>
      <mapping>L</mapping>
      <alt>capital L, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Nacute</name>
      <value>Ń</value>
      <unicode>0143</unicode>
      <mapping>N</mapping>
      <alt>capital N, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ncaron</name>
      <value>Ň</value>
      <unicode>0147</unicode>
      <mapping>N</mapping>
      <alt>capital N, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ncedil</name>
      <value>Ņ</value>
      <unicode>0145</unicode>
      <mapping>N</mapping>
      <alt>capital N, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Odblac</name>
      <value>Ő</value>
      <unicode>0150</unicode>
      <mapping>O</mapping>
      <alt>capital O, double acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Omacr</name>
      <value>Ō</value>
      <unicode>014c</unicode>
      <mapping>O</mapping>
      <alt>capital O, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Racute</name>
      <value>Ŕ</value>
      <unicode>0154</unicode>
      <mapping>R</mapping>
      <alt>capital R, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Rcaron</name>
      <value>Ř</value>
      <unicode>0158</unicode>
      <mapping>R</mapping>
      <alt>capital R, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Rcedil</name>
      <value>Ŗ</value>
      <unicode>0156</unicode>
      <mapping>R</mapping>
      <alt>capital R, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Sacute</name>
      <value>Ś</value>
      <unicode>015a</unicode>
      <mapping>S</mapping>
      <alt>capital S, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Scaron</name>
      <value>Š</value>
      <unicode>0160</unicode>
      <mapping>S</mapping>
      <alt>capital S, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Scedil</name>
      <value>Ş</value>
      <unicode>015e</unicode>
      <mapping>S</mapping>
      <alt>capital S, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Scirc</name>
      <value>Ŝ</value>
      <unicode>015c</unicode>
      <mapping>S</mapping>
      <alt>capital S, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Tcaron</name>
      <value>Ť</value>
      <unicode>0164</unicode>
      <mapping>T</mapping>
      <alt>capital T, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Tcedil</name>
      <value>Ţ</value>
      <unicode>0162</unicode>
      <mapping>T</mapping>
      <alt>capital T, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Tstrok</name>
      <value>Ŧ</value>
      <unicode>0166</unicode>
      <mapping>T</mapping>
      <alt>capital T, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ubreve</name>
      <value>Ŭ</value>
      <unicode>016c</unicode>
      <mapping>U</mapping>
      <alt>capital U, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Udblac</name>
      <value>Ű</value>
      <unicode>0170</unicode>
      <mapping>U</mapping>
      <alt>capital U, double acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Umacr</name>
      <value>Ū</value>
      <unicode>016a</unicode>
      <mapping>U</mapping>
      <alt>capital U, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Uogon</name>
      <value>Ų</value>
      <unicode>0172</unicode>
      <mapping>U</mapping>
      <alt>capital U, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Uring</name>
      <value>Ů</value>
      <unicode>016e</unicode>
      <mapping>U</mapping>
      <alt>capital U, ring</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Utilde</name>
      <value>Ũ</value>
      <unicode>0168</unicode>
      <mapping>U</mapping>
      <alt>capital U, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Wcirc</name>
      <value>Ŵ</value>
      <unicode>0174</unicode>
      <mapping>W</mapping>
      <alt>capital W, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Ycirc</name>
      <value>Ŷ</value>
      <unicode>0176</unicode>
      <mapping>Y</mapping>
      <alt>capital Y, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Yuml</name>
      <value>Ÿ</value>
      <unicode>0178</unicode>
      <mapping>Y</mapping>
      <alt>capital Y, dieresis or umlaut mark</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Zacute</name>
      <value>Ź</value>
      <unicode>0179</unicode>
      <mapping>Z</mapping>
      <alt>capital Z, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Zcaron</name>
      <value>Ž</value>
      <unicode>017d</unicode>
      <mapping>Z</mapping>
      <alt>capital Z, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>Zdot</name>
      <value>Ż</value>
      <unicode>017b</unicode>
      <mapping>Z</mapping>
      <alt>capital Z, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>abreve</name>
      <value>ă</value>
      <unicode>0103</unicode>
      <mapping>a</mapping>
      <alt>small a, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>amacr</name>
      <value>ā</value>
      <unicode>0101</unicode>
      <mapping>a</mapping>
      <alt>small a, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>aogon</name>
      <value>ą</value>
      <unicode>0105</unicode>
      <mapping>a</mapping>
      <alt>small a, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cacute</name>
      <value>ć</value>
      <unicode>0107</unicode>
      <mapping>c</mapping>
      <alt>small c, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ccaron</name>
      <value>č</value>
      <unicode>010d</unicode>
      <mapping>c</mapping>
      <alt>small c, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ccirc</name>
      <value>ĉ</value>
      <unicode>0109</unicode>
      <mapping>c</mapping>
      <alt>small c, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>cdot</name>
      <value>ċ</value>
      <unicode>010b</unicode>
      <mapping>c</mapping>
      <alt>small c, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dcaron</name>
      <value>ď</value>
      <unicode>010f</unicode>
      <mapping>d</mapping>
      <alt>small d, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>delta</name>
      <value>δ</value>
      <unicode>03b4</unicode>
      <mapping>[small delta]</mapping>
      <alt>small delta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>diam</name>
      <value>⋄</value>
      <unicode>22c4</unicode>
      <mapping>[open diamond]</mapping>
      <alt>open diamond</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>diams</name>
      <value>♦</value>
      <unicode>2666</unicode>
      <mapping>[diamond suit symbol]</mapping>
      <alt>diamond suit symbol </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>divide</name>
      <value>÷</value>
      <unicode>00f7</unicode>
      <mapping>[divide sign]</mapping>
      <alt>divide sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>dstrok</name>
      <value>đ</value>
      <unicode>0111</unicode>
      <mapping>d</mapping>
      <alt>small d, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ecaron</name>
      <value>ě</value>
      <unicode>011b</unicode>
      <mapping>e</mapping>
      <alt>small e, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <!-- I made this name up NHu -->
      <name>ecirtil</name>
      <value>ễ</value>
      <unicode>1ec5</unicode>
      <mapping>e</mapping>
      <alt>small e with circumflex and tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>edot</name>
      <value>ė</value>
      <unicode>0117</unicode>
      <mapping>e</mapping>
      <alt>small e, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>emacr</name>
      <value>ē</value>
      <unicode>0113</unicode>
      <mapping>e</mapping>
      <alt>small e, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>eogon</name>
      <value>ę</value>
      <unicode>0119</unicode>
      <mapping>e</mapping>
      <alt>small e, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gacute</name>
      <value>ǵ</value>
      <unicode>01f5</unicode>
      <mapping>g</mapping>
      <alt>small g, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gamma</name>
      <value>γ</value>
      <unicode>03b3</unicode>
      <mapping>[gamma]</mapping>
      <alt>small gamma, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gbreve</name>
      <value>ğ</value>
      <unicode>011f</unicode>
      <mapping>g</mapping>
      <alt>small g, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gcaron</name>
      <value>ǧ</value>
      <unicode>01e7</unicode>
      <mapping>g</mapping>
      <alt>small g, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gcirc</name>
      <value>ĝ</value>
      <unicode>011d</unicode>
      <mapping>g</mapping>
      <alt>small g, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>gdot</name>
      <value>ġ</value>
      <unicode>0121</unicode>
      <mapping>g</mapping>
      <alt>small g, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>hcirc</name>
      <value>ĥ</value>
      <unicode>0125</unicode>
      <mapping>h</mapping>
      <alt>small h, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>hstrok</name>
      <value>ħ</value>
      <unicode>0127</unicode>
      <mapping>h</mapping>
      <alt>small h, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ibreve</name>
      <value>ĭ</value>
      <unicode>012d</unicode>
      <mapping>i</mapping>
      <alt>small i, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>imacr</name>
      <value>ī</value>
      <unicode>012b</unicode>
      <mapping>i</mapping>
      <alt>small i, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>iogon</name>
      <value>į</value>
      <unicode>012f</unicode>
      <mapping>i</mapping>
      <alt>small i, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>itilde</name>
      <value>ĩ</value>
      <unicode>0129</unicode>
      <mapping>i</mapping>
      <alt>small i, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>jcirc</name>
      <value>ĵ</value>
      <unicode>0135</unicode>
      <mapping>j</mapping>
      <alt>small j, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>kcedil</name>
      <value>ķ</value>
      <unicode>0137</unicode>
      <mapping>k</mapping>
      <alt>small k, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lacute</name>
      <value>ĺ</value>
      <unicode>013a</unicode>
      <mapping>l</mapping>
      <alt>small l, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lcaron</name>
      <value>ľ</value>
      <unicode>013e</unicode>
      <mapping>l</mapping>
      <alt>small l, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lcedil</name>
      <value>ļ</value>
      <unicode>013c</unicode>
      <mapping>l</mapping>
      <alt>small l, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>lstrok</name>
      <value>ł</value>
      <unicode>0142</unicode>
      <mapping>l</mapping>
      <alt>small l, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>nacute</name>
      <value>ń</value>
      <unicode>0144</unicode>
      <mapping>n</mapping>
      <alt>small n, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ncaron</name>
      <value>ň</value>
      <unicode>0148</unicode>
      <mapping>n</mapping>
      <alt>small n, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ncedil</name>
      <value>ņ</value>
      <unicode>0146</unicode>
      <mapping>n</mapping>
      <alt>small n, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>odblac</name>
      <value>ő</value>
      <unicode>0151</unicode>
      <mapping>o</mapping>
      <alt>small o, double acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>odot</name>
      <value>⊙</value>
      <unicode>2299</unicode>
      <mapping>o</mapping>
      <alt>odot B: middle dot in circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>omacr</name>
      <value>ō</value>
      <unicode>014d</unicode>
      <mapping>o</mapping>
      <alt>small o, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>omega</name>
      <value>ω</value>
      <unicode>03c9</unicode>
      <mapping>[small omega]</mapping>
      <alt>omega small omega, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>racute</name>
      <value>ŕ</value>
      <unicode>0155</unicode>
      <mapping>r</mapping>
      <alt>small r, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rcaron</name>
      <value>ř</value>
      <unicode>0159</unicode>
      <mapping>r</mapping>
      <alt>small r, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rcedil</name>
      <value>ŗ</value>
      <unicode>0157</unicode>
      <mapping>r</mapping>
      <alt>small r, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>sacute</name>
      <value>ś</value>
      <unicode>015b</unicode>
      <mapping>s</mapping>
      <alt>small s, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>scaron</name>
      <value>š</value>
      <unicode>0161</unicode>
      <mapping>s</mapping>
      <alt>small s, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>scedil</name>
      <value>ş</value>
      <unicode>015f</unicode>
      <mapping>s</mapping>
      <alt>small s, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>scirc</name>
      <value>ŝ</value>
      <unicode>015d</unicode>
      <mapping>s</mapping>
      <alt>small s, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>spades</name>
      <value>♠</value>
      <unicode>2660</unicode>
      <mapping>[spade suit symbol]</mapping>
      <alt>spade suit symbol </alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tcaron</name>
      <value>ť</value>
      <unicode>0165</unicode>
      <mapping>t</mapping>
      <alt>small t, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tcedil</name>
      <value>ţ</value>
      <unicode>0163</unicode>
      <mapping>t</mapping>
      <alt>small t, cedilla</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>there4</name>
      <value>∴</value>
      <unicode>2234</unicode>
      <mapping>[therefore]</mapping>
      <alt>therefore</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>thetas</name>
      <value>θ</value>
      <unicode>03b8</unicode>
      <mapping>[small theta]</mapping>
      <alt>small theta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>thetav</name>
      <value>ϑ</value>
      <unicode>03d1</unicode>
      <mapping>[curly or open theta]</mapping>
      <alt>curly or open theta</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>timesb</name>
      <value>⊠</value>
      <unicode>22a0</unicode>
      <mapping>[boxtimes]</mapping>
      <alt>multiply sign in box</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tprime</name>
      <value>‴</value>
      <unicode>2034</unicode>
      <mapping>[triple prime]</mapping>
      <alt>triple prime</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>trade</name>
      <value>™</value>
      <unicode>2122</unicode>
      <mapping>[trade mark sign]</mapping>
      <alt>trade mark sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tridot</name>
      <value>◬</value>
      <unicode>25ec</unicode>
      <mapping>[dot in triangle]</mapping>
      <alt>dot in triangle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>trie</name>
      <value>≜</value>
      <unicode>225c</unicode>
      <mapping>[triangleq R: triangle, equals]</mapping>
      <alt>triangle, equals</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>tstrok</name>
      <value>ŧ</value>
      <unicode>0167</unicode>
      <mapping>t</mapping>
      <alt>small t, stroke</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uArr</name>
      <value>⇑</value>
      <unicode>21d1</unicode>
      <mapping>[double upward arrow]</mapping>
      <alt>double upward arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uarr</name>
      <value>↑</value>
      <unicode>2191</unicode>
      <mapping>[upward arrow]</mapping>
      <alt>upward arrow</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ubreve</name>
      <value>ŭ</value>
      <unicode>016d</unicode>
      <mapping>u</mapping>
      <alt>small u, breve</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>udblac</name>
      <value>ű</value>
      <unicode>0171</unicode>
      <mapping>[double acute accent]</mapping>
      <alt>small u, double acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uharr</name>
      <value>↾</value>
      <unicode>21be</unicode>
      <mapping>[upharpoonright]</mapping>
      <alt>up harpoon right, restriction</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>umacr</name>
      <value>ū</value>
      <unicode>016b</unicode>
      <mapping>u</mapping>
      <alt>small u, macron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uogon</name>
      <value>ų</value>
      <unicode>0173</unicode>
      <mapping>u</mapping>
      <alt>small u, ogonek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>upsi</name>
      <value>υ</value>
      <unicode>03c5</unicode>
      <mapping>[small upsilon]</mapping>
      <alt>small upsilon, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>uring</name>
      <value>ů</value>
      <unicode>016f</unicode>
      <mapping>u</mapping>
      <alt>small u, ring</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>utilde</name>
      <value>ũ</value>
      <unicode>0169</unicode>
      <mapping>u</mapping>
      <alt>small u, tilde</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>utri</name>
      <value>▵</value>
      <unicode>25b5</unicode>
      <mapping>[up triangle, open]</mapping>
      <alt>up triangle, open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>utrif</name>
      <value>▴</value>
      <unicode>25b4</unicode>
      <mapping>[blacktriangle]</mapping>
      <alt>black triangle up, filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>vellip</name>
      <value>⋮</value>
      <unicode>22ee</unicode>
      <mapping>[vertical ellipsis]</mapping>
      <alt>vertical ellipsis</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>wcirc</name>
      <value>ŵ</value>
      <unicode>0175</unicode>
      <mapping>w</mapping>
      <alt>small w, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>wedgeq</name>
      <value>≙</value>
      <unicode>2259</unicode>
      <mapping>[corresponds to]</mapping>
      <alt>corresponds to (wedge, equals)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>xi</name>
      <value>ξ</value>
      <unicode>03be</unicode>
      <mapping>[small xi]</mapping>
      <alt>small xi, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>ycirc</name>
      <value>ŷ</value>
      <unicode>0177</unicode>
      <mapping>y</mapping>
      <alt>small y, circumflex accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>yen</name>
      <value>¥</value>
      <unicode>00a5</unicode>
      <mapping>[yen sign]</mapping>
      <alt>yen sign</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>zacute</name>
      <value>ź</value>
      <unicode>017a</unicode>
      <mapping>z</mapping>
      <alt>small z, acute accent</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>zcaron</name>
      <value>ž</value>
      <unicode>017e</unicode>
      <mapping>z</mapping>
      <alt>small z, caron</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>zdot</name>
      <value>ż</value>
      <unicode>017c</unicode>
      <mapping>z</mapping>
      <alt>small z, dot above</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>zeta</name>
      <value>ζ</value>
      <unicode>03b6</unicode>
      <mapping>[small zeta]</mapping>
      <alt>small zeta, Greek</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.cirfr</name>
      <value>◑</value>
      <unicode>25d1</unicode>
      <mapping>[circle filled right]</mapping>
      <alt>circle filled right</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.olcir</name>
      <value>◎</value>
      <unicode>25ce</unicode>
      <mapping>[bullseye (circle within another circle)]</mapping>
      <alt>bullseye (circle within another circle)</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squt</name>
      <value>◇</value>
      <unicode>25c7</unicode>
      <mapping>[square tilted open]</mapping>
      <alt>square tilted open</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squtf</name>
      <value>◆</value>
      <unicode>25c6</unicode>
      <mapping>[square tilted filled]</mapping>
      <alt>square tilted filled</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshr</name>
      <value>▨</value>
      <unicode>25a8</unicode>
      <mapping>[square shaded right slant]</mapping>
      <alt>square shaded right slant</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshl</name>
      <value>▧</value>
      <unicode>25a7</unicode>
      <mapping>[square shaded left slant]</mapping>
      <alt>square shaded left slant</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshv</name>
      <value>▥</value>
      <unicode>25a5</unicode>
      <mapping>[square shaded vert]</mapping>
      <alt>square shaded vert</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshh</name>
      <value>▤</value>
      <unicode>25a4</unicode>
      <mapping>[square shaded horiz]</mapping>
      <alt>square shaded horiz</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshlr</name>
      <value>▩</value>
      <unicode>25a9</unicode>
      <mapping>[square shaded diagonal crosshatch]</mapping>
      <alt>square shaded diagonal crosshatch</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.squshhv</name>
      <value>▦</value>
      <unicode>25a6</unicode>
      <mapping>[square shaded horizontal/vertical crosshatch]</mapping>
      <alt>square shaded horizontal/vertical crosshatch</alt>
      <websafe>true</websafe>
    </char>
    <!-- The following RSC entities do not fall into the Unicode User Area -->
    <char>
      <name>z.oone</name>
      <value>①</value>
      <unicode>2460</unicode>
      <mapping>[circle containing 1]</mapping>
      <alt>circle containing 1</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.otwo</name>
      <value>②</value>
      <unicode>2461</unicode>
      <mapping>[circle containing 2]</mapping>
      <alt>circle containing 2</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.othr</name>
      <value>③</value>
      <unicode>2462</unicode>
      <mapping>[circle containing 3]</mapping>
      <alt>circle containing 3</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.ofour</name>
      <value>④</value>
      <unicode>2463</unicode>
      <mapping>[circle containing 4]</mapping>
      <alt>circle containing 4</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.ofive</name>
      <value>⑤</value>
      <unicode>2464</unicode>
      <mapping>[circle containing 5]</mapping>
      <alt>circle containing 5</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.osix</name>
      <value>⑥</value>
      <unicode>2465</unicode>
      <mapping>[circle containing 6]</mapping>
      <alt>circle containing 6</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.osev</name>
      <value>⑦</value>
      <unicode>2466</unicode>
      <mapping>[circle containing 7]</mapping>
      <alt>circle containing 7</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.oeigh</name>
      <value>⑧</value>
      <unicode>2467</unicode>
      <mapping>[circle containing 8]</mapping>
      <alt>circle containing 8</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.onine</name>
      <value>⑨</value>
      <unicode>2468</unicode>
      <mapping>[circle containing 9]</mapping>
      <alt>circle containing 9</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.cirf</name>
      <value>●</value>
      <unicode>25cf</unicode>
      <mapping>[black circle]</mapping>
      <alt>black circle</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.cirfl</name>
      <value>◐</value>
      <unicode>25d0</unicode>
      <mapping>[circle filled left]</mapping>
      <alt>circle filled left</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>z.rad</name>
      <value>˙</value>
      <unicode>02d9</unicode>
      <mapping>[radical dot]</mapping>
      <alt>radical dot</alt>
      <websafe>true</websafe>
    </char>
  </xsl:template>
</xsl:stylesheet>