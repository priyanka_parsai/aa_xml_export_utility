﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <!-- lsquo the same as rsquor -->
  <xsl:template name="RISSpecificValuesList">
    <char >
      <name>lsquo</name>
      <value>‘</value>
      <unicode>2018</unicode>
      <mapping>'</mapping>
      <alt>single quotation mark, left</alt>
      <websafe>true</websafe>
    </char>
    <!-- ldquo the same as rdquor -->
    <char>
      <name>ldquo</name>
      <value>“</value>
      <unicode>201c</unicode>
      <mapping>"</mapping>
      <alt>double quotation mark, left</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rsquo</name>
      <value>’</value>
      <unicode>2019</unicode>
      <mapping>'</mapping>
      <alt>single quotation mark, right</alt>
      <websafe>true</websafe>
    </char>
    <char>
      <name>rdquo</name>
      <value>”</value>
      <unicode>201d</unicode>
      <mapping>"</mapping>
      <alt>double quotation mark, right</alt>
      <websafe>true</websafe>
    </char>
    <!-- rdquor the sames ldquo -->
    <char>
      <name>rdquor</name>
      <value>“</value>
      <unicode>201c</unicode>
      <mapping>[rising dbl quote]</mapping>
      <alt>rising dbl quote, right (high)</alt>
      <websafe>true</websafe>
    </char>
    <!-- rsquor the same as lsquo -->
    <char>
      <name>rsquor</name>
      <value>‘</value>
      <unicode>2018</unicode>
      <mapping>'</mapping>
      <alt>rising single quote, right (high)</alt>
      <websafe>true</websafe>
    </char>
  </xsl:template>
</xsl:stylesheet>