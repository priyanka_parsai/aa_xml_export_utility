﻿using RSC.Database.Api.Common.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AAPDataContract = RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using RSC.Database.Api.Common.Helpers.AnalyticalAbstractProduction;
using RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using System;
using System.Collections.Concurrent;
using System.Text;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;
using NLog;

namespace AnalyticalAbstractXmlImportUtility
{
    public class ArticlesApi
    {
        #region Private Fields
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private const string ClassName = "AnalyticalAbstractXmlImportUtility.ArticleApi";
        private const string MLStatusFormat = "{0} {1} {2} {3} {4} {5} {6}";
        private const string XmlHeaderValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        private const string DTDEntitiesDeclarationFormat = "<!DOCTYPE monographs[{0}]>";
        private const string AADTDCacheKey = "AADTDCacheKey";
        private const string MonographsTagStart = "<monographs>";
        private const string MonographsTagEnd = "</monographs>";
        public const string EncodedGreaterThan = "&gt;";
        public const string GreaterThanPlaceholder = "#greaterthan#";
        private const string Ampersand = "&amp;";

        #endregion Private Fields

        #region Public Constructors

        public ArticlesApi()
        {
            //_logger = LogManager.GetCurrentClassLogger();
        }

        #endregion Public Constructors

        #region Public Methods

        public AAPDataContract.SearchResults SearchArticles(AAPDataContract.SearchCriteria searchCriteria)
        {
              var searchResult = new AAPDataContract.SearchResults
            {
                Results = new AAPDataContract.Results
                {
                    Result = new List<AAPDataContract.Result>()
                }
            };

            var concurrentResults = new ConcurrentBag<AAPDataContract.Result>();
            var dataApader = new DataAdapter();
            string searchResultsData = dataApader.GetArticleSearchResults(searchCriteria);

            if (!string.IsNullOrWhiteSpace(searchResultsData))
            {
                var helper = new MarkLogicSearchHelper(searchResultsData);
                var searchResults = helper.GetSearchResults();
                var uri = ConfigurationManager.AppSettings.Get("GetArticleByUriUri");
                if (searchResults != null && searchResults.results != null &&
                    searchResults.results.result != null)
                {

                    int maxDegreeOfParallelism = Convert.ToInt32(ConfigurationManager.AppSettings.Get(Constants.MaxDegreeOfParallelism));
                    //No of parallel threads to be run at a time
                    ParallelOptions parallelOptions = new ParallelOptions();
                    parallelOptions.MaxDegreeOfParallelism = maxDegreeOfParallelism;

                    Parallel.ForEach(searchResults.results.result, parallelOptions, result =>
                    {
                        AAPDataContract.Article article = null;
                        if (searchCriteria.IgnoreArticleDetails)
                        {
                            article = new Article();
                            article.Uri = result.uri;
                            article.SystemId = result.uri.Substring(result.uri.LastIndexOf(("/"), StringComparison.Ordinal) + 1).Split('.')[0];
                        }
                        else
                        {
                            article = new Article();
                            article = GetArticleFromML(string.Format(uri, result.uri), true);
                            article.Uri = result.uri;
                        }
                        concurrentResults.Add(new AAPDataContract.Result { Article = article });
                    });

                    searchResult.Results.Result = concurrentResults.OrderBy(d => searchResults.results.result.Select(i => i.uri).ToList().IndexOf(d.Article.Uri)).ToList();
                    searchResult.Results.PageNumber = searchResults.results.pageNo;
                    searchResult.Results.PageSize = searchResults.results.pageSize;
                    searchResult.Results.TotalCount = searchResults.results.totalCount;
                }
            }

            return searchResult;
        }

        //public AAPDataContract.XMLSearchResults GetArticleXML(AAPDataContract.SearchCriteria searchCriteria)
        //{
        //    var searchResult = new AAPDataContract.XMLSearchResults();
        //    searchResult.Monograph = new List<string>();
        //    try
        //    {
        //        if (searchCriteria.Id != null)
        //        {
        //            //var article = GetArticleById(searchCriteria.Id, false);
        //            //var results = new List<string>();
        //            //searchResult.Monograph.Add(article.RawMonographXml);
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        _logger.Error(exception);
        //    }
        //    return searchResult;
        //}

        public AAPDataContract.XMLSearchResults SearchArticlesXML(AAPDataContract.SearchCriteria searchCriteria)
        {
            var searchResult = new AAPDataContract.XMLSearchResults();
            searchResult.Monograph = new List<string>();
            try
            {

                var dataApader = new DataAdapter();
                searchCriteria.PageNo = 1;
                searchCriteria.PageSize = 120;
                var uri = ConfigurationManager.AppSettings.Get("GetArticleByUriUri");
                string searchResultsData = dataApader.GetArticleSearchResults(searchCriteria);
                var exceptions = new ConcurrentQueue<Exception>();
                var results = new List<string>();
                if (!string.IsNullOrWhiteSpace(searchResultsData))
                {
                    var helper = new MarkLogicSearchHelper(searchResultsData);
                    var searchResults = helper.GetSearchResults();
                    int pageIndex = 1;
                    var totalCount = searchResults.results.totalCount;
                    if (totalCount == 0) return searchResult;
                    Parallel.ForEach(searchResults.results.result, result =>
                    {
                        if (result != null && !string.IsNullOrEmpty(result.uri))
                        {
                            try
                            {
                                var article = GetArticleFromML(string.Format(uri, result.uri), true);
                                if (article != null)
                                {
                                    searchResult.Monograph.Add(article.RawMonographXml);
                                }
                            }
                            catch (Exception ex)
                            {
                                exceptions.Enqueue(ex);
                                    //_logger.Error(ex);
                                }
                        }
                    });

                    var totalPages = 0;
                    var pageSize = 120;

                    if (totalCount > 120)
                    {
                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("XmlExtractCount")))
                        {
                            totalCount = Convert.ToInt16(ConfigurationManager.AppSettings.Get("XmlExtractCount"));
                        }
                        totalPages = (int)Math.Ceiling(totalCount / (double)pageSize);

                        for (var i = 1; i < totalPages; i++)
                        {
                            pageIndex += 1;
                            searchCriteria.PageNo = pageIndex;
                            searchCriteria.PageSize = 120;
                            string searchResultsRawDataNextSet = dataApader.GetArticleSearchResults(searchCriteria);
                            var mlHelperResultsNextSet = new MarkLogicSearchHelper(searchResultsRawDataNextSet);

                            var searchResultsNextSet = mlHelperResultsNextSet.GetSearchResults();
                            if (searchResultsNextSet != null && searchResultsNextSet.results != null &&
                       searchResultsNextSet.results.result != null)
                            {
                                Parallel.ForEach(searchResultsNextSet.results.result, result =>
                                {
                                    if (result != null && !string.IsNullOrEmpty(result.uri))
                                    {
                                        try
                                        {
                                            var article = GetArticleFromML(string.Format(uri, result.uri), true);
                                            if (article != null)
                                            {
                                                searchResult.Monograph.Add(article.RawMonographXml);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            exceptions.Enqueue(ex);
                                                //_logger.Error(ex);
                                            }
                                    }
                                });
                            }
                        }
                    }

                }

            }
            catch (Exception exception)
            {
                _logger.Error(exception);
            }
            finally
            {

            }
            return searchResult;
        }

        public string GetXMlExtract(AAPDataContract.SearchCriteria searchCriteria, bool singleXMlExtract = false)
        {
            // _logger.Trace("Started " + ClassName + ".GetXMlExtract(SearchCriteria searchCriteria)");
            var xmlSearchResults = string.Empty;
            var searchResult = new AAPDataContract.XMLSearchResults();
            if (singleXMlExtract)
            {
                //searchResult = GetArticleXML(searchCriteria);
            }
            else
            {
                searchResult = SearchArticlesXML(searchCriteria);
            }
            //Convert Search Results to XML
            xmlSearchResults = SearchResultsToXML(searchResult);
            //Replace EncodedLessThan with Less than place holder and EncodedGreaterThan than with GreaterThanPlaceholder
            xmlSearchResults = xmlSearchResults.Replace(Constants.EncodedLessThan, Constants.LessThanPlaceholder).Replace(EncodedGreaterThan, GreaterThanPlaceholder);
            //Transform XML results to XMLExtract
            var dataApader = new DataAdapter();
            xmlSearchResults = dataApader.TransformSearchresultstoXMlExtract(xmlSearchResults, singleXMlExtract);
            //Replace  Less than place holder with  EncodedLessThan  and GreaterThanPlaceholder than with EncodedGreaterThan
            xmlSearchResults = xmlSearchResults.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan).Replace(GreaterThanPlaceholder, EncodedGreaterThan);
            //ReplaceAmperSand  
            xmlSearchResults = ReplaceAmperSand(xmlSearchResults);

            //_logger.Trace("Completed " + ClassName + ".GetXMlExtract(SearchCriteria searchCriteria)");
            return xmlSearchResults;
        }

        #endregion Public Methods

        #region Private Methods

        private static AAPDataContract.Article DeserializeArticle(string articleXml)
        {
            AAPDataContract.Article article = null;
            if (!string.IsNullOrWhiteSpace(articleXml))
            {
                //Replace dummy string #lessthan# to &lt; , required to save in marklogic
                if (articleXml.IndexOf(Constants.LessThanPlaceholder) >= 0)
                {
                    articleXml = articleXml.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan);
                }

                ArticleHelper helper = new ArticleHelper(articleXml);
                article = helper.GetArticle();
            }
            return article;
        }

        private AAPDataContract.Article GetArticleFromML(string uri, bool fullText)
        {
            AAPDataContract.Article article = null;
            var adapter = new DataAdapter();
            var articleData = adapter.GetArticle(uri);
            if (!string.IsNullOrEmpty(articleData))
            {
                //Replace dummy string #lessthan# to &lt; , required to save in marklogic
                if (articleData.IndexOf(Constants.LessThanPlaceholder) >= 0)
                {
                    articleData = articleData.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan);
                }
                ArticleHelper helper = new ArticleHelper(articleData);
                article = helper.GetArticle();
                article.RawMonographXml = adapter.GetRawMonograph();
                _logger.Trace("Article Id: " + article.SystemId);
            }
            if (article == null)
            {
                //var httpException = new RscApiException("Article not found", RSC.Database.Api.Common.Exceptions.ErrorCodes.Not_Found);
                //throw httpException;
                _logger.Trace("Article Not found: " + uri);
            }
            return article;
        }

        private static string RemoveLessThanConstant(string resultArticle)
        {
            //Replace dummy string #lessthan# to < 
            if (resultArticle.IndexOf(Constants.LessThanPlaceholder) >= 0)
            {
                resultArticle = resultArticle.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan);
            }
            return resultArticle;
        }

        private string Serialize<T>(T tData)
        {
            return XmlSerializeDeserializeHelper.SerializeToXml<T>(tData);
        }
        private string SearchResultsToXML(XMLSearchResults xmlSRObj)
        {
            StringBuilder xmlBuilder = new StringBuilder();
            if (xmlSRObj != null && xmlSRObj.Monograph.Count() > 0)
            {
                var xmlWithDTD = AddDTDEntiesValue();
                xmlBuilder.Append(xmlWithDTD);
                xmlBuilder.Append(MonographsTagStart);
                foreach (var xml in xmlSRObj.Monograph.ToList())
                {

                    xmlBuilder.Append(XMLSerializeHelper.ReplaceHexadecimalUnicodeValues(xml.ToString()));
                }
                xmlBuilder.Append(MonographsTagEnd);

            }
            return xmlBuilder.ToString();
        }

        /// <summary>
        /// Method to Handle Entities
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private string AddDTDEntiesValue()
        {
            string xmlnode = XmlHeaderValue;
            var cachedDTD = (string)CacheHelper.GetDataFromCache(AADTDCacheKey);
            var dtdString = string.Empty;
            if (!string.IsNullOrEmpty(cachedDTD))
            {
                dtdString = cachedDTD;
            }
            else
            {
                dtdString = ReadDTDFile();
                CacheHelper.AddDataToCache(AADTDCacheKey, dtdString);
            }
            return string.Format("{0}{1}", xmlnode, string.Format(DTDEntitiesDeclarationFormat, dtdString));

        }
        private string ReadDTDFile()
        {
            var dtdString = string.Empty;
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = ConfigurationManager.AppSettings.Get("DTDEntitiesFile");
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    dtdString = reader.ReadToEnd();
                }
            }
            return dtdString;
        }

        /// <summary>
        /// Replace & to &amp; and make sure &amp; is not replaced again.
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        private string ReplaceAmperSand(string requestData)
        {
            if (!string.IsNullOrEmpty(requestData))
            {
                //Encode & to &amp; 
                Regex ampersandRegex = new Regex("&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)");
                requestData = ampersandRegex.Replace(requestData, Ampersand);
            }
            return requestData;
        }
        #endregion Private Methods
    }
}