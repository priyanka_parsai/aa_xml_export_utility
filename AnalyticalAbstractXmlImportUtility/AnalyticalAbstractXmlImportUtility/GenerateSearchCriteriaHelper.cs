﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DC = RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
namespace AnalyticalAbstractXmlImportUtility
{
    public static class GenerateSearchCriteriaHelper
    {
        #region Private Constants

        private const string Author = "rs:citauth";
        private const string DateFormat = "yyyy-MM-dd";
        private const string DOI = "rs:citdoi";
        private const string FromDate = "rs:pubdate1";
        private const string ID = "rs:id";
        private const string Journal = "rs:citjnl";
        private const string MFreeText = "rs:mfreetext";
        private const string Monograph = "monograph";
        private const string PageNo = "rs:pageno";
        private const string PageSize = "rs:pagesize";
        private const string PublicId = "rs:pubid";
        private const string OrderDirection = "rs:sortdir";
        private const string SortBy = "rs:sortby";
        private const string Title = "rs:title";
        private const string ToDate = "rs:pubdate2";
        private const string Type = "rs:type";
        private const string Status = "rs:status";
        private const string PubmedId = "rs:citpmid";
        private const string User = "rs:user";
        private const string Creator = "rs:crtid";
        private const string CreatorDate1 = "rs:crtdate1";
        private const string CreatorDate2 = "rs:crtdate2";
        private const string Modifier = "rs:modid";
        private const string ModifierDate1 = "rs:moddate1";
        private const string ModifierDate2 = "rs:moddate2";
        private const string Analyte = "rs:analyte";
        private const string Matrix = "rs:matrix";
        private const string Concept = "rs:concept";
        private const string Category = "rs:ancat";
        private const string PrintId = "rs:printid";
        private const string SortDirection = "rs:sortdir";

        private const char Separator = '|';
        #endregion Private Constants

        #region Methods

        public static string CreateArticleSearchCriteria(DC.SearchCriteria searchCriteria)
        {
            var queryParameter = string.Empty;
            if (searchCriteria == null)
            {
                searchCriteria = new DC.SearchCriteria();
            }
            searchCriteria.Type = Monograph;
            queryParameter = GenerateSearchCriteria(searchCriteria);
            queryParameter = CreatePageCriteria(queryParameter, searchCriteria.PageNo, searchCriteria.PageSize);
            return queryParameter;
        }

        private static string CreatePageCriteria(string queryString, int pageNumber, int pageSize)
        {
            var mlParametersToSend = new StringBuilder(queryString);
            if (!string.IsNullOrEmpty(queryString))
                mlParametersToSend.Append("&");
            if (pageNumber > 0)
                mlParametersToSend.Append(CreateMlQueryParameters(PageNo, pageNumber.ToString(CultureInfo.InvariantCulture)));
            if (pageSize > 0)
                mlParametersToSend.Append(CreateMlQueryParameters(PageSize, pageSize.ToString(CultureInfo.InvariantCulture)));
            return mlParametersToSend.ToString().TrimEnd('&'); ;
        }

        private static string GenerateSearchCriteria(DC.SearchCriteria searchCriteria)
        {
            var mlParametersToSend = new StringBuilder();

            if (!string.IsNullOrEmpty(searchCriteria.Authors))
            {
                var authorList = searchCriteria.Authors.Split(',');
                foreach (var author in authorList.Where(author => !string.IsNullOrEmpty(author)))
                {
                    mlParametersToSend.Append(CreateMlQueryParameters(Author, author));
                }
                authorList = null;
            }
            if (!string.IsNullOrEmpty(searchCriteria.FromDate))
            {
                DateTime dt;
                if (DateTime.TryParseExact(searchCriteria.FromDate, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                {
                    mlParametersToSend.Append(CreateMlQueryParameters(FromDate, searchCriteria.FromDate));
                }
            }
            if (!string.IsNullOrEmpty(searchCriteria.ToDate))
            {
                DateTime dt;
                if (DateTime.TryParseExact(searchCriteria.ToDate, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                {
                    mlParametersToSend.Append(CreateMlQueryParameters(ToDate, searchCriteria.ToDate));
                }
            }
            if (!string.IsNullOrEmpty(searchCriteria.AFreeText))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(MFreeText, searchCriteria.AFreeText, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Title))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(Title, searchCriteria.Title, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Id))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(ID, searchCriteria.Id));
            }
            if (!string.IsNullOrEmpty(searchCriteria.JournalTitle))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Journal, searchCriteria.JournalTitle, '|', true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.PublicId))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(PublicId, searchCriteria.PublicId));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Doi))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(DOI, searchCriteria.Doi));
            }
            if (!string.IsNullOrEmpty(searchCriteria.OrderDirection))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(OrderDirection, searchCriteria.OrderDirection));
            }
            if (!string.IsNullOrEmpty(searchCriteria.SortBy))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(SortBy, searchCriteria.SortBy));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Status))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Status, searchCriteria.Status, ','));
            }
            if (!string.IsNullOrEmpty(searchCriteria.PubmedId))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(PubmedId, searchCriteria.PubmedId, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.User))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(User, searchCriteria.User, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.CreatedBy))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(Creator, searchCriteria.CreatedBy));
            }
            if (!string.IsNullOrEmpty(searchCriteria.ModifiedBy))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(Modifier, searchCriteria.ModifiedBy));
            }
            if (searchCriteria.CreatedDate1 != null)
            {
                mlParametersToSend.Append(CreateMlQueryParameters(CreatorDate1, searchCriteria.CreatedDate1));
            }
            if (searchCriteria.CreatedDate2 != null)
            {
                mlParametersToSend.Append(CreateMlQueryParameters(CreatorDate2, searchCriteria.CreatedDate2));
            }
            if (searchCriteria.ModifiedDate1 != null)
            {
                mlParametersToSend.Append(CreateMlQueryParameters(ModifierDate1, searchCriteria.ModifiedDate1));
            }
            if (searchCriteria.ModifiedDate2 != null)
            {
                mlParametersToSend.Append(CreateMlQueryParameters(ModifierDate2, searchCriteria.ModifiedDate2));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Analyte))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Analyte, searchCriteria.Analyte, Separator, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Matrix))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Matrix, searchCriteria.Matrix, Separator, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Concept))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Concept, searchCriteria.Concept, Separator, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.Category))
            {
                mlParametersToSend.Append(CreateMlParametersForMultiple(Category, searchCriteria.Category, Separator, true));
            }
            if (!string.IsNullOrEmpty(searchCriteria.PrintId))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(PrintId, searchCriteria.PrintId));
            }
            if (!string.IsNullOrEmpty(searchCriteria.SortByDirection))
            {
                mlParametersToSend.Append(CreateMlQueryParameters(SortDirection, searchCriteria.SortByDirection));
            }

            return mlParametersToSend.ToString().TrimEnd('&');
        }

        private static string CreateMlQueryParameters(string name, string value, bool isEncoded = false)
        {
            return name + "=" + (isEncoded ? HttpUtility.UrlEncode(value) : value) + "&";
        }

        private static string CreateMlParametersForMultiple(string name, string value, char separator = ',', bool isEncoded = false)
        {
            var parametersBuilder = new StringBuilder();
            if (value.Contains(separator))
            {
                var list = value.Split(separator);
                foreach (string key in list)
                {
                    parametersBuilder.Append(CreateMlQueryParameters(name, key, isEncoded));
                }
            }
            else
            {
                parametersBuilder.Append(CreateMlQueryParameters(name, value, isEncoded));
            }
            return parametersBuilder.ToString();
        }

        #endregion Methods
    }
}
