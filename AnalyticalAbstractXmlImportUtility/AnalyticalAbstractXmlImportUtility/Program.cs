﻿using NLog;
using RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using System;
using System.Configuration;
using System.IO;

namespace AnalyticalAbstractXmlImportUtility
{
    class Program
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            string startDate = ConfigurationManager.AppSettings.Get("startDate");
            string endDate = Convert.ToDateTime(startDate).AddMonths(3).ToString("yyyy-MM-dd");            
            process(startDate, endDate);
            Console.ReadLine();

        }

        static bool process(string startDate, string endDate)
        {

            try
            {
                Console.WriteLine("Process started for articles published between " + startDate + " and " + endDate);
                CreateXmlFiles(startDate, endDate);
                Console.WriteLine("Process completed for articles published between " + startDate + " and " + endDate);
                Console.WriteLine("************************************************");
                Console.WriteLine("                    ");
                startDate = Convert.ToDateTime(endDate).AddDays(1).ToString("yyyy-MM-dd");
                if (Convert.ToDateTime(startDate) <= DateTime.Today)
                {
                    endDate = Convert.ToDateTime(startDate).AddMonths(3).ToString("yyyy-MM-dd");
                    if (Convert.ToDateTime(endDate) > DateTime.Today) endDate = DateTime.Today.ToString("yyyy-MM-dd");
                    process(startDate, endDate);
                    return true;
                }
                else
                {
                    Console.WriteLine("The process is completed. The application can now be closed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                _logger.Trace("Exception occurred for articles published between " + startDate + " and " + endDate);
                _logger.Error(ex, ex.Message);
                Console.WriteLine("An error has occurred. Please check logs for details");
                return true;
            }

        }

        static void CreateXmlFiles(string startDate, string endDate)
        {
            string filePath = ConfigurationManager.AppSettings.Get("filePath");
            string fileName = string.Format("AA_Xml_Data_{0}_to_{1}.xml", startDate, endDate);
            string file = Path.Combine(filePath, fileName);            
            File.WriteAllText(file, GetXmlExtract(startDate, endDate));
            Console.WriteLine(fileName + " created");
        }

        static string GetXmlExtract(string startDate, string endDate)
        {
            var articleApi = new ArticlesApi();
            var searchCriteria = new SearchCriteria();
            searchCriteria.SortBy = "moddate";
            searchCriteria.Status = "published";
            searchCriteria.PageNo = 1;
            searchCriteria.PageSize = 120;
            searchCriteria.FromDate = startDate;
            searchCriteria.ToDate = endDate;
            string content = string.Empty;
            var xmlstring = articleApi.GetXMlExtract(searchCriteria);
            return xmlstring;
        }
    }
}
