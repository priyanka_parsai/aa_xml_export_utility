﻿using NLog;
using RSC.Database.Api.Common.DataContracts.AnalyticalAbstractProduction;
using RSC.Database.Api.Common.Helpers;
using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace AnalyticalAbstractXmlImportUtility
{
    public class DataAdapter
    {
        #region Private Fields

        private const string ClassName = "AnalyticalAbstractXmlImportUtility.DataAdapter";
        private const string DataMonographToArticleKey = "AAProductionDataMonographToArticle";
        private const string ArticleToDataMonographKey = "AAProductionArticleToDataMonograph";
        private const string SearchResultsToXMlExtractFormatKey = "AAProductionSearchResultsToXMlExtractFormatKey";
        private const string SingleResultToXMlExtractFormatKey = "AAProductionSingleResultToXMlExtractFormatKey";

        private const string Ampersand = "&amp;";


        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private string RawMonograph = string.Empty;
        #endregion Private Fields

        #region Public Constructors

        public DataAdapter()
        {
            //_logger = LogManager.GetCurrentClassLogger();
        }

        #endregion Public Constructors

        #region Internal Methods

        internal string GetArticle(string uri)
        {
           // _logger.Trace(string.Format("Started {0}.GetArticle(uri:{1})", ClassName, uri));
            var articles = string.Empty;

            if (!string.IsNullOrEmpty(uri))
            {
                articles = MarkLogicHelper.MarkLogicRequestHelper(uri, Constants.Tunis, Constants.GET, string.Empty);
                if (!string.IsNullOrEmpty(articles))
                    RawMonograph = articles;
                articles = GetArticleXmlFromMonographXml(articles);
            }

            //_logger.Trace(string.Format("Completed {0}.GetArticle(uri:{1})", ClassName, uri));
            return articles;
        }

        internal string GetRawMonograph()
        {
            //TODO: Check the same functionality using XElement and GetValueFor()
            XDocument xdocActualFile = XDocument.Parse(RawMonograph, LoadOptions.PreserveWhitespace | LoadOptions.SetLineInfo);
            XElement monograph = xdocActualFile.Descendants().Where(x => x.Name == "{http://www.rsc.org/schema/chemical/monograph}monograph").FirstOrDefault();
            XElement contentFlags = xdocActualFile.Descendants().Where(x => x.Name == "{http://www.rsc.org/schema/tangier/container}content-flags").FirstOrDefault();
            XElement publishedDate = xdocActualFile.Descendants().Where(x => x.Name == "{http://www.rsc.org/schema/tangier/container}first-published").FirstOrDefault();

            if (contentFlags != null)
            {
                monograph.Add(contentFlags);
            }

            if (publishedDate != null)
            {
                if (publishedDate.HasAttributes && publishedDate.FirstAttribute.Name == "date")
                {
                    var pubDateTime = Convert.ToDateTime(publishedDate.FirstAttribute.Value);
                    publishedDate.FirstAttribute.SetValue(pubDateTime.ToString("dd MMM yyyy"));
                }
                monograph.Add(publishedDate);
            }

            RawMonograph = monograph.ToString();
            return RawMonograph;
        }

        internal string GetArticleSearchResults(SearchCriteria searchCriteria)
        {
            var queryParameter = string.Empty;
            var searchResults = string.Empty;
            if (searchCriteria != null)
            {
                queryParameter = CreateSearchParameter(searchCriteria);
            }
            //_logger.Trace(string.Format("Started {0} GetArticleSearchResults({1})", ClassName, queryParameter));
            if (!string.IsNullOrWhiteSpace(queryParameter))
            {
                var url =string.Format("{0}&{1}", ConfigurationManager.AppSettings.Get("SearchArticleBaseUri"), queryParameter);
                searchResults = MarkLogicHelper.MarkLogicRequestHelper(url, Constants.Tunis, Constants.GET);
            }
            //_logger.Trace(string.Format("Completed {0} GetArticleSearchResults({1})", ClassName, queryParameter));
            return searchResults;
        }

        internal string TransformSearchresultstoXMlExtract(string searchResultsXml, bool singleXMlExtract = false)
        {
            //_logger.Trace(string.Format("Started {0} TransformSearchresultstoXMlExtract({1})", ClassName, searchResultsXml));
            string transformedXMl = string.Empty;
            if (!string.IsNullOrWhiteSpace(searchResultsXml))
            {
                var xslName = ConfigurationManager.AppSettings.Get("XMLresultsToXMLExtractFile");
                transformedXMl = TransformXml(searchResultsXml, xslName, SearchResultsToXMlExtractFormatKey, true);
                transformedXMl = RemoveCdataAndMetaTag(transformedXMl);
            }
            //_logger.Trace(string.Format("Completed {0} TransformSearchresultstoXMlExtract({1})", ClassName, searchResultsXml));
            return transformedXMl;
        }

        #endregion Internal Methods

        #region Private Methods

        private string GetMonographXmlFromArticleXml(string articleXml)
        {
            string monographXml = string.Empty;
            if (!string.IsNullOrWhiteSpace(articleXml))
            {
                var xslName = "";// App.Configuration.ArticleToMonographXSLTFile;
                articleXml = RemoveCdataAndMetaTag(articleXml);
                //Replace & to &amp; before Transformation
                articleXml = ReplaceAmperSand(articleXml);

                monographXml = TransformXml(articleXml, xslName, ArticleToDataMonographKey);
            }
            return monographXml;
        }

        private string RemoveCdataAndMetaTag(string xmlData)
        {
            if (!string.IsNullOrWhiteSpace(xmlData))
            {
                xmlData = Regex.Replace(Regex.Replace(Regex.Replace(xmlData, @"<!\[CDATA\[>?", ""), @"\]+>+", ""), "meta:", string.Empty);
            }
            return xmlData;
        }

        private string GetArticleXmlFromMonographXml(string monographXml)
        {
            if (!string.IsNullOrWhiteSpace(monographXml))
            {
                var xslName = ConfigurationManager.AppSettings.Get("MonographToArticleXSLTFile");
                return TransformXml(monographXml, xslName, DataMonographToArticleKey);
            }
            return null;
        }

        private string TransformXml(string xml, string xslName, string key, bool isDTDSettingRequired = false)
        {
            string transformedXml = string.Empty;
            if (!string.IsNullOrWhiteSpace(xml))
            {
                var assembly = Assembly.GetExecutingAssembly();
                using (var xslStream = assembly.GetManifestResourceStream(xslName))
                {
                    //Transform the data resource xml received from MarkLogic into article xml.
                    var transformationHelper = new TransformationHelper();
                    transformedXml = transformationHelper.Transform(xml, key, xslStream, false, isDTDSettingRequired);
                }
            }

            return transformedXml;
        }

        private string CreateSearchParameter(SearchCriteria searchCriteria)
        {
            string queryParameter = string.Empty;
            queryParameter = GenerateSearchCriteriaHelper.CreateArticleSearchCriteria(searchCriteria);
            return queryParameter;
        }

        /// <summary>
        /// Replace dummy string #lessthan# to &lt; , required to save in marklogic
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        private string ReplaceLessthanPlaceholder(string requestData)
        {
            if (!string.IsNullOrEmpty(requestData))
            {
                if (requestData.IndexOf(Constants.LessThanPlaceholder) >= 0)
                {
                    requestData = requestData.Replace(Constants.LessThanPlaceholder, Constants.EncodedLessThan);
                }
            }
            return requestData;
        }


        /// <summary>
        /// Replace & to &amp; and make sure &amp; is not replaced again.
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        private string ReplaceAmperSand(string requestData)
        {
            if (!string.IsNullOrEmpty(requestData))
            {
                //Encode & to &amp; 
                Regex ampersandRegex = new Regex("&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)");
                requestData = ampersandRegex.Replace(requestData, Ampersand);
            }
            return requestData;
        }


        #endregion Private Methods
    }
}
