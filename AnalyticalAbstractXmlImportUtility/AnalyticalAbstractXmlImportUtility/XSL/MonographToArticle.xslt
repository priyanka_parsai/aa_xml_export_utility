﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:meta="http://www.rsc.org/schema/tangier/metadata" xmlns:con="http://www.rsc.org/schema/tangier/container" xmlns:mg="http://www.rsc.org/schema/chemical/monograph">

  <xsl:output method="xml" omit-xml-declaration="yes" encoding="utf-8" indent="no"/>
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="con:container">
    <Article>
      <xsl:for-each select="con:content-meta">
        <xsl:for-each select="con:admin">
          <Status>
            <xsl:value-of select="con:status"/>
          </Status>
          <CreatedBy>
            <xsl:value-of select="con:created-by"/>
          </CreatedBy>
          <FirstPublishedBy>
            <xsl:value-of select="con:first-published"/>
          </FirstPublishedBy>
          <FirstPublishedDate>
            <xsl:value-of select="con:first-published/@date"/>
          </FirstPublishedDate>
        </xsl:for-each>
      </xsl:for-each>
      
      <xsl:for-each select="con:content">
        <xsl:for-each select="mg:monograph">
          <systemid>
            <xsl:value-of select="mg:sysid"/>
          </systemid>
          <publicid>
            <xsl:value-of select="mg:publicid"/>
          </publicid>
          <printid>
            <xsl:value-of select="mg:printid"/>
          </printid>
          <title>
            <xsl:apply-templates select="mg:title"/>
          </title>
          <description>
            <xsl:apply-templates select="mg:description"/>
          </description>
           <info>
            <xsl:apply-templates select="mg:info"/>
          </info>
          <xsl:for-each select="mg:literature-references">
            <xsl:for-each select="mg:cit">
             <language>
                <xsl:value-of select="@lang"/>
              </language>
              <authors>
                <xsl:for-each select="mg:cit-auth">
                  <author>
                    <firstname>
                      <xsl:value-of select="mg:fname"/>
                    </firstname>
                    <lastname>
                      <xsl:value-of select="mg:surname"/>
                    </lastname>
                  </author>
                </xsl:for-each>
              </authors>
              <journal>
                <title>
                  <xsl:value-of select="mg:cit-title"/>
                </title>
              </journal>
              <subsyear>
                <xsl:value-of select="mg:cit-year"/>
              </subsyear>
              <journal-volume>
                <xsl:value-of select="mg:cit-vol"/>
              </journal-volume>
              <journal-issueno>
                <xsl:value-of select="mg:cit-iss"/>
              </journal-issueno>
              <firstpage>
                <xsl:value-of select="mg:cit-fpage"/>
              </firstpage>
              <lastpage>
                <xsl:value-of select="mg:cit-lpage"/>
              </lastpage>
              <doi>
                <xsl:value-of select="mg:doi-link"/>
              </doi>
              <PubmedId>
                <xsl:value-of select="mg:pmid-link"/>
              </PubmedId>
            </xsl:for-each>
          </xsl:for-each>
          <xsl:for-each select="mg:data">
            <xsl:for-each select="mg:analytes">
              <analytes>
                <xsl:for-each select="mg:analyte">
                  <analyte>
                    <xsl:attribute name="CASNo">
                      <xsl:value-of select="./@cas-no"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </analyte>
                </xsl:for-each>
              </analytes>
            </xsl:for-each>
            <xsl:for-each select="mg:concepts">
              <techniques>
                <xsl:for-each select="mg:concept">
                  <technique>
                    <xsl:value-of select="."/>
                  </technique>
                </xsl:for-each>
              </techniques>
            </xsl:for-each>
            <xsl:for-each select="mg:matrices">
              <matrices>
                <xsl:for-each select="mg:matrix">
                  <matrix>
                    <xsl:value-of select="."/>
                  </matrix>
                </xsl:for-each>
              </matrices>
            </xsl:for-each>
          </xsl:for-each>
          <xsl:for-each select="mg:keywords">
            <keywords>
              <xsl:for-each select="mg:keyword">
                <keyword>
                  <xsl:value-of select="."/>
                </keyword>
              </xsl:for-each>
            </keywords>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:for-each select="con:content-flags">
        <xsl:if test="con:analytical-category">
          <xsl:apply-templates select="."/>
        </xsl:if>
      </xsl:for-each>
     <xsl:for-each select="con:content-flags">
        <xsl:if test="con:jnlcontent-category">
          <xsl:apply-templates select="."/>
        </xsl:if>
      </xsl:for-each>
    </Article>
  </xsl:template>
  <xsl:template match="con:analytical-category">
    <FacetTree>
      <Id>
        <xsl:value-of select="./con:anc-sysid" />
      </Id>
      <Name>
        <xsl:value-of select="./con:anc-name" />
      </Name>
      <xsl:for-each select="./con:analytical-category">
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </FacetTree>
  </xsl:template>
  <xsl:template match="con:jnlcontent-category">
    <JournalCategoryTree>
      <Id>
        <xsl:value-of select="./con:jcc-sysid" />
      </Id>
      <Name>
        <xsl:value-of select="./con:jcc-name" />
      </Name>
      <xsl:for-each select="./con:jnlcontent-category">
        <xsl:apply-templates select="."/>
      </xsl:for-each>
    </JournalCategoryTree>
  </xsl:template>
  <xsl:template match="mg:it">
    <i>
      <xsl:apply-templates select="@*|node()" />
    </i>
  </xsl:template>
  <xsl:template match="mg:cit-arttitle">
    <xsl:copy-of select="node()" />
  </xsl:template>
  <xsl:template match="mg:inf">
    <sub>
      <xsl:apply-templates select="@*|node()" />
    </sub>
  </xsl:template>
 <xsl:template match="mg:sup">
    <sup>
      <xsl:apply-templates select="@*|node()" />
    </sup>
  </xsl:template>
  <xsl:template match="mg:bo">
    <b>
      <xsl:apply-templates select="@*|node()" />
    </b>
  </xsl:template>
    <xsl:template match="mg:location">
    <location>
      <xsl:apply-templates select="@*|node()" />
    </location>
  </xsl:template>
     <xsl:template match="mg:orgname">
    <orgname>
      <xsl:apply-templates select="@*|node()" />
    </orgname>
  </xsl:template>
     <xsl:template match="mg:email">
    <email>
      <xsl:apply-templates select="@*|node()" />
    </email>
  </xsl:template>
  <xsl:template match="mg:address">
    <address>
      <xsl:apply-templates select="@*|node()" />
    </address>
  </xsl:template>
</xsl:stylesheet>