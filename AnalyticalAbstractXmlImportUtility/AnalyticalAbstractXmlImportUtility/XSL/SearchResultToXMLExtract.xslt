﻿<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
exclude-result-prefixes="msxsl xsi cs" xmlns:cs="urn:cs"
>
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="utf-8" indent="yes"  />
  <msxsl:script language="C#" implements-prefix="cs">
    <![CDATA[
      public string datenow()
     {
        return(DateTime.Now.ToString("dd MMM yyyy"));
     }
     ]]>
  </msxsl:script>
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
 <xsl:variable name="languages">
    <entry key="AR">Arabic1</entry>
    <entry key="AZ">Azerbaijani ZZ</entry>
   <entry key="EN">English</entry>
 </xsl:variable>
 <xsl:variable name="categories">
    <entry key="A00000">
      <value>anc000001000000</value>
    </entry>
    <entry key="A10000">
      <value>anc000001010000</value>
    </entry>
    <entry key="A20000">
      <value>anc000001020000</value>
    </entry>
    <entry key="A30000">
      <value>anc000001030000</value>
    </entry>
    <entry key="A40000">
      <value>anc000001040000</value>
    </entry>
    <entry key="A50000">
      <value>anc000001050000</value>
    </entry>
    <entry key="A60000">
      <value>anc000001060000</value>
    </entry>
    <entry key="A70000">
      <value>anc000001070000</value>
    </entry>
    <entry key="A80000">
      <value>anc000001080000</value>
    </entry>
    <entry key="A90000">
      <value>anc000001090000</value>
    </entry>
    <entry key="B00000">
      <value>anc000002000000</value>
    </entry>
    <entry key="B10000">
      <value>anc000002010000</value>
    </entry>
    <entry key="B20000">
      <value>anc000002020000</value>
    </entry>
    <entry key="B30000">
      <value>anc000002030000</value>
    </entry>
    <entry key="B40000">
      <value>anc000002040000</value>
    </entry>
    <entry key="B50000">
      <value>anc000002050000</value>
    </entry>
    <entry key="C00000">
      <value>anc000003000000</value>
    </entry>
    <entry key="C10000">
      <value>anc000003010000</value>
    </entry>
    <entry key="C20000">
      <value>anc000003020000</value>
    </entry>
    <entry key="C30000">
      <value>anc000003030000</value>
    </entry>
    <entry key="C40000">
      <value>anc000003040000</value>
    </entry>
    <entry key="C50000">
      <value>anc000003050000</value>
    </entry>
    <entry key="D00000">
      <value>anc000004000000</value>
    </entry>
    <entry key="D10000">
      <value>anc000004010000</value>
    </entry>
    <entry key="D20000">
      <value>anc000004010100</value>
    </entry>
    <entry key="D21000">
      <value>anc000004010200</value>
    </entry>
    <entry key="D22000">
      <value>anc000004010300</value>
    </entry>
    <entry key="D23000">
      <value>anc000004010400</value>
    </entry>
    <entry key="D24000">
      <value>anc000004010500</value>
    </entry>
    <entry key="D25000">
      <value>anc000004010600</value>
    </entry>
    <entry key="D26000">
      <value>anc000004010700</value>
    </entry>
    <entry key="D27000">
      <value>anc000004010800</value>
    </entry>
    <entry key="D28000">
      <value>anc000004010900</value>
    </entry>
    <entry key="D29000">
      <value>anc000004020000</value>
    </entry>
    <entry key="D30000">
      <value>anc000004030000</value>
    </entry>
    <entry key="D31000">
      <value>anc000004030100</value>
    </entry>
    <entry key="D32000">
      <value>anc000004030200</value>
    </entry>
    <entry key="D33000">
      <value>anc000004030300</value>
    </entry>
    <entry key="D34000">
      <value>anc000004030400</value>
    </entry>
    <entry key="E00000">
      <value>anc000005000000</value>
    </entry>
    <entry key="E10000">
      <value>anc000005010000</value>
    </entry>
    <entry key="E20000">
      <value>anc000005020000</value>
    </entry>
    <entry key="E30000">
      <value>anc000005030000</value>
    </entry>
    <entry key="E40000">
      <value>anc000005040000</value>
    </entry>
    <entry key="E50000">
      <value>anc000005050000</value>
    </entry>
    <entry key="E60000">
      <value>anc000005060000</value>
    </entry>
    <entry key="E70000">
      <value>anc000005070000</value>
    </entry>
    <entry key="F00000">
      <value>anc000006000000</value>
    </entry>
    <entry key="F10000">
      <value>anc000006010000</value>
    </entry>
    <entry key="F20000">
      <value>anc000006020000</value>
    </entry>
    <entry key="F30000">
      <value>anc000006030000</value>
    </entry>
    <entry key="F31000">
      <value>anc000006040000</value>
    </entry>
    <entry key="F40000">
      <value>anc000006050000</value>
    </entry>
    <entry key="F41000">
      <value>anc000006060000</value>
    </entry>
    <entry key="F42000">
      <value>anc000006070000</value>
    </entry>
    <entry key="F50000">
      <value>anc000006080000</value>
    </entry>
    <entry key="F51000">
      <value>anc000006090000</value>
    </entry>
    <entry key="F60000">
      <value>anc000006100000</value>
    </entry>
    <entry key="F70000">
      <value>anc000006110000</value>
    </entry>
    <entry key="F71000">
      <value>anc000006120000</value>
    </entry>
    <entry key="F72000">
      <value>anc000006130000</value>
    </entry>
    <entry key="F80000">
      <value>anc000006140000</value>
    </entry>
    <entry key="F90000">
      <value>anc000006150000</value>
    </entry>
    <entry key="G00100">
      <value>anc000007000000 anc000007010000</value>
    </entry>
    <entry key="G00101">
      <value>anc000007000000 anc000007010000 anc000007020000</value>
    </entry>
    <entry key="G00102">
      <value>anc000007000000 anc000007020000</value>
    </entry>
    <entry key="G10100">
      <value>anc000007010000 anc000007030000</value>
    </entry>
    <entry key="G10101">
      <value>anc000007010000 anc000007020000 anc000007030000</value>
    </entry>
    <entry key="G10102">
      <value>anc000007020000 anc000007030000</value>
    </entry>
    <entry key="G10200">
      <value>anc000007010000 anc000007040000</value>
    </entry>
    <entry key="G10201">
      <value>anc000007010000 anc000007020000 anc000007040000</value>
    </entry>
    <entry key="G10202">
      <value>anc000007020000 anc000007040000</value>
    </entry>
    <entry key="G10300">
      <value>anc000007010000 anc000007050000</value>
    </entry>
    <entry key="G10301">
      <value>anc000007010000 anc000007020000 anc000007050000</value>
    </entry>
    <entry key="G10302">
      <value>anc000007020000 anc000007050000</value>
    </entry>
    <entry key="G10400">
      <value>anc000007010000 anc000007060000</value>
    </entry>
    <entry key="G10401">
      <value>anc000007010000 anc000007020000 anc000007060000</value>
    </entry>
    <entry key="G10402">
      <value>anc000007020000 anc000007060000</value>
    </entry>
    <entry key="G10500">
      <value>anc000007010000 anc000007070000</value>
    </entry>
    <entry key="G10501">
      <value>anc000007010000 anc000007020000 anc000007070000</value>
    </entry>
    <entry key="G10502">
      <value>anc000007020000 anc000007070000</value>
    </entry>
    <entry key="G10600">
      <value>anc000007010000 anc000007080000</value>
    </entry>
    <entry key="G10601">
      <value>anc000007010000 anc000007020000 anc000007080000</value>
    </entry>
    <entry key="G10602">
      <value>anc000007020000 anc000007080000</value>
    </entry>
    <entry key="G10700">
      <value>anc000007010000 anc000007090000</value>
    </entry>
    <entry key="G10701">
      <value>anc000007010000 anc000007020000 anc000007090000</value>
    </entry>
    <entry key="G10702">
      <value>anc000007020000 anc000007090000</value>
    </entry>
    <entry key="G10800">
      <value>anc000007010000 anc000007100000</value>
    </entry>
    <entry key="G10801">
      <value>anc000007010000 anc000007020000 anc000007100000</value>
    </entry>
    <entry key="G10802">
      <value>anc000007020000 anc000007100000</value>
    </entry>
    <entry key="G10900">
      <value>anc000007010000 anc000007110000</value>
    </entry>
    <entry key="G10901">
      <value>anc000007010000 anc000007020000 anc000007110000</value>
    </entry>
    <entry key="G10902">
      <value>anc000007020000 anc000007110000</value>
    </entry>
    <entry key="G11000">
      <value>anc000007010000 anc000007120000</value>
    </entry>
    <entry key="G11001">
      <value>anc000007010000 anc000007020000 anc000007120000</value>
    </entry>
    <entry key="G11002">
      <value>anc000007020000 anc000007120000</value>
    </entry>
    <entry key="G11100">
      <value>anc000007010000 anc000007130000</value>
    </entry>
    <entry key="G11101">
      <value>anc000007010000 anc000007020000 anc000007130000</value>
    </entry>
    <entry key="G11102">
      <value>anc000007020000 anc000007130000</value>
    </entry>
    <entry key="G11200">
      <value>anc000007010000 anc000007140000</value>
    </entry>
    <entry key="G11201">
      <value>anc000007010000 anc000007020000 anc000007140000</value>
    </entry>
    <entry key="G11202">
      <value>anc000007020000 anc000007140000</value>
    </entry>
    <entry key="G11300">
      <value>anc000007010000 anc000007150000</value>
    </entry>
    <entry key="G11301">
      <value>anc000007010000 anc000007020000 anc000007150000</value>
    </entry>
    <entry key="G11302">
      <value>anc000007020000 anc000007150000</value>
    </entry>
    <entry key="G11400">
      <value>anc000007010000 anc000007160000</value>
    </entry>
    <entry key="G11401">
      <value>anc000007010000 anc000007020000 anc000007160000</value>
    </entry>
    <entry key="G11402">
      <value>anc000007020000 anc000007160000</value>
    </entry>
    <entry key="G11500">
      <value>anc000007010000 anc000007170000</value>
    </entry>
    <entry key="G11501">
      <value>anc000007010000 anc000007020000 anc000007170000</value>
    </entry>
    <entry key="G11502">
      <value>anc000007020000 anc000007170000</value>
    </entry>
    <entry key="G11600">
      <value>anc000007010000 anc000007180000</value>
    </entry>
    <entry key="G11601">
      <value>anc000007010000 anc000007020000 anc000007180000</value>
    </entry>
    <entry key="G11602">
      <value>anc000007020000 anc000007180000</value>
    </entry>
    <entry key="G11800">
      <value>anc000007010000 anc000007190000</value>
    </entry>
    <entry key="G11801">
      <value>anc000007010000 anc000007020000 anc000007190000</value>
    </entry>
    <entry key="G11802">
      <value>anc000007020000 anc000007190000</value>
    </entry>
    <entry key="G11900">
      <value>anc000007010000 anc000007200000</value>
    </entry>
    <entry key="G11901">
      <value>anc000007010000 anc000007020000 anc000007200000</value>
    </entry>
    <entry key="G11902">
      <value>anc000007020000 anc000007200000</value>
    </entry>
    <entry key="G12000">
      <value>anc000007010000 anc000007210000</value>
    </entry>
    <entry key="G12001">
      <value>anc000007010000 anc000007020000 anc000007210000</value>
    </entry>
    <entry key="G12002">
      <value>anc000007210000</value>
    </entry>
    <entry key="G12200">
      <value>anc000007010000 anc000007220000</value>
    </entry>
    <entry key="G12201">
      <value>anc000007010000 anc000007220000</value>
    </entry>
    <entry key="G12202">
      <value>anc000007020000 anc000007220000</value>
    </entry>
    <entry key="G12300">
      <value>anc000007010000 anc000007230000</value>
    </entry>
    <entry key="G12301">
      <value>anc000007010000 anc000007020000 anc000007230000</value>
    </entry>
    <entry key="G12302">
      <value>anc000007020000 anc000007230000</value>
    </entry>
    <entry key="G12400">
      <value>anc000007010000 anc000007240000</value>
    </entry>
    <entry key="G12401">
      <value>anc000007010000 anc000007020000 anc000007240000</value>
    </entry>
    <entry key="G12402">
      <value>anc000007020000 anc000007240000</value>
    </entry>
    <entry key="G12600">
      <value>anc000007010000 anc000007250000</value>
    </entry>
    <entry key="G12601">
      <value>anc000007010000 anc000007020000 anc000007250000</value>
    </entry>
    <entry key="G12602">
      <value>anc000007020000 anc000007250000</value>
    </entry>
    <entry key="G20000">
      <value>anc000007010000 anc000007260000</value>
    </entry>
    <entry key="G20001">
      <value>anc000007010000 anc000007020000 anc000007260000</value>
    </entry>
    <entry key="G20002">
      <value>anc000007020000 anc000007260000</value>
    </entry>
    <entry key="H00000">
      <value>anc000008000000</value>
    </entry>
    <entry key="H10000">
      <value>anc000008010000</value>
    </entry>
    <entry key="H20000">
      <value>anc000008020000</value>
    </entry>
    <entry key="H30000">
      <value>anc000008030000</value>
    </entry>
    <entry key="H40000">
      <value>anc000008040000</value>
    </entry>
    <entry key="H50000">
      <value>anc000008050000</value>
    </entry>
    <entry key="H60000">
      <value>anc000008060000</value>
    </entry>
    <entry key="H70000">
      <value>anc000008070000</value>
    </entry>
    <entry key="H80000">
      <value>anc000008080000</value>
    </entry>
    <entry key="H81000">
      <value>anc000008080100</value>
    </entry>
    <entry key="H82000">
      <value>anc000008080200</value>
    </entry>
    <entry key="H83000">
      <value>anc000008080300</value>
    </entry>
    <entry key="H84000">
      <value>anc000008080400</value>
    </entry>
    <entry key="H85000">
      <value>anc000008080500</value>
    </entry>
    <entry key="H86000">
      <value>anc000008080600</value>
    </entry>
    <entry key="H87000">
      <value>anc000008080700</value>
    </entry>
    <entry key="H88000">
      <value>anc000008080800</value>
    </entry>
    <entry key="H89000">
      <value>anc000008080900</value>
    </entry>
  </xsl:variable>
  <xsl:template match="SearchResults">
    <AA>
      <RSCHead>
        <COP>The Royal Society of Chemistry</COP>
        <CDT>
          <xsl:value-of select="cs:datenow()"/>
        </CDT>
      </RSCHead>
      <xsl:for-each select="Results">
        <xsl:for-each select="Result">
          <xsl:for-each select="Result/Article">
            <RECORD>
              <REF>
                <xsl:value-of select="systemid" />
              </REF>
              <xsl:variable name="ancatmap" select="msxsl:node-set($categories)/entry"/>
              <xsl:for-each select="FacetTree/.//Id">
                <xsl:variable name="ancat" select="." />
                <xsl:if test="$ancat !='' and position()=2">
                
                  <xsl:for-each select="$ancatmap">
                    <xsl:if test="current()[contains(value, $ancat)]">
                      <SEC>
                        <xsl:value-of select="substring(@key,1,1)" />
                      </SEC>
                      <SSC>
                        <xsl:value-of select="substring(@key,2,5)"/>
                      </SSC>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
              <IDC>J</IDC>
              <DOC>Journal</DOC>
              <FJL>
                <xsl:apply-templates select="journal/title"/>
              </FJL>
              <JNL>
                <xsl:apply-templates select="journal/title" />
              </JNL>
              <LCD>
                <xsl:value-of select="language" />
              </LCD>
              <LAN>
                <xsl:variable name="lcd" select="language"></xsl:variable>
                <xsl:value-of select="msxsl:node-set($languages)/entry[@key=$lcd]"/>
              </LAN>
              <VOL>
                <xsl:value-of select="journal-volume" />
              </VOL>
              <ISS>
                <xsl:value-of select="journal-issueno" />
              </ISS>
              <ATL>
                <xsl:apply-templates select="title" />
              </ATL>
              <xsl:if test="authors">
                <xsl:for-each select="authors">
                  <xsl:if test="author/lastname !='' or  author/firstname !=''">
                    <AUT>
                      <xsl:value-of select="author/lastname" />
                      <xsl:if test="author/firstname !=''">
                        <xsl:text>,&#xA0;</xsl:text>
                        <xsl:value-of select="author/firstname" />
                      </xsl:if>
                    </AUT>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="firstpage !='' or  lastpage !=''">
                <PAG>
                  <xsl:value-of select="firstpage" />
                  <xsl:if test="lastpage !=''">
                    <xsl:text>–</xsl:text>
                    <xsl:value-of select="lastpage" />
                  </xsl:if>
                </PAG>
              </xsl:if>
              <DIV>
               <xsl:value-of select="info/location/orgname" />
              </DIV>
              <ORG>
               <xsl:value-of select="info/location/orgname" />
              </ORG>
              <CTR>
              <xsl:value-of select="info/location/address" />
              </CTR>
              <xsl:if test="analytes">
                <ANALYTE>
                  <xsl:for-each select="analytes">
                    <AYT>
                      <xsl:value-of select="analyte" />
                    </AYT>
                  </xsl:for-each>
                </ANALYTE>
              </xsl:if>
              <xsl:if test="matrices">
                <MATRIX>
                  <xsl:for-each select="matrices">
                    <MTX>
                      <xsl:value-of select="matrix" />
                    </MTX>
                  </xsl:for-each>
                </MATRIX>
              </xsl:if>
              <xsl:if test="techniques">
                <CONCEPT>
                  <xsl:for-each select="techniques">
                    <CPT>
                      <xsl:value-of select="technique" />
                    </CPT>
                  </xsl:for-each>
                </CONCEPT>
              </xsl:if>
              <ABS>
                <xsl:apply-templates select="description" />
              </ABS>
              <xsl:for-each select="FacetTree/.//Id">
                <xsl:variable name="ancat" select="."></xsl:variable>
                <xsl:if test="$ancat !='' and position()>=3">
                  <xsl:for-each select="$ancatmap">
                    <xsl:if test="current()[contains(value, $ancat)]">
                      <XRF>
                        <xsl:value-of select="@key" />
                      </XRF>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
            </RECORD>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
    </AA>
  </xsl:template>
  <xsl:template match="em|i">
    <it>
      <xsl:apply-templates select="@*|node()" />
    </it>
  </xsl:template>
  <xsl:template match="strong|b">
    <bo>
      <xsl:apply-templates select="@*|node()" />
    </bo>
  </xsl:template>
  <xsl:template match="sub">
    <inf>
      <xsl:apply-templates select="@*|node()"  />
    </inf>
  </xsl:template>
  <xsl:template match="text()">
    <xsl:value-of disable-output-escaping="no" select="."/>
  </xsl:template>
</xsl:stylesheet>
