﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="utf-8" indent="no"  />
  <xsl:strip-space elements="Title" />
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="Article">
    <xsl:if test ="title != ''">
      <monograph xmlns="http://www.rsc.org/schema/chemical/monograph">
        <sysid>
          <xsl:value-of select="systemid" />
        </sysid>
        <publicid>
          <xsl:value-of select="publicid" />
        </publicid>
        <doi>
          <xsl:value-of select="doi" />
        </doi>
        <printid>
          <xsl:value-of select="printid" />
        </printid>
        <title>
          <xsl:apply-templates select="title/node()" />
        </title>
        <description>
          <xsl:apply-templates select="description/node()" />
        </description>
        <info>
            <xsl:apply-templates select="info/node()"/>
          </info>
        <image/>
        <literature-references>
              <cit>
			   <xsl:if test="language != ''">
                  <xsl:attribute name="lang" >
                    <xsl:value-of select="language"/>
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="authors">
                  <xsl:variable name="row" select="authors/author" />
                  <xsl:variable name="totalcount" select="count($row)" />
                     <xsl:for-each select="authors">
                    <xsl:for-each select="$row">
                         <cit-auth>
                        <xsl:if test="firstname !=''">
                          <fname>
                            <xsl:value-of select="firstname" disable-output-escaping="yes" />
                          </fname>
                        </xsl:if>
                        <xsl:if test="firstname != '' and lastname != ''" >
                        <xsl:text>&#xA0;</xsl:text>
                        </xsl:if>
                          <xsl:if test="lastname !=''">
                          <surname>
                            <xsl:value-of select="lastname" disable-output-escaping="yes"/>
                          </surname>
                        </xsl:if>
                       </cit-auth>
                      <xsl:choose>
                        <xsl:when test="$totalcount &gt; 1 and position() = ($totalcount - 1)" >
                          <xsl:text>&#xA0;and&#xA0;</xsl:text>
                        </xsl:when>
                        <xsl:when  test="$totalcount &gt; 1 and position() != $totalcount" >
                          <xsl:text>,&#xA0;</xsl:text>
                        </xsl:when>
                      </xsl:choose>
                    </xsl:for-each>
                  </xsl:for-each>
                </xsl:if>
               <xsl:if test="journal !=''">
                 <xsl:if test="authors !=''">
                   <xsl:text>,&#xA0;</xsl:text>
                 </xsl:if>
                  <cit-title>
                    <xsl:value-of select="journal/title"/>
                  </cit-title>
                 <xsl:if test="subsyear !='' or  journal-volume !='' or  journal-issueno !='' or  firstpage !='' or  lastpage !='' or  doi !='' or  PubmedId !='' ">
                 <xsl:text>,&#xA0;</xsl:text>
                 </xsl:if>
                </xsl:if>
                <xsl:if test="subsyear !=''">
                  <cit-year>
                    <xsl:value-of select="subsyear" />
                  </cit-year>
                  <xsl:if test="journal-volume !='' or  journal-issueno !='' or  firstpage !='' or  lastpage !='' or  doi !='' or  PubmedId !='' ">
                  <xsl:text>,&#xA0;</xsl:text>
                  </xsl:if>
                </xsl:if>
                <xsl:if test="journal-volume !=''">
                  <cit-vol>
                    <xsl:value-of select="journal-volume" />
                  </cit-vol>
                  <xsl:if test="journal-issueno ='' or not(journal-issueno)">
                      <xsl:text>,&#xA0;</xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="journal-issueno!='' ">
                    <xsl:text>(</xsl:text>
                  <cit-iss>
                    <xsl:value-of select="journal-issueno"/>
                  </cit-iss>
                    <xsl:text>)</xsl:text>
                  <xsl:if test="firstpage !='' or  lastpage !='' or  doi !='' or  PubmedId !='' ">
                      <xsl:text>,&#xA0;</xsl:text>
                  </xsl:if>
                </xsl:if>
                <xsl:if test="firstpage !=''">
                  <cit-fpage>
                    <xsl:value-of select="firstpage" />
                  </cit-fpage>
                  <xsl:choose>
                    <xsl:when test="lastpage !=''">
                      <xsl:text>-</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:if test="doi !='' or  PubmedId !='' ">
                      <xsl:text>,&#xA0;</xsl:text>
                      </xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:if>
                <xsl:if test="lastpage !=''">
                   <cit-lpage>
                    <xsl:value-of select="lastpage" />
                  </cit-lpage>
                  <xsl:if test="doi !='' or  PubmedId !='' ">
                  <xsl:text>,&#xA0;</xsl:text>
                  </xsl:if>
                </xsl:if>
                <xsl:if test="doi !=''">
                  <doi-link>
                    <xsl:value-of select="doi" disable-output-escaping="no" />
                  </doi-link>
                  <xsl:if test="PubmedId !=''">
                  <xsl:text>,&#xA0;</xsl:text>
                  </xsl:if>
                </xsl:if>
                <xsl:if test="PubmedId">
                  <pmid-link>
                    <xsl:value-of select="PubmedId"/>
                  </pmid-link>
                </xsl:if>
              </cit>
        </literature-references>
        <data>
          <analytes>
            <xsl:if test="analytes">
              <xsl:for-each select="analytes">
                <xsl:for-each select="analyte">
                  <analyte>
                    <xsl:attribute name="cas-no">
                      <xsl:value-of select="./@CASNo"/>
                    </xsl:attribute>
                    <xsl:value-of select="." />
                  </analyte>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:if>
          </analytes>
          <matrices>
            <xsl:if test="matrices">
              <xsl:for-each select="matrices">
                <xsl:for-each select="matrix">
                  <matrix>
                    <xsl:value-of select="." />
                  </matrix>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:if>
          </matrices>
          <concepts>
            <xsl:if test="techniques">
              <xsl:for-each select="techniques">
                <xsl:for-each select="technique">
                  <concept>
                    <xsl:value-of select="." />
                  </concept>
                </xsl:for-each>
              </xsl:for-each>
            </xsl:if>
          </concepts>
        </data>
        <keywords>
          <xsl:if test="keywords">
            <xsl:for-each select="keywords">
              <xsl:for-each select="keyword">
                <keyword>
                  <xsl:value-of select="." />
                </keyword>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:if>
        </keywords>
      </monograph>
    </xsl:if>
  </xsl:template>
  <xsl:template match="em|i">
    <it>
      <xsl:apply-templates select="@*|node()" />
    </it>
  </xsl:template>
  <xsl:template match="strong|b">
    <bo>
      <xsl:apply-templates select="@*|node()" />
    </bo>
  </xsl:template>
  <xsl:template match="sub">
    <inf>
      <xsl:apply-templates select="@*|node()"  />
    </inf>
  </xsl:template>
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="text()">
    <xsl:value-of disable-output-escaping="yes" select="."/>
  </xsl:template>
</xsl:stylesheet>
