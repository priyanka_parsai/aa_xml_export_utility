<?xml version="1.0" encoding="utf-16"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
xmlns:mg="http://www.rsc.org/schema/chemical/monograph"
xmlns:cr="http://www.rsc.org/schema/tangier/container"
exclude-result-prefixes="msxsl xsi cs cr mg" xmlns:cs="urn:cs"
>
  <xsl:output method="xml"  omit-xml-declaration="yes" encoding="UTF-8" indent="yes"  />
  <msxsl:script language="C#" implements-prefix="cs">
    <msxsl:assembly name="System.Core" />
    <msxsl:using namespace="System.Linq" />
    <msxsl:using namespace="System.Collections.Generic" />
    <![CDATA[
      public string datenow()
     {
        return(DateTime.Now.ToString("dd.MMM.yyyy"));
     }
     public XPathNodeIterator GetPermutationsForAncat(string delimatorSeperatedValue, string delimiter)
         {
            string[] ancatValues = delimatorSeperatedValue.Split(delimiter.ToCharArray(), StringSplitOptions.None);

            string[] ancatValues2 = delimatorSeperatedValue.Split(delimiter.ToCharArray(), StringSplitOptions.None);
            List<string> values = new List<string>();
            XmlDocument doc = new XmlDocument();

            doc.AppendChild(doc.CreateElement("root"));
            using (XmlWriter writer = doc.DocumentElement.CreateNavigator().AppendChild())
            {
                double count = Math.Pow(2, ancatValues.Count());
                for (int i = 1; i <= count - 1; i++)
                {
                    string str = Convert.ToString(i, 2).PadLeft(ancatValues.Count(), '0');
                    string currentValue = string.Empty;

                    var multiItems = string.Empty;
                    for (int j = 0; j < str.Length; j++)
                    {
                        if (str[j] == '1')
                        {
                         if (values.Any(item => item.Contains(ancatValues[j])))
                          {
                            if (multiItems.Count(Char.IsWhiteSpace) <= 2)
                            {
                                multiItems += ancatValues[j] + " ";
                            }
                          }
                           else
                            {
                                values.Add(ancatValues[j]);
                            }

                        }
                    }
                    if (!string.IsNullOrEmpty(multiItems))
                    {
                     if(!values.Contains(multiItems.TrimEnd()))
                    {
                    values.Add(multiItems.TrimEnd());
                    }
                    }
                }
                foreach (var val in values)
                {
                    writer.WriteElementString("item", val);
                }
            }
            return doc.DocumentElement.CreateNavigator().Select("item");
        }
        
          public XPathNodeIterator RemoveParentItems(string delimatorSeperatedValue, string delimiter, string delimatorSeperatedAllValue)
        {
            string[] sinleitemsValues = delimatorSeperatedValue.Split(delimiter.ToCharArray(), StringSplitOptions.None);
            string[] mutipleValues = delimatorSeperatedAllValue.Split(delimiter.ToCharArray(), StringSplitOptions.None);
            var multipleValuesList = mutipleValues.ToList<string>();
            var subChildListD1 = "D10000";
            var subChildListD2 = "D30000";
            var subChildListH8 = "H80000";
            var subChildListD1ChildList = new List<string> {"D21000", "D22000", "D21000", "D22000", "D23000", "D24000", "D25000", "D26000", "D27000", "D28000" };
            var subChildListD2ChildList = new List<string> {"D31000", "D32000", "D33000", "D34000" };
            var subChildListH8ChildList = new List<string> {"H81000", "H82000", "H83000", "H84000", "H85000", "H86000", "H87000", "H88000", "H89000" };
            var result = from s in sinleitemsValues
                         group s by s into groupValue
                         select new
                         {
                             Name = groupValue.Key,
                             Total = groupValue.Count(),
                         };
            foreach (var i in result)
            {
                if (i.Total > 1)
                {
                    multipleValuesList.Remove(i.Name + "00000");
                }
            }
            foreach (var i in multipleValuesList.ToList<string>())
            {
                if (i == subChildListD1 && subChildListD1ChildList
                             .Intersect(multipleValuesList)
                             .Any())
                {
                    multipleValuesList.Remove(subChildListD1);
                }
                else if (i == subChildListD2 && subChildListD2ChildList
                                         .Intersect(multipleValuesList)
                                         .Any())
                {
                    multipleValuesList.Remove(subChildListD2);
                }
                else if (i == subChildListH8 && subChildListH8ChildList
                                         .Intersect(multipleValuesList)
                                         .Any())
                {
                    multipleValuesList.Remove(subChildListH8);
                }

            }
            XmlDocument doc = new XmlDocument();

            doc.AppendChild(doc.CreateElement("root"));
            using (XmlWriter writer = doc.DocumentElement.CreateNavigator().AppendChild())
            {
                foreach (var val in multipleValuesList)
                {
                    writer.WriteElementString("FinalMatch", val);
                }
            }
            return doc.DocumentElement.CreateNavigator().Select("FinalMatch");
        }
        
        public XPathNodeIterator SortByAscValues(string delimatorSeperatedValue, string delimiter)
        {
            string[] finalValues = delimatorSeperatedValue.Split(delimiter.ToCharArray(), StringSplitOptions.None);

            var finalValuesList = finalValues.ToList<string>();
            finalValuesList = finalValuesList.OrderBy(x => x).ToList<string>();

            XmlDocument doc = new XmlDocument();

            doc.AppendChild(doc.CreateElement("root"));
            using (XmlWriter writer = doc.DocumentElement.CreateNavigator().AppendChild())
            {
                foreach (var val in finalValuesList)
                {
                    writer.WriteElementString("FinalMatch", val);
                }
            }
            return doc.DocumentElement.CreateNavigator().Select("FinalMatch");
        }
        
  public string ReplaceMutipleStrings(string stringTobeReplaced,string dictionaryItemsstring)
        {
            if (!string.IsNullOrEmpty(stringTobeReplaced))
            {
            
        var dictionaryValues = dictionaryItemsstring.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries)
               .Select(part => part.Split('='))
               .ToDictionary(split => split[0], split => split[1]);
               
                StringBuilder replacedString = new StringBuilder(stringTobeReplaced);

                foreach (var dict in dictionaryValues)
                {
                    if (replacedString.ToString().Contains(dict.Key.ToString()))
                    {
                        replacedString.Replace(dict.Key.ToString(), dict.Value.ToString());
                    }
                }
                return replacedString.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
     ]]>
  </msxsl:script>
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:variable name="Unicodecharacters">
    <entry key="#HEXCONSTANT#x03b1;">#NAMEDCONSTANT#alpha;</entry>
    <entry key="#HEXCONSTANT#x0026;">#NAMEDCONSTANT#amp;</entry>
    <entry key="#HEXCONSTANT#x212b;">#NAMEDCONSTANT#angst;</entry>
    <entry key="#HEXCONSTANT#x0027;">#NAMEDCONSTANT#apos;</entry>
    <entry key="#HEXCONSTANT#x002a;">#NAMEDCONSTANT#ast;</entry>
    <entry key="#HEXCONSTANT#x03b2;">#NAMEDCONSTANT#beta;</entry>
    <entry key="#HEXCONSTANT#x22a5;">#NAMEDCONSTANT#bottom;</entry>
    <entry key="#HEXCONSTANT#x2022;">#NAMEDCONSTANT#bull;</entry>
    <entry key="#HEXCONSTANT#x03c7;">#NAMEDCONSTANT#chi;</entry>
    <entry key="#HEXCONSTANT#x00a9;">#NAMEDCONSTANT#copy;</entry>
    <entry key="#HEXCONSTANT#x2020;">#NAMEDCONSTANT#dagger;</entry>
    <entry key="#HEXCONSTANT#x2193;">#NAMEDCONSTANT#darr;</entry>
    <entry key="#HEXCONSTANT#x00b0;">#NAMEDCONSTANT#deg;</entry>
    <entry key="#HEXCONSTANT#x03b4;">#NAMEDCONSTANT#delta;</entry>
    <entry key="#HEXCONSTANT#x0394;">#NAMEDCONSTANT#Delta;</entry>
    <entry key="#HEXCONSTANT#x00f7;">#NAMEDCONSTANT#divide;</entry>
    <entry key="#HEXCONSTANT#x0024;">#NAMEDCONSTANT#dollar;</entry>
    <entry key="#HEXCONSTANT#x025b;">#NAMEDCONSTANT#epsiv;</entry>
    <entry key="#HEXCONSTANT#x2261;">#NAMEDCONSTANT#equiv;</entry>
    <entry key="#HEXCONSTANT#x03b7;">#NAMEDCONSTANT#eta;</entry>
    <entry key="#HEXCONSTANT#x20ac;">#NAMEDCONSTANT#euro;</entry>
    <entry key="#HEXCONSTANT#x0021;">#NAMEDCONSTANT#excl;</entry>
    <entry key="#HEXCONSTANT#x2640;">#NAMEDCONSTANT#female;</entry>
    <entry key="#HEXCONSTANT#x03b3;">#NAMEDCONSTANT#gamma;</entry>
    <entry key="#HEXCONSTANT#x0393;">#NAMEDCONSTANT#Gamma;</entry>
    <entry key="#HEXCONSTANT#x2265;">#NAMEDCONSTANT#ge;</entry>
    <entry key="#HEXCONSTANT#x2273;">#NAMEDCONSTANT#gsim;</entry>
    <entry key="#HEXCONSTANT#x003e;">#NAMEDCONSTANT#gt;</entry>
    <entry key="#HEXCONSTANT#x226b;">#NAMEDCONSTANT#Gt;</entry>
    <entry key="#HEXCONSTANT#x00bd;">#NAMEDCONSTANT#half;</entry>
    <entry key="#HEXCONSTANT#x2194;">#NAMEDCONSTANT#harr;</entry>
    <entry key="#HEXCONSTANT#x221e;">#NAMEDCONSTANT#infin;</entry>
    <entry key="#HEXCONSTANT#x03b9;">#NAMEDCONSTANT#iota;</entry>
    <entry key="#HEXCONSTANT#x03ba;">#NAMEDCONSTANT#kappa;</entry>
    <entry key="#HEXCONSTANT#x03bb;">#NAMEDCONSTANT#lambda;</entry>
    <entry key="#HEXCONSTANT#x039b;">#NAMEDCONSTANT#Lambda;</entry>
    <entry key="#HEXCONSTANT#x2190;">#NAMEDCONSTANT#larr;</entry>
    <entry key="#HEXCONSTANT#x201c;">#NAMEDCONSTANT#ldquo;</entry>
    <entry key="#HEXCONSTANT#x2264;">#NAMEDCONSTANT#le;</entry>
    <entry key="#HEXCONSTANT#x2272;">#NAMEDCONSTANT#lsim;</entry>
    <entry key="#HEXCONSTANT#x2018;">#NAMEDCONSTANT#lsquo;</entry>
    <entry key="#HEXCONSTANT#x003c;">#NAMEDCONSTANT#lt;</entry>
    <entry key="#HEXCONSTANT#x226a;">#NAMEDCONSTANT#Lt;</entry>
    <entry key="#HEXCONSTANT#x2642;">#NAMEDCONSTANT#male;</entry>
    <entry key="#HEXCONSTANT#x2014;">#NAMEDCONSTANT#mdash;</entry>
    <entry key="#HEXCONSTANT#x00b7;">#NAMEDCONSTANT#middot;</entry>
    <entry key="#HEXCONSTANT#x2013;">#NAMEDCONSTANT#minus;</entry>
    <entry key="#HEXCONSTANT#x2213;">#NAMEDCONSTANT#mnplus;</entry>
    <entry key="#HEXCONSTANT#x00b5;">#NAMEDCONSTANT#mu;</entry>
    <entry key="#HEXCONSTANT#xa0;">#NAMEDCONSTANT#nbsp;</entry>
    <entry key="#HEXCONSTANT#x2013;">#NAMEDCONSTANT#ndash;</entry>
    <entry key="#HEXCONSTANT#x03bd;">#NAMEDCONSTANT#nu;</entry>
    <entry key="#HEXCONSTANT#x0023;">#NAMEDCONSTANT#num;</entry>
    <entry key="#HEXCONSTANT#x03c9;">#NAMEDCONSTANT#omega;</entry>
    <entry key="#HEXCONSTANT#x03a9;">#NAMEDCONSTANT#Omega;</entry>
    <entry key="#HEXCONSTANT#x2030;">#NAMEDCONSTANT#permil;</entry>
    <entry key="#HEXCONSTANT#x03c6;">#NAMEDCONSTANT#phis;</entry>
    <entry key="#HEXCONSTANT#x03a6;">#NAMEDCONSTANT#Phi;</entry>
    <entry key="#HEXCONSTANT#x03d5;">#NAMEDCONSTANT#phiv;</entry>
    <entry key="#HEXCONSTANT#x03c0;">#NAMEDCONSTANT#pi;</entry>
    <entry key="#HEXCONSTANT#x03a0;">#NAMEDCONSTANT#Pi;</entry>
    <entry key="#HEXCONSTANT#x03d6;">#NAMEDCONSTANT#piv;</entry>
    <entry key="#HEXCONSTANT#x002b;">#NAMEDCONSTANT#plus;</entry>
    <entry key="#HEXCONSTANT#x00b1;">#NAMEDCONSTANT#plusmn;</entry>
    <entry key="#HEXCONSTANT#x00a3;">#NAMEDCONSTANT#pound;</entry>
    <entry key="#HEXCONSTANT#x2032;">#NAMEDCONSTANT#prime;</entry>
    <entry key="#HEXCONSTANT#x221d;">#NAMEDCONSTANT#prop;</entry>
    <entry key="#HEXCONSTANT#x03c8;">#NAMEDCONSTANT#psi;</entry>
    <entry key="#HEXCONSTANT#x03a8;">#NAMEDCONSTANT#Psi;</entry>
    <entry key="#HEXCONSTANT#x2192;">#NAMEDCONSTANT#rarr;</entry>
    <entry key="#HEXCONSTANT#x201d;">#NAMEDCONSTANT#rdquo;</entry>
    <entry key="#HEXCONSTANT#x00ae;">#NAMEDCONSTANT#reg;</entry>
    <entry key="#HEXCONSTANT#x03c1;">#NAMEDCONSTANT#rho;</entry>
    <entry key="#HEXCONSTANT#x21cc;">#NAMEDCONSTANT#rlhar;</entry>
    <entry key="#HEXCONSTANT#x2019;">#NAMEDCONSTANT#rsquo;</entry>
    <entry key="#HEXCONSTANT#x221a;">#NAMEDCONSTANT#radic;</entry>
    <entry key="#HEXCONSTANT#x03c3;">#NAMEDCONSTANT#sigma;</entry>
    <entry key="#HEXCONSTANT#x03a3;">#NAMEDCONSTANT#Sigma;</entry>
    <entry key="#HEXCONSTANT#x223c;">#NAMEDCONSTANT#sim;</entry>
    <entry key="#HEXCONSTANT#x2243;">#NAMEDCONSTANT#sime;</entry>
    <entry key="#HEXCONSTANT#x002f;">#NAMEDCONSTANT#sol;</entry>
    <entry key="#HEXCONSTANT#x03c4;">#NAMEDCONSTANT#tau;</entry>
    <entry key="#HEXCONSTANT#2261;">#NAMEDCONSTANT#tbond;</entry>
    <entry key="#HEXCONSTANT#x03b8;">#NAMEDCONSTANT#thetas;</entry>
    <entry key="#HEXCONSTANT#x0398;">#NAMEDCONSTANT#Theta;</entry>
    <entry key="#HEXCONSTANT#x03d1;">#NAMEDCONSTANT#thetav;</entry>
    <entry key="#HEXCONSTANT#x2009;">#NAMEDCONSTANT#thinsp;</entry>
    <entry key="#HEXCONSTANT#x00d7;">#NAMEDCONSTANT#times;</entry>
    <entry key="#HEXCONSTANT#x2122;">#NAMEDCONSTANT#trade;</entry>
    <entry key="#HEXCONSTANT#x2191;">#NAMEDCONSTANT#uarr;</entry>
    <entry key="#HEXCONSTANT#x03c5;">#NAMEDCONSTANT#upsi;</entry>
    <entry key="#HEXCONSTANT#x03d2;">#NAMEDCONSTANT#Upsi;</entry>
    <entry key="#HEXCONSTANT#x007c;">#NAMEDCONSTANT#verbar;</entry>
    <entry key="#HEXCONSTANT#x2016;">#NAMEDCONSTANT#Verbar;</entry>
    <entry key="#HEXCONSTANT#x03be;">#NAMEDCONSTANT#xi;</entry>
    <entry key="#HEXCONSTANT#x039e;">#NAMEDCONSTANT#Xi;</entry>
    <entry key="#HEXCONSTANT#x03b6;">#NAMEDCONSTANT#zeta;</entry>
    <entry key="#HEXCONSTANT#x29b5;">#NAMEDCONSTANT#z.ccsh;</entry>
    <entry key="#HEXCONSTANT#x3d;">#NAMEDCONSTANT#z.dbd;</entry>
    <entry key="#HEXCONSTANT#x2261;">#NAMEDCONSTANT#z.tbd;</entry>
    <entry key="#HEXCONSTANT#x2248;">#NAMEDCONSTANT#simde;</entry>
    <entry key="#HEXCONSTANT#x2265;">#NAMEDCONSTANT#gr;</entry>
  </xsl:variable>
  <xsl:variable name="languages">
    <entry key="AR">Arabic1</entry>
    <entry key="AZ">Azerbaijani ZZ</entry>
    <entry key="BE">Byelorussian</entry>
    <entry key="BG">Bulgarian</entry>
    <entry key="BN">Bengali</entry>
    <entry key="CS">Czech</entry>
    <entry key="DA">Danish</entry>
    <entry key="DE">German</entry>
    <entry key="EL">Greek</entry>
    <entry key="EN">English</entry>
    <entry key="ES">Spanish</entry>
    <entry key="ET">Estonian</entry>
    <entry key="FA">Persian</entry>
    <entry key="FI">Finnish</entry>
    <entry key="FR">French</entry>
    <entry key="GA">Irish</entry>
    <entry key="HI">Hindi</entry>
    <entry key="HU">Hungarian</entry>
    <entry key="HY">Armenian</entry>
    <entry key="IN">Indonesian</entry>
    <entry key="IS">Icelandic</entry>
    <entry key="IT">Italian</entry>
    <entry key="IW">Hebrew</entry>
    <entry key="JA">Japanese</entry>
    <entry key="KA">Georgian</entry>
    <entry key="KK">Kazakh</entry>
    <entry key="KO">Korean</entry>
    <entry key="LT">Lithuanian</entry>
    <entry key="LV">Latvian</entry>
    <entry key="NL">Dutch</entry>
    <entry key="NO">Norwegian</entry>
    <entry key="PA">Punjabi</entry>
    <entry key="PL">Polish</entry>
    <entry key="PT">Portuguese</entry>
    <entry key="RO">Romanian</entry>
    <entry key="RU">Russian</entry>
    <entry key="SH">Serbo-Croatian</entry>
    <entry key="SK">Slovak</entry>
    <entry key="SL">Slovenian</entry>
    <entry key="SQ">Albanian</entry>
    <entry key="SS">Swedish</entry>
    <entry key="SV">Swedish</entry>
    <entry key="SW">Swahili</entry>
    <entry key="TG">Tajik</entry>
    <entry key="TH">Thai</entry>
    <entry key="TR">Turkish</entry>
    <entry key="UK">Ukrainian</entry>
    <entry key="UR">Urdu</entry>
    <entry key="UZ">Uzbek</entry>
    <entry key="VI">Vietnamese</entry>
    <entry key="ZH">Chinese</entry>
  </xsl:variable>
  <xsl:variable name="categories">
    <entry key="A00000" >
      <value key="A00000">anc000001000000</value>
    </entry>
    <entry key="A10000">
      <value  key="A10000">anc000001010000</value>
    </entry>
    <entry key="A20000">
      <value  key="A20000">anc000001020000</value>
    </entry>
    <entry key="A30000">
      <value  key="A30000">anc000001030000</value>
    </entry>
    <entry key="A40000">
      <value  key="A40000">anc000001040000</value>
    </entry>
    <entry key="A50000">
      <value  key="A50000">anc000001050000</value>
    </entry>
    <entry key="A60000">
      <value  key="A60000">anc000001060000</value>
    </entry>
    <entry key="A70000">
      <value  key="A70000">anc000001070000</value>
    </entry>
    <entry key="A80000">
      <value  key="A80000">anc000001080000</value>
    </entry>
    <entry key="A90000">
      <value  key="A90000">anc000001090000</value>
    </entry>
    <entry key="B00000">
      <value key="B00000">anc000002000000</value>
    </entry>
    <entry key="B10000">
      <value key="B10000" >anc000002010000</value>
    </entry>
    <entry key="B20000">
      <value key="B20000">anc000002020000</value>
    </entry>
    <entry key="B30000">
      <value key="B30000">anc000002030000</value>
    </entry>
    <entry key="B40000">
      <value key="B40000">anc000002040000</value>
    </entry>
    <entry key="B50000">
      <value key="B50000">anc000002050000</value>
    </entry>
    <entry key="C00000">
      <value key="C00000">anc000003000000</value>
    </entry>
    <entry key="C10000">
      <value  key="C10000">anc000003010000</value>
    </entry>
    <entry key="C20000">
      <value key="C20000">anc000003020000</value>
    </entry>
    <entry key="C30000">
      <value key="C30000">anc000003030000</value>
    </entry>
    <entry key="C40000">
      <value key="C40000">anc000003040000</value>
    </entry>
    <entry key="C50000">
      <value key="C50000">anc000003050000</value>
    </entry>
    <entry key="D00000">
      <value key="D00000">anc000004000000</value>
    </entry>
    <entry key="D10000">
      <value key="D10000" hassubchild="true">anc000004010000</value>
    </entry>
    <entry key="D20000">
      <value key="D20000">anc000004010100</value>
    </entry>
    <entry key="D21000">
      <value key="D21000">anc000004010200</value>
    </entry>
    <entry key="D22000">
      <value key="D22000">anc000004010300</value>
    </entry>
    <entry key="D23000">
      <value key="D23000">anc000004010400</value>
    </entry>
    <entry key="D24000">
      <value key="D24000">anc000004010500</value>
    </entry>
    <entry key="D25000">
      <value key="D25000">anc000004010600</value>
    </entry>
    <entry key="D26000">
      <value key="D26000">anc000004010700</value>
    </entry>
    <entry key="D27000">
      <value key="D27000">anc000004010800</value>
    </entry>
    <entry key="D28000">
      <value  key="D28000">anc000004010900</value>
    </entry>
    <entry key="D29000">
      <value key="D29000">anc000004020000</value>
    </entry>
    <entry key="D30000">
      <value key="D30000" hassubchild="true">anc000004030000</value>
    </entry>
    <entry key="D31000">
      <value key="D31000">anc000004030100</value>
    </entry>
    <entry key="D32000">
      <value key="D32000">anc000004030200</value>
    </entry>
    <entry key="D33000">
      <value  key="D33000">anc000004030300</value>
    </entry>
    <entry key="D34000">
      <value key="D34000">anc000004030400</value>
    </entry>
    <entry key="E00000">
      <value key="E00000">anc000005000000</value>
    </entry>
    <entry key="E10000">
      <value key="E10000">anc000005010000</value>
    </entry>
    <entry key="E20000">
      <value key="E20000">anc000005020000</value>
    </entry>
    <entry key="E30000">
      <value key="E30000">anc000005030000</value>
    </entry>
    <entry key="E40000">
      <value key="E40000">anc000005040000</value>
    </entry>
    <entry key="E50000">
      <value key="E50000">anc000005050000</value>
    </entry>
    <entry key="E60000">
      <value key="E60000">anc000005060000</value>
    </entry>
    <entry key="E70000">
      <value key="E70000">anc000005070000</value>
    </entry>
    <entry key="F00000">
      <value key="F00000">anc000006000000</value>
    </entry>
    <entry key="F10000">
      <value  key="F10000">anc000006010000</value>
    </entry>
    <entry key="F20000">
      <value key="F20000">anc000006020000</value>
    </entry>
    <entry key="F30000">
      <value  key="F30000">anc000006030000</value>
    </entry>
    <entry key="F31000">
      <value  key="F31000">anc000006040000</value>
    </entry>
    <entry key="F40000">
      <value key="F40000">anc000006050000</value>
    </entry>
    <entry key="F41000">
      <value key="F41000">anc000006060000</value>
    </entry>
    <entry key="F42000">
      <value key="F42000">anc000006070000</value>
    </entry>
    <entry key="F50000">
      <value key="F50000">anc000006080000</value>
    </entry>
    <entry key="F51000">
      <value  key="F51000">anc000006090000</value>
    </entry>
    <entry key="F60000">
      <value key="F60000">anc000006100000</value>
    </entry>
    <entry key="F70000">
      <value key="F70000">anc000006110000</value>
    </entry>
    <entry key="F71000">
      <value key="F71000">anc000006120000</value>
    </entry>
    <entry key="F72000">
      <value key="F72000">anc000006130000</value>
    </entry>
    <entry key="F80000">
      <value key="F80000">anc000006140000</value>
    </entry>
    <entry key="F90000">
      <value  key="F90000">anc000006150000</value>
    </entry>
    <entry key="G00100">
      <value key="G00100">anc000007000000 anc000007010000</value>
    </entry>
    <entry key="G00101">
      <value key="G00101">anc000007000000 anc000007010000 anc000007020000</value>
    </entry>
    <entry key="G00102">
      <value key="G00102">anc000007000000 anc000007020000</value>
    </entry>
    <entry key="G10100">
      <value key="G10100">anc000007010000 anc000007030000</value>
    </entry>
    <entry key="G10101">
      <value key="G10101">anc000007010000 anc000007020000 anc000007030000</value>
    </entry>
    <entry key="G10102">
      <value key="G10102">anc000007020000 anc000007030000</value>
    </entry>
    <entry key="G10200">
      <value key="G10200">anc000007010000 anc000007040000</value>
    </entry>
    <entry key="G10201">
      <value key="G10201">anc000007010000 anc000007020000 anc000007040000</value>
    </entry>
    <entry key="G10202">
      <value key="G10202">anc000007020000 anc000007040000</value>
    </entry>
    <entry key="G10300">
      <value key="G10300">anc000007010000 anc000007050000</value>
    </entry>
    <entry key="G10301">
      <value key="G10301">anc000007010000 anc000007020000 anc000007050000</value>
    </entry>
    <entry key="G10302">
      <value key="G10302">anc000007020000 anc000007050000</value>
    </entry>
    <entry key="G10400">
      <value  key="G10400">anc000007010000 anc000007060000</value>
    </entry>
    <entry key="G10401">
      <value key="G10401">anc000007010000 anc000007020000 anc000007060000</value>
    </entry>
    <entry key="G10402">
      <value key="G10402">anc000007020000 anc000007060000</value>
    </entry>
    <entry key="G10500">
      <value key="G10500">anc000007010000 anc000007070000</value>
    </entry>
    <entry key="G10501">
      <value  key="G10501">anc000007010000 anc000007020000 anc000007070000</value>
    </entry>
    <entry key="G10502">
      <value  key="G10502">anc000007020000 anc000007070000</value>
    </entry>
    <entry key="G10600">
      <value  key="G10600">anc000007010000 anc000007080000</value>
    </entry>
    <entry key="G10601">
      <value key="G10601">anc000007010000 anc000007020000 anc000007080000</value>
    </entry>
    <entry key="G10602">
      <value key="G10602">anc000007020000 anc000007080000</value>
    </entry>
    <entry key="G10700">
      <value key="G10700">anc000007010000 anc000007090000</value>
    </entry>
    <entry key="G10701">
      <value key="G10701">anc000007010000 anc000007020000 anc000007090000</value>
    </entry>
    <entry key="G10702">
      <value  key="G10702">anc000007020000 anc000007090000</value>
    </entry>
    <entry key="G10800">
      <value key="G10800">anc000007010000 anc000007100000</value>
    </entry>
    <entry key="G10801">
      <value key="G10801">anc000007010000 anc000007020000 anc000007100000</value>
    </entry>
    <entry key="G10802">
      <value key="G10802">anc000007020000 anc000007100000</value>
    </entry>
    <entry key="G10900">
      <value key="G10900">anc000007010000 anc000007110000</value>
    </entry>
    <entry key="G10901">
      <value key="G10901">anc000007010000 anc000007020000 anc000007110000</value>
    </entry>
    <entry key="G10902">
      <value key="G10902">anc000007020000 anc000007110000</value>
    </entry>
    <entry key="G11000">
      <value key="G11000">anc000007010000 anc000007120000</value>
    </entry>
    <entry key="G11001">
      <value key="G11001">anc000007010000 anc000007020000 anc000007120000</value>
    </entry>
    <entry key="G11002">
      <value key="G11002">anc000007020000 anc000007120000</value>
    </entry>
    <entry key="G11100">
      <value key="G11100">anc000007010000 anc000007130000</value>
    </entry>
    <entry key="G11101">
      <value key="G11101">anc000007010000 anc000007020000 anc000007130000</value>
    </entry>
    <entry key="G11102">
      <value key="G11102">anc000007020000 anc000007130000</value>
    </entry>
    <entry key="G11200">
      <value key="G11200">anc000007010000 anc000007140000</value>
    </entry>
    <entry key="G11201">
      <value  key="G11201">anc000007010000 anc000007020000 anc000007140000</value>
    </entry>
    <entry key="G11202">
      <value key="G11202">anc000007020000 anc000007140000</value>
    </entry>
    <entry key="G11300">
      <value  key="G11300">anc000007010000 anc000007150000</value>
    </entry>
    <entry key="G11301">
      <value key="G11301">anc000007010000 anc000007020000 anc000007150000</value>
    </entry>
    <entry key="G11302">
      <value key="G11302">anc000007020000 anc000007150000</value>
    </entry>
    <entry key="G11400">
      <value  key="G11400">anc000007010000 anc000007160000</value>
    </entry>
    <entry key="G11401">
      <value key="G11401">anc000007010000 anc000007020000 anc000007160000</value>
    </entry>
    <entry key="G11402">
      <value key="G11402">anc000007020000 anc000007160000</value>
    </entry>
    <entry key="G11500">
      <value key="G11500">anc000007010000 anc000007170000</value>
    </entry>
    <entry key="G11501">
      <value  key="G11501">anc000007010000 anc000007020000 anc000007170000</value>
    </entry>
    <entry key="G11502">
      <value key="G11502">anc000007020000 anc000007170000</value>
    </entry>
    <entry key="G11600">
      <value key="G11600">anc000007010000 anc000007180000</value>
    </entry>
    <entry key="G11601">
      <value key="G11601">anc000007010000 anc000007020000 anc000007180000</value>
    </entry>
    <entry key="G11602">
      <value key="G11602">anc000007020000 anc000007180000</value>
    </entry>
    <entry key="G11800">
      <value  key="G11800">anc000007010000 anc000007190000</value>
    </entry>
    <entry key="G11801">
      <value key="G11801">anc000007010000 anc000007020000 anc000007190000</value>
    </entry>
    <entry key="G11802">
      <value key="G11802">anc000007020000 anc000007190000</value>
    </entry>
    <entry key="G11900">
      <value key="G11900">anc000007010000 anc000007200000</value>
    </entry>
    <entry key="G11901">
      <value key="G11901">anc000007010000 anc000007020000 anc000007200000</value>
    </entry>
    <entry key="G11902">
      <value  key="G11902">anc000007020000 anc000007200000</value>
    </entry>
    <entry key="G12000">
      <value  key="G12000">anc000007010000 anc000007210000</value>
    </entry>
    <entry key="G12001">
      <value key="G12001">anc000007010000 anc000007020000 anc000007210000</value>
    </entry>
    <entry key="G12002">
      <value key="G12002">anc000007020000 anc000007210000</value>
    </entry>
    <entry key="G12200">
      <value key="G12200">anc000007010000 anc000007220000</value>
    </entry>
    <entry key="G12201">
      <value key="G12201">anc000007010000 anc000007020000 anc000007220000</value>
    </entry>
    <entry key="G12202">
      <value key="G12202">anc000007020000 anc000007220000</value>
    </entry>
    <entry key="G12300">
      <value key="G12300">anc000007010000 anc000007230000</value>
    </entry>
    <entry key="G12301">
      <value  key="G12301">anc000007010000 anc000007020000 anc000007230000</value>
    </entry>
    <entry key="G12302">
      <value key="G12302">anc000007020000 anc000007230000</value>
    </entry>
    <entry key="G12400">
      <value key="G12400">anc000007010000 anc000007240000</value>
    </entry>
    <entry key="G12401">
      <value key="G12401">anc000007010000 anc000007020000 anc000007240000</value>
    </entry>
    <entry key="G12402">
      <value  key="G12402">anc000007020000 anc000007240000</value>
    </entry>
    <entry key="G12600">
      <value key="G12600">anc000007010000 anc000007250000</value>
    </entry>
    <entry key="G12601">
      <value key="G12601">anc000007010000 anc000007020000 anc000007250000</value>
    </entry>
    <entry key="G12602">
      <value key="G12602">anc000007020000 anc000007250000</value>
    </entry>
    <entry key="G20000">
      <value key="G20000">anc000007010000 anc000007260000</value>
    </entry>
    <entry key="G20001">
      <value  key="G20001">anc000007010000 anc000007020000 anc000007260000</value>
    </entry>
    <entry key="G20002">
      <value key="G20002">anc000007020000 anc000007260000</value>
    </entry>
    <entry key="H00000">
      <value key="H00000">anc000008000000</value>
    </entry>
    <entry key="H10000">
      <value key="H10000">anc000008010000</value>
    </entry>
    <entry key="H20000">
      <value key="H20000">anc000008020000</value>
    </entry>
    <entry key="H30000">
      <value key="H30000">anc000008030000</value>
    </entry>
    <entry key="H40000">
      <value key="H40000">anc000008040000</value>
    </entry>
    <entry key="H50000">
      <value key="H50000">anc000008050000</value>
    </entry>
    <entry key="H60000">
      <value key="H60000">anc000008060000</value>
    </entry>
    <entry key="H70000">
      <value key="H70000">anc000008070000</value>
    </entry>
    <entry key="H80000">
      <value key="H80000" hassubchild="true">anc000008080000</value>
    </entry>
    <entry key="H81000">
      <value key="H81000">anc000008080100</value>
    </entry>
    <entry key="H82000">
      <value key="H82000">anc000008080200</value>
    </entry>
    <entry key="H83000">
      <value key="H83000">anc000008080300</value>
    </entry>
    <entry key="H84000">
      <value  key="H84000">anc000008080400</value>
    </entry>
    <entry key="H85000">
      <value key="H85000">anc000008080500</value>
    </entry>
    <entry key="H86000">
      <value key="H86000">anc000008080600</value>
    </entry>
    <entry key="H87000">
      <value key="H87000">anc000008080700</value>
    </entry>
    <entry key="H88000">
      <value key="H88000">anc000008080800</value>
    </entry>
    <entry key="H89000">
      <value key="H89000">anc000008080900</value>
    </entry>
  </xsl:variable>
  <xsl:template match="monographs">
       <xsl:for-each select="mg:monograph">
        <RECORD>
          <xsl:text>&#10;</xsl:text>
          <REF>
            <xsl:value-of select="mg:sysid" />
          </REF>
          <xsl:text>&#10;</xsl:text>
          <xsl:variable name="ancatmap" select="msxsl:node-set($categories)/entry"/>
          <xsl:variable name="ancatCatSec" select="(cr:content-flags/.//cr:anc-sysid)[position() >1]/text()" />
          <xsl:variable name="joinedAncatCatSec">
            <xsl:call-template name="join">
              <xsl:with-param name="valueList" select="$ancatCatSec"/>
              <xsl:with-param name="separator" select="' '"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="joinedAncatCatSecValuesPlus">
            <xsl:copy-of select="cs:GetPermutationsForAncat($joinedAncatCatSec,' ')"/>
          </xsl:variable>
          <xsl:variable name="joinedAncatCatSecValuesPlusTopMatch">
            <xsl:for-each select="$ancatmap/value">
              <xsl:variable name="selectedValue" select="." />
              <xsl:for-each select="msxsl:node-set($joinedAncatCatSecValuesPlus)/item">
                <xsl:variable name="selectedValueComb" select="." />
                <xsl:choose>
                  <xsl:when test="(string-length($selectedValue/node()))-(string-length(translate($selectedValue/node(),' ','')))=2 and $selectedValueComb/node()=$selectedValue/node()">
                    <PerfectMatch>
                      <xsl:value-of select="."></xsl:value-of>
                    </PerfectMatch>
                  </xsl:when>
                  <xsl:when test="(string-length($selectedValue/node()))-(string-length(translate($selectedValue/node(),' ','')))=1 and $selectedValueComb/node()=$selectedValue/node()">
                    <PerfectMatch>
                      <xsl:value-of select="."></xsl:value-of>
                    </PerfectMatch>
                  </xsl:when>
                  <xsl:when test="(string-length($selectedValue/node()))-(string-length(translate($selectedValue/node(),' ','')))=0 and $selectedValueComb/node()=$selectedValue/node()">
                    <PerfectMatch>
                      <xsl:value-of select="."></xsl:value-of>
                    </PerfectMatch>
                  </xsl:when>
                  <xsl:otherwise>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="joinedAncatCatSecValues">
            <xsl:for-each select="msxsl:node-set($joinedAncatCatSecValuesPlusTopMatch)/PerfectMatch">
              <xsl:variable name="ancatSecCurrentValue" select="." />
              <xsl:for-each select="msxsl:node-set($joinedAncatCatSecValuesPlus)/item">
                <xsl:variable name="joinedAncatCatSecValuesPlusValue" select="." />
                <xsl:choose>
                  <xsl:when test="msxsl:node-set($joinedAncatCatSecValuesPlusTopMatch)/node() !=''">
                    <xsl:if test="$ancatSecCurrentValue/node()=msxsl:node-set($joinedAncatCatSecValuesPlusValue)/text() and $ancatSecCurrentValue/node()=msxsl:node-set($joinedAncatCatSecValuesPlusValue)/text()">
                      <Bestmatch>
                        <xsl:attribute name="count">
                          <xsl:value-of select="$ancatSecCurrentValue/text()"/>
                        </xsl:attribute>
                        <xsl:value-of select="msxsl:node-set($categories)/node()/value[text()=$ancatSecCurrentValue/node()]/@key"/>
                      </Bestmatch>
                    </xsl:if>
                  </xsl:when>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="XRFValues" >
            <xsl:for-each select="msxsl:node-set($joinedAncatCatSecValuesPlusTopMatch)/PerfectMatch">
              <xsl:variable name="ancatXRFCurrentValue" select="." />
              <xsl:for-each select="msxsl:node-set($joinedAncatCatSecValuesPlus)/item">
                <xsl:variable name="joinedAncatCatXRFValuesPlusValue" select="." />
                <xsl:choose>
                  <xsl:when test="msxsl:node-set($joinedAncatCatSecValuesPlusTopMatch)/node() !=''">
                    <xsl:if test="$ancatXRFCurrentValue/node()=$joinedAncatCatXRFValuesPlusValue/text() and $ancatXRFCurrentValue/node()=$joinedAncatCatXRFValuesPlusValue/text()">
                      <XRF>
                        <xsl:attribute name="count">
                          <xsl:value-of select="$joinedAncatCatXRFValuesPlusValue/text()"/>
                        </xsl:attribute>
                        <xsl:value-of select="msxsl:node-set($categories)/node()/value[text()=msxsl:node-set($ancatXRFCurrentValue)/node()]/@key" />
                      </XRF>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="$ancatXRFCurrentValue/value/text()=$joinedAncatCatXRFValuesPlusValue/text()">
                      <XRF>
                        <xsl:value-of select="msxsl:node-set($categories)/node()/value[text()=msxsl:node-set($ancatXRFCurrentValue)/node()]/@key" />
                      </XRF>
                    </xsl:if>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="XRF3Values" >
            <xsl:if test="$XRFValues !=''">
              <xsl:for-each select="msxsl:node-set($XRFValues)/node()">
                <xsl:variable name="Xrf3CurrentnodeValue" select="current()" />
                <xsl:variable name="XrfCurrentValue" select="(string-length($Xrf3CurrentnodeValue/@count))-(string-length(translate($Xrf3CurrentnodeValue/@count,' ','')))" />
                <xsl:if test="$XrfCurrentValue=2">
                  <FinalMatch>
                    <xsl:value-of select="./node()"/>
                  </FinalMatch>
                </xsl:if>
              </xsl:for-each>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="XRF2Values" >
            <xsl:if test ="msxsl:node-set($XRF3Values) =''">
              <xsl:for-each select="msxsl:node-set($XRFValues)/node()">
                <xsl:variable name="Xrf2CurrentnodeValue" select="current()" />
                <xsl:variable name="XrfCurrentValue" select="(string-length($Xrf2CurrentnodeValue/@count))-(string-length(translate($Xrf2CurrentnodeValue/@count,' ','')))" />
                <xsl:if test="$XrfCurrentValue=1">
                  <FinalMatch>
                    <xsl:value-of select="./node()"/>
                  </FinalMatch>
                </xsl:if>
              </xsl:for-each>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="XRF1Values" >
            <xsl:if test ="msxsl:node-set($XRF2Values) =''">
              <xsl:for-each select="msxsl:node-set($XRFValues)/node()">
                <xsl:variable name="Xrf1CurrentnodeValue" select="current()" />
                <xsl:variable name="XrfCurrentValue" select="(string-length($Xrf1CurrentnodeValue/@count))-(string-length(translate($Xrf1CurrentnodeValue/@count,' ','')))" />
                <xsl:if test="$XrfCurrentValue=0">
                  <FinalMatch>
                    <xsl:value-of select="./node()"/>
                  </FinalMatch>
                </xsl:if>
              </xsl:for-each>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="XRF0Values" >
            <xsl:if test ="msxsl:node-set($XRF1Values)/node()">
              <xsl:for-each select="msxsl:node-set($XRF1Values)/node()">
                <SinglIitems>
                  <xsl:value-of select="substring(./node(),1,1)"/>
                </SinglIitems>
              </xsl:for-each>
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="JoinedSingleitems">
            <xsl:call-template name="join">
              <xsl:with-param name="valueList" select="msxsl:node-set($XRF0Values)/SinglIitems/text()"/>
              <xsl:with-param name="separator" select="' '"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="AllSingleitemsValue">
            <xsl:call-template name="join">
              <xsl:with-param name="valueList" select="msxsl:node-set($XRF1Values)/FinalMatch/text()"/>
              <xsl:with-param name="separator" select="' '"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="ParentRemoveditems">
            <xsl:copy-of select="cs:RemoveParentItems($JoinedSingleitems,' ',$AllSingleitemsValue)"/>
          </xsl:variable>
          <xsl:variable name="FinalBestmatch" >
            <xsl:if test="$ParentRemoveditems !=''">
              <xsl:copy-of select="msxsl:node-set($ParentRemoveditems)/node()" />
            </xsl:if>
            <xsl:if test="$XRF3Values !=''">
              <xsl:copy-of select="msxsl:node-set($XRF3Values)/node()" />
            </xsl:if>
            <xsl:if test="$XRF2Values !=''">
              <xsl:copy-of select="msxsl:node-set($XRF2Values)/node()" />
            </xsl:if>
            <xsl:if test="$XRF1Values !='' and $ParentRemoveditems =''">
              <xsl:copy-of select="msxsl:node-set($XRF1Values)/node()" />
            </xsl:if>
          </xsl:variable>
          <xsl:variable name="FinalBestmatchJoinedValues" >
            <xsl:call-template name="join">
              <xsl:with-param name="valueList" select="msxsl:node-set($FinalBestmatch)/FinalMatch/text()"/>
              <xsl:with-param name="separator" select="' '"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="FinalBestmatchValuesSortedByAsc">
            <xsl:copy-of select="cs:SortByAscValues($FinalBestmatchJoinedValues,' ')"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when  test ="msxsl:node-set($FinalBestmatchValuesSortedByAsc)/FinalMatch !=''">
              <xsl:for-each select="msxsl:node-set($FinalBestmatchValuesSortedByAsc)/node()">
                <xsl:if test="position() =1">
                  <SEC>
                    <xsl:value-of select="substring(current(),1,1)" />
                  </SEC>
                  <xsl:text>&#10;</xsl:text>
                  <SSC>
                    <xsl:value-of select="substring(current(),2,5)"/>
                  </SSC>
                  <xsl:text>&#10;</xsl:text>
                </xsl:if>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <SEC />
              <xsl:text>&#10;</xsl:text>
              <SSC />
              <xsl:text>&#10;</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
          <IDC>J</IDC>
          <xsl:text>&#10;</xsl:text>
          <DOC>Journal</DOC>
          <xsl:text>&#10;</xsl:text>
          <FJL>
            <xsl:apply-templates select="mg:literature-references/mg:cit/mg:cit-title"/>
          </FJL>
          <xsl:text>&#10;</xsl:text>
          <JNL>
            <xsl:apply-templates select="mg:literature-references/mg:cit/mg:cit-title" />
          </JNL>
          <xsl:text>&#10;</xsl:text>
          <CDN></CDN>
          <xsl:text>&#10;</xsl:text>
          <ISN></ISN>
          <xsl:text>&#10;</xsl:text>
          <LCD>
            <xsl:value-of select="mg:literature-references/mg:cit/@lang" />
          </LCD>
          <xsl:text>&#10;</xsl:text>
          <LAN>
            <xsl:variable name="lcd" select="mg:literature-references/mg:cit/@lang"></xsl:variable>
            <xsl:value-of select="msxsl:node-set($languages)/entry[@key=$lcd]"/>
          </LAN>
          <xsl:text>&#10;</xsl:text>
          <VOL>
            <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-vol" />
          </VOL>
          <xsl:text>&#10;</xsl:text>
          <ISS>
            <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-iss" />
          </ISS>
          <xsl:text>&#10;</xsl:text>
          <ATL>
            <xsl:apply-templates select="mg:title" />
          </ATL>
          <xsl:text>&#10;</xsl:text>
          <xsl:if test="mg:literature-references">
            <xsl:for-each select="mg:literature-references/mg:cit/mg:cit-auth">
              <xsl:if test="mg:fname !='' or mg:surname !=''">
                <AUT>
                  <xsl:value-of select="mg:surname" />
                  <xsl:if test="mg:surname !=''">
                    <xsl:text>, </xsl:text><xsl:value-of select="mg:fname" />
                  </xsl:if>
                </AUT>
                <xsl:text>&#10;</xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="mg:literature-references/mg:cit/mg:cit-fpage !='' or  mg:literature-references/mg:cit/mg:cit-lpage !=''">
            <PAG>
              <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-fpage" />
              <xsl:if test="mg:literature-references/mg:cit/mg:cit-lpage !=''">
                <xsl:text>–</xsl:text>
                <xsl:value-of select="mg:literature-references/mg:cit/mg:cit-lpage" />
              </xsl:if>
            </PAG>
            <xsl:text>&#10;</xsl:text>
          </xsl:if>
          <DIV>
            <xsl:variable name="orgname" select="mg:info/mg:location/mg:orgname"/>
            <xsl:choose>
              <xsl:when test="not(contains($orgname,','))">
                <xsl:value-of select="$orgname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="substring-before-last">
                  <xsl:with-param name="list">
                    <xsl:value-of select="$orgname" />
                  </xsl:with-param>
                  <xsl:with-param name="delimiter" select="','"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </DIV>
          <xsl:text>&#10;</xsl:text>
          <ORG>
            <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="mg:info/mg:location/mg:orgname" />
              </xsl:with-param>
              <xsl:with-param name="delimiter">,</xsl:with-param>
            </xsl:call-template>
          </ORG>
          <xsl:text>&#10;</xsl:text>
          <CTR>
            <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="mg:info/mg:location/mg:address" />
              </xsl:with-param>
              <xsl:with-param name="delimiter">,</xsl:with-param>
            </xsl:call-template>
          </CTR>
          <xsl:text>&#10;</xsl:text>
          <PBD>
            <xsl:value-of select="cr:first-published/@date"/>
          </PBD>
          <xsl:text>&#10;</xsl:text>
          <ANALYTE>
            <xsl:variable name="analyteCount" select="mg:data/mg:analytes/mg:analyte"/>
            <xsl:if test="count($analyteCount) >= 1">
                 <xsl:text>&#10;</xsl:text>
            </xsl:if>
            <xsl:for-each select="$analyteCount">
              <AYT>
                <xsl:value-of select="." />
              </AYT>
                <xsl:text>&#10;</xsl:text>
            </xsl:for-each>
          </ANALYTE>
          <xsl:text>&#10;</xsl:text>
          <MATRIX>
            <xsl:variable name="matCount" select="mg:data/mg:matrices/mg:matrix"/>
            <xsl:if test="count($matCount) >= 1">
                 <xsl:text>&#10;</xsl:text>
            </xsl:if>
            <xsl:for-each select="$matCount">
              <MTX>
                <xsl:value-of select="." />
              </MTX>
                <xsl:text>&#10;</xsl:text>
            </xsl:for-each>
          </MATRIX>
          <xsl:text>&#10;</xsl:text>
          <CONCEPT>
            <xsl:variable name="conceptcount" select="mg:data/mg:concepts/mg:concept"/>
             <xsl:if test="count($conceptcount) >= 1">
             <xsl:text>&#10;</xsl:text>
            </xsl:if>
            <xsl:for-each select="$conceptcount">
              <CPT>
                <xsl:value-of select="." />
              </CPT>
              <xsl:text>&#10;</xsl:text>
            </xsl:for-each>
          </CONCEPT>
          <xsl:text>&#10;</xsl:text>
          <ABS>
            <xsl:apply-templates select="mg:description" />
          </ABS>
          <xsl:text>&#10;</xsl:text>
          <xsl:if test ="msxsl:node-set($FinalBestmatchValuesSortedByAsc)/FinalMatch !=''">
            <xsl:for-each select="msxsl:node-set($FinalBestmatchValuesSortedByAsc)/node()">
              <xsl:if test="position() !=1">
                <XRF>
                  <xsl:value-of select="current()" />
                </XRF>
               <xsl:text>&#10;</xsl:text>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </RECORD>
      </xsl:for-each>
  </xsl:template>
  <xsl:template match="mg:it">
    <it>
      <xsl:apply-templates select="@*|node()" />
    </it>
  </xsl:template>
  <xsl:template match="mg:bo">
    <bo>
      <xsl:apply-templates select="@*|node()" />
    </bo>
  </xsl:template>
  <xsl:template match="mg:inf">
    <inf>
      <xsl:apply-templates select="@*|node()"  />
    </inf>
  </xsl:template>
  <xsl:template match="mg:em|mg:i">
    <it>
      <xsl:apply-templates select="@*|node()" />
    </it>
  </xsl:template>
  <xsl:template match="mg:strong|mg:b">
    <bo>
      <xsl:apply-templates select="@*|node()" />
    </bo>
  </xsl:template>
  <xsl:template match="mg:sub">
    <inf>
      <xsl:apply-templates select="@*|node()"  />
    </inf>
  </xsl:template>
  <xsl:template match="mg:sup">
    <sup>
      <xsl:apply-templates select="@*|node()"  />
    </sup>
  </xsl:template>
  <!--<xsl:template match="text()">
    <xsl:value-of disable-output-escaping="yes" select="."/>
  </xsl:template>-->
  <xsl:template match="text()">
    <xsl:variable name="selectedTextHasValue">
      <xsl:variable name="selectedText" select="." />
      <xsl:for-each select="msxsl:node-set($Unicodecharacters)/entry">
        <xsl:variable name="selectedValue" select="." />
        <xsl:variable name="selectedkey" select="@key" />
        <xsl:if test="contains($selectedText, @key)">
          <Value>
            <xsl:attribute name="key">
              <xsl:value-of select="@key"/>
            </xsl:attribute>
            <xsl:value-of select="$selectedValue"/>
          </Value>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="msxsl:node-set($selectedTextHasValue)/Value">
        <xsl:variable name="selectedText" select="." />
        <xsl:variable name="JoinedValue" >
          <xsl:call-template name="joinKeyValuePair">
            <xsl:with-param name="valueList" select="msxsl:node-set($selectedTextHasValue)/Value"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of disable-output-escaping="yes" select="cs:ReplaceMutipleStrings($selectedText,$JoinedValue)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of disable-output-escaping="yes" select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--Template to get last value on comma seperator field  -->
  <!-- of a specific delemiter-->
  <xsl:template name="output-tokens">
    <xsl:param name="list" />
    <xsl:param name="delimiter" />
    <xsl:variable name="newlist">
      <xsl:choose>
        <xsl:when test="contains($list, $delimiter)">
          <xsl:value-of select="normalize-space($list)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat(normalize-space($list), $delimiter)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
    <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
    <!--Check if it's last comma value-->
    <xsl:if test="string-length($list) 
          - string-length(translate($list, ',', '')) 
          + 1 * (string-length($list) != 0) =1" >
      <xsl:value-of select="$first" />
    </xsl:if>
    <xsl:if test="$remaining">
      <xsl:call-template name="output-tokens">
        <xsl:with-param name="list" select="$remaining" />
        <xsl:with-param name="delimiter">
          <xsl:value-of select="$delimiter"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <!-- Template to determine Substring before last occurence-->
  <!-- of a specific delemiter-->
  <xsl:template name="substring-before-last">
    <!--passed template parameter -->
    <xsl:param name="list"/>
    <xsl:param name="delimiter"/>
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)">
        <!-- get everything in front of the first delimiter -->
        <xsl:value-of select="substring-before($list,$delimiter)"/>
        <xsl:choose>
          <xsl:when test="contains(substring-after($list,$delimiter),$delimiter)">
            <xsl:value-of select="$delimiter"/>
          </xsl:when>
        </xsl:choose>
        <xsl:call-template name="substring-before-last">
          <!-- store anything left in another variable -->
          <xsl:with-param name="list" select="substring-after($list,$delimiter)"/>
          <xsl:with-param name="delimiter" select="$delimiter"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <!-- Template to Join List-->
  <xsl:template name="join" >
    <xsl:param name="valueList" select="''"/>
    <xsl:param name="separator" select="' '"/>
    <xsl:for-each select="$valueList">
      <xsl:choose>
        <xsl:when test="position() = 1">
          <xsl:value-of select="."/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($separator, .) "/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="joinKeyValuePair" >
    <xsl:param name="valueList" select="''"/>
    <xsl:for-each select="$valueList">
      <xsl:choose>
        <xsl:when test="position() = 1">
          <xsl:value-of select="concat(@key,'=',.)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat(@key,'=',.,'|')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="joinlist" >
    <xsl:param name="valueList" select="''"/>
    <xsl:param name="separator" select="' '"/>
    <xsl:for-each select="$valueList [7 >= position()]">
      <xsl:choose>
        <xsl:when test="position() = 1">
          <xsl:value-of select="."/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($separator, .) "/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="with"/>
    <xsl:choose>
      <xsl:when test="contains($text,$replace)">
        <xsl:value-of select="substring-before($text,$replace)"/>
        <xsl:copy-of select="msxsl:node-set($with)"/>
        <xsl:call-template name="replace-string">
          <xsl:with-param name="text"
select="substring-after($text,$replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="with" select="$with"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
